import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route } from 'react-router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import App from './app';
import Vanik from './vanik';
import Luc from './luc';
import Adrian from './adrian';
import Dylan from './dylan';

const pageRoutes = (
  <MuiThemeProvider muiTheme={getMuiTheme()}>
    <Router>
      <Route path="/" component={App} />
      <Route path="/luc" component={Luc} />
      <Route path="/vanik" component={Vanik} />
      <Route path="/adrian" component={Adrian} />
      <Route path="/dylan" component={Dylan} />
    </Router>
  </MuiThemeProvider>
);

ReactDOM.render(pageRoutes, document.querySelector('#app'));

