import React from 'react';
import { Link } from 'react-router';

import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
        <span>
        <Link style={{ margin: '10px' }} className="btn btn-primary btn-lg" to={'/vanik/'}>Vanik</Link>
        <Link style={{ margin: '10px' }} className="btn btn-primary btn-lg" to={'/luc/'}>Luc</Link>
        <Link style={{ margin: '10px' }} className="btn btn-primary btn-lg" to={'/adrian/'}>Adrian</Link>
        <Link style={{ margin: '10px' }} className="btn btn-primary btn-lg" to={'/dylan/'}>Dylan</Link>
        </span>
    );
  }
}


export default App;
