import React, { PropTypes } from 'react';

const Box = (props) => {
  return (
    <div style={{ backgroundColor: props.color, width: '100%', height: '100px' }}>
      {props.color}
    </div>
  );
};

Box.propTypes = {
  color: PropTypes.string,
};
export default Box;
