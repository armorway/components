import React from 'react';
import Immutable from 'immutable';
import * as config from './config';
import { utils } from 'src/utils';
import ArmorIncidentTable from 'src/ArmorIncidentTable';
import './styles';
import ArmorScrollToTop from 'src/ArmorScrollToTop';

utils.setLogoUrl('http://ci.sentinel.armorway.com:8081/images/armorway.png');

class LucIndex extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      printing: false,
    };

    this.incidents = Immutable.fromJS(config.incidents);
  }

  render() {
    return (
      <div>
        <button
          onClick={() => this.setState({ printing: !this.state.printing })}
        >
          print view
        </button>
        <ArmorIncidentTable incidents={this.incidents} printing={this.state.printing} />
      </div>
    );
  }
}


export default LucIndex;
