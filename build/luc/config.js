/* eslint-disable max-len */
const data1 = [{
  id: 1,
  blerg: 'Blerg1',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
  img: 'http://reactfordesigners.com/images/react-logo-dark.svg',
}, {
  id: 2,
  blerg: 'Blerg2',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
  img: 'http://reactfordesigners.com/images/react-logo-dark.svg',
}, {
  id: 3,
  blerg: 'Blerg3',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 4,
  blerg: 'Blerg4',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 5,
  blerg: 'Blerg5',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 6,
  blerg: 'Blerg7',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 7,
  blerg: 'Blerg7',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 8,
  blerg: 'Blerg8',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 9,
  blerg: 'Blerg9',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}];
const data2 = [{
  id: 10,
  blerg: 'Blerg10',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 11,
  blerg: 'Blerg11',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 12,
  blerg: 'Blerg12',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 13,
  blerg: 'Blerg13',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 14,
  blerg: 'Blerg14',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 15,
  blerg: 'Blerg15',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 16,
  blerg: 'Blerg16',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 17,
  blerg: 'Blerg17',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 18,
  blerg: 'Blerg18',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 19,
  blerg: 'Blerg19',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 20,
  blerg: 'Blerg20',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}];

const data3 = [{
  id: 21,
  blerg: 'Blerg21',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 22,
  blerg: 'Blerg22',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}, {
  id: 23,
  blerg: 'Blerg23',
  name: 'I am A Name',
  thing: 'blah',
  otherItem: 'bleh',
  type: 'crime',
}];

/* eslint-disable quote-props, quotes */
const incidents = [{
  "id": 69371,
  "name": "",
  "caseNumber": "0502318",
  "occurredFrom": "09/19/2005 3:30 PM",
  "occurredTo": "09/19/2005 5:00 PM",
  "reportedAt": "09/19/2005 5:01 PM",
  "building": "MARIANNE & J.DOUGLAS PARDEE TOWER",
  "address": "614  HELLMAN WY LOS ANGELES CA 90089 ",
  "campus": ["UPC: Beat A"],
  "crimeDescription": "Possession of a Controlled Substance",
  "personalLoss": "180.00",
  "propertyLoss": "",
  "propertyValue": "",
  "other": {},
  "policeInformation": 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
  "publicInformation": 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
  "narrative": 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
  "mapData": {
    "markerType": "other",
    "center": {
      "lat": 34.02,
      "lon": -118.283,
    },
    "hoverText": "NARCOTICS @ 614  HELLMAN WY LOS ANGELES CA 90089 ",
    "coordinates": null,
    "lineColor": null,
    "lineOpacity": null,
    "fillColor": null,
    "fillOpacity": null,
  },
}, {
  "id": 71742,
  "name": "",
  "caseNumber": "0502314",
  "occurredFrom": "08/31/2005 10:00 AM",
  "occurredTo": "08/31/2005 10:05 AM",
  "reportedAt": "09/19/2005 3:32 PM",
  "building": "MARIANNE & J.DOUGLAS PARDEE TOWER",
  "address": "614  HELLMAN WY LOS ANGELES CA 90089 ",
  "campus": ["UPC: Beat A"],
  "crimeDescription": "Lost/Missing Property",
  "personalLoss": "0.00",
  "propertyLoss": "",
  "propertyValue": "",
  "other": {},
  "policeInformation": 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
  "publicInformation": 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
  "narrative": 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
  "mapData": {
    "markerType": "other",
    "center": {
      "lat": 34.02,
      "lon": -118.283,
    },
    "hoverText": "PROPERTY @ 614  HELLMAN WY LOS ANGELES CA 90089 ",
    "coordinates": null,
    "lineColor": null,
    "lineOpacity": null,
    "fillColor": null,
    "fillOpacity": null,
  },
}, {
  "id": 72012,
  "name": "",
  "caseNumber": "0502313",
  "occurredFrom": "09/19/2005 3:28 PM",
  "occurredTo": "09/19/2005 3:28 PM",
  "reportedAt": "09/19/2005 3:51 PM",
  "building": "2670 MAGNOLIA AV",
  "address": "2670  MAGNOLIA AV LOS ANGELES CA 90007 ",
  "campus": ["UPC: Beat D"],
  "crimeDescription": "Lost/Missing Property",
  "personalLoss": "0.00",
  "propertyLoss": "",
  "propertyValue": "",
  "other": {},
  "policeInformation": 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
  "publicInformation": 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
  "narrative": 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
  "mapData": {
    "markerType": "other",
    "center": {
      "lat": 34.0311,
      "lon": -118.286,
    },
    "hoverText": "PROPERTY @ 2670  MAGNOLIA AV LOS ANGELES CA 90007 ",
    "coordinates": null,
    "lineColor": null,
    "lineOpacity": null,
    "fillColor": null,
    "fillOpacity": null,
  },
}, {
  "id": 72427,
  "name": "",
  "caseNumber": "0502315",
  "occurredFrom": "09/19/2005 3:24 PM",
  "occurredTo": "09/19/2005 3:24 PM",
  "reportedAt": "09/19/2005 3:24 PM",
  "building": "PARKSIDE APARTMENTS I",
  "address": "3730  MCCLINTOCK AV LOS ANGELES CA 90089 ",
  "campus": ["UPC: Beat A"],
  "crimeDescription": "Welfare Check",
  "personalLoss": "0.00",
  "propertyLoss": "",
  "propertyValue": "",
  "other": {},
  "policeInformation": 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
  "publicInformation": 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
  "narrative": 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
  "mapData": {
    "markerType": "other",
    "center": {
      "lat": 34.0193,
      "lon": -118.29,
    },
    "hoverText": "SERVICE @ 3730  MCCLINTOCK AV LOS ANGELES CA 90089 ",
    "coordinates": null,
    "lineColor": null,
    "lineOpacity": null,
    "fillColor": null,
    "fillOpacity": null,
  },
}, {
  "id": 72583,
  "name": "",
  "caseNumber": "0502316",
  "occurredFrom": "09/09/2005 12:00 AM",
  "occurredTo": "09/09/2005 12:00 AM",
  "reportedAt": "09/19/2005 3:42 PM",
  "building": "29TH ST & ELLENDALE PL",
  "address": "   LOS ANGELES CA 90007 ",
  "campus": ["UPC: Beat D"],
  "crimeDescription": "Lost/Missing Property",
  "personalLoss": "90.00",
  "propertyLoss": "",
  "propertyValue": "",
  "other": {},
  "policeInformation": 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
  "publicInformation": 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
  "narrative": 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
  "mapData": {
    "markerType": "other",
    "center": {
      "lat": 34.0284,
      "lon": -118.289,
    },
    "hoverText": "PROPERTY @    LOS ANGELES CA 90007 ",
    "coordinates": null,
    "lineColor": null,
    "lineOpacity": null,
    "fillColor": null,
    "fillOpacity": null,
  },
}, {
  "id": 72734,
  "name": "",
  "caseNumber": "0502319",
  "occurredFrom": "09/19/2005 5:00 PM",
  "occurredTo": "09/19/2005 7:57 PM",
  "reportedAt": "09/19/2005 8:00 PM",
  "building": "THOMAS & DOROTHY LEAVEY LIBRARY",
  "address": "650  35TH ST LOS ANGELES CA 90089 ",
  "campus": ["UPC: Beat A"],
  "crimeDescription": "Lost/Missing Property",
  "personalLoss": "110.00",
  "propertyLoss": "",
  "propertyValue": "",
  "other": {},
  "mapData": {
    "markerType": "other",
    "center": {
      "lat": 34.0218,
      "lon": -118.283,
    },
    "hoverText": "PROPERTY @ 650  35TH ST LOS ANGELES CA 90089 ",
    "coordinates": null,
    "lineColor": null,
    "lineOpacity": null,
    "fillColor": null,
    "fillOpacity": null,
  },
}, {
  "id": 75168,
  "name": "",
  "caseNumber": "0502310",
  "occurredFrom": "09/15/2005 7:00 AM",
  "occurredTo": "09/15/2005 7:45 PM",
  "reportedAt": "09/19/2005 10:46 AM",
  "building": "TUSCANY APARTMENTS",
  "address": "3760  FIGUEROA ST LOS ANGELES CA 90007 ",
  "campus": ["UPC: Beat B"],
  "crimeDescription": "Theft Petty-Plain",
  "personalLoss": "0.00",
  "propertyLoss": "",
  "propertyValue": "",
  "other": {},
  "policeInformation": 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
  "publicInformation": 'Lorem super ipsum',
  "narrative": 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
  "mapData": {
    "markerType": "theftPetty",
    "center": {
      "lat": 34.0902,
      "lon": -118.212,
    },
    "hoverText": "THEFT-PETTY @ 3760  FIGUEROA ST LOS ANGELES CA 90007 ",
    "coordinates": null,
    "lineColor": null,
    "lineOpacity": null,
    "fillColor": null,
    "fillOpacity": null,
  },
}, {
  "id": 75212,
  "name": "",
  "caseNumber": "0502312",
  "occurredFrom": "09/17/2005 4:30 PM",
  "occurredTo": "09/17/2005 11:30 PM",
  "reportedAt": "09/19/2005 11:51 AM",
  "building": "STARBUCKS",
  "address": "3303  HOOVER ST LOS ANGELES CA 90007 ",
  "campus": ["UPC: Beat D"],
  "crimeDescription": "Recovered Property Without a Crime",
  "personalLoss": "70.00",
  "propertyLoss": "",
  "propertyValue": "",
  "other": {},
  "mapData": {
    "markerType": "other",
    "center": {
      "lat": 34.0254,
      "lon": -118.284,
    },
    "hoverText": "PROPERTY @ 3303  HOOVER ST LOS ANGELES CA 90007 ",
    "coordinates": null,
    "lineColor": null,
    "lineOpacity": null,
    "fillColor": null,
    "fillOpacity": null,
  },
}, {
  "id": 75231,
  "name": "",
  "caseNumber": "0502311",
  "occurredFrom": "09/16/2005 5:00 PM",
  "occurredTo": "09/19/2005 9:00 AM",
  "reportedAt": "09/19/2005 11:24 AM",
  "building": "MARIANNE & J.DOUGLAS PARDEE TOWER",
  "address": "614  HELLMAN WY LOS ANGELES CA 90089 ",
  "campus": ["UPC: Beat A"],
  "crimeDescription": "Alarm Accidental",
  "personalLoss": "200.00",
  "propertyLoss": "",
  "propertyValue": "",
  "other": {},
  "mapData": {
    "markerType": "fire",
    "center": {
      "lat": 34.02,
      "lon": -118.283,
    },
    "hoverText": "FIRE @ 614  HELLMAN WY LOS ANGELES CA 90089 ",
    "coordinates": null,
    "lineColor": null,
    "lineOpacity": null,
    "fillColor": null,
    "fillOpacity": null,
  },
}, {
  "id": 75849,
  "name": "",
  "caseNumber": "0502317",
  "occurredFrom": "09/19/2005 2:00 PM",
  "occurredTo": "09/19/2005 2:30 PM",
  "reportedAt": "09/19/2005 3:44 PM",
  "building": "ANNENBERG HOUSE",
  "address": "711  27TH ST LOS ANGELES CA 90007 ",
  "campus": ["UPC: Beat C"],
  "crimeDescription": "Lost/Missing Property",
  "personalLoss": "300.00",
  "propertyLoss": "",
  "propertyValue": "",
  "other": {},
  "mapData": {
    "markerType": "other",
    "center": {
      "lat": 34.029,
      "lon": -118.279,
    },
    "hoverText": "PROPERTY @ 711  27TH ST LOS ANGELES CA 90007 ",
    "coordinates": null,
    "lineColor": null,
    "lineOpacity": null,
    "fillColor": null,
    "fillOpacity": null,
  },
}];

export {
  data1,
  data2,
  data3,
  incidents,
};
