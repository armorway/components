import React from 'react';
import moment from 'moment';
import ArmorViewFilter from 'src/ArmorViewFilter';
import { Row, Col } from 'react-bootstrap';
import FontAwesome from 'src/FontAwesome';
import ArmorLoader from 'src/ArmorLoader';
import ArmorPrintMap from 'src/ArmorPrintMap';
import ArmorVFDatespan from 'src/ArmorVFDatespan';

class VanikIndex extends React.Component {
  constructor(props) {
    super(props);
    this.markers = _.map(new Array(5000), (itm, idx) => {
      return {
        id: idx,
        lat: 23.0 + _.random(-0.9, 0.9),
        lng: 24.0 + _.random(-0.9, 0.9),
        intensity: 60,
        text: `Marker #${idx}`,
      };
    });
    this.state = {
      lat: 23,
      lng: 24,
      zoom: 9,
      circleRad: 500,
      markers: this.markers,
    };
  }

  render() {
    const overlays = [
      {
        name: 'Views',
        type: 'checkbox',
        options: [
          {
            id: 'all1',
            name: 'All',
            active: false,
          },
          {
            id: 'hotspots',
            name: 'Hotspots',
            active: true,
          },
          {
            id: 'heatMap',
            name: 'Heat Map',
            active: false,
          },
          {
            id: 'markers',
            name: 'Markers',
            active: false,
          },
        ],
      },
    ];

    const datespanInfo = {
      name: 'Date picker yo',
      type: 'datespan',
      value: {
        startDate: moment(),
        endDate: moment().subtract(1, 'weeks'),
      },
    };

    const filters = [
      {
        name: 'NewCrimes',
        type: 'crimepicker',
        selected: 0,
        groups: [
          {
            primaryText: 'SOME GROUP 1',
            value: 0,
            metadata: ['gtp'],
          },
          {
            primaryText: 'SOME GROUP 2',
            value: 1,
            metadata: ['gtp', 'gta'],
          },
          {
            primaryText: 'Other',
            value: 2,
            metadata: 'other',
          },
        ],
        options: {
          cat1: [
            {
              id: 'gtp',
              name: 'Grand Theft Person',
              icon: <img src="http://avatarbox.net/avatars/img39/small_fly_unfair_life_avatar_picture_93766.gif" />,
              active: true,
            },
            {
              id: 'gta',
              name: 'Grand Assault Person',
              active: false,
            },
          ],
          cat3: [
            {
              id: 'gtp',
              name: 'Grand Theft Padsferson',
              active: true,
            },
            {
              id: 'gta',
              name: 'Grand Assault Person',
              active: false,
            },
          ],
        },
      },
      {
        name: 'Beats',
        type: 'checkbox',
        onHelpClick: (name) => alert(name),
        options: [
          {
            id: 'all2',
            name: 'All',
            active: false,
          },
          {
            id: 'Beat A',
            name: 'Beat A',
            active: false,
          },
          {
            id: 'Beat B',
            name: 'Beat B',
            active: false,
          },
          {
            id: 'c',
            children: ['a', 'pm'],
            name: 'C',
            active: false,
          },
        ],
      },
      {
        name: 'Beats222',
        type: 'checkbox',
        onHelpClick: (name) => alert(name),
        options: [
          {
            id: 'all2',
            name: 'All',
            active: false,
          },
        ],
      },
      {
        name: 'Dates',
        type: 'datepicker',
        value: moment().toDate(),
      },
      {
        name: 'Time',
        type: 'timepicker',
        defaultTime: moment().toDate(),
      },
      {
        name: 'Campus',
        type: 'dropdown',
        value: 5,
        options: [
          {
            primaryText: 'USC',
            value: 5,
          },
          {
            primaryText: 'UCLA',
            value: 6,
          },
        ],
      },
      {
        name: 'Patrol Length',
        type: 'input',
        placeholder: 'Enter Length in Meters',
        value: '',
      },
      {
        name: 'Watch',
        type: 'checkbox',
        onHelpClick: (name) => alert(name),
        options: [
          {
            id: 'all3',
            name: 'All',
            active: false,
          },
          {
            id: 'am',
            name: 'AM',
            children: ['b', 'c'],
            active: false,
          },
          {
            id: 'pm',
            name: 'PM',
            active: false,
          },
        ],
      },
      {
        name: 'Keywords',
        type: 'input',
        placeholder: 'Enter Keywords',
        value: '',
      },
      {
        name: 'Crimes',
        type: 'checkbox',
        options: [
          {
            id: 'all5',
            name: 'All',
            active: false,
          },
          {
            id: 'cm',
            name: 'Murder',
            active: false,
          },
          {
            id: 'bm',
            name: 'Burglery',
            active: false,
          },
          {
            id: 'as',
            name: 'Assault',
            active: false,
          },
          {
            id: 'em',
            name: 'Emergency',
            active: false,
          },
          {
            id: 'tp',
            name: 'Petty Theft',
            active: false,
          },
          {
            id: 'bc',
            name: 'Burglery Commercial',
            active: false,
          },
          {
            id: 'gtp',
            name: 'Grand Theft Person',
            active: false,
          },
        ],
      },
    ];
    const position = [this.state.lat, this.state.lng];
    const someMarkers = this.markers.slice().splice(23, 253);
    const mapboxTiles = 'https://api.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoidmFuaWswOCIsImEiOiJjaW5nbzJtc2YwYWpudWtseXBlYThwd3hpIn0.X1L9mMrLYfE0w3mbJVksRw';
    const osmTiles = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png';

    return (
      <div id="page">
        <div style={{ zIndex: '3', position: 'fixed', bottom: '35px', right: '35px' }}>
          <button onClick={() => this.setState({ markers: this.markers })}>All Markers</button>
          <button onClick={() => this.setState({ markers: someMarkers })}>Some Markers</button>
          <button onClick={() => this.setState({ zoom: this.state.zoom >= 1 ? this.state.zoom -= 1 : this.state.zoom })}>Zoom Out</button>
          <button onClick={() => this.setState({ zoom: this.state.zoom += 1 })}>Zoom In</button>
          <button onClick={() => this.setState({ circleRad: this.state.circleRad * 2 })}>Make Circle Bigger</button>
          <ArmorPrintMap mapId="leafletMap" options={['<h3>Sample</h3>', '<span>Filters</span>']} />
        </div>
        <Row>
          <Col className="filter" style={{ zIndex: '1', position: 'absolute', top: '6px', left: '0px' }} lg={4} md={4} sm={12} xs={12}>
            <ArmorViewFilter style={{ marginLeft: '15px', marginTop: '15px', width: '650px' }}
              filters={filters}
              onApply={(values) => console.log(values)}
              allKey="All"
              dynamicFilters={['Views', 'Crimes']}
              onChange={(name, filter) => console.log('changed!', name, filter)}
              overlays={overlays}
              timeoutInterval={2000000}
              staticOverlays={[overlays[0]]}
              title={<FontAwesome title="Maps App" icon="map" padIcon />}
              onIconMenuChange={(evt, val) => console.log('hello there vil', val)}
              overlayMessage="hey yall, a message here yall"
              iconMenu={[
                {
                  label: 'Bla!',
                  value: 23,
                },
                {
                  label: 'Joe!',
                  value: 25,
                },
              ]}
            >
                <ArmorVFDatespan
                  showLabel
                  name="Datespan!"
                  value={datespanInfo.value}
                  onChange={(evt, evt2) => console.log('hi!', evt, evt2)}
                />
            </ArmorViewFilter>
          </Col>
        </Row>
        <Row>
          <Col xs={9}>
            <ArmorLoader loaded={false} />
            <div>
            </div>
          </Col>
          <Col xs={3}>
            <ArmorLoader loaded={false} />
            <h3>Hello</h3>
          </Col>
        </Row>
      </div>
    );
  }
}


export default VanikIndex;
