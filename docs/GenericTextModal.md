`GenericTextModal.jsx` (component)
==================================

GenericModal is a simple modal designed to display text and a body.
it is designed to replace constantly making simple and cookie cutter modals.
Use it just like you would any modal in AppActions.toggleModal

Props
-----

### `body`

Content to be displayed within the modal body

type: `string`


### `title`

type: `string`

