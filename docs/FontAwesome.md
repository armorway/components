`FontAwesome.jsx` (component)
=============================



Props
-----

### `children`

type: `any`


### `className`

type: `string`


### `icon` (required)

type: `union(object|string)`


### `iconStyle`

type: `object`


### `padIcon`

type: `bool`


### `size`

type: `string`


### `style`

type: `object`


### `title`

type: `string`

