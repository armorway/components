`ArmorInput.jsx` (component)
============================

This component represents an inputBox with
an optional label and error configuration

Props
-----

### `bsStyle`

Style for the input box
Is only used for 'error', otherwise remains undefined

type: `string`


### `children`

inner children of the component

type: `object`


### `errorMsg`

Message to be displayed when error occurs

type: `string`


### `label`

Label for the input box
placed before the box on the left

type: `string`


### `noAsterisk`

If true, input box will not display an asterisk
which indicates a required label.

type: `bool`


### `onChange`

Function which takes in an event to be called
when a change occurs

type: `func`


### `valid`

Is true when input box is valid to update,
Defaults at true and will not update if false

type: `bool`

