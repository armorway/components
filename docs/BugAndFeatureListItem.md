`BugAndFeatureListItem.jsx` (component)
=======================================



Props
-----

### `data` (required)

type: `object`


### `isSelected` (required)

type: `bool`


### `onAgreeClick` (required)

type: `func`


### `onClick` (required)

type: `func`


### `onRespondClick`

type: `func`


### `onUpdateClick` (required)

type: `func`

