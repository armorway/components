`ArmorFilter.jsx` (component)
=============================



Props
-----

### `list` (required)

type: `array`


### `onSearch` (required)

type: `func`


### `sortButtons`

type: `element`

