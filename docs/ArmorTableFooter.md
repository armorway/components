`ArmorTableFooter.jsx` (component)
==================================



Props
-----

### `numPages` (required)

type: `number`


### `onChange` (required)

type: `func`


### `pageSize` (required)

type: `number`

