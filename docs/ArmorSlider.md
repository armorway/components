`ArmorSlider.jsx` (component)
=============================

Slider object with a value that can slide from
left to right and has an optional label.
Start and end values (min and max) can be customized
but default at 0 and 100

Props
-----

### `endValue`

Max value for slider
defaults at 100

type: `number`


### `label`

label for slider
displayed as label: value

type: `string`


### `name`

name of component. Pass in change/drag

type: `any`


### `onChange`

type: `func`


### `onDrag`

type: `func`


### `startValue`

Min value for slider
defaults at 0

type: `number`


### `value`

current value of slider

type: `number`

