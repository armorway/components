`ArmorFilterListItem.jsx` (component)
=====================================

ArmorFilterListItem is designed to be used as an internal component
to the ArmorFilterListItem component. The applied filters will be
displayed with a remove button.

Props
-----

### `bsStyle`

Bootstrap type to apply to filter labels. Default: primary

type: `string`


### `className`

class to be applied.

type: `string`


### `name` (required)

Name/text of the component. It is the displayed text.

type: `string`


### `onRemove` (required)

Function to excute when item is removed.
onRemove(type, name)

type: `func`


### `type` (required)

Type of the filter. Used to identify the filter.

type: `string`

