`ArmorKeyValueRow.jsx` (component)
==================================

component used to render one row of a key map

Props
-----

### `className`

custom className for ArmorKeyValueRow object

type: `string`


### `formatKeys`

If true, this will format the keyData with camelToReadable.
Defaults at true

type: `bool`
defaultValue: `true`


### `formatValues`

If true, this will format the value with camelToReadable
Defaults at true

type: `bool`
defaultValue: `true`


### `keyData` (required)

key-name string

type: `any`


### `smallKey`

If true, makes key col xs=4

type: `bool`


### `value` (required)

value string mapped to the key-name

type: `any`

