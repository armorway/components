`ArmorViewFilter.jsx` (component)
=================================



Props
-----

### `allKey`

String representing the key that will select/deselect all checkboxes

type: `string`
defaultValue: `'All'`


### `className`

Class name for the entire component.

type: `string`
defaultValue: `'armor-view-filter'`


### `dynamicFilters`

A list of filters which will automatically trigger an export when changes

type: `array`


### `filters` (required)

Array of multiple types of filters
```
[
  {
    name: 'Beats',
    type: 'checkbox',
    options: [
      {
        id: 'all',
        name: 'All',
        active: false,
      },
      {
        id: 'a',
        name: 'A',
        children: ['b', 'c'],
        active: false,
      },
      {
        id: 'b',
        name: 'B',
        active: false,
      },
    ],
  },
  {
    name: 'Patrol Length',
    type: 'input',
    placeholder: 'Enter Length in Meters',
    value: '',
  },
]
```

type: `array`


### `filtersButtonLabel`

Label for filters button, defaults to "Filters"

type: `string`
defaultValue: `'Filters'`


### `formatData`

Function called before apply is pressed.
Function will format the data before it has been passed to onApply()

type: `func`


### `iconMenu`

A menu which only appears on mobile view requiring a
```{label: 'String', value: 'string'}```

type: `array`


### `onApply` (required)

Function called when pressing Apply.
First parameter contains Object representing applied filters.

type: `func`


### `onIconMenuChange`

Function fired upon the selection from the IconMenu, signature: function(event, value);

type: `func`


### `onReset`

Function called when reset is hit, Function()

type: `func`


### `overlays` (required)

Array of overlay filters
```
[
 {
   name: 'Views',
   type: 'checkbox',
   options: [
     {
       id: 'all',
       name: 'All',
       active: false,
     },
     {
       id: 'hotspots',
       name: 'Hotspots',
       active: true,
     },
     {
       id: 'heatMap',
       name: 'Heat Map',
       active: false,
     },
   ],
 },
]
```

type: `array`


### `overlaysButtonLabel`

Label for overlays button, defaults to "Overlays"

type: `string`
defaultValue: `'Overlays'`


### `staticOverlays`

Array of staticOverlay filters displayed by default when Filters or Overlays are not open.
```
[
 {
   name: 'Views',
   type: 'checkbox',
   options: [
     {
       id: 'all',
       name: 'All',
       active: false,
     },
     {
       id: 'hotspots',
       name: 'Hotspots',
       active: true,
     },
     {
       id: 'heatMap',
       name: 'Heat Map',
       active: false,
     },
   ],
 },
]
```

type: `array`


### `timeoutInterval`

defaultValue: `Infinity`


### `title`

Object or string that will be the heading

type: `any`

