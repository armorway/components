`ArmorAgreeButton.jsx` (component)
==================================

ArmorAgreeButton is used to show 'agrees' to something on a page.
It is similar to a +1 or like button.

Props
-----

### `agreed` (required)

Used to show if the user has agreed already.

type: `bool`


### `count` (required)

The current number of agrees to display.

type: `number`


### `onClick` (required)

Called when agreeing and props.agreed is false.

type: `func`

