`ArmorFilterList.jsx` (component)
=================================

ArmorFilterList is used to display applied filters.

Props
-----

### `bsStyle`

Bootstrap type to apply to filter labels. Default: primary
Only applies to labels, not dropdown.

type: `string`


### `className`

class to be applied.

type: `string`


### `filters` (required)

Array of applied filters to build into ArmorFilterListItems.

type: `array`


### `onFilterRemove` (required)

Function to execute when a filter remove is clicked.

type: `func`


### `style`

Style to be applied.

type: `object`

