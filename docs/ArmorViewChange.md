`ArmorViewChange.jsx` (component)
=================================

ArmorViewChange creates a row of links for
the user to click on. This component's intended
usage is to change views within a page.

Props
-----

### `active` (required)

Value of the current active view

type: `string`


### `onSelect` (required)

Function that takes in a key event and
a string to be executed when a pill is selected.

type: `func`


### `title` (required)

Component title to be rendered as text to the left

type: `string`


### `views` (required)

List of Json objects that represent the views.
Structure of each object is { key: 'value',... }

type: `array`

