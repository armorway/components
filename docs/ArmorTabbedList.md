`ArmorTabbedList.jsx` (component)
=================================



Props
-----

### `children` (required)

Child ArmorList components, must have a value prop.

type: `array`


### `value`

Value that matches a child component prop to set as active.

type: `string`

