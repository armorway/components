`ArmorFileUpload.jsx` (component)
=================================



Props
-----

### `loaded`

type: `bool`
defaultValue: `true`


### `multiple`

type: `bool`
defaultValue: `true`


### `onFilesAttached`

type: `func`


### `onUpload`

type: `func`


### `showAttachedFiles`

type: `bool`
defaultValue: `true`


### `style`

type: `object`

