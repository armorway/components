`ArmorRealTimeSlider.jsx` (component)
=====================================



Props
-----

### `date` (required)

type: `string`


### `onChange` (required)

type: `func`


### `onDrag` (required)

type: `func`


### `startOfDay` (required)

type: `string`


### `value`

type: `number`

