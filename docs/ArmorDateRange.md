`ArmorDateRange.jsx` (component)
================================



Props
-----

### `className`

type: `string`


### `end`

type: `object`
defaultValue: `''`


### `onAccept` (required)

type: `func`


### `start`

type: `object`
defaultValue: `''`


### `style`

type: `object`

