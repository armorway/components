`ArmorGriddle.jsx` (component)
==============================



Props
-----

### `activePage`

type: `number`


### `className`

type: `string`


### `filterPlaceholderText`

defaultValue: `'Search'`


### `itemsPerPage`

type: `number`


### `onSave`

type: `func`


### `results`

type: `union(object|array)`


### `resultsPerPage`

type: `number`


### `settingsIconComponent`

defaultValue: `<FontAwesome icon="cog"/>`


### `showPager`

defaultValue: `false`


### `showPagination`

type: `bool`
defaultValue: `true`


### `showPrint`

type: `bool`


### `showSave`

type: `bool`


### `showSettings`

defaultValue: `false`


### `sortAscendingComponent`

defaultValue: `<FontAwesome icon="long-arrow-up"/>`


### `sortDefaultComponent`

defaultValue: `undefined`


### `sortDescendingComponent`

defaultValue: `<FontAwesome icon="long-arrow-down"/>`


### `tableClassName`

defaultValue: `'table table-hover'`


### `useGriddleStyles`

defaultValue: `false`

