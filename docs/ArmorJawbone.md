`ArmorJawbone.jsx` (component)
==============================



Props
-----

### `className`

type: `string`


### `date`

type: `string`


### `onAccept` (required)

type: `func`

