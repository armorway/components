`ArmorList.jsx` (component)
===========================

Creates a list of for displaying data. Also supports actions.

Props
-----

### `actions`

Array of two actions in an array to be displayed
at the bottom of the component.
```
{
 name: String
 action: Function
}
```

type: `array`


### `allActive`

Makes row expanded and permanenmt active.

type: `bool`


### `className`

class to be applied.

type: `string`


### `data` (required)

Each item in this array will be a row in the list.
The named keys below are all 'special' in that they
each have a unique useage in ArmorListItem.
Every other key is displayed as a ArmorKeyValueRow.
```
 {
 id: String | Number,
 img: String | Element,
 dateTime: String
 name: String,
 ... rest of data
 }
```

type: `array`


### `defaultKey`

Default sort key to replace dateTime

type: `string`


### `defaultSortAscending`

Direction to sort on by default.
If not present, sort is ascending

type: `bool`


### `defaultSortKey`

Key to sort on by default.
If not present, sort is on dateTime

type: `string`


### `fabItems`

Array in the format used for a ArmorFab Component

type: `array`


### `formatKeys`

Boolean which will format keys in material design if true

type: `bool`


### `formatValues`

Boolean which will format values in material design if true

type: `bool`


### `hideKeys`

Keys in data object to not visually show. Still sortable.

type: `array`


### `name`

Name for list when printing/saving.

type: `string`


### `noDateTimeSort`

Removes Datetime from the sort list.

type: `bool`


### `onClear`

Function to execute when the close button is clicked

type: `func`


### `onClick` (required)

Function to execute when a list item is clicked.

type: `func`


### `onPrint`

Function to be executed before printing

type: `func`


### `onSave`

Function to be executed before saving

type: `func`


### `pageSize`

Number which specifies how many items per page.
If not specified, all will render.

type: `number`


### `resetPageOnNewData`

Stops ArmorList from going to page 1 when new data comes in.

type: `bool`
defaultValue: `true`


### `selected`

Id of currently active/selected list item.

type: `any`


### `smallKey`

Sets size for ArmorKeyValueRow
use in ArmorListItem

type: `string`


### `sortKeys`

Keys matching data to sort on

type: `array`


### `style`

Style to be applied.

type: `object`


### `value`

Value for ArmorTabbedList to key in on

type: `string`

