`ArmorCheckboxSelect.jsx` (component)
=====================================



Props
-----

### `colSize`

Column size for each checkbox

type: `number`
defaultValue: `4`


### `filters` (required)

List of objects with id (String), name (String), active (boolean)
      to generate checkboxes from.
```
[
  {
    id: 'all',
    name: 'All',
    active: false,
  },
  {
    id: 'a',
    name: 'A',
    active: true,
  },
  {
    id: 'b',
    name: 'B',
    active: false,
  },
]
```

type: `array`


### `name` (required)

Name of the filter that will be returned when onChange is fired.

type: `string`


### `onChange` (required)

Callback function returning the results (name, id) of filter clicked.

type: `func`


### `styles`

Object representing custom styles.

type: `object`

