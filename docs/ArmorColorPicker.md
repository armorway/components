`ArmorColorPicker.jsx` (component)
==================================

ArmorColorPicker renders a button
that toggles a color picker.

Props
-----

### `color` (required)

Color to set as currently selected color.

type: `string`


### `onChange` (required)

onChange is called when a new color is picked

type: `func`

