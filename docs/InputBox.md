`InputBox.jsx` (component)
==========================



Props
-----

### `children`

type: `object`


### `errorMsg`

type: `string`


### `icon`

type: `string`


### `label`

type: `string`


### `onChange`

type: `func`


### `placeholder`

type: `string`


### `type` (required)

type: `string`


### `value`

type: `string`

