`ArmorIncidentTable.jsx` (component)
====================================



Props
-----

### `incidents` (required)

type: `object`
defaultValue: `Immutable.fromJS([])`


### `loading`

type: `bool`


### `onFindRelated` (required)

type: `func`


### `printing`

type: `bool`

