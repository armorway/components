`ArmorIncidentRow.jsx` (component)
==================================



Props
-----

### `idx`

type: `number`


### `incident`

type: `object`


### `onChevronClick`

type: `func`


### `onRowSelection`

type: `func`


### `printing`

type: `bool`


### `userSelected`

type: `bool`

