`ArmorVFCheckbox.jsx` (component)
=================================



Props
-----

### `colSize`

Column size for each checkbox

type: `number`
defaultValue: `4`


### `errorMessage`

Error Message if fitler is invalid

type: `string`


### `filters` (required)

List of objects with id (String), name (String), active (boolean)
      to generate checkboxes from.
```
[
  {
    id: 'all',
    name: 'All',
    active: false,
  },
  {
    id: 'a',
    name: 'A',
    active: true,
    icon: undefined | <h5>joe</h5> | <img src="" />
  },
  {
    id: 'b',
    name: 'B',
    active: false,
  },
]
```

type: `array`


### `invalidFilter`

Optional switch to stop filter from rendering

type: `bool`


### `name` (required)

Name of the filter that will be returned when onChange is fired.

type: `string`


### `onChange` (required)

Callback function returning the results (name, id) of filter clicked.

type: `func`


### `onHelpClick`

Function which will return the name of this filter, when a help tip is clicked on

type: `func`


### `style`

Styles

type: `object`


### `styles`

Object representing custom styles.

type: `object`

