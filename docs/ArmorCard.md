`ArmorCard.jsx` (component)
===========================



Props
-----

### `children`

type: `any`


### `className`

type: `string`


### `onClose` (required)

type: `func`


### `subtitle`

type: `any`


### `title`

type: `any`

