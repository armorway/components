`ArmorDivider.jsx` (component)
==============================

ArmorDivider provides a spacing for content
and can display a label in the center of it.

Props
-----

### `label`

Label rendered as text in the center of the divider

type: `string`


### `labelStyle`

Style to be applied to label.

type: `object`


### `spaceOnly`

Determines if background is transparent or not
If true will automatically create a divider and ignore label

type: `bool`

