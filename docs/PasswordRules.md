`PasswordRules.jsx` (component)
===============================



Props
-----

### `rules` (required)

type: `object`


### `setStyles`

type: `object`
defaultValue: `{}`


### `small`

type: `bool`

