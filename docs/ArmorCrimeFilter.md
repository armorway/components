`ArmorCrimeFilter.jsx` (component)
==================================



Props
-----

### `dateLabel`

type: `string`
defaultValue: `'Last:'`


### `onAccept` (required)

type: `func`


### `onChange` (required)

type: `func`


### `showApplyBtn`

type: `bool`
defaultValue: `false`


### `showAreas`

type: `bool`
defaultValue: `true`


### `showBeats`

type: `bool`
defaultValue: `true`


### `showCrimeFilter`

type: `bool`
defaultValue: `true`


### `showWatch`

type: `bool`
defaultValue: `true`

