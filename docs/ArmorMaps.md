`ArmorMaps.jsx` (component)
===========================



Props
-----

### `beats`

type: `any`
defaultValue: `'true'`


### `centerLat` (required)

type: `number`


### `centerLong` (required)

type: `number`


### `drawing`

type: `custom`


### `icon`

type: `object`


### `infoWindowKey`

type: `string`
defaultValue: `'name'`


### `loaded`

type: `bool`


### `locations`

type: `union(array|object)`


### `mapId`

type: `string`


### `markerIconColor`

type: `string`


### `markerIconScale`

type: `number`


### `noControls`

type: `bool`


### `onDrag`

type: `func`


### `onMarkerClick`

type: `func`


### `show`

type: `bool`


### `showBeatsToggle`

type: `bool`
defaultValue: `true`


### `style`

type: `object`


### `type`

type: `union(array|string)`
defaultValue: `'marker'`


### `zoomValue` (required)

type: `number`

