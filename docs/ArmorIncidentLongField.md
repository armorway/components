`ArmorIncidentLongField.jsx` (component)
========================================



Props
-----

### `name`

type: `string`


### `printing`

type: `bool`


### `text`

type: `string`
defaultValue: `''`

