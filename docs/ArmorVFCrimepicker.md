`ArmorVFCrimepicker.jsx` (component)
====================================



Props
-----

### `onChange` (required)

Function will fire when menu changes
signature: function(event, index, value)

type: `func`


### `options` (required)

List of properties supported by <MenuItem />
such as: primaryText, value etc...

type: `array`


### `value`

Initial selection value for the dropdown

type: `any`

