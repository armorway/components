`UserInfoBox.jsx` (component)
=============================



Props
-----

### `company`

type: `string`


### `firstname`

type: `string`


### `lastname`

type: `string`


### `profilePic`

type: `string`


### `userrole`

type: `object`

