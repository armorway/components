`ArmorFabItem.jsx` (component)
==============================

ArmorFabItem is designed to be used as an internal component
to the ArmorFab component. They will be displayed above the
FAB and have represent actions one can take.

Props
-----

### `color` (required)

Background color for the Item

type: `string`


### `eventKey`

Event key passed on click to identify the clicked item

type: `union(string|number)`


### `icon` (required)

When a string is passed, a FontAwesome icon will be rendered.
 When an object is passed, that will be rendered as the icon.

type: `union(string|object)`


### `onClick` (required)

Function to excute when item is clicked.

type: `func`

