`ArmorFAB.jsx` (component)
==========================

ArmorFab is used to display a list of actions when clicked on.

Props
-----

### `bsStyle`

Bootstrap type of main FAB Icon. Default: primary

type: `string`


### `className`

class to be applied.

type: `string`


### `items` (required)

Array of Items to build into ArmorFABItems.

type: `array`


### `onClick`

Function to excute when FAB is clicked.

type: `func`


### `small`

Designates size of FAB Icon.

type: `bool`


### `style`

Style to be applied.

type: `object`

