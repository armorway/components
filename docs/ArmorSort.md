`ArmorSort.jsx` (component)
===========================



Props
-----

### `descriptionAlias`

type: `string`


### `list` (required)

type: `array`


### `onSort` (required)

type: `func`


### `titleAlias`

type: `string`

