`ArmorListItem.jsx` (component)
===============================

ArmorListItem is designed to be used as an internal component
to the ArmorList component. It is a list of information with
actions, FAB and an image.

Props
-----

### `actions`

Actions to be displayed at buttom of component.
If provided, it expects 2.

type: `array`


### `active`

When active, the row expands and shows more data
and the FAB Icon.

type: `bool`


### `allActive`

Makes row expanded and permanent active.

type: `bool`


### `className`

class to be applied.

type: `string`


### `clearable`

Shows close button on selected items

type: `bool`


### `customRow`

React Component to be rendered above actions
and below data rows

type: `object`


### `data`

Meta data to be displayed as rows when item is active.

type: `object`


### `dateTime` (required)

DateTime of event

type: `string`


### `fabItems`

Array in the format used for a ArmorFab Component.

type: `array`


### `formatKeys`

Boolean which will format keys in material design if true

type: `bool`


### `formatValues`

Boolean which will format values in material design if true

type: `bool`


### `id` (required)

Id for the List item, it used to know which row is clicked

type: `any`


### `img`

Logo for type

type: `union(element|string)`


### `name` (required)

Name to be displayed

type: `string`


### `onClear`

Function to be called when clear button is clicked

type: `func`


### `onClick` (required)

Triggered when row is clicked.

type: `func`


### `printing`

Used to inform the component printing is happening

type: `bool`


### `smallKey`

Sets size for ArmorKeyValueRow

type: `string`


### `style`

Style to be applied.

type: `object`

