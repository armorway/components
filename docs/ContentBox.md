`ContentBox.jsx` (component)
============================



Props
-----

### `children`

Contents for inside the ContentBox.

type: `any`


### `header`

When true, props.title takes the whole title row.

type: `bool`


### `height`

Set a height for the container.

type: `number`


### `noHeader`

When true, ignores all props related
to the header except sideElement.
Renders no header, w/ sideElement
positioned in the top right

type: `bool`


### `sideElement`

Side element to be displayed on the top right.

type: `element`


### `sideSize`

Number of cols to allocate for props.sideElement
props.title automatically gets the inverse from 12
titleSize = 12 - props.sizeSize
range: 0-12

type: `number`


### `style`

Style object to be applied.

type: `object`


### `title`

Title element to be displayed on the top left.

type: `any`

