`ArmorToggle.jsx` (component)
=============================



Props
-----

### `checked`

type: `bool`
defaultValue: `true`


### `labelOff`

type: `string`


### `labelOn`

type: `string`


### `onChange`

type: `func`

