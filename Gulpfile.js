const fs = require('fs');
const gulp = require('gulp');
const babel = require('gulp-babel');
const react = require('gulp-react');
const eslint = require('gulp-eslint');
const mocha = require('gulp-mocha');
const notifierReporter = require('mocha-notifier-reporter');

const gulpUtil = require('gulp-util');

const generateMarkdown = require('react-docgen/example/generateMarkdown.js');
const reactDocgen = require('react-docgen');
const WebpackDevServer = require('webpack-dev-server');
const webpack = require('webpack');
const webpackConfig = require('./webpack.config.js');

require('babel-core/register');

gulp.task('default', ['index', 'docs'], () => {});

gulp.task('react', () => {
  return gulp.src('src/*.js*')
    .pipe(babel({
      presets: ['es2015', 'react', 'stage-0'],
      plugins: ['transform-decorators-legacy'],
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('index', () => {
  fs.readdir('src/', (err, fileNames) => {
    if (err) throw err;

    const jsxFileNames = fileNames
      .filter((fileName) => fileName.indexOf('.jsx') !== -1)
      .map((fileName) => {
        return [
          `import ${fileName.replace('.jsx', '')} from './${fileName.replace('.jsx', '.js')}';`,
          `export {${fileName.replace('.jsx', '')} as ${fileName.replace('.jsx', '')}};`,
        ].join('\n');
      }).join('\n');

    fs.writeFile('src/index.js', jsxFileNames, (erra) => {
      if (erra) throw erra;
    });
  });
});
gulp.task('lint', () => {
  return gulp.src('src/*.js*')
  .pipe(eslint())
  .pipe(eslint.format());
});

gulp.task('docs', () => {
  fs.readdir('src/', (err, fileNames) => {
    fileNames.forEach((fileName) => {
      if (fileName.indexOf('.jsx') !== -1) {
        fs.readFile(`src/${fileName}`, (erra, data) => {
          const jsonDocs = reactDocgen.parse(data);

          jsonDocs.props = jsonDocs.props || {};
          const mdDocs = generateMarkdown(fileName, jsonDocs);
          const docFileName = `docs/${fileName.replace('.jsx', '.md')}`;

          fs.writeFile(docFileName, mdDocs, (errs) => {
            if (errs) { throw errs; }
          });
        });
      }
    });
  });
});

gulp.task('test', () => {
  return gulp.src('test/*.js')
      .pipe(mocha({
        reporter: notifierReporter.decorate('spec'),
      }))
      .on('error', function error(err) {
        gulpUtil.log(gulpUtil.colors.red(err.message));
        gulpUtil.log(gulpUtil.colors.red(err));
        this.emit('end');
      });
});

gulp.task('watch-test', ['test'], () => {
  gulp.watch('test/*.js', ['test']);
});

gulp.task('watch', ['lint', 'docs'], () => {
  gulp.watch('src/*.jsx', ['lint']);
});

gulp.task('webpack', () => {
  new WebpackDevServer(webpack(webpackConfig), {
      // server and middleware options
  }).listen(8080, 'localhost', (err) => {
    if (err) throw new gulpUtil.PluginError('webpack-dev-server', err);

    gulpUtil.log('[webpack-dev-server]', 'http://localhost:8080/webpack-dev-server/index.html');
  });
});
