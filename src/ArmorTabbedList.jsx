import React, { PropTypes } from 'react';
import FlatButton from 'material-ui/FlatButton';
import '../styles/ArmorTabbedList';

const proptypes = {
  /**
   * Child ArmorList components, must have a value prop.
   */
  children: PropTypes.array.isRequired,
  /**
   * Value that matches a child component prop to set as active.
   */
  value: PropTypes.string,
};
const contextTypes = {
  muiTheme: PropTypes.array,
};

const styles = {
  inkBar: { display: 'none' },
  tabContainer: {
    width: '100%',
    overflowX: 'auto',
    backgroundColor: 'transparent',
    overflowY: 'hidden',
    whiteSpace: 'nowrap',
  },
  tab: {
    padding: '0 10px',
    width: 'auto',
    display: 'inline',
    borderRadius: '0px',
    color: '#fff',
  },
  activeTab: {
    padding: '0 10px',
    width: 'auto',
    display: 'inline',
    borderRadius: '0px',
    color: '#fff',
    backgroundColor: '#fff',
  },
};

class ArmorTabbedList extends React.Component {
  constructor(props) {
    super(props);

    let active = undefined;

    if (props.value) {
      active = props.value;
    } else if (props.children) {
      active = props.children[0].props.value;
    }

    this.state = {
      children: this.getOnlyDefined(props.children),
      active,
    };

    this.onChange = this.onChange.bind(this);
  }

  componentWillMount() {
    styles.tabContainer.backgroundColor = this.context.muiTheme.palette.primary1Color;
    styles.activeTab.backgroundColor = this.context.muiTheme.palette.primary2Color;
  }

  componentWillReceiveProps(nextProps) {
    const passedActive = (
      (nextProps.value !== this.props.value) ||
      (nextProps.value !== this.state.value)
    ) ? nextProps.value : undefined;

    const children = this.getOnlyDefined(nextProps.children);

    if (nextProps.children && nextProps.children.length === 0) {
      this.setState({
        active: undefined,
        children,
      });
    } else {
      const childValues = React.Children
      .map(children, child => {
        return child.props.value;
      });

      if (passedActive && _.includes(childValues, passedActive)) {
        this.setState({
          active: passedActive,
          children,
        });
      } else if (!_.includes(childValues, this.state.active)) {
        this.setState({
          active: children[0].props.value,
          children,
        });
      } else {
        this.setState({ children });
      }
    }
  }

  onChange(evt) {
    this.setState({
      active: evt.currentTarget.getAttribute('value'),
    });
  }

  getOnlyDefined(children) {
    return React.Children
      .toArray(children)
      .filter(child => child);
  }

  renderTabs() {
    return (
    <div
      style={styles.tabContainer}
      initialSelectedIndex={0}
      onChange={this.onChange}
      value={this.state.active}
    >
      {
        React.Children.map(this.state.children, (child) => {
          if (child && child.props) {
            return (
              <FlatButton
                value={child.props.value}
                label={child.props.value}
                style={(child.props.value === this.state.active) ? styles.activeTab : styles.tab}
                onClick={this.onChange}
              />
            );
          }
          return undefined;
        })
      }
      </div>
    );
  }

  renderComponent() {
    return _.find(this.state.children, (child) => child.props.value === this.state.active);
  }

  render() {
    return (
      <div className="armor-tabbed-list">
        {this.renderTabs()}
        {this.renderComponent()}
      </div>
    );
  }
}

ArmorTabbedList.propTypes = proptypes;
ArmorTabbedList.contextTypes = contextTypes;
export default ArmorTabbedList;
