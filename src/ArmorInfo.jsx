import React, { PropTypes } from 'react';
import IconButton from 'material-ui/IconButton';
import { Popover, PopoverAnimationVertical } from 'material-ui/Popover';

const propTypes = {
  title: PropTypes.any,
  children: PropTypes.any,
};
const anchorOrigin = {
  horizontal: 'right',
  vertical: 'center',
};
const targetOrigin = {
  horizontal: 'left',
  vertical: 'center',
};

const styles = {
  icon: {
    fontSize: '18px',
  },
  popover: {
    padding: '5px 20px',
    width: '300px',
  },
};

class ArmorInfo extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      anchorEl: undefined,
    };

    this.showPopover = this.showPopover.bind(this);
    this.closePopover = this.closePopover.bind(this);
  }

  showPopover(evt) {
    this.setState({
      anchorEl: evt.currentTarget,
      open: !this.state.open,
    });
  }

  closePopover() {
    this.setState({ open: false });
  }

  render() {
    return (
      <span>
        <IconButton
          iconStyle={styles.icon}
          onClick={this.showPopover}
          iconClassName="fa fa-question-circle"
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={anchorOrigin}
          targetOrigin={targetOrigin}
          onRequestClose={this.closePopover}
          animation={PopoverAnimationVertical}
        >
          <div style={styles.popover}>
            <h5>{this.props.title}</h5>
            {this.props.children}
          </div>
        </Popover>
      </span>
    );
  }
}
ArmorInfo.propTypes = propTypes;
export default ArmorInfo;
