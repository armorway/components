import React, { PropTypes } from 'react';

import { Row, Col } from 'react-bootstrap';

const propTypes = {
  /**
   * Determines if background is transparent or not
   * If true will automatically create a divider and ignore label
   */
  spaceOnly: PropTypes.bool,
  /**
   * Label rendered as text in the center of the divider
   */
  label: PropTypes.string,
  /**
   * Style to be applied to label.
   */
  labelStyle: PropTypes.object,
};

/**
  * ArmorDivider provides a spacing for content
  * and can display a label in the center of it.
  */
class ArmorDivider extends React.Component {
  constructor() {
    super();
    this.buildWithLabel = this.buildWithLabel.bind(this);
  }

  buildDivider(styleOverride) {
    return <Row><div className="divider" style={styleOverride}></div></Row>;
  }

  buildWithLabel(styleOverride) {
    return (
      <div>
        <Col xs={5}>
          {this.buildDivider(styleOverride)}
        </Col>
        <Col xs={2} style={{ padding: '0' }} className="divider">
          <span style={this.props.labelStyle}>{this.props.label}</span>
        </Col>
        <Col xs={5}>
          {this.buildDivider(styleOverride)}
        </Col>
      </div>
    );
  }

  render() {
    const { label, spaceOnly } = this.props;
    const styleOverride = (spaceOnly) ?
       { backgroundColor: 'transparent' } :
       undefined;

    return (
      <div>
        {label && !spaceOnly ?
          this.buildWithLabel(styleOverride) :
          this.buildDivider(styleOverride)}
      </div>
    );
  }
}
ArmorDivider.propTypes = propTypes;
export default ArmorDivider;
