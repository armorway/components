import React, { PropTypes } from 'react';
import _ from 'lodash';
import Select from 'react-select';

import AppStore from 'stores/global/AppStore';
import OSEStore from 'components/ose/OSEStore';

const propTypes = {
  value: PropTypes.any,
  onChange: PropTypes.func,
};

class ArmorWatchSelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = _.assign({}, OSEStore.getState(), AppStore.getState().watch.data);
    this.onUpdate = this.onUpdate.bind(this);
  }

  componentDidMount() {
    OSEStore.listen(this.onUpdate);
  }

  componentWillUnmount() {
    OSEStore.unlisten(this.onUpdate);
  }

  onUpdate(newState) {
    this.setState({ watch: newState.watch });
  }

  getWatchOptions() {
    return this.formatWatch(this.state.watch || []);
  }

  formatWatch(watches) {
    return _.map(watches, (watch) => {
      return {
        label: watch,
        value: watch,
      };
    });
  }

  render() {
    return (
      <Select
        className="watch-dropdown"
        name="watch-dropdown"
        placeholder="Select Watch"
        value={this.props.value}
        options={this.getWatchOptions()}
        onChange={this.props.onChange}
      />
    );
  }
}

ArmorWatchSelect.propTypes = propTypes;
export default ArmorWatchSelect;
