import React, { PropTypes } from 'react';
import FontAwesome from './FontAwesome';
import { Label } from 'react-bootstrap';

import '../styles/ArmorFilterListItem.scss';

const propTypes = {
  /**
   * Function to excute when item is removed.
   * onRemove(type, name)
   */
  onRemove: PropTypes.func.isRequired,
  /**
   *  Name/text of the component. It is the displayed text.
   */
  name: PropTypes.string.isRequired,
  /**
   *  Type of the filter. Used to identify the filter.
   */
  type: PropTypes.string.isRequired,
 /**
   * Bootstrap type to apply to filter labels. Default: primary
   */
  bsStyle: PropTypes.string,
  /**
   * class to be applied.
   */
  className: PropTypes.string,
};

/**
 * ArmorFilterListItem is designed to be used as an internal component
 * to the ArmorFilterListItem component. The applied filters will be
 * displayed with a remove button.
 */
class ArmorFilterListItem extends React.Component {
  constructor(props) {
    super(props);

    this.onRemoveClick = this.onRemoveClick.bind(this);
  }

  onRemoveClick() {
    this.props.onRemove(this.props.type, this.props.name);
  }

  render() {
    return (
      <Label bsStyle={this.props.bsStyle || 'primary'} className={`${this.props.className || ''} filter-item`}>
        <button onClick={this.onRemoveClick} className="remove-btn">
          <FontAwesome icon="close" className="remove-divider" />
        </button>
        <span className="filter-item-name" onClick={this.onRemoveClick}>
          {this.props.name}
        </span>
      </Label>
    );
  }
}

ArmorFilterListItem.propTypes = propTypes;
export default ArmorFilterListItem;
