import React, {PropTypes} from 'react';
import {Tooltip, OverlayTrigger} from 'react-bootstrap';

import FontAwesome from 'components/global/FontAwesome';

const editTooltip = (
  <Tooltip id="1">Edit</Tooltip>
);

const deleteTooltip = (
  <Tooltip id="2">Delete</Tooltip>
);

const propTypes = {
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
};

class ArmorEdit extends React.Component {
  constructor() {
    super();
    this.onEdit = this.onEdit.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  onEdit(event) {
    event.stopPropagation();
    this.props.onEdit(event);
  }

  onDelete(event) {
    event.stopPropagation();
    this.props.onDelete(event);
  }

  buildEditIfNeeded() {
    return this.buildModifyBtn('onEdit', editTooltip, this.onEdit, 'pencil');
  }

  buildDeleteIfNeeded() {
    return this.buildModifyBtn('onDelete', deleteTooltip, this.onDelete, 'trash-o');
  }

  buildModifyBtn(propName, tooltip, action, icon) {
    if (this.props[propName]) {
      return (
        <OverlayTrigger placement="top" overlay={tooltip}>
          <span role="button" onClick={action} style={{fontSize: '24px', padding: '10px 20px 0 0'}}>
            <FontAwesome icon={icon} />
          </span>
        </OverlayTrigger>
      );
    }
  }

  render() {
    return (
      <div className="pull-right">
        {this.buildEditIfNeeded()}
        {this.buildDeleteIfNeeded()}
      </div>
    );
  }

}

ArmorEdit.propTypes = propTypes;
export default ArmorEdit;
