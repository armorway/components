import React, { PropTypes } from 'react';
import Color from 'react-color';
import { Button } from 'react-bootstrap';
import FontAwesome from './FontAwesome.jsx';

import '../styles/ArmorColorPicker.scss';

const propTypes = {
  /**
    * onChange is called when a new color is picked
  */
  onChange: PropTypes.func.isRequired,
  /**
    * Color to set as currently selected color.
  */
  color: PropTypes.string.isRequired,
};
const wrapperClassName = 'wrapper-color-picker';

/**
  * ArmorColorPicker renders a button
  * that toggles a color picker.
*/
class ArmorColorPicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayColorPicker: false,
    };
    this.onColorChange = this.onColorChange.bind(this);
    this.onClick = this.onClick.bind(this);
    this.onWindowClick = this.onWindowClick.bind(this);
  }

  componentDidMount() {
    window.addEventListener('mousedown', this.onWindowClick);
  }

  componentWillUnmount() {
    window.removeEventListener('mousedown', this.onWindowClick);
  }

  onColorChange() {
    // eslint-disable-next-line
    this.props.onChange(...arguments);
  }

  onClick(newState) {
    this.setState({ displayColorPicker: newState });
  }

  onWindowClick(event) {
    let { target } = event;

    while (target.parentElement) {
      if (target.parentElement.className === wrapperClassName) {
        return;
      }
      target = target.parentElement;
    }
    this.setState({ displayColorPicker: false });
  }

  render() {
    return (
      <span style={{ marginLeft: '15px' }} className={wrapperClassName} >
        <Button
          id="color"
          onClick={() => this.onClick(!this.state.displayColorPicker)}
          className="color-button"
        >
          <span
            style={{ color: this.props.color }}
            className="color"
          />
          <FontAwesome
            icon="caret-down"
            iconStyle={{ position: 'absolute', marginTop: '6px' }}
          />
        </Button>
        <div
          style={{
            display: (this.state.displayColorPicker) ?
            'block' : 'none', backgroundColor: 'white' }}
        >
          <Color
            type="chrome"
            onChangeComplete={this.onColorChange}
            onClose={this.onClick.bind(this, false)}
            color={this.props.color}
            positionCSS={{ width: '100%', position: 'absolute', zIndex: 10 }}
            className="color-class"
          />
        </div>
      </span>
    );
  }
}
ArmorColorPicker.propTypes = propTypes;
export default ArmorColorPicker;
