import React, { PropTypes } from 'react';
import ReactDOM from 'react-dom';
import Paper from 'material-ui/Paper';
import { Row, Col } from 'react-bootstrap';
import { utils } from './utils';

import '../styles/ContentBox.scss';
const propTypes = {
  /**
   * Contents for inside the ContentBox.
   */
  children: PropTypes.any,
  /**
   * When true, props.title takes the whole title row.
   */
  header: PropTypes.bool,
  /**
   * Title element to be displayed on the top left.
   */
  title: PropTypes.any,
  /**
   * Side element to be displayed on the top right.
   */
  sideElement: PropTypes.element,
  /**
   * Number of cols to allocate for props.sideElement
   * props.title automatically gets the inverse from 12
   * titleSize = 12 - props.sizeSize
   * range: 0-12
   */
  sideSize: PropTypes.number,
  /**
   * Style object to be applied.
   */
  style: PropTypes.object,

  /**
   * Set a height for the container.
   */
  height: PropTypes.number,

  /**
   * When true, ignores all props related
   * to the header except sideElement.
   * Renders no header, w/ sideElement
   * positioned in the top right
   */
  noHeader: PropTypes.bool,
};

class ContentBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sideOpen: false,
      height: 'auto',
    };

    if (props.height) {
      this.state.height = props.height;
    }
    this.getColSizes = this.getColSizes.bind(this);
  }

  componentDidMount() {
    if (this.props.height) {
      const top = ReactDOM.findDOMNode(this).getBoundingClientRect().top;
      const height = utils.calculateFitHeight(this.props.height, top);

      /* eslint react/no-did-mount-set-state: 0 */
      this.setState({ height });
    }
  }

  componentWillReceiveProps(newProps, oldProps) {
    if (newProps.height !== oldProps.height) {
      const top = ReactDOM.findDOMNode(this).getBoundingClientRect().top;
      const height = utils.calculateFitHeight(newProps.windowHeight, top);

      this.setState({ height });
    }
  }

  getHeader() {
    const sizes = this.getColSizes();

    if (this.props.noHeader) {
      return (
        <div className="side-no-header">
          {this.props.sideElement}
        </div>
      );
    }
    return (
      <Row className={'content-box-title-row'} style={{ margin: 0 }}>
        <Col xs={sizes.left} className="title">
          <span className="title">{this.props.title}</span>
        </Col>
        <Col xs={sizes.right} className="title-side">
          <span className="pull-right">{this.props.sideElement}</span>
        </Col>
      </Row>
    );
  }

  getColSizes() {
    const sizes = {
      left: 6,
      right: 6,
    };

    if (this.props.header) {
      sizes.left = 12;
      sizes.right = 0;
    } else if (this.props.sideSize !== undefined) {
      sizes.left = 12 - this.props.sideSize;
      sizes.right = this.props.sideSize;
    }

    return sizes;
  }

  render() {
    const header = this.getHeader();

    const style = _.assign({
      height: `${(this.state.height === 'auto') ? 'auto' : this.state.height}px`,
    }, this.props.style);

    return (
      <div>
        <Paper className={`content-box ${this.props.className || ''}`} style={style} zDepth={1}>
        <div>
          {header}
          <div>{this.props.children}</div>
        </div>
        </Paper>
      </div>

    );
  }
}

ContentBox.propTypes = propTypes;

export default ContentBox;
