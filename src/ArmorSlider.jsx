import React, { PropTypes } from 'react';
import Slider from 'react-slide';

import '../styles/ArmorSlider.scss';
const propTypes = {
  /**
     * current value of slider
  */
  value: PropTypes.number,
  /**
     * label for slider
     * displayed as label: value
  */
  label: PropTypes.string,
  /**
     * Min value for slider
     * defaults at 0
  */
  startValue: PropTypes.number,
  /**
     * Max value for slider
     * defaults at 100
  */
  endValue: PropTypes.number,

  /**
   * name of component. Pass in change/drag
  */
  name: PropTypes.any,

  onDrag: PropTypes.func,
  onChange: PropTypes.func,
};

/**
  * Slider object with a value that can slide from
  * left to right and has an optional label.
  * Start and end values (min and max) can be customized
  * but default at 0 and 100
*/

class ArmorSlider extends React.Component {
  constructor() {
    super();
    this.state = {};

    this.onChange = this.onChange.bind(this);
    this.onDrag = this.onDrag.bind(this);
  }

  onChange(...args) {
    this.props.onChange(this.props.name, ...args);
  }

  onDrag(...args) {
    this.props.onDrag(this.props.name, ...args);
  }

  render() {
    const props = _.omit(this.props, ['onChange', 'onDrag']);

    return (
      <div className="armor-slider">
        <span className="armor-slider-label-start">
          <span style={{ fontWeight: 800 }} className="center-slider armor-slider-label">
            {this.props.label}
          </span>
          <span className="armor-slider-startValue pull-right">
            {this.props.startValue}
          </span>
       </span>
        <span className="armor-slider-wrapper">
          <div className="tear-wrapper">
            <div style={{ marginLeft: `${this.props.value * 100 / this.props.endValue}%` }}>
              <div className="tear-sizer">
                <div className="" />
                <div className="value">
                  {this.props.value}
                </div>
              </div>
            </div>
          </div>
          <Slider
            onDrag={this.onDrag}
            onChange={this.onChange}
            {...props}
          />
        </span>
        <span xs={1} className="armor-slider-endValue">
        <span>{this.props.endValue}</span>
        </span>

      </div>
    );
  }
}

ArmorSlider.propTypes = propTypes;
export default ArmorSlider;
