import React, {PropTypes} from 'react';
import {Row, Col, Button} from 'react-bootstrap';
import Dropzone from 'react-dropzone';
import FontAwesome from './FontAwesome';
import Loader from 'react-loader';

const defaultProps = {
  multiple: true,
  loaded: true,
  showAttachedFiles: true,
};

const propTypes = {
  onFilesAttached: PropTypes.func,
  onUpload: PropTypes.func,
  showAttachedFiles: PropTypes.bool,
  loaded: PropTypes.bool,
  multiple: PropTypes.bool,
  style: PropTypes.object,
};

class ArmorFileUpload extends React.Component {
  constructor() {
    super();
    this.state = {files: []};
    this.onDrop = this.onDrop.bind(this);
    this.buildFileList = this.buildFileList.bind(this);
    this.buildFileNames = this.buildFileNames.bind(this);
    this.buildDropzone = this.buildDropzone.bind(this);
    this.buildLoader = this.buildLoader.bind(this);
    this.onUploadFile = this.onUploadFile.bind(this);
  }

  onDrop(files) {
    this.setState({files});
    this.props.onFilesAttached(this.props.multiple ? files : files[0]);
  }

  onUploadFile() {
    const files = this.props.multiple ? this.state.files : this.state.files[0];

    this.props.onUpload(files);
  }

  buildFileNames() {
    return _.map(this.state.files, (file, key) => {
      return (
        <li key={key}>
          <span>{file.name}</span>
          <span style={{marginLeft: '5px'}}>
            {(file.size / 1000).toFixed(2) + 'KB'}
          </span>
        </li>
      );
    });
  }

  buildFileList() {
    return (
      <div>
        {this.state.files.length > 0
        ?
          <h5>{this.props.multiple ? 'Files' : 'File'} Chosen</h5>
        : ''}
        <ul>{this.buildFileNames()}</ul>
      </div>
    );
  }

  buildButton() {
    if (this.state.files.length > 0) {
      return (
        <div>
          <Button bsStyle="success"
                  disabled={!this.props.loaded}
                  style={{marginTop: '5px'}}
                  onClick={this.onUploadFile}>
            <FontAwesome icon="upload" title="Upload!" padIcon />
          </Button>
          <Button bsStyle="danger"
                  disabled={!this.props.loaded}
                  style={{marginTop: '5px', marginLeft: '10px'}}
                  onClick={() => {this.setState({files: []});}}>
            <FontAwesome icon="close" title="Cancel" padIcon />
          </Button>
        </div>

      );
    }
  }

  buildDropzone() {
    return (
      <Dropzone style={this.props.style} onDrop={this.onDrop} className="dropzone" multiple={this.props.multiple}>
        <FontAwesome icon="paperclip" padIcon/>
        <span className="attach-label">Drag files or <a>Browse</a></span>
      </Dropzone>
    );
  }

  buildLoader() {
    return (
      <div style={{position: 'relative', paddingBottom: '15px'}} className="dropzone">
        <Loader loaded={this.props.loaded}/>
      </div>
    );
  }


  render() {
    return (
      <div id="armorFileUpload">
        <Row>
          <Col xs={12}>
            {this.props.loaded ? this.buildDropzone() : this.buildLoader()}
          </Col>
        </Row>
        <Row style={{marginTop: '5px'}}>
          <Col xs={6}>
            {this.props.showAttachedFiles ? this.buildFileList() : ''}
          </Col>
          <Col xs={4} xsOffset={2}>
            <div className="pull-right">
              {this.props.onUpload ? this.buildButton() : ''}
            </div>
          </Col>
        </Row>
      </div>

    );
  }
}

ArmorFileUpload.defaultProps = defaultProps;
ArmorFileUpload.propTypes = propTypes;
export default ArmorFileUpload;
