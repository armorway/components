
import React, {PropTypes} from 'react';
import _ from 'lodash';
import {OverlayTrigger, Tooltip, Button, ButtonGroup} from 'react-bootstrap';
import FontAwesome from 'components/global/FontAwesome';

import {utils} from 'utils';

const propTypes = {
  options: PropTypes.array.isRequired,
  onSelect: PropTypes.func.isRequired,
};

class ArmorIntervalDates extends React.Component {
  constructor() {
    super();
    this.state = {};
    this.onSelect = this.onSelect.bind(this);
  }

  componentDidMount() {
    this.onSelect(this.props.options[0]);
  }

  onSelect(item) {
    const active = (_.isString(item)) ? item : item.keys;
    const previousActive = this.state.active;

    this.setState({active});
    this.props.onSelect(item, previousActive);
  }

  render() {
    const result = _.map(this.props.options, (item, idx) => {
      return (
        <OverlayTrigger key={idx} placement="top" overlay={<Tooltip id="dateId">{utils.camelToReadable(item.keys)}</Tooltip>}>
          <Button onClick={this.onSelect.bind(this, item)}
                  active={this.state.active === item.keys}>
            {item.label}
          </Button>
        </OverlayTrigger>
      );
    });

    result.push(
      <OverlayTrigger key={this.props.options.length + 1}
                      placement="top"
                      overlay={<Tooltip id="dateTip">Custom Date</Tooltip>}>
        <Button onClick={this.onSelect.bind(this, 'calendar')}
                          active={this.state.active === 'calendar'}>
        <FontAwesome icon="calendar" /></Button>
      </OverlayTrigger>
    );

    return (
      <ButtonGroup>
        {result}
      </ButtonGroup>
    );
  }
}

ArmorIntervalDates.propTypes = propTypes;
export default ArmorIntervalDates;
