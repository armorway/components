import React, {PropTypes} from 'react';

import CrimeModal from 'components/global/modals/CrimeModal';
import AppActions from 'actions/global/AppActions';
import CrimeStore from 'stores/CrimeStore';


const propTypes = {
  'data': PropTypes.any.isRequired,
};

class ArmorLink extends React.Component {
  constructor(props) {
    super(props);
    this.state = CrimeStore.getState();
    this.updateStates = this.updateStates.bind(this);
    this.onCrimeDetails = this.onCrimeDetails.bind(this);
  }

  componentDidMount() {
    CrimeStore.listen(this.updateStates);
  }

  componentWillUnmount() {
    CrimeStore.unlisten(this.updateStates);
  }

  onCrimeDetails() {
    AppActions.toggleModal({
      showModal: true,
      size: 'large',
      modal: <CrimeModal caseNumber={this.props.data}/>,
    });
  }

  updateStates(newState) {
    this.setState(newState);
  }

  render() {
    return <a style={{cursor: 'pointer'}} onClick={this.onCrimeDetails}>{this.props.data}</a>;
  }
}


ArmorLink.propTypes = propTypes;
export default ArmorLink;
