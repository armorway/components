import React, { PropTypes } from 'react';
import _ from 'lodash';
import FontAwesome from './FontAwesome';
import '../styles/ArmorFabItem.scss';

const propTypes = {
  /**
   * Function to excute when item is clicked.
   */
  onClick: PropTypes.func.isRequired,
  /**
   *  When a string is passed, a FontAwesome icon will be rendered.
   *  When an object is passed, that will be rendered as the icon.
   */
  icon: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
  /**
   * Background color for the Item
   */
  color: PropTypes.string.isRequired,
  /**
   * Event key passed on click to identify the clicked item
   */
  eventKey: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

const styles = {
  icon: {
    color: '#fff',
  },
  innerIcon: {
    position: 'relative',
  },
};

/**
 * ArmorFabItem is designed to be used as an internal component
 * to the ArmorFab component. They will be displayed above the
 * FAB and have represent actions one can take.
 */
class ArmorFabItem extends React.Component {
  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.props.onClick(this.props.eventKey);
  }

  render() {
    const icon = (_.isString(this.props.icon)) ? (
      <FontAwesome icon={this.props.icon} size="2x" iconStyle={styles.icon} />
    ) : this.props.icon;

    const { color: backgroundColor } = this.props;

    return (
      <button className="fab-item" style={{ backgroundColor }} onClick={this.onClick}>
        <div className="icon-wrapper" style={styles.innerIcon}>
          {icon}
        </div>
      </button>
    );
  }
}

ArmorFabItem.propTypes = propTypes;
export default ArmorFabItem;
