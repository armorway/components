import React, { PropTypes } from 'react';
import FlatButton from 'material-ui/FlatButton';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import RaisedButton from 'material-ui/RaisedButton';
import Checkbox from 'material-ui/Checkbox';

import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import { Row, Col, Input, Button } from 'react-bootstrap';
import { VelocityTransitionGroup } from 'velocity-react';
import autobind from 'autobind-decorator'

import _ from 'lodash';
import '../styles/ArmorViewFilter';

import ArmorVFCheckbox from './ArmorVFCheckbox';
import ArmorVFDropdown from './ArmorVFDropdown';
import ArmorVFCrimepicker from './ArmorVFCrimepicker';

class ArmorViewFilter extends React.Component {

  static propTypes = {
    /**
     * Array of multiple types of filters
     * ```
     * [
     *   {
     *     name: 'Beats',
     *     type: 'checkbox',
     *     options: [
     *       {
     *         id: 'all',
     *         name: 'All',
     *         active: false,
     *       },
     *       {
     *         id: 'a',
     *         name: 'A',
     *         children: ['b', 'c'],
     *         active: false,
     *       },
     *       {
     *         id: 'b',
     *         name: 'B',
     *         active: false,
     *       },
     *     ],
     *   },
     *   {
     *     name: 'Patrol Length',
     *     type: 'input',
     *     placeholder: 'Enter Length in Meters',
     *     value: '',
     *   },
     * ]
     * ```
     */
    filters: PropTypes.array.isRequired,
    /**
     * Array of overlay filters
     *```
     * [
     *  {
     *    name: 'Views',
     *    type: 'checkbox',
     *    options: [
     *      {
     *        id: 'all',
     *        name: 'All',
     *        active: false,
     *      },
     *      {
     *        id: 'hotspots',
     *        name: 'Hotspots',
     *        active: true,
     *      },
     *      {
     *        id: 'heatMap',
     *        name: 'Heat Map',
     *        active: false,
     *      },
     *    ],
     *  },
     * ]
     * ```
     */
    overlays: PropTypes.array.isRequired,
    /**
     * Array of staticOverlay filters displayed by default when Filters or Overlays are not open.
     *```
     * [
     *  {
     *    name: 'Views',
     *    type: 'checkbox',
     *    options: [
     *      {
     *        id: 'all',
     *        name: 'All',
     *        active: false,
     *      },
     *      {
     *        id: 'hotspots',
     *        name: 'Hotspots',
     *        active: true,
     *      },
     *      {
     *        id: 'heatMap',
     *        name: 'Heat Map',
     *        active: false,
     *      },
     *    ],
     *  },
     * ]
     * ```
     */
    staticOverlays: PropTypes.array,
    /**
     * Function called when pressing Apply.
     * First parameter contains Object representing applied filters.
     */
    onApply: PropTypes.func.isRequired,
    /**
     * Function called when reset is hit, Function()
     */
    onReset: PropTypes.func,
    /**
     * Object or string that will be the heading
     */
    title: PropTypes.any,
    /**
     * Function called before apply is pressed.
     * Function will format the data before it has been passed to onApply()
     */
    formatData: PropTypes.func,
    /**
     * A menu which only appears on mobile view requiring a
     * ```{label: 'String', value: 'string'}```
     */
    iconMenu: PropTypes.array,
    /**
     * Function fired upon the selection from the IconMenu, signature: function(event, value);
     */
    onIconMenuChange: PropTypes.func,
    /**
     * String representing the key that will select/deselect all checkboxes
     */
    allKey: PropTypes.string,
    /**
     * Class name for the entire component.
     */
    className: PropTypes.string,
    /**
     * A list of filters which will automatically trigger an export when changes
     */
    dynamicFilters: PropTypes.array,
    /**
     * Label for filters button, defaults to "Filters"
     */
    filtersButtonLabel: PropTypes.string,
    /**
     * Label for overlays button, defaults to "Overlays"
     */
    overlaysButtonLabel: PropTypes.string,
  };

  static defaultProps = {
    allKey: 'All',
    className: 'armor-view-filter',
    timeoutInterval: Infinity,
    filtersButtonLabel: 'Filters',
    overlaysButtonLabel: 'Overlays',
  };

  constructor(props) {
    super(props);
    const { filters, overlays, staticOverlays } = props;

    this.state = {
      showFilters: false,
      showOverlays: false,
      showStaticOverlays: true,
      filters: filters,
      overlays: overlays,
      staticOverlays: staticOverlays,
    };
    this.closeState = {
      showFilters: false,
      showOverlays: false,
      showStaticOverlays: true,
    };
    this.hideTimout = undefined;
    this.applyFilters = this.exportActiveFilters;
    this.onCloseFilters = this.onCloseFilters.bind(this);

    this.closeFilters = this.onCloseFilters;
  }

  componentWillReceiveProps(nextProps) {
    const { filters, overlays, staticOverlays } = nextProps;

    if (!_.isEqual(this.props.filters, nextProps.filters) ||
        !_.isEqual(this.props.overlays, nextProps.overlays) ||
        !_.isEqual(this.props.staticOverlays, nextProps.overlays)) {
        this.setState({
          filters: filters,
          overlays: overlays,
          staticOverlays: staticOverlays,
        });
     }
  }
  componentWillUnmount() {
    window.clearTimeout(this.hideTimeout);
  }

  @autobind
  onFilterChange(name, filter) {
    // Grab metadata about the filter
    const { id, name: optionName, children } = filter;
    const view = this.getView();
    // Clones the state of the opened window (filters or overlas)
    const newState = this.state[view];
    // Gets the correct filter, searches by name
    const idxType = _.findIndex(this.state[view], (itm) => itm.name === name);
    // Gets the correct filter selected from the new set of filters
    const newFilter = newState[idxType];

    // Grabs 3 indices, the old filter, new filter and the All selection
    // Then uses them by getting the actual options based of the index
    const idxNewState = this.getIndex(newFilter, id);
    const idxState = this.getIndex(this.state[view][idxType], id);
    const idxAll = this.getAllIndex(newFilter);
    const shouldSelectOption = !this.state[view][idxType].options[idxState].active;
    const allFilter = newFilter.options[idxAll];

    newFilter.options[idxNewState].active = shouldSelectOption;

    // Looks for anything else related (children, all options) then selects them
    this.setState({[view]: newState}, () => {
      this.selectAllWithId(filter, newFilter.name, shouldSelectOption);
      if (_.includes(this.props.dynamicFilters, name)) {
        this.props.onChange(name, filter);
      }
    });
  }

  @autobind
  onSomeChange(filterName, value) {
    const view = this.getView();
    const newState = this.state[view];
    const idx = _.findIndex(newState, filter => filter.name === filterName);

    newState[idx].value = value;

    this.setState({[view]: newState}, () => {
      if (_.includes(this.props.dynamicFilters, filterName)) {
        this.props.onChange(filterName, value);
      }
    });
  }

  onInputChange(filterName, event) {
    this.onSomeChange(filterName, event.target.value)
  }

  onDropdownChange(filterName, event, index, value) {
    this.onSomeChange(filterName, value);
  }

  onTimeChange(filterName, event, value) {
    this.onSomeChange(filterName, value);
  }

  @autobind
  onCrimepickerDropdown(filterName, value) {
    const view = this.getView();
    const newState = this.state[view];
    const idx = _.findIndex(newState, filter => filter.name === filterName);

    newState[idx].selected = value;

    const crimeGroup = newState[idx].groups[newState[idx].selected].metadata;

    if (crimeGroup !== 'other') {
      _.forEach(newState[idx].options, option => {
        console.log('op', option);
        _.forEach(option, opVal => {
          opVal.active = _.includes(crimeGroup, opVal.id);
        })
      });
    }

    this.setState({[view]: newState});
  }

  @autobind
  onCrimepickerChange(filterName, newOption, category) {
    const view = this.getView();
    const newState = this.state[view];
    const idx = _.findIndex(newState, filter => filter.name === filterName);

    _.forEach(newState[idx].options[category], option => {
      if (newOption.id === option.id) {
        option.active = !option.active;
      }
    });
    this.setState({[view]: newState});
  }

  @autobind
  onReset() {
    this.props.onReset();
    this.onCloseFilters();
  }

  onResetStaticOverlays() {
    const resetState = this.props.staticOverlays;

    this.setState({staticOverlays: resetState});
  }

  @autobind
  onApply(externalView) {
    this.exportActiveFilters(this.getView());
  }

  @autobind
  onMouseLeave() {
    this.hideTimeout = window.setTimeout(() => {
      this.onCloseFilters();
    }, this.props.timeoutInterval);
  }

  @autobind
  onMouseEnter() {
    window.clearTimeout(this.hideTimeout);
  }

  onClick(view) {
    const shouldOpenFilters = view === 'filters' && !this.state.showFilters;
    const shouldOpenOverlays = view === 'overlays' && !this.state.showOverlays;

    this.setState({
      showFilters: shouldOpenFilters,
      showOverlays: shouldOpenOverlays,
      showStaticOverlays: !(shouldOpenFilters || shouldOpenOverlays),
    });
  }

  exportActiveFilters(view) {
    if (this.props.formatData) {
      this.props.onApply(this.props.formatData(this.state[view]));
    } else {
      this.props.onApply(this.formatData(this.state[view]));
    }
  }

  getIndex(list, id) {
    return  _.findIndex(list.options, (itm) => itm.id === id);
  }

  getAllIndex(list) {
    return  _.findIndex(list.options, (itm) => itm.name === this.props.allKey);
  }

  getActiveFilters(idxType) {
    const view = this.getView();
    const result = _.filter(this.state[view][idxType].options, (filter, key) => {
      if (filter.active) {
        return filter;
      }
    });
    return result;
  }

  getView() {
    let result = 'filters';

    if (this.state.showOverlays) {
      result = 'overlays';
    } else if (this.state.showStaticOverlays) {
      result = 'staticOverlays';
    }
    return result;
  }

  getFormattedValue(filter) {
    let value;

    switch(filter.type) {
      case 'checkbox':
        value =  _.filter(filter.options, (itm) => itm.active === true);
        break;
      case 'input':
        value = filter.value;
        break;
      case 'dropdown':
        value = filter.value;
        break;
      case 'datepicker':
        value = filter.value;
        break;
      case 'timepicker':
        value = filter.value;
        break;
      case 'crimepicker':
        value = _.chain(filter.options)
                 .values()
                 .flatten()
                 .filter((itm) => itm.active)
                 .value();
        break;
      default:
        value =  _.filter(filter.options, (itm) => itm.active === true);
        break;
    }
    return value;
  }

  @autobind
  chooseFilterToBuild(name, type, filter, showLabel) {
    let result;

    switch(type) {
      case 'checkbox':
        result = <ArmorVFCheckbox
                   name={name}
                   filters={filter.options}
                   onChange={this.onFilterChange}
                   colSize={showLabel ? 4 : 3}
                   {...filter}
                  />;
      break;
      case 'input':
        result =  <Input
                    placeholder={filter.placeholder}
                    type="text"
                    value={filter.value}
                    onChange={this.onInputChange.bind(this, name)}
                  />;
      break;
      case 'dropdown':
        result =  <ArmorVFDropdown
                    value={filter.value}
                    style={{marginLeft: '-37px', marginBottom: '5px'}}
                    onChange={this.onDropdownChange.bind(this, name)}
                    options={filter.options}
                  />;
      break;
      case 'datepicker':
        result =  <DatePicker
                    value={filter.value}
                    style={{marginLeft: '-15px'}}
                    onChange={this.onTimeChange.bind(this, name)}
                    {...filter}
                  />;
      break;
      case 'timepicker':
        result =  <TimePicker
                    value={filter.value}
                    style={{marginLeft: '-15px'}}
                    onChange={this.onTimeChange.bind(this, name)}
                    {...filter}
                  />;
      break;
      case 'crimepicker':
        result =  <ArmorVFCrimepicker
                    value={filter.value}
                    style={{marginLeft: '-37px'}}
                    onChange={this.onCrimepickerChange}
                    onDropdownChange={this.onCrimepickerDropdown}
                    {...filter}
                  />;
      break;
      default:
        result = <ArmorVFCheckbox
                  name={name}
                  filters={filter.options}
                  onChange={this.onFilterChange}
                  colSize={showLabel ? 4 : 3}
                 />;
        break;
    }
    return <div className={`${type}-inner-filter inner-filter`}>{result}</div>;
  }

  buildFilters(view, showLabel = true) {
    const result = _.map(this.state[view], (filter, idx) => {
      const { name, type } = filter;

      return (
        <Row key={idx} className="filter-row">
          <Col md={3} style={!showLabel ? {display: 'none'} : {} } className="filter-label">
            <h6>{name}</h6>
          </Col>
          <Col md={showLabel ? 9 : 12} className="filters">
            {this.chooseFilterToBuild(name, type, filter, showLabel)}
          </Col>
        </Row>
      );
    });
    return  (
      <div className="filter-container">
        {view === 'filters' ? this.buildChildFilters() : ''}
        {result}
        {(view === 'overlays' && this.props.overlayMessage) ? (
          <h4 className="overlay-message">{this.props.overlayMessage}</h4>
        ) : ''}
      </div>
    );
  }

  buildChildFilters() {
    return (
      React.Children.map(this.props.children, ((child, idx) => {
        if (child) {
          return (
            <Row key={idx} className="filter-row">
              <Col md={3} style={!child.props.showLabel ? {display: 'none'} : {} } className="filter-label">
                <h6>{child.props.name}</h6>
              </Col>
              <Col md={child.props.showLabel ? 9 : 12} className="filters">
                {React.cloneElement(child, {})}
              </Col>
            </Row>
          );
        }
      }))
    )
  }

  buildIconMenu() {
    return (
      <div Col className="armor-view-filter-icon-menu">
        <IconMenu
          iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
          anchorOrigin={{horizontal: 'left', vertical: 'top'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onChange={this.props.onIconMenuChange}
        >
          {_.map(this.props.iconMenu, (item) => {
            return (
              <MenuItem value={item.value} primaryText={item.label} />
            );
          })}
        </IconMenu>
      </div>
    );
  }

  formatData(filters) {
    const results = {};

   _.forEach(filters, (filter) => {
      results[_.camelCase(filter.name)] = this.getFormattedValue(filter);
    });

   return results;
  }

  shouldSelectAll(idxType) {
    let result;
    const view = this.getView();
    const filter = this.state[view][idxType];
    const idx = this.getAllIndex(filter);

    if (filter.options[idx].active) {
      result = filter.options.length;
    } else {
      result = filter.options.length - 1;
    }
    return this.getActiveFilters(idxType).length === result;
  }

  selectAllWithId(filter, filterName, selected) {
    const { id, name, children } = filter;
    const newState = this.state;

    _.forEach(newState, (view) => {
      _.forEach(view, (filter, index) => {
        if (_.has(filter, 'options')) {
          _.forEach(filter.options, (option) => {
            const allSelected = (filterName === filter.name && name === this.props.allKey);
            const isChild = _.includes(children, option.id);

            if (id === option.id || allSelected || isChild) {
              option.active = selected;
            }
          });
          if (name !== this.props.allKey) {
            const activeOptions = _.filter(filter.options, (itm) => itm.active === true).length;
            const allOptions = filter.options.length;
            const idxAll = this.getAllIndex(filter);

            try {
              if (allOptions - 1 === activeOptions) {
                filter.options[idxAll].active = true;
              }
              if (!selected && allOptions > activeOptions) {
                filter.options[idxAll].active = false;
              }
            } catch (exe) {
            }
          }
        }
      });
    });
    this.setState(newState, this.applyStaticFilters);
  }

  applyStaticFilters() {
    if (this.state.showStaticOverlays) {
      this.exportActiveFilters(this.getView());
    }
  }

  onCloseFilters() {
    this.setState(this.closeState);
  }

  render() {
    const filterClasses = {
      'pull-right': true,
      'active': this.state.showFilters,
    }

    const overlayClasses = {
      'pull-right': true,
      'active': this.state.showOverlays,
    }

    return (
      <div className={this.props.className}
           onMouseLeave={this.onMouseLeave}
           onMouseEnter={this.onMouseEnter}
           style={this.props.style}>
        <Row className="header">
          <Col sm={7}>
            {this.props.iconMenu ? this.buildIconMenu() : null}
            <div className="header-title">
              {this.props.title ? this.props.title : <h5>Filters</h5>}
            </div>
          </Col>
          <Col sm={5} className="buttons-col">
            <Button id="overlaysButton"
                    className={overlayClasses}
                    bsStyle="link"
                    onClick={this.onClick.bind(this, 'overlays')}>
                    <span>{this.props.overlaysButtonLabel}</span>
            </Button>
            <Button id="filtersButton"
                    className={filterClasses}
                    bsStyle="link"
                    onClick={this.onClick.bind(this, 'filters')}>
                    <span>{this.props.filtersButtonLabel}</span>
            </Button>
          </Col>
        </Row>
        <VelocityTransitionGroup enter={{animation: "slideDown"}} leave={{animation: "slideUp"}}>
          {this.state.showFilters ? (
            <div>
              {this.buildFilters('filters')}
            </div>
          ) : ''}
        </VelocityTransitionGroup>
        <VelocityTransitionGroup enter={{animation: "slideDown"}} leave={{animation: "slideUp"}}>
          {this.state.showOverlays ? this.buildFilters('overlays', false) : ''}
        </VelocityTransitionGroup>
        <div className="static-overlays">
          <VelocityTransitionGroup enter={{animation: "slideDown"}} leave={{animation: "slideUp"}}>
            {this.state.showStaticOverlays ? this.buildFilters('staticOverlays', false) : ''}
          </VelocityTransitionGroup>
        </div>
        {this.state.showFilters ?
          <div className="action-buttons">
            <div className="pull-right">
              <FlatButton
                onClick={this.onReset}
                className="reset-button pull-left"
                label="Reset"
              />
              <RaisedButton
                onClick={this.onApply}
                className="apply-button pull-right"
                label="Apply"
                primary
              />
          </div>
          <div style={{ clear: 'both' }}/>
        </div>
          : ''
        }
      </div>
    );
  }
}

export default ArmorViewFilter;
