import React, { PropTypes } from 'react';
import { Row, Col, Label } from 'react-bootstrap';

import Toggle from 'react-toggle';
import 'react-toggle/style.css';

const defaultProps = {
  checked: true,
};

const propTypes = {
  checked: PropTypes.bool,
  labelOn: PropTypes.string,
  labelOff: PropTypes.string,
  onChange: PropTypes.func,
};

class ArmorToggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: props.checked,
    };
    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(newProps) {
    this.setState({checked: newProps.checked});
  }

  onChange() {
    this.refs.toggle.setState({checked: this.state.checked});
    this.props.onChange(this.state.checked, this.state.checked);
  }

  render() {
    return (
      <div>
        <Row style={{width: '110px'}}>
          <Col xs={6} onClick={evt => evt.stopPropagation()}>
            <Toggle
              onChange={this.onChange}
              id="toggle"
              ref="toggle"
              defaultChecked={this.state.checked}/>
          </Col>
          <Col xs={6}>
            <Label bsStyle={this.state.checked ? 'success' : 'danger'}>
              {this.state.checked ? this.props.labelOn : this.props.labelOff}
            </Label>
          </Col>
        </Row>
      </div>
    );
  }
}

ArmorToggle.defaultProps = defaultProps;
ArmorToggle.propTypes = propTypes;

export default ArmorToggle;
