import React, { PropTypes } from 'react';
import { utils } from './utils';
import _ from 'lodash';

import { Nav, NavItem } from 'react-bootstrap';

import '../styles/ArmorViewChange';

const propTypes = {
  /**
    * List of Json objects that represent the views.
    * Structure of each object is { key: 'value',... }
  */
  views: PropTypes.array.isRequired,
  /**
    * Component title to be rendered as text to the left
  */
  title: PropTypes.string.isRequired,
  /**
    * Value of the current active view
  */
  active: PropTypes.string.isRequired,
  /**
    * Function that takes in a key event and
    * a string to be executed when a pill is selected.
  */
  onSelect: PropTypes.func.isRequired,
};

/**
  * ArmorViewChange creates a row of links for
  * the user to click on. This component's intended
  * usage is to change views within a page.
*/
class ArmorViewChange extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.buildPills = this.buildPills.bind(this);
    this.onPillSelect = this.onPillSelect.bind(this);
  }

  onPillSelect(toBeActive, event) {
    if (this.props.onSelect) {
      this.props.onSelect.call(this, event, toBeActive);
    }
  }


  buildPills() {
    return (
      <Nav id="armorViewPills"
        bsStyle="pills"
        activeKey={this.props.active}
        onSelect={this.onPillSelect}
        className="armor-pill"
        style={{ display: 'inline-block', top: '4px' }}
      >
        {_.map(this.props.views, (view, idx) => {
          return (
            <NavItem id={`${view.value} + ViewLink`}
              key={idx}
              eventKey={view.value}
              className="armor-view-item"
            >
              {utils.camelToReadable(view.value)}
            </NavItem>
          );
        })}
      </Nav>
    );
  }

  render() {
    return (
      <span>
        <span className="active-view">{this.props.title}</span>
        {this.buildPills()}
      </span>
    );
  }
}

ArmorViewChange.propTypes = propTypes;
export default ArmorViewChange;
