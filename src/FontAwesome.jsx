import React, { PropTypes } from 'react';
import _ from 'lodash';


const propTypes = {
  /*
    Nested components of rendered FontAwesome component.
  */
  children: PropTypes.any,
  /*
   Name of icon from FontAwesome library.
  */
  icon: PropTypes.oneOfType([PropTypes.object, PropTypes.string]).isRequired,
  /*
    Text to be rendered after icon.
  */
  title: PropTypes.string,
  /*
     Adds 5px right margin to icon.
  */
  padIcon: PropTypes.bool,
  /*
    Any valid font size from FontAwesome library.
    Sizes include: 2x, 3x, 4x, 5x, or lg (33% increase)
  */
  size: PropTypes.string,
  /*
    Class name to be applied to wrapper span on icon.
  */
  className: PropTypes.string,
  /*
    Style to be applied to wrappper span on icon.
  */
  style: PropTypes.object,
  /*
    Style to be applied to the FontAwesome icon
  */
  iconStyle: PropTypes.object,
};

/*
  Wrapper React component to be used for rendering FontAwesome Icons
*/
class FontAwesome extends React.Component {
  constructor(props) {
    super(props);
    // this.state = {};
  }

  render() {
    const iconStyles = _.assign(
      (this.props.padIcon) ? { marginRight: '5px' } : {},
      this.props.iconStyle || {}
    );

    return (
      <span className={this.props.className} style={this.props.style}>
          <span
            className={
              `fa fa-${this.props.icon} ${this.props.size ? `fa-${this.props.size}` : ''}`
            }
            style={iconStyles}
          >
        </span>
        <span className="fa-title">{this.props.title}</span>
        {this.props.children}
      </span>
    );
  }
}

FontAwesome.propTypes = propTypes;
export default FontAwesome;
