import React, { PropTypes } from 'react';
import { Row, Col } from 'react-bootstrap';
import { utils } from './utils';


const propTypes = {
  report: PropTypes.object,
};

class ArmorCrimeReport extends React.Component {
  constructor() {
    super();
    this.state = {
      policeReport: false,
      publicReport: false,
    };
    this.onExpandReport = this.onExpandReport.bind(this);
    this.buildLine = this.buildLine.bind(this);
    this.buildReportContainer = this.buildReportContainer.bind(this);
  }

  onExpandReport(event) {
    const key = event.target.dataset.key;

    this.setState({ [key]: !this.state[key] });
  }

  buildReport(type, reportParam) {
    let report = '';

    if (_.includes(['policeReport', 'publicReport'], type)) {
      report = reportParam;
    }
    return report;
  }

  buildReportContainer(reportKey, report) {
    if (report) {
      const isShortReport = report.length < 300;
      const shortReport = _.first(report.split(','));

      return (
        <span>
          {!this.state[reportKey] ? <span>{isShortReport ? report : shortReport}</span> : ''}
          <p style={{ display: 'inline' }}>{ (this.state[reportKey] && !isShortReport) ? this.buildReport(reportKey, report) : ''}</p>
          {!isShortReport ? <a style={{ marginLeft: '5px', cursor: 'pointer' }}
            data-key={reportKey}
            onClick={this.onExpandReport}
      >
             {this.state[reportKey] ? 'show less' : 'show more'}
          </a> : ''}
        </span>
      );
    }
  }

  buildLine(label, value) {
    let result = value;

    if (value && value.isArray) {
      result = value.join(',');
    }
    return (
      <p><strong style={{ marginRight: '5px' }}>{label}</strong>{utils.camelToReadable(result, true)}</p>
    );
  }

  render() {
    const { address, beat, building, listValue, crimeClass, crimeDescription, dateReported,
            streetName, timeReported, policeInformation, publicInformation, caseNbr } = this.props.report;

    return (
      <div>
        <Row>
          <Col md={6}>
            {this.buildLine('Case Number:', caseNbr)}
            {this.buildLine('Address:', `${address} ${streetName}`)}
            {this.buildLine('Date Reported:', dateReported)}
            {this.buildLine('Location:', building)}
            {this.buildLine('Crime Class:', crimeClass)}
            {this.buildLine('Crime Description:', crimeDescription)}
          </Col>
          <Col md={6}>
            {this.buildLine('Beat:', beat)}
            {this.buildLine('Building:', building)}
            {this.buildLine('Time Reported:', timeReported)}
            {this.buildLine('List Value:', listValue)}
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <div style={{ marginBottom: '10px' }}>
             <span><strong>Police Report:</strong> {this.buildReportContainer('policeReport', policeInformation)}</span>
            </div>
            <div>
             <span><strong>Public Report:</strong> {this.buildReportContainer('publicReport', publicInformation)}</span>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

ArmorCrimeReport.propTypes = propTypes;
export default ArmorCrimeReport;
