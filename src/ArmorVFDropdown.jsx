import React, { PropTypes } from 'react';
import _ from 'lodash';

import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

import '../styles/ArmorVFCheckbox';

import FontAwesome from './FontAwesome';

class ArmorVFDropdown extends React.Component {
  static propTypes = {
    /**
     * Initial selection value for the dropdown
     */
    value: PropTypes.any,
    /**
     * Function will fire when menu changes
     * signature: function(event, index, value)
     */
    onChange: PropTypes.func.isRequired,
    /**
     * List of properties supported by <MenuItem />
     * such as: primaryText, value etc...
     */
    options: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="armor-vf-dropdown">
        <DropDownMenu
          style={this.props.style}
          value={this.props.value}
          onChange={this.props.onChange}
        >
        {
          _.map(this.props.options,
            option => <MenuItem {...option} />)
        }
        </DropDownMenu>
        {this.props.onHelpClick ? (
          <div onClick={() => this.props.onHelpClick(this.props.name)}>
            <FontAwesome className="help-icon" icon="question-circle" />
          </div>
        ) : ''}
      </div>
    );
  }
}

export default ArmorVFDropdown;
