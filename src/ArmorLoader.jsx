import React, { PropTypes } from 'react';
import CircularProgress from 'material-ui/CircularProgress';

const propTypes = {
  loaded: PropTypes.bool.isRequired,
  style: PropTypes.object,
};

import _ from 'lodash';

const overlay = {
  position: 'absolute',
  width: '100%',
  height: '100%',
  zIndex: '99999',
  left: '0px',
};

const loaderStyle = {
  position: 'fixed',
  zIndex: '999',
  height: '2em',
  width: '2em',
  overflow: 'show',
  margin: 'auto',
  top: '0px',
  left: '0px',
  bottom: '0px',
  right: '0px',
};

class ArmorLoader extends React.Component {
  constructor() {
    super();
  }

  render() {
    const props = _.clone(this.props);

    if (props.width) {
      overlay.width = props.width;
      props.width = undefined;
    }
    const style = _.assign({ display: (!props.loaded) ? 'block' : 'none' }, overlay, this.props.style);

    return (
      <div style={style} {...props}>
        <div style={loaderStyle}><CircularProgress size={1.5} /></div>
      </div>
    );
  }
}

ArmorLoader.propTypes = propTypes;
export default ArmorLoader;
