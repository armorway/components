import React, { PropTypes } from 'react';

import { Modal } from 'react-bootstrap';

import AppActions from 'lib/actions/AppActions';

const propTypes = {
  /*
    Title to be displayed at top of modal
  */
  title: PropTypes.string,
  /**
   * Content to be displayed within the modal body
   */
  body: PropTypes.string,
};

/**
  * GenericModal is a simple modal designed to display text and a body.
  * it is designed to replace constantly making simple and cookie cutter modals.
  * Use it just like you would any modal in AppActions.toggleModal
  */
class GenericTextModal extends React.Component {
  constructor(props) {
    super(props);
    this.onClose = this.onClose.bind(this);
  }

  onClose() {
    AppActions.toggleModal({ showModal: false });
  }

  render() {
    return (
      <div>
        <Modal.Header closeButton onHide={this.onClose}>
          <Modal.Title>{this.props.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {this.props.body}
        </Modal.Body>
      </div>
    );
  }
}

GenericTextModal.propTypes = propTypes;
export default GenericTextModal;
