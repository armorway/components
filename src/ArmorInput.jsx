import React, { PropTypes } from 'react';
import _ from 'lodash';
import { Input, Tooltip, OverlayTrigger } from 'react-bootstrap';

const propTypes = {
  /**
    * Label for the input box
    * placed before the box on the left
  */
  label: PropTypes.string,
  /**
    * Message to be displayed when error occurs
  */
  errorMsg: PropTypes.string,
  /**
    * Function which takes in an event to be called
    * when a change occurs
  */
  onChange: PropTypes.func,
  /**
    * inner children of the component
  */
  children: PropTypes.object,
  /**
    * Is true when input box is valid to update,
    * Defaults at true and will not update if false
  */
  valid: PropTypes.bool,
  /**
    * Style for the input box
    * Is only used for 'error', otherwise remains undefined
  */
  bsStyle: PropTypes.string,
  /**
    * If true, input box will not display an asterisk
    * which indicates a required label.
  */
  noAsterisk: PropTypes.bool,
};

/**
  * This component represents an inputBox with
  * an optional label and error configuration
*/
class ArmorInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = _.assign({ valid: true, bsStyle: undefined }, this.props);

    this.state.label = this.formatLabel(this.state);

    this.checkValidity = this.checkValidity.bind(this);
    this.buildToolTipIfNeeded = this.buildToolTipIfNeeded.bind(this);
  }

  /*
    When the Virtual DOM rerenders, it doesnt change
    the component and since we are pushing props through,
    we must update.
  */
  componentWillReceiveProps(newProps) {
    const otherProps = _.clone(newProps);

    otherProps.label = this.formatLabel(newProps);
    this.setState(otherProps);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (nextState.valid !== this.state.valid)
           || (nextState.value !== this.state.value)
           || nextProps.bsStyle !== this.state.bsStyle
           || nextProps !== this.props;
  }

  formatLabel(state) {
    let label = '';

    try {
      if (state.required && !state.noAsterisk) {
        label = (
          <span className="input-label">{state.label} <span className="text-danger">*</span></span>
        );
      } else {
        label = <span className="input-label">{state.label}</span>;
      }
    } catch (exe) {
      /* eslint-disable no-console */
      console.log(exe);
      /* eslint-enable no-console */
    }


    return label;
  }

  checkValidity(event) {
    if (event) {
      const bsStyle = (this.state.required && !_.trim(event.target.value)) ? 'error' : undefined;

      this.setState({
        bsStyle,
        value: event.target.value,
      });
    }
    // This has to be after above code.
    // or else you will over ride the props.onChange changes
    if (this.state.onChange) {
      this.state.onChange(event);
    }
  }

  buildToolTipIfNeeded() {
    if (this.state.bsStyle === 'error' && this.state.errorMsg) {
      return (
        <Tooltip placement="top" className="in" id={this.state.errorMsg} >
          {this.state.errorMsg}
        </Tooltip>
      );
    }
    return <span></span>;
  }


  render() {
    const inputBox = (
      <Input {...this.state} onChange={this.checkValidity} className="input-box">
        {this.props.children}
      </Input>
    );

    return (
        <OverlayTrigger placement="top" overlay={this.buildToolTipIfNeeded()}>
          {inputBox}
        </OverlayTrigger>
    );
  }
}

ArmorInput.propTypes = propTypes;

export default ArmorInput;
