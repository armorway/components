import React, { PropTypes } from 'react';
import _ from 'lodash';
import ArmorFilterListItem from './ArmorFilterListItem';
import FontAwesome from './FontAwesome';
import '../styles/ArmorFilterList';
import { MenuItem, DropdownButton } from 'react-bootstrap';
const propTypes = {
  /**
   * Function to execute when a filter remove is clicked.
   */
  onFilterRemove: PropTypes.func.isRequired,
  /**
   * Array of applied filters to build into ArmorFilterListItems.
   */
  filters: PropTypes.array.isRequired,
  /**
   * Bootstrap type to apply to filter labels. Default: primary
   * Only applies to labels, not dropdown.
   */
  bsStyle: PropTypes.string,
  /**
   * class to be applied.
   */
  className: PropTypes.string,
  /**
   * Style to be applied.
   */
  style: PropTypes.object,
};

/**
 * ArmorFilterList is used to display applied filters.
 */
class ArmorFilterList extends React.Component {
  constructor() {
    super();
    this.state = {};

    this.onFilterRemoveClick = this.onFilterRemoveClick.bind(this);
  }

  onFilterRemoveClick(type, name) {
    this.props.onFilterRemove(type, name);
  }

  render() {
    return (
      <div style={this.props.style} className={`${this.props.className} filter-list`}>
        <span className="hidden-xs hidden-sm">
          {
            _.map(this.props.filters, (filter, idx) => {
              return (
                <ArmorFilterListItem
                  key={idx}
                  {...filter}
                  bsStyle={this.props.bsStyle}
                  onRemove={this.onFilterRemoveClick}
                  className="armor-filter-list-item"
                />
              );
            })
          }
        </span>
        <div className="armor-filter-dropdown hidden-md hidden-lg">
          <DropdownButton
            id="armor-filter-dropdown"
            title={`(${this.props.filters.length}) Applied Filters`}
            bsStyle={this.props.bsStyle ? this.props.bsStyle : 'default'}
          >
            {
              _.map(this.props.filters, (filter, idx) => {
                return (
                  <MenuItem
                    key={idx}
                    className="armor-filter-menu-item"
                    onSelect={() => this.onFilterRemoveClick(filter.type, filter.name) }
                  >
                  <FontAwesome icon="times" className="remove-btn" padIcon />
                  {filter.name}
                  </MenuItem>
                );
              })
            }
          </DropdownButton>
        </div>
      </div>
    );
  }
}

ArmorFilterList.propTypes = propTypes;
export default ArmorFilterList;
