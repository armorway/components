/* eslint-disable */
import _ from 'lodash';
import moment from 'moment';
import '../styles/print';
// import * as encrypt from 'services/encrypt';
const dateFormat = 'YYYY-MM-DD';
const timeFormat = 'HH-MM-SS';
const siteCode = 'upc';

class Utils {

  setLogoUrl(url) {
    this.logoUrl = url;
  }

  hashPrefix(href) {
    if (href[0] === '#') {
      return href;
    }
    return `#${href}`;
  }

  booleanToYesNo(bool) {
    if (bool) {
      return 'Yes';
    }
    return 'No';
  }

  // hashPassword(password) {
  //   return encrypt.calcMD5(password);
  // }

  filter(list, query) {
    if (!query) {
      return list;
    }
    return list.filter((item) => {
      const title = item.title.toLowerCase();
      const searchQuery = query.toLowerCase();

      return _.includes(title, searchQuery);
    });
  }

  search(searchString, dataset) {
    if (!searchString) {
      return dataset;
    }
    const lowerSearch = searchString.toLowerCase();

    return _.filter(dataset,
      (data) => _.includes(_.values(data).join('').toLowerCase(), lowerSearch));
  }

  searchImmutable(string, list) {
    return list.filter(itm => {
      return itm.valueSeq().join('').toLowerCase().includes(string.toLowerCase());
    });
  }

  getDate() {
    return moment().format('MMMM Do, YYYY');
  }

  getAMorPM(currentHour) {
    const hour = (typeof currentHour !== undefined) ? currentHour : moment().hour();

    return (hour >= 12) ? 'PM' : 'AM';
  }

  downloadAsCSV(JSONData, reportTitle) {
    if (typeof JSONData === undefined) {
      return;
    }

    const link = document.createElement('a');
    const arrData = !_.isObject(JSONData) ? JSON.parse(JSONData) : JSONData;
    let CSV = [_.keys(arrData[0]).join(',')];

    CSV = CSV.concat(_.map(arrData, (item) => _.values(item).join(','))).join('\r\n');

    link.href = `data:text/xls;charset=utf-8,${encodeURI(CSV)}`;
    link.style.visibility = 'hidden';
    link.download = `Report_${reportTitle.replace(/ /g, '_')}.csv`;

    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  createElement(type, attributes) {
    const element = document.createElement(type);

    _.forEach(attributes, (value, attribute) => {
      element[attribute] = value;
    });
    return element;
  }

  camelToReadable(str, makeCamelCase) {
    let string = str;

    if (makeCamelCase) {
      string = _.camelCase(str);
    }
    return string.replace(/([A-Z])/g, ' $1').replace(/^./, (str2) => str2.toUpperCase());
  }

  getHotspotRadius(events, minCount, maxCount) {
    const count = events.length;
    const min = 60;
    const max = 120;
    let maxSum = maxCount;

    if (maxCount === minCount) {
      maxSum = minCount + 1;
    }

    return (count - minCount)
      / (maxSum - minCount)
      * (max - min) + min;
  }

  createUniqueIds(list) {
    _.forEach(list, (item, index) => {
      if (!item.id) {
        item.id = index;
      } else {
        return;
      }
    });
  }

  containsNoFalsyValues(data) {
    return _.chain(data).values().every((val) => !!_.trim(val)).value();
  }

  generateHash() {
    const min = 65; // a
    const max = 90; // z
    const numbers = _.map(new Array(10), () => parseInt(Math.random() * (max - min) + min, 10));

    return String.fromCharCode.apply(this, numbers);
  }

  toQueryString(obj) {
    return _.map(obj, (value, key) => [key, value].join('=')).join('&');
  }

  getActiveView() {
    return location.hash.split('/').pop().split('?')[0];
  }

  toReadableList(list) {
    return _.map(list, (item) => this.toCrimeType(item));
  }

  getPieData(list, type) {
    const result = [];

    _.forEach(list, (value, key) => {
      if (_.isArray(value)) {
        if (type === 'frequency') {
          result.push(value.length || 0);
        } else if (type === 'label' && key.length) {
          result.push(this.toCrimeType(key));
        }
      }
    });
    return result;
  }

  calculatePercentChange(curCount, prevCount) {
    let result;

    if (prevCount === 0) {
      result = 0;
    } else if (curCount === 0) {
      result = -100;
    } else {
      result = Number(Math.round(
        (
          (
            (curCount * 1.0 - prevCount * 1.0) * (100 * 1.0)) / (prevCount * 1.0)
        )
      )
      );
    }
    return result;
  }

  calculateSumOfSubarrays(lists) {
    let sum = 0;

    _.forEach(lists, (list) => {
      if (list.length) {
        sum += list.length;
      }
    });
    return sum;
  }

  getNow() {
    return moment().format(dateFormat);
  }

  getEndTime(selection) {
    let result = '';
    const now = moment();

    switch (selection) {
      case '7Days':
        result = now.subtract(1, 'weeks'); break;
      case '14Days':
        result = now.subtract(2, 'weeks'); break;
      case '1Month':
        result = now.subtract(1, 'months'); break;
      case '1Year':
        result = now.subtract(1, 'years'); break;
      default:
        result = now.subtract(1, 'weeks');
    }


    return result.format(dateFormat);
  }

  buildTrendIcon(trendValue, positiveTrend, negativeTrend, neutralTrend) {
    let icon;

    if (trendValue === 0) {
      return neutralTrend;
    } else if (trendValue > 0) {
      icon = positiveTrend;
    } else {
      icon = negativeTrend;
    }
    return icon;
  }

  formatTime(time) {
    return time.split(':').splice(0, 2).join(':') || [];
  }

  formatForDropdown(data) {
    function ddMap(val) {
      return { label: val, value: val };
    }

    return _.chain(data)
      .map(ddMap)
      .value();
  }

  createQuery(end, start, beats, watch, crimeType) {
    return this.toQueryString({
      crimeType,
      beats,
      watch,
      startDate: start,
      endDate: end,
    });
  }

  createSimpleQuery(end, start, watch) {
    return this.toQueryString({
      watch,
      startDate: start,
      endDate: end,
    });
  }

  createDateQuery(end, start) {
    return this.toQueryString({
      startDate: start,
      endDate: end,
    });
  }

  calcTrend(current, prev, prev2) {
    return current - Math.min(current, prev, prev2);
  }

  calcVariance(current, prev) {
    let result = 0;

    if (current !== 0) {
      result = Math.floor(((current - prev) / current) * 100);
    }
    return result;
  }

  toCrimeType(key) {
    return key;
  }

  fromCrimeType(str) {
    return str;
  }

  transposeKeys(keys, targ, keyName) {
    const target = _.clone(targ);
    const sourceData = target[keyName];

    target[keyName] = _.chain(keys)
      .map((key) => [this.camelToReadable(key.name), sourceData[key.id]])
      .zipObject()
      .value();
    return target;
  }

  getPrintHeader() {
    const header = document.createElement('div');
    const logo = new Image();
    const timeStamp = document.createElement('div');
    const optionalBody = document.createElement('div');

    timeStamp.innerHTML =
      `<h5>${moment().format('hh:mmA MM/DD/YYYY')}</h5>
     <h6>${this.getActiveView()}</h6>`;

    timeStamp.classList.add('time-stamp-print');
    header.classList.add('header-print');

    logo.setAttribute('width', '200px');
    const promi = new Promise((res, rej) => {
      logo.onload = () => {
        res(header);
      }
      logo.src = this.logoUrl;
    });



    header.appendChild(logo);
    header.appendChild(timeStamp);
    return promi;
  }

  /**
   * Expects the following options list
   * [
   *  '<h3>Filters: Beats: A, B, C</h3>',
   *  'Current view of the page',
   *  '200 Entities',
   * ]
   */
  print(selector, isMap = false, options = '') {
    return new Promise((res, rej) => {
      this.getPrintHeader()
        .then((header) => {
          let clonedNode;
          let optionsNode;
          const app = document.getElementById('app');
          const modal = document.getElementById('genericModal');
          const leaflet = document.querySelector(selector);
          const leafletParent = leaflet.parentNode;

          if (options) {
            optionsNode = document.createElement('span');
            optionsNode.classList.add('options-print');
            optionsNode.innerHTML = options;
          }

          app.style.display = 'none';
          if (isMap) {
            clonedNode = document.querySelector(selector);
            leaflet.classList.add('map-print');
          } else {
            clonedNode = document.querySelector(selector).cloneNode(true);
          }
          if (modal) {
            modal.style.display = 'none';
          }
          clonedNode.style.marginTop = '65px';
          document.body.appendChild(header);

          if (options) {
            document.body.appendChild(optionsNode);
          }

          document.body.appendChild(clonedNode);
          window.print();
          app.style.display = 'block';
          if (modal) {
            modal.style.display = 'block';
          }
          document.body.removeChild(header);
          if (isMap) {
            leafletParent.appendChild(clonedNode);
            leaflet.classList.remove('map-print');
          } else {
            document.body.removeChild(clonedNode);
          }
          document.body.removeChild(optionsNode);
          res();
      });
    });
  }

  calculateFitHeight(windowHeight, topSpacing) {
        const bottomSpacing = 10;

        return windowHeight - topSpacing - bottomSpacing;
      }

  dateToReadable(date) {
        let result = moment(date, dateFormat).format(dateFormat);

        if(_.isNumber(date)) {
          result = moment(date).format(dateFormat);
        }
    return result;
      }

  toDateQuery(date) {
        const formattedDate = date.format('MM/DD/YYYY');

        return this.toQueryString({ date: formattedDate });
      }

  trimToNumber(value) {
        const num = parseInt(value.replace(/\D/g, ''), 10);

        if(!_.isNaN(num)) {
      return num;
    } else if (_.includes([0, 1], value.length)) {
      return undefined;
    }
    return undefined;
  }

  toReadableTime(time) {
    return moment(time).format(timeFormat);
  }

  formatDuration(date) {
    const duration = moment(moment() - moment(date)).format('m');
    let label;

    switch (Number(duration)) {
      case 0: {
        label = 'Less than a minute ago';
        break;
      }
      case 1: {
        label = 'a minute go';
        break;
      }
      default: {
        label = `${duration} minutes ago`;
        break;
      }
    }

    return label;
  }

  immutableToJS(data) {
    let result = data;

    if (data instanceof Immutable.List || data instanceof Immutable.Map) {
      result = data.toJS();
    }
    return result;
  }

  getTimezoneOffset() {
    return -(new Date().getTimezoneOffset());
  }

 formatDate(rawDate, format = 'MMM DD, YYYY hh:mm a', offset = false) {
    let result = moment(parseInt(rawDate))

    if (offset) {
      result.utcOffset(this.getTimezoneOffset());
    }
    return result.format(format);
  }
}

const inst = new Utils();

export default Utils;

export { dateFormat, siteCode, inst as utils };
