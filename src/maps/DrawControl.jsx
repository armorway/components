import { PropTypes } from 'react';
import Leaflet from 'leaflet';
import { MapControl } from 'react-leaflet';
import 'leaflet-draw';

class DrawControl extends MapControl {
  static propTypes = {
    /**
     * Function called when a layer gets created, arguments: event
     */
    onCreate: PropTypes.func,
    /**
     * Function called when a layer gets edited, arguments: event
     */
    onEdit: PropTypes.func,
    /**
     * Function called when a layer gets deleted, arguments: event
     */
    onDelete: PropTypes.func,
  };

  componentWillMount() {
    const { center, map: _map, layerContainer: _lc, radius, ...props } = this.props;

    const featureGroup = new Leaflet.FeatureGroup();

    _map.addLayer(featureGroup);

    this.leafletElement = new Leaflet.Control.Draw({
      position: this.props.position || 'topright',
      edit: this.props.edit || { featureGroup },
      draw: this.props.draw || {},
    });
    _map.on('draw:created', (evt) => {
      featureGroup.addLayer(evt.layer);
      if (this.props.onCreate) {
        this.props.onCreate(evt);
      }
    });
    if (this.props.onEdit) {
        _map.on('draw:edited', this.props.onEdit);
    }
    if (this.props.onDelete) {
        _map.on('draw:deleted', this.props.onDelete);
    }
  }
}

export default DrawControl;
