import React, { PropTypes } from 'react';
import ReactTransitionGroup from 'react-addons-css-transition-group';
import { Row, Col } from 'react-bootstrap';
import Immutable from 'immutable';
import {
  Table, TableBody, TableHeader, TableHeaderColumn,
  TableRow, TableRowColumn, TableFooter,
  } from 'material-ui/Table';
import IncidentRow from './ArmorIncidentRow';
import SortArrow from './ArmorIncidentSortArrow';
import ArmorIncidentLongField from './ArmorIncidentLongField';
import Paper from 'material-ui/Paper';

import ArmorKeyValueRow from './ArmorKeyValueRow';
import ArmorTableFooter from './ArmorTableFooter';

const propTypes = {
  incidents: PropTypes.object.isRequired,
  printing: PropTypes.bool,
  loading: PropTypes.bool,
  onFindRelated: PropTypes.func.isRequired,
};

const defaultProps = {
  incidents: Immutable.fromJS([]),
};

const omittedKeys = [
  'mapData', 'id', 'other', 'caseNumber',
  'name', 'crimeDescription', 'reportedAt',
  'policeInformation', 'publicInformation', 'narrative',
];

const headers = [
  {
    name: 'caseNumber',
    displayName: 'Case Number',
    size: '1',
  },
  {
    name: 'address',
    displayName: 'Address',
    size: '2',
  },
  {
    name: 'building',
    displayName: 'Building',
    size: '2',
  },
  {
    name: 'campus',
    displayName: 'Beat',
    size: '1',
  },
  {
    name: 'crimeDescription',
    displayName: 'Type',
    size: '1',
  },
  {
    name: 'occurredFrom',
    displayName: 'Date/Time',
    size: '1',
  },
  {
    name: undefined,
    displayName: undefined,
    size: '1',
  },
];

const styles = {
  container: {
    margin: '0',
  },
  row: {
    margin: '30px auto',
    textAlign: 'center',
    minWidth: '600px',
  },
  name: {
    marginRight: '60px',
    marginBottom: '5px',
    fontWeight: '500',
  },
  caseNumber: {
    marginRight: '5px',
    fontWeight: '500',
  },
  selected: {
    background: 'rgba(224, 224, 224, .2)',
  },
};

const pageSize = 20;

class IncidentsTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedCaseNumber: undefined,
      sortBy: 'caseNumber',
      sortAsc: true,
      page: 0,
    };

    this.onRowSelection = this.onRowSelection.bind(this);
    this.onSearchRelated = this.onSearchRelated.bind(this);
    this.onChevronClick = this.onChevronClick.bind(this);
    this.onHeaderClick = this.onHeaderClick.bind(this);
    this.onPageChange = this.onPageChange.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !_.isEqual(nextProps, this.props) || !_.isEqual(nextState, this.state);
  }

  onSearchRelated(incident) {
    this.props.onFindRelated(incident);
  }

  onRowSelection(evt) {
    this.setState({
      selectedCaseNumber: evt.currentTarget.parentElement.dataset.cn,
    });
  }

  onChevronClick(evt, selectedCaseNumber) {
    if (selectedCaseNumber === this.state.selectedCaseNumber) {
      evt.stopPropagation();
      this.setState({ selectedCaseNumber: undefined });
    }
  }

  onHeaderClick(evt) {
    const { name: newSort } = evt.target.dataset;
    const { sortBy: currentSort, sortAsc } = this.state;

    if (newSort) {
      if (currentSort === newSort) {
        this.setState({
          sortBy: newSort,
          sortAsc: !sortAsc,
        });
      } else {
        this.setState({
          sortBy: newSort,
          sortAsc: true,
        });
      }
    }
  }


  onPageChange(page) {
    this.setState({ page });
  }

  buildHeaders() {
    return _.map(headers, header => (
      <TableHeaderColumn colSpan={header.size} data-name={header.name}>
        {header.displayName}
        <SortArrow
          show={this.state.sortBy === header.name}
          sortAsc={this.state.sortAsc}
        />
      </TableHeaderColumn>
    ));
  }

  buildIncidentRows() {
    const doesInclude = _.partial(_.includes, omittedKeys);
    const { sortAsc, sortBy } = this.state;

    // eslint-disable-next-line id-length
    let incidents = this.props.incidents.sort((a, b) => {
      return a.get(sortBy).localeCompare(b.get(sortBy));
    });

    if (!sortAsc) {
      incidents = incidents.reverse();
    }
    return incidents.slice(this.state.page * pageSize, (this.state.page + 1) * pageSize).map((incident, idx) => {
      const selected =
        this.state.selectedCaseNumber === incident.get('caseNumber') || (this.props.printing);

      const row = [(
        <IncidentRow
          incident={incident}
          userSelected={selected}
          idx={idx}
          onRowSelection={this.onRowSelection}
          onChevronClick={this.onChevronClick}
          printing={this.props.printing}
        />
      )];


      if (selected) {
        row.push(
          <TableRow style={styles.selected}>
            <TableRowColumn
              colSpan={12}
              style={{ whiteSpace: 'initial' }}
            >
              <ReactTransitionGroup
                transitionName="incident-detail-row-transition"
                transitionAppear
                component="div"
                style={styles.row}
              >
                <Row style={{ paddingBottom: '10px' }}>
                {
                  incident
                  .filter((val, key) => !doesInclude(key))
                  .map((val, key) => (
                    <Col xs={12} md={6}>
                      <ArmorKeyValueRow
                        value={val}
                        formatValues={false}
                        formatKeys={false}
                        keyData={_.words(key).map(_.upperFirst).join(' ')}
                        smallKey
                      />
                    </Col>
                  ))
                }
                <ArmorIncidentLongField
                  name="Police Information"
                  text={incident.get('policeInformation')}
                  printing={this.props.printing}
                />
                <ArmorIncidentLongField
                  name="Public Information"
                  text={incident.get('publicInformation')}
                  printing={this.props.printing}
                />
                <ArmorIncidentLongField
                  name="Narrative"
                  text={incident.get('narrative')}
                  printing={this.props.printing}
                />
              </Row>
              </ReactTransitionGroup>
            </TableRowColumn>
          </TableRow>
        );
      }

      return row;
    });
  }


  render() {
    const { incidents } = this.props;

    if (this.props.loading) {
      return (
        <h3>loading</h3>
      );
    }

    return (
      <Row style={styles.container}>
        <Col xs={12}>
          <Paper zDepth={1}>
            <Table selectable={false}>
              <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
                <TableRow onClick={this.onHeaderClick}>
                  {this.buildHeaders()}
                </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={false} showRowHover>

                {
                  (incidents.size) ? this.buildIncidentRows() : (
                    <TableRow className="no-results">
                      <TableRowColumn>
                        <h4>No incidents available</h4>
                      </TableRowColumn>
                    </TableRow>
                  )
                }
              </TableBody>
              <TableFooter>
                <ArmorTableFooter
                  pageSize={pageSize}
                  onChange={this.onPageChange}
                  numPages={Math.ceil(this.props.incidents.size / pageSize)}
                />
              </TableFooter>
            </Table>
          </Paper>
        </Col>
      </Row>
    );
  }
}

IncidentsTable.defaultProps = defaultProps;
IncidentsTable.propTypes = propTypes;
export default IncidentsTable;
