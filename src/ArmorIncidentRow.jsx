import React, { PropTypes } from 'react';
import { TableRow, TableRowColumn } from 'material-ui/Table';
import IconButton from 'material-ui/IconButton';
import KeyboardArrowDown from 'material-ui/svg-icons/hardware/keyboard-arrow-down';

const propTypes = {
  incident: PropTypes.object,
  userSelected: PropTypes.bool,
  idx: PropTypes.number,
  onRowSelection: PropTypes.func,
  onChevronClick: PropTypes.func,
  printing: PropTypes.bool,
};

const styles = {
  rotate: {
    transform: 'rotate(180deg)',
  },
  selected: {
    background: 'rgba(224, 224, 224, .2)',
    borderBottom: '0px solid transparent',
  },
  caseStyle: {
    textOverflow: 'initial',
  },
};

class IncidentRow extends React.Component {
  constructor(props) {
    super(props);

    this.onChevronClick = this.onChevronClick.bind(this);
  }

  shouldComponentUpdate(nextProps) {
    return (
      nextProps.incident !== this.props.incident ||
      nextProps.userSelected !== this.props.userSelected
    );
  }

  onChevronClick(evt) {
    this.props.onChevronClick(evt, this.props.incident.get('caseNumber'));
  }

  render() {
    return (
      <TableRow
        {...this.props}
        data-cn={this.props.incident.get('caseNumber')}
        onRowClick={this.props.onRowSelection}
        style={(this.props.userSelected) ? styles.selected : {}}
      >
        <TableRowColumn style={styles.caseStyle}>{this.props.incident.get('caseNumber')}</TableRowColumn>
        <TableRowColumn colSpan={2}>{ _.replace(_.startCase(_.toLower(this.props.incident.get('address'))), new RegExp(' Los Angeles Ca', 'g'), ', LA, CA')}</TableRowColumn>
        <TableRowColumn colSpan={2}>{ _.startCase(_.toLower(this.props.incident.get('building')))}</TableRowColumn>
        <TableRowColumn colSpan={1}>{_.replace(_.startCase(_.toLower(this.props.incident.get('campus'))), new RegExp('List Upc Beat', 'g'), '')}</TableRowColumn>
        <TableRowColumn colSpan={1} style={{ padding: '0px' }}>{this.props.incident.get('crimeDescription')}</TableRowColumn>
        <TableRowColumn colSpan={1} style={{ padding: '0px' }}>{_.replace(this.props.incident.get('reportedAt'), new RegExp('2018', 'g'), '18')}</TableRowColumn>
        <TableRowColumn>
         { (!this.props.printing) ? (
          <IconButton
            style={(this.props.userSelected) ? styles.rotate : {}}
            onClick={this.onChevronClick}
          >
            <KeyboardArrowDown />
          </IconButton>
         ) : ''}

        </TableRowColumn>
      </TableRow>
    );
  }
}

IncidentRow.propTypes = propTypes;
export default IncidentRow;
