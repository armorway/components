import React, { PropTypes } from 'react';

import { Card, CardText, CardTitle } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

const propTypes = {
  title: PropTypes.any,
  subtitle: PropTypes.any,
  children: PropTypes.any,
  onClose: PropTypes.func.isRequired,
  className: PropTypes.string,
};

class ArmorCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <Card className={`${this.props.className || ''} marker-card`} zDepth={2}>
          <FlatButton
            label="x"
            className="close-camera"
            onClick={this.props.onClose}
          />
          <CardTitle title={this.props.title} subtitle={this.props.subtitle} />
          <CardText>
            {this.props.children}
          </CardText>
        </Card>
      </div>
    );
  }
}
ArmorCard.propTypes = propTypes;
export default ArmorCard;
