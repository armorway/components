import React, { PropTypes } from 'react';
import classNames from 'classnames';
import { Row, Col } from 'react-bootstrap';
import autobind from 'autobind-decorator';
import Checkbox from 'material-ui/Checkbox';
import FontAwesome from './FontAwesome';
import _ from 'lodash';

import '../styles/ArmorVFCheckbox';

const propTypes = {
  /** List of objects with id (String), name (String), active (boolean)
      to generate checkboxes from.
   * ```
   * [
   *   {
   *     id: 'all',
   *     name: 'All',
   *     active: false,
   *   },
   *   {
   *     id: 'a',
   *     name: 'A',
   *     active: true,
   *     icon: undefined | <h5>joe</h5> | <img src="" />
   *   },
   *   {
   *     id: 'b',
   *     name: 'B',
   *     active: false,
   *   },
   * ]
   *```
   */
  filters: PropTypes.array.isRequired,
  /** Callback function returning the results (name, id) of filter clicked.
   */
  onChange: PropTypes.func.isRequired,
  /** Name of the filter that will be returned when onChange is fired.
   */
  name: PropTypes.string.isRequired,
  /**
   * Column size for each checkbox
  */
  colSize: PropTypes.number,
  /** Object representing custom styles.
   */
  styles: PropTypes.object,
  /**
   * Function which will return the name of this filter, when a help tip is clicked on
   */
  onHelpClick: PropTypes.func,
  /**
   * Optional switch to stop filter from rendering
   */
  invalidFilter: PropTypes.bool,
  /**
   * Error Message if fitler is invalid
   */
  errorMessage: PropTypes.string,
  /**
   * Styles
   */
  style: PropTypes.object,
};

const defaultProps = {
  colSize: 4,
};

class ArmorVFCheckbox extends React.Component {
  constructor(props) {
    super(props);
  }

  buildFilter() {
    return _.map(this.props.filters, (filter, idx) => {
      const filterLength = filter.name.length;
      const classes = classNames({
        'small-filter-box': filterLength >= 10,
        'medium-filter-box': filterLength > 7 && filterLength < 10,
      });
      const label = filter.icon
       ? <span className="option-label"><img className="option-icon" src={filter.icon} /> {filter.name}</span>
       : <span className="option-label">{filter.name}</span>;

      return (
        <Col xs={this.props.colSize} key={idx} className={classes}>
          <Checkbox
            className="armor-vf-input"
            label={label}
            iconStyle={{ marginRight: '3px' }}
            checked={filter.active}
            onCheck={() => this.props.onChange(this.props.name, filter) }
          />
        </Col>
      );
    });
  }

  render() {
    return (
      <div className="armor-vf-checkbox" style={this.props.style}>
        <Row>
          {this.props.invalidFilter ? <p className="error-message">{this.props.errorMessage}</p> : this.buildFilter()}
        </Row>
        {this.props.onHelpClick ? (
          <div onClick={() => this.props.onHelpClick(this.props.name)}>
            <FontAwesome className="help-icon" icon="question-circle" />
          </div>
        ) : ''}
      </div>
    );
  }
}

ArmorVFCheckbox.defaultProps = defaultProps;
ArmorVFCheckbox.propTypes = propTypes;
export default ArmorVFCheckbox;
