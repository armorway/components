import React, { PropTypes } from 'react';
import _ from 'lodash';
import moment from 'moment';
import ArmorVFDropdown from './ArmorVFDropdown';
import autobind from 'autobind-decorator';
import DatePicker from 'material-ui/DatePicker';
import { Row, Col } from 'react-bootstrap';

import '../styles/ArmorVFDatespan';

class ArmorVFDatespan extends React.Component {
  static propTypes = {
    /**
     * Initial selection value for the dropdown
     */
    value: PropTypes.any,
    /**
     * Function will fire when menu changes
     * signature: function(event, index, value)
     */
    onChange: PropTypes.func.isRequired,
    /**
     * List of properties supported by <MenuItem />
     * such as: primaryText, value etc...
     */
    options: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);
    this.allDateSpans = [
      {
        primaryText: 'Last 7 Days',
        value: '7 days',
      },
      {
        primaryText: 'Last 14 Days',
        value: '14 days',
      },
      {
        primaryText: 'Last Month',
        value: '1 months',
      },
      {
        primaryText: 'Last Year',
        value: '1 years',
      },
      {
        primaryText: 'Custom',
        value: 'custom',
      },
    ];
    this.state = {
      datespanValue: this.props.datespanValue,
      startDate: this.props.value.startDate,
      endDate: this.props.value.endDate,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      datespanValue: nextProps.datespanValue,
      startDate: nextProps.value.startDate,
      endDate: nextProps.value.endDate,
    })
  }

  @autobind
  onSpanChange(evt, index, value) {
    if (value !== 'custom') {
      const [ amount, unit ] = value.split(' ');

      this.setState({
        datespanValue: value,
        startDate: moment().subtract(amount, unit),
        endDate: moment(),
      }, () => {
        this.exportValue();
      });
    }
  }

  onDateChange(filterName, index, datePicked) {
    console.log(filterName, index, datePicked);

    this.setState({ datespanValue: 'custom', [filterName]: moment(datePicked) }, () => {
      this.exportValue();
    });
  }

  exportValue() {
    const { startDate, endDate, datespanValue } = this.state;

    this.props.onChange(this.props.name, {startDate, endDate});
    this.props.onDatespanChange(datespanValue);
  }

  render() {
    return (
      <div className="armor-vf-dropdown">
        <Row>
          <Col md={12}>
          <ArmorVFDropdown
            value={this.state.datespanValue}
            onChange={this.onSpanChange}
            options={this.allDateSpans}
          />
          </Col>
          <Col md={6}>
          <DatePicker
            value={this.state.startDate.toDate()}
            onChange={this.onDateChange.bind(this, 'startDate')}
          />
          </Col>
          <Col md={6} className="end-date">
            <DatePicker
              value={this.state.endDate.toDate()}
              onChange={this.onDateChange.bind(this, 'endDate')}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

export default ArmorVFDatespan;
