import React, { PropTypes } from 'react';
import ArmorFilterList from './ArmorFilterList';
import FontAwesome from './FontAwesome';

import { Col, Input, Row } from 'react-bootstrap';
import '../styles/ArmorSearchAndFilter';

const propTypes = {
  /**
   * Function to execute when a filter remove is clicked.
   */
  onFilterRemove: PropTypes.func.isRequired,
  /**
   * Function to execute when a search field is typed in.
   */
  onSearch: PropTypes.func.isRequired,
  /**
   * Array of applied filters to build into ArmorFilterListItems.
   */
  filters: PropTypes.array.isRequired,
  /**
   * Bootstrap type to apply to filter labels. Default: primary
   */
  bsStyle: PropTypes.string,
  /**
   * class to be applied.
   */
  className: PropTypes.string,
  /**
   * Style to be applied.
   */
  style: PropTypes.object,
};

/**
 * ArmorFilterList is used to display applied filters.
 */
class ArmorSearchAndFilter extends React.Component {
  constructor() {
    super();
    this.state = {
      show: false,
    };

    this.onFilterRemoveClick = this.onFilterRemoveClick.bind(this);
    this.onSearch = this.onSearch.bind(this);
  }

  onFilterRemoveClick(type, name) {
    this.props.onFilterRemove(type, name);
  }

  onSearch(evt) {
    this.props.onSearch(evt.target.value);
  }

  render() {
    return (
      <Row style={this.props.style} className={`${this.props.className} search-filter-list`}>
        <Col md={4} xs={6}>
          <Input
            type="text"
            placeholder="Search"
            addonBefore={<FontAwesome icon="search" />}
            onChange={this.onSearch}
            className="armor-filter-search-field"
          />
        </Col>
        <Col md={8} xs={6}>
          <ArmorFilterList
            filters={this.props.filters}
            bsStyle={this.props.bsStyle}
            onFilterRemove={this.props.onFilterRemove}
          />
        </Col>
      </Row>
    );
  }
}

ArmorSearchAndFilter.propTypes = propTypes;
export default ArmorSearchAndFilter;
