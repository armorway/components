import React from 'react';
import IconButton from 'material-ui/IconButton';
import FontAwesome from './FontAwesome';
import '../styles/ArmorScrollToTop.scss';

class ArmorScrollToTop extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showTopButton: !!window.pageYOffset,
    };

    this.onScroll = this.onScroll.bind(this);
    this.onBackToTopClick = this.onBackToTopClick.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.onScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll);
  }

  onScroll() {
    this.setState({ showTopButton: !!window.pageYOffset });
  }

  onBackToTopClick() {
    window.scrollTo(0, 0);
  }

  render() {
    if (!this.state.showTopButton) {
      return <span />;
    }
    return (
      <IconButton
        className="armor-back-to-top"
        onClick={this.onBackToTopClick}
        iconClassName="fa-chevron-circle-up fa"
        tooltip="Return to Top"
        tooltipPosition="top-left"
      />

    );
  }
}

export default ArmorScrollToTop;
