import React, { PropTypes } from 'react';
import { TableRow, TableRowColumn } from 'material-ui/Table';
import IconButton from 'material-ui/IconButton';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import ArrowForward from 'material-ui/svg-icons/navigation/arrow-forward';

const propTypes = {
  onChange: PropTypes.func.isRequired,
  pageSize: PropTypes.number.isRequired,
  numPages: PropTypes.number.isRequired,
};

const styles = {
  pageText: {
    position: 'relative',
    top: '-8px',
    margin: '10px',
  },
};

class ArmorTableFooter extends React.Component {
  constructor() {
    super();

    this.state = {
      page: 0,
    };

    this.onPageClick = this.onPageClick.bind(this);
    this.onNavigate = this.onNavigate.bind(this);
  }

  onPageClick() {
    this.setState({ page: (1 + this.state.page) % (this.props.numPages) }, () => {
      this.props.onChange(this.state.page);
    });
  }

  onNavigate(evt) {
    const amount = parseInt(evt.currentTarget.dataset.navigate, 10);
    const page = (amount + this.props.numPages + this.state.page) % (this.props.numPages);

    this.setState({ page }, () => {
      this.props.onChange(this.state.page);
    });
  }

  render() {
    if (this.props.numPages <= 1) {
      return null;
    }

    return (
      <TableRow style={{ float: 'right' }}>
        <TableRowColumn>
          <IconButton
            data-navigate="-1"
            onClick={this.onNavigate}
          >
            <ArrowBack />
          </IconButton>
          <span style={styles.pageText}>{this.state.page + 1} of {this.props.numPages}</span>
          <IconButton
            data-navigate="1"
            onClick={this.onNavigate}
          >
            <ArrowForward />
          </IconButton>
        </TableRowColumn>
      </TableRow>
    );
  }
}

ArmorTableFooter.propTypes = propTypes;
export default ArmorTableFooter;

