import React, { PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Pagination, ListGroup, Button } from 'react-bootstrap';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import ArmorListItem from './ArmorListItem';
import FontAwesome from './FontAwesome';
import _ from 'lodash';
import { utils } from './utils';

import '../styles/ArmorList.scss';

const defaultProps = {
  resetPageOnNewData: true,
};

const propTypes = {
  /**
   * Each item in this array will be a row in the list.
   * The named keys below are all 'special' in that they
   * each have a unique useage in ArmorListItem.
   * Every other key is displayed as a ArmorKeyValueRow.
   * ```
   *  {
   *  id: String | Number,
   *  img: String | Element,
   *  dateTime: String
   *  name: String,
   *  ... rest of data
   *  }
   * ```
   */
  data: PropTypes.array.isRequired,
  /**
   * Name for list when printing/saving.
   */
  name: PropTypes.string,
  /**
   * Value for ArmorTabbedList to key in on
   */
  value: PropTypes.string,
  /**
   * Function to execute when a list item is clicked.
   */
  onClick: PropTypes.func.isRequired,
  /**
   * Function to execute when the close button is clicked
   */
  onClear: PropTypes.func,
  /**
   * Id of currently active/selected list item.
   */
  selected: PropTypes.any,
  /**
   * Makes row expanded and permanenmt active.
   */
  allActive: PropTypes.bool,
  /**
   * Key to sort on by default.
   * If not present, sort is on dateTime
   */
  defaultSortKey: PropTypes.string,
  /**
   * Direction to sort on by default.
   * If not present, sort is ascending
   */
  defaultSortAscending: PropTypes.bool,
  /**
   * Keys matching data to sort on
   */
  sortKeys: PropTypes.array,
  /**
   * Keys in data object to not visually show. Still sortable.
   */
  /**
   * Display names of the sortKeys list, must be same length as sortKeys
   */
  sortKeyDisplayNames: PropTypes.array,
  hideKeys: PropTypes.array,
  /**
   * Array of two actions in an array to be displayed
   * at the bottom of the component.
   * ```
   * {
   *  name: String
   *  action: Function
   * }
   * ```
   */
  actions: PropTypes.array,
  /**
   * Array in the format used for a ArmorFab Component
   */
  fabItems: PropTypes.array,
  /**
   * Function to be executed before printing
   */
  onPrint: PropTypes.func,
  /**
   * Function to be executed before saving
   */
  onSave: PropTypes.func,
  /**
   * class to be applied.
   */
  className: PropTypes.string,
  /**
   * Default sort key to replace dateTime
   */
  defaultKey: PropTypes.string,
  /**
   * Sets size for ArmorKeyValueRow
   * use in ArmorListItem
   */
  smallKey: PropTypes.string,
  /**
   * Style to be applied.
   */
  style: PropTypes.object,
  /**
   * Boolean which will format keys in material design if true
   */
  formatKeys: PropTypes.bool,
  /**
   * Boolean which will format values in material design if true
   */
  formatValues: PropTypes.bool,
  /**
   * Number which specifies how many items per page.
   * If not specified, all will render.
   */
  pageSize: PropTypes.number,
  /**
   * Removes Datetime from the sort list.
   */
  noDateTimeSort: PropTypes.bool,
  /**
   * Stops ArmorList from going to page 1 when new data comes in.
   */
  resetPageOnNewData: PropTypes.bool,
};

/**
 * Creates a list of for displaying data. Also supports actions.
 */
class ArmorList extends React.Component {
  constructor(props) {
    super(props);

    const ascending =
      (!_.isUndefined(props.defaultSortAscending)) ? props.defaultSortAscending : true;
    let sortKey =
      (!_.isUndefined(props.defaultSortKey)) ? props.defaultSortKey : this.getDefaultKey();

    if (this.props.noDateTimeSort) {
      sortKey = this.props.sortKeys[0];
    }

    const sortedData = this.sort(props.data, sortKey, ascending);
    let currentPage = 1;

    if (props.pageSize && props.selected) {
      const index = _.findIndex(sortedData, (dataItem) => dataItem.id === props.selected) + 1;

      currentPage = Math.ceil(index / props.pageSize);
    }

    this.state = {
      print: false,
      sortMethod: sortKey,
      currentPage,
      sortedData,
      ascending,
    };

    this.onClick = this.onClick.bind(this);
    this.buildSortOptions = this.buildSortOptions.bind(this);
    this.onSortOptionSelect = this.onSortOptionSelect.bind(this);
    this.getDefaultKey = this.getDefaultKey.bind(this);
    this.onPaginationSelect = this.onPaginationSelect.bind(this);
    this.onPrint = this.onPrint.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const newState = {};

    if (!_.isEqual(this.props.data, nextProps.data)) {
      const { sortMethod, ascending } = this.state;
      const sortedData = this.sort(nextProps.data, sortMethod, ascending);

      newState.sortedData = sortedData;
      // reset page because data is new

      const currentData = _.map(this.props.data, (data) => _.omit(data, 'img'));
      const nextData = _.map(nextProps.data, (data) => _.omit(data, 'img'));

      if (!_.isEqual(currentData, nextData)) {
        newState.currentPage = 1;
      }
    }
    // change page to newly selected list item
    if ((nextProps.selected !== this.props.selected || !this.props.resetPageOnNewData)
        && nextProps.selected && nextProps.pageSize) {
      const data = newState.sortedData || this.state.sortedData;
      const index = _.findIndex(data, (dataItem) => dataItem.id === nextProps.selected) + 1;

      newState.currentPage = Math.ceil(index / nextProps.pageSize);
    }

    if (!newState.currentPage || newState.currentPage <= 0) {
      newState.currentPage = 1;
    }

    if (!_.isEmpty(newState)) {
      this.setState(newState);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.value !== prevProps.value) {
      const sortMethod =
        (!_.isUndefined(this.props.defaultSortKey))
        ? this.props.defaultSortKey : this.getDefaultKey();
      const ascending =
        (!_.isUndefined(prevProps.defaultSortAscending))
          ? prevProps.defaultSortAscending : true;
      const sortedData = this.sort(prevProps.data, sortMethod, ascending);

      // eslint-disable-next-line
      this.setState({
        ascending,
        sortMethod,
        sortedData,
      });
    }
  }

  onClick(...args) {
    this.props.onClick(...args);
  }

  onPrint() {
    // set view into print mode to expand all elements
    this.setState({ print: true }, () => {
      utils.print('.armor-list-print', false, `<p>${this.props.name || ''}</p>`)
        .then(() => {
          // revert print view
          this.setState({ print: false });
        });
    });
  }

  onSave() {
    const data = this.props.onPrint(this.props.data) || this.props.data;
    const formattedData = _.map(data, (dat) => _.omit(dat, 'img'));

    utils.downloadAsCSV(formattedData, this.props.name);
  }

  onPaginationSelect(event, eventData) {
    this.setState({ currentPage: eventData.eventKey });

    try {
      ReactDOM.findDOMNode(this.refs.listGroup).scrollTop = 0;
    } catch (exe) {
      // eslint-disable-next-line
      console.log(exe);
    }
  }

  onSortOptionSelect(idx, evt, sortMethod) {
    const ascending =
      (sortMethod === this.state.sortMethod) ? !this.state.ascending : true;
    const sortedData = this.sort(this.props.data, sortMethod, ascending);

    this.setState({
      ascending,
      sortMethod,
      sortedData,
    });
  }

  getListData() {
    const { currentPage } = this.state;
    const { pageSize } = this.props;
    const listData = this.state.sortedData;

    if (pageSize) {
      const start = (currentPage - 1) * pageSize;
      const end = ((currentPage * pageSize));

      return listData.slice(start, end);
    }
    return listData;
  }

  getDefaultKey(type) {
    if (type === 'key') {
      return this.props.defaultKey || 'dateTime';
    }
    return this.props.defaultKey || 'Date';
  }

  getHiddenKeys(dateTimeKey) {
    const defaultHiddenKeys =
      ['img', 'actions', 'name', dateTimeKey, 'id', 'className', 'style', 'customRow'];

    return defaultHiddenKeys.concat(this.props.hideKeys || []);
  }

  buildSortOptions() {
    const dateTimeKey = this.getDefaultKey();
    let options = _.cloneDeep(this.props.sortKeys || []);

    if (!this.props.noDateTimeSort) {
      options = _.concat([dateTimeKey], options);
    }

    const sortKeyDisplayNames = this.props.sortKeyDisplayNames || options;

    return (
      _.map(options, (option, idx) => {
        return (
          <MenuItem
            value={option}
            key={idx}
            className="sort-menu-item"
            primaryText={sortKeyDisplayNames[idx]}
            label={this.buildMenuItemLabel(sortKeyDisplayNames[idx])}
          />
        );
      })
    );
  }

  buildMenuItemLabel(displayName) {
    const title = (
      <span>
        <span>{`Sort: ${displayName}`}</span>
        <FontAwesome
          icon={(this.state.ascending) ? 'arrow-up' : 'arrow-down'}
          style={{ padding: '0 3px 0px 6px' }}
        />
      </span>
    );

    return title;
  }

  sort(data, method, ascending) {
    let sortedData;
    const dateTimeKey = this.getDefaultKey();

    switch (method) {
      case dateTimeKey:
        sortedData = _.sortBy(data, dateTimeKey);
        break;
      default:
        sortedData = _.sortBy(data, method);
        break;
    }
    if (!ascending) {
      sortedData = sortedData.reverse();
    }
    return sortedData;
  }

  render() {
    const { sortMethod } = this.state;
    const listData = this.getListData();

    const dateTimeKey = this.getDefaultKey('key');
    const hiddenKeys = this.getHiddenKeys(dateTimeKey);
    const printClass = this.state.print ? 'print' : '';

    return (
      <div
        className={`armor-list armor-list-print ${printClass}`}
      >
        <div className="armor-list-header">
          <div className="armor-list-sort">
              <DropDownMenu
                id="armor-list-sort-dropdown"
                className="armor-list-sort-options"
                value={sortMethod}
                onChange={this.onSortOptionSelect}
              >
                {this.buildSortOptions()}
              </DropDownMenu>
          </div>
          <div className="armor-list-print-save">
            <div>
              {
                (this.props.onPrint) ? (
                  <Button bsStyle="link" onClick={this.onPrint}>
                    <FontAwesome icon="print" />
                  </Button>
                ) : ''
              }
              {
                (this.props.onSave) ? (
                    <Button bsStyle="link" onClick={this.onSave}>
                      <FontAwesome icon="download" />
                    </Button>
                  ) : ''
              }
            </div>
          </div>
        </div>
        <div className="armor-list-and-pagination">
        <ListGroup
          ref="listGroup"
          onClick={this.onItemClicked}
          className={`${this.props.className} armor-list`}
        >
        {
            _.map(listData, (data) => {
              const trimmedData =
                _.omit(data, hiddenKeys);

              return (
                  <ArmorListItem
                    ref="listItem"
                    key={data.id}
                    img={data.img}
                    id={data.id}
                    name={data.name}
                    customRow={data.customRow}
                    actions={this.props.actions}
                    dateTime={data[dateTimeKey]}
                    data={trimmedData}
                    onClick={this.onClick}
                    fabItems={this.props.fabItems}
                    active={this.props.selected === data.id}
                    allActive={this.props.allActive || this.state.print}
                    formatKeys={this.props.formatKeys}
                    formatValues={this.props.formatValues}
                    smallKey={this.props.smallKey}
                    className={data.className}
                    style={data.style}
                    printing={this.state.print}
                    clearable={this.props.onClear !== undefined}
                    onClear={this.props.onClear}
                  />
              );
            })
        }
        </ListGroup>
        {
          (this.props.pageSize && this.state.sortedData.length) ? (
           <div className="pagination-wrapper">
             <Pagination
               prev
               next
               maxButtons={4}
               items={Math.ceil(this.props.data.length / this.props.pageSize)}
               activePage={this.state.currentPage}
               onSelect={this.onPaginationSelect}
             />
          </div>
          ) : ''
        }
        </div>
      </div>
    );
  }
}

ArmorList.defaultProps = defaultProps;
ArmorList.propTypes = propTypes;
export default ArmorList;
