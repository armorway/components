import React, { Component, PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import { utils } from './utils';
import autobind from 'autobind-decorator';

class ArmorPrintMap extends Component {
  /**
   * List of strings that may have HTML markup contained, which will
   * render into the printed page.
   */
  static propTypes = {
    options: PropTypes.arrayOf(PropTypes.string),
  };

  @autobind
  onPrint() {
    utils.print(this.props.mapId, true, this.props.options);
  }

  render() {
    return (
      <div id="armorPrintMap">
        <RaisedButton onMouseDown={this.onPrint} label="Print" primary />
      </div>
    );
  }
}

export default ArmorPrintMap;
