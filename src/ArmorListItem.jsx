import React, { PropTypes } from 'react';
import ReactDOM from 'react-dom';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import { Row, Col, Grid } from 'react-bootstrap';
import ArmorKeyValueRow from './ArmorKeyValueRow';
import ArmorFAB from './ArmorFAB';
import _ from 'lodash';
import '../styles/ArmorListItem';

const propTypes = {
  /**
   * Id for the List item, it used to know which row is clicked
   */
  id: PropTypes.any.isRequired,
  /**
   * Name to be displayed
   */
  name: PropTypes.string.isRequired,
  /**
   * DateTime of event
   */
  dateTime: PropTypes.string.isRequired,
  /**
   * Logo for type
   */
  img: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
  ]),
  /**
   * Triggered when row is clicked.
   */
  onClick: PropTypes.func.isRequired,
  /**
   * Function to be called when clear button is clicked
   */
  onClear: PropTypes.func,
  /**
   * Meta data to be displayed as rows when item is active.
   */
  data: PropTypes.object,
  /**
   * When active, the row expands and shows more data
   * and the FAB Icon.
   */
  active: PropTypes.bool,
  /**
   * Makes row expanded and permanent active.
   */
  allActive: PropTypes.bool,
  /**
   * Shows close button on selected items
   */
  clearable: PropTypes.bool,
  /**
   * Actions to be displayed at buttom of component.
   * If provided, it expects 2.
   */
  actions: PropTypes.array,
  /**
    * Array in the format used for a ArmorFab Component.
    */
  fabItems: PropTypes.array,
  /**
   * React Component to be rendered above actions
   * and below data rows
   */
  customRow: PropTypes.object,
  /**
   * class to be applied.
   */
  className: PropTypes.string,
  /**
   * Style to be applied.
   */
  style: PropTypes.object,

  /**
   * Boolean which will format keys in material design if true
   */
  formatKeys: PropTypes.bool,
  /**
   * Boolean which will format values in material design if true
   */
  formatValues: PropTypes.bool,
  /**
   * Sets size for ArmorKeyValueRow
   */
  smallKey: PropTypes.string,
  /**
   * Used to inform the component printing is happening
   */
  printing: PropTypes.bool,
};


/**
 * ArmorListItem is designed to be used as an internal component
 * to the ArmorList component. It is a list of information with
 * actions, FAB and an image.
 */
class ArmorListItem extends React.Component {
  constructor(props) {
    super(props);

    this.onItemClick = this.onItemClick.bind(this);
    this.buildDataRows = this.buildDataRows.bind(this);
    this.onActionClick = this.onActionClick.bind(this);
    this.onClearClick = this.onClearClick.bind(this);
  }

  componentDidMount() {
    if (this.props.active) {
      this.scrollIntoView();
    }
  }

  shouldComponentUpdate(nextProps) {
    return (
      nextProps.active !== this.props.active ||
      nextProps.printing !== this.props.printing ||
      nextProps.customRow !== this.props.customRow ||
      !_.isEqual(nextProps.img, this.props.img)
    );
  }

  componentDidUpdate(prevProps) {
    if (this.props.active && !prevProps.active) {
      this.scrollIntoView();
    }
  }

  onClearClick(evt) {
    evt.stopPropagation();
    this.props.onClear();
  }

  onItemClick() {
    this.props.onClick(this.props.id);
  }

  onActionClick(evt) {
    const { value: idx } = evt.currentTarget;
    const { action } = this.props.actions[idx];

    if (action) {
      action(evt, this.props.id, this.props.name, this.props.data);
    }
  }

  scrollIntoView() {
    this.refs.listItem.scrollIntoView();
  }

  buildDataRows(data) {
    const { actions, fabItems, customRow } = this.props;

    // either you have both actions or you have none.
    return (
      <span>
        {
          (this.props.fabItems && !this.props.printing) ? (
            <ArmorFAB
              items={fabItems}
              className="armor-list-item-fab"
              small
            />
          ) : ''
        }
        <Col xs={12} className="armor-list-data">
          <div className="armor-list-item-data-rows">
            {
              _.map(data, (value, rowKey) => {
                return (
                  <ArmorKeyValueRow
                    key={rowKey}
                    keyData={rowKey}
                    value={value}
                    formatKeys={this.props.formatKeys}
                    formatValues={this.props.formatValues}
                    smallKey={this.props.smallKey}
                  />
                );
              })
            }
          </div>
          {
            (customRow) ? (
              <div>
                {customRow}
              </div>
            ) : ''
          }
          {
            (actions && actions.length >= 2) ? (
              <div>
                <Col xs={6} className="btn-col">
                  <FlatButton
                    value={0}
                    className="armor-list-action"
                    onClick={this.onActionClick}
                    label={actions[0].name}
                    primary
                  />
                </Col>
                <Col xs={6} className="btn-col">
                  <FlatButton
                    value={1}
                    className="armor-list-action"
                    onClick={this.onActionClick}
                    label={actions[1].name}
                    primary
                  />
                </Col>
              </div>
            ) : ''
          }
        </Col>
      </span>
    );
  }

  buildImg() {
    const { img } = this.props;

    if (!img) {
      return '';
    }
    return (_.isString(img)) ? (
      <img src={this.props.img} className="armor-list-row-img" />
    ) : <span className="armor-list-row-img armor-list-row-element">{img}</span>;
  }

  isActive() {
    return this.props.active;
  }

  render() {
    const className = (this.props.className) ? this.props.className : '';
    const active = (this.isActive()) ? 'active' : '';
    const allActive = (this.props.allActive) ? 'all-active' : '';

    return (
      <div
        onClick={this.onItemClick}
        className={`${className} ${active} list-group-item ${allActive}`}
        ref="listItem"
        style={this.props.style}
      >
        <Grid fluid>
          {
            (this.props.clearable && active) ? (
              <IconButton
                className="close-button-container"
                iconClassName="fa fa-close close-clear"
                onClick={this.onClearClick}
                tooltipPosition="bottom-left"
                tooltip="Close Selection"
              />
            ) : ''
          }
          <Row>
            <Col xs={2} className="armor-list-item-img-col">
              {this.buildImg()}
            </Col>
            <Col xs={10}>
              <div className="armor-list-item-name">
                {this.props.name}
              </div>
              <div className="armor-list-item-date-time">
                {this.props.dateTime}
              </div>
            </Col>
          </Row>
          <Row>
           <Col xs={12}>
            <div>
              {
                (this.isActive() || this.props.allActive) ? (
                  this.buildDataRows(this.props.data)
                ) : ''
              }
            </div>
          </Col>
        </Row>
      </Grid>
    </div>);
  }
}

ArmorListItem.propTypes = propTypes;
export default ArmorListItem;
