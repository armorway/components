import React, { PropTypes } from 'react';
import _ from 'lodash';
import { Button } from 'react-bootstrap';
import ArmorFabItem from './ArmorFabItem';
import FontAwesome from './FontAwesome';
import '../styles/ArmorFAB';

const propTypes = {
  /**
   * Function to excute when FAB is clicked.
   */
  onClick: PropTypes.func,
  /**
   * Array of Items to build into ArmorFABItems.
   */
  items: PropTypes.array.isRequired,
  /**
   * Bootstrap type of main FAB Icon. Default: primary
   */
  bsStyle: PropTypes.string,
  /**
   * Designates size of FAB Icon.
   */
  small: PropTypes.bool,
  /**
   * class to be applied.
   */

  className: PropTypes.string,
  /**
   * Style to be applied.
   */
  style: PropTypes.object,
};

const styles = {
  plusIcon: {
    color: '#fafafa',
    marginTop: '5px',
  },
  visible: {
    visibility: 'visible',
  },
  hidden: {
    visibility: 'hidden',
  },
};

/**
 * ArmorFab is used to display a list of actions when clicked on.
 */
class ArmorFab extends React.Component {
  constructor() {
    super();
    this.state = {
      show: false,
    };
    this.onItemClick = this.onItemClick.bind(this);
    this.onMainFabClick = this.onMainFabClick.bind(this);
  }

  onItemClick(eventKey) {
    // expected eventKey is the Index of the clicked item
    const clickedItem = this.props.items[eventKey];

    if (clickedItem.onClick) {
      this.props.items[eventKey].onClick();
    }
    this.setState({ show: false });
  }

  onMainFabClick() {
    this.setState({ show: !this.state.show });
    if (this.props.onClick) {
      this.props.onClick();
    }
  }

  render() {
    const visibility = (this.state.show) ? styles.visible : styles.hidden;

    return (
      <div
        className={`fab ${this.props.className} ${(this.props.small) ? 'small' : 'normal'}`}
        style={this.props.style}
      >
        <div style={visibility}>
          {
          _.map(this.props.items, (item, idx) => {
            return (
              <ArmorFabItem
                key={idx}
                icon={item.icon}
                color={item.color}
                onClick={this.onItemClick}
                eventKey={idx}
              />
            );
          })
          }
        </div>
        <Button
          bsStyle={this.props.bsStyle || 'primary'}
          className="fab-button"
          onClick={this.onMainFabClick}
        >
          <FontAwesome icon="plus" iconStyle={styles.plusIcon} />
        </Button>
      </div>
    );
  }
}

ArmorFab.propTypes = propTypes;
export default ArmorFab;
