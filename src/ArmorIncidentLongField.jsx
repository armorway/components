import React, { PropTypes } from 'react';
import FlatButton from 'material-ui/FlatButton';

const propTypes = {
  text: PropTypes.string,
  name: PropTypes.string,
  printing: PropTypes.bool,
};

const defaultProps = {
  text: '',
};
const styles = {
  container: {
    margin: '10px 0 5px 15px',
  },
  name: {
    textAlign: 'initial',
    fontWeight: '600',
  },
  text: {
    textAlign: 'initial',
  },
};

class ArmorIncidentLongField extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
    };

    this.maxSize = 250;

    this.onShowMoreClick = this.onShowMoreClick.bind(this);
    this.onShowLessClick = this.onShowLessClick.bind(this);
  }

  onShowMoreClick() {
    this.setState({
      open: true,
    });
  }

  onShowLessClick() {
    this.setState({
      open: false,
    });
  }

  getText() {
    let onClick = this.onShowLessClick;
    let text = this.props.text;
    let buttonText = 'Show Less';
    const isLongEnough = this.props.text.length > this.maxSize;

    if ((isLongEnough && !this.state.open) && !this.props.printing) {
      onClick = this.onShowMoreClick;
      text = this.props.text.slice(0, this.maxSize);
      buttonText = 'Show More';
    }

    return (
      <div>
        <span>{text}</span>
        <span>
          {
            (isLongEnough && !this.props.printing) ?
              <FlatButton onClick={onClick} label={buttonText} primary /> : ''
          }
        </span>
      </div>
    );
  }

  render() {
    if (!this.props.text) {
      return <span />;
    }

    return (
      <div style={styles.container}>
        <div style={styles.name}>{this.props.name}</div>
        <div style={styles.text}>{this.getText()}</div>
      </div>
    );
  }
}

ArmorIncidentLongField.propTypes = propTypes;
ArmorIncidentLongField.defaultProps = defaultProps;
export default ArmorIncidentLongField;
