import React from 'react';
import { Well, Grid, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router';

const Error404 = () => {
  return (
    <Grid>
      <Row>
        <Col xs={6} xsOffset={3}>
          <Well bsSize="large">
            <h4>Sorry but that Page does not exist!</h4>
            <Link to={'/'}>Return Home</Link>
          </Well>
        </Col>
      </Row>
    </Grid>
  );
};

export default Error404;
