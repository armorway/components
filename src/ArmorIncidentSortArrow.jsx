import React, { PropTypes } from 'react';
import FontAwesome from './FontAwesome';


const propTypes = {
  show: PropTypes.bool.isRequired,
  sortAsc: PropTypes.bool,
};

const styles = {
  arrow: { marginLeft: '10px' },
};

const SortArrow = (props) => {
  if (!props.show) {
    return <span />;
  }
  return (
    <FontAwesome
      icon={props.sortAsc ? 'arrow-down' : 'arrow-up'}
      iconStyle={styles.arrow}
    />
  );
};

SortArrow.propTypes = propTypes;
export default SortArrow;
