import React, { PropTypes } from 'react';
import ReactDOM from 'react-dom';

import { Button } from 'react-bootstrap';
import FontAwesome from './FontAwesome';
import _ from 'lodash';

import Slider from 'react-slide';
import moment from 'moment';
import { utils } from './utils';

const sliderStyles = {
  height: '20px',
  width: '94%',
  display: 'inline-block',
  paddingTop: '10px',
  marginLeft: '5px',
};
const propTypes = {
  value: PropTypes.number,
  onDrag: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  startOfDay: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
};

const startValue = 1;
const endValue = 1439;

class ArmorRealTimeSlider extends React.Component {
  constructor(props) {
    super(props);
    const { time } = this.getTime();

    this.state = {
      value: time,
      sliderId: utils.generateHash(),
      titleLeftPosition: 0,
    };

    this.getSliderTitle = this.getSliderTitle.bind(this);
    this.getTime = this.getTime.bind(this);
    this.moveNextMinute = this.moveNextMinute.bind(this);
    this.startRealTimeSliding = this.startRealTimeSliding.bind(this);
    this.setValue = this.setValue.bind(this);
    this.onDrag = this.onDrag.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onResetSlidingClick = this.onResetSlidingClick.bind(this);
  }

  componentDidMount() {
    const titleLeftPosition = this.getTitlePosition();
    const slider = ReactDOM.findDOMNode(this);
    const maxRightPosition = slider.clientWidth - ReactDOM.findDOMNode(this).clientWidth * 0.02;

    this.startRealTimeSliding();
    this.startRealTimeSliding = this.startRealTimeSliding.bind(this);
    /* eslint react/no-did-mount-set-state: 0 */
    this.setState({ titleLeftPosition, maxRightPosition });

    this._sliderInterval = undefined;
    this._sliderDebounce = undefined;
  }

  shouldComponentUpdate(newProps, newState) {
    return (
      newState.value !== this.state.value ||
      newState.titleLeftPosition !== this.state.titleLeftPosition ||
      newProps.date !== this.props.date
    );
  }

  componentWillUnmount() {
    window.clearTimeout(this.sliderIntialTimeout);
    window.clearInterval(this._sliderInterval);
  }

  onDrag(value) {
    const titleLeftPosition = this.getTitlePosition();

    this.setState({ value, titleLeftPosition });

    window.clearTimeout(this._sliderDebounce);

    // eslint-disable-next-line
    this._sliderDebounce = setTimeout(() => this.props.onDrag(...arguments), 100);
  }

  onChange(value, keepSliding) {
    // keepSliding is set true for automatic increments
    // undefined for user changes
    if (!keepSliding) {
      window.clearInterval(this._sliderInterval);
    }
    this.setState({ value }, () => {
      this.setState({ titleLeftPosition: this.getTitlePosition() });
    });

    // eslint-disable-next-line
    this.props.onChange(...arguments);
  }

  onResetSlidingClick() {
    this.startRealTimeSliding();
  }

  getTitlePosition() {
    const handle = ReactDOM
                    .findDOMNode(this)
                    .querySelector(`#${this.state.sliderId} .z-handle`);

    // console.log(handle.offsetLeft, handle.getBoundingClientRect().left);
    return handle.offsetLeft;
  }

  getTime() {
    const currentTime = moment().format('HH:mm:ss');
    const { date: shiftDate, startOfDay } = this.props;

    const ret = {
      time: moment(`${shiftDate} ${currentTime}`).diff(moment(`${shiftDate} ${startOfDay}`), 'minutes'),
    };

    if (currentTime < startOfDay) {
      ret.time = 1440 + ret.time;
    }

    return ret;
  }

  setValue(value) {
    this.setState({ value });
  }

  getSliderTitle() {
    const { date, startOfDay } = this.props;
    let dateTime = '';
    const { value } = this.state;
    const calculatePosition = (left) => {
      if (left <= 0) {
        return 0;
      } else if (left >= this.state.maxRightPosition) {
        return this.state.maxRightPosition;
      }
      return left;
    };

    const titleLeftPosition = (this.state.titleLeftPosition) - 24;
    const titleStyles = {
      position: 'relative',
      left: calculatePosition(titleLeftPosition),
      display: 'inline',
      top: '10px',
    };

    dateTime = moment(`${date} ${startOfDay}`).add(value, 'minutes');

    return (
      <span style={titleStyles}>
        <div>{dateTime.format('MMM DD, h:mm A')}</div>
      </span>
      );
  }

  startRealTimeSliding() {
    window.clearTimeout(this.sliderIntialTimeout);
    const { time } = this.getTime();

    this.onChange(time, true);

    this.sliderIntialTimeout = setTimeout(() => {
      this.moveNextMinute();
      window.clearInterval(this._sliderInterval);
      this._sliderInterval = setInterval(this.moveNextMinute, 60000);
    }, (60 - parseInt(moment().format('ss'), 10)) * 1000);
  }


  moveNextMinute() {
    const value = this.state.value + 1;

    if (value < 1440) {
      this.onChange(value, true);
    }
  }

  resetTime() {
    this.setState({ value: this.getTime() });
    this.startRealTimeSliding();
  }

  render() {
    const trimmedProps = _.omit(this.props, 'onChange', 'onDrag', 'value');

    return (
      <div className={this.props.className} style={{padding: '5px 0', marginTop: '-15px'}}>
          {this.getSliderTitle()}
        <Slider id={this.state.sliderId}
                {...trimmedProps}
                tickStep={1}
                trackRadius={100}
                style={sliderStyles}
                value={this.state.value}
                onChange={this.onChange}
                onDrag={this.onDrag}
                startValue={startValue}
                endValue={endValue}/>
        <span style={{marginLeft: '10px'}}>
          <Button onClick={this.onResetSlidingClick}><FontAwesome icon="refresh"/></Button>
        </span>
      </div>
    );
  }
}

ArmorRealTimeSlider.propTypes = propTypes;
export default ArmorRealTimeSlider;
