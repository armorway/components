import React, {PropTypes} from 'react';
import ReactDOM from 'react-dom';

import _ from 'lodash';
import ArmorLoader from 'components/global/ArmorLoader';

import GoogleMaps from 'services/GoogleMaps';
import { utils } from 'utils';

import AppStore from 'stores/global/AppStore';

const propTypes = {
  zoomValue: PropTypes.number.isRequired,
  centerLat: PropTypes.number.isRequired,
  centerLong: PropTypes.number.isRequired,
  onMarkerClick: PropTypes.func,
  onDrag: PropTypes.func,
  locations: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  type: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
  loaded: PropTypes.bool,
  markerIconColor: PropTypes.string,
  markerIconScale: PropTypes.number,
  icon: PropTypes.object,
  beats: PropTypes.any,
  mapId: PropTypes.string,
  style: PropTypes.object,
  noControls: PropTypes.bool,
  show: PropTypes.bool,
  showBeatsToggle: PropTypes.bool,
  infoWindowKey: PropTypes.string,
  drawing: (props, propName) => {
    const drawItems = props[propName];

    if (!_.isArray(props[propName])) {
      return new Error('Validation failed!');
    }

    for (let idx = 0; idx < drawItems.length; idx++ ) {
      const { type } = drawItems[idx];
      const { onDraw } = drawItems[idx];

      if ((_.isUndefined(type) || !_.isString(type)) ||
          (_.isUndefined(onDraw) || !_.isFunction(onDraw))) {
        return new Error('Invalid format for drawing in ArmorMaps. Requires array of {type:string, onDraw:function}');
      }
    }
  },
};

const gm = new GoogleMaps();

const defaultProps = {
  type: 'marker',
  beats: 'true',
  showBeatsToggle: true,
  infoWindowKey: 'name',
};

const iconFillColor = '#444';
const initIcon = {
  scale: 0.3,
  // strokeWeight: 0.2,
  strokeColor: 'black',
  strokeOpacity: 1,
  fillColor: iconFillColor,
  fillOpacity: 1,
};

class ArmorMap extends React.Component {
  constructor(props) {
    super(props);
    const defaultIcon = _.clone(initIcon);

    defaultIcon.path = this.props.icon || undefined;
    defaultIcon.fillColor = this.props.markerIconColor || defaultIcon.fillColor;
    defaultIcon.scale = this.props.markerIconScale || defaultIcon.scale;

    const localStates = {
      mapComponents: {
        marker: [],
        hotspots: [],
        heatMap: [],
        beats: [],
      },
      mapBeats: [],
      hotspots: [],
      showBeats: this.props.beats,
      mapId: utils.generateHash(),
      print: false,
      defaultIcon,
      height: 0,
    };

    this.state = _.assign(localStates, AppStore.getState());

    // set default overrides from the props


    this.renderNewMapComponents = this.renderNewMapComponents.bind(this);
    this.clearSelectedMarker = this.clearSelectedMarker.bind(this);
    // this.onMapPrintClick = this.onMapPrintClick.bind(this);
    this.onMarkerClick = this.onMarkerClick.bind(this);
    this.renderBeatsToMap = this.renderBeatsToMap.bind(this);
    this.onBeatsUpdate = this.onBeatsUpdate.bind(this);
    this.toggleBeats = this.toggleBeats.bind(this);
    this.renderHeatlayersToMap = this.renderHeatlayersToMap.bind(this);
    this.createInfoWindow = this.createInfoWindow.bind(this);
    this.fetchOrCreateMarker = this.fetchOrCreateMarker.bind(this);
    this.fetchOrCreatePolygon = this.fetchOrCreatePolygon.bind(this);
    this.createMarker = this.createMarker.bind(this);
    this.createPolygon = this.createPolygon.bind(this);
  }

  componentDidMount() {
    AppStore.listen(this.onBeatsUpdate);
    const beatsToggleId = this.props.mapId + 'beats';
    const { zoomValue, centerLat, centerLong} = this.props;

    const mapInst = gm.getMapInstance(this.state.mapId, zoomValue, centerLat, centerLong);

    if (!this.props.noControls) {
      const recenter = utils.createElement('button', {
        innerHTML: 'RE-CENTER',
        className: 'btn btn-default map-button',
        onclick: () => {
          gm.setMapCenter(mapInst, centerLat, centerLong);
          gm.setMapZoom(mapInst, zoomValue);
        },
      });

      // const print = utils.createElement('button', {
      //   innerHTML: '<i class="fa fa-print"></i> PRINT',
      //   className: 'btn btn-default map-button',
      //   onclick: () => {
      //     this.onMapPrintClick();
      //   },
      // });

      // const toggleBeats = utils.createElement('button', {
      //   innerHTML: `<input id=${beatsToggleId} ${this.state.showBeats ? 'checked' : ''} type="checkbox">Beats</input>`,
      //   className: 'btn btn-default map-button',
      //   onclick: () => {
      //     document.getElementById(beatsToggleId).checked = !this.state.showBeats;
      //     this.toggleBeats();
      //   },
      // });

      gm.addElement(mapInst, recenter, google.maps.ControlPosition.TOP_RIGHT);
      // gm.addElement(mapInst, print, google.maps.ControlPosition.TOP_RIGHT);
      // if (this.props.showBeatsToggle) {
      //   gm.addElement(mapInst, toggleBeats, google.maps.ControlPosition.TOP_RIGHT);
      // }
    }

    // add drawing controls if needed
    if (this.props.drawing) {
      const drawingOptions = {};
      const { drawing } = this.props;

      // Build drawing manager options.
      _.forEach(drawing, (itemToDraw) => {
        if (itemToDraw.type === 'marker') {
          if (!_.isEmpty(itemToDraw.markerSymbol)) {
            drawingOptions.marker = {
              icon: _.assign(initIcon, itemToDraw.markerSymbol),
            };
          } else {
            drawingOptions.marker = gm.createMarkerOptions();
          }
        } else if (itemToDraw.type === 'polygon') {
          drawingOptions.polygon = gm.createPolygonOptions('#1e8eff', 4, 1, '#1e8eff', 0.5, 1, null);
        }
      });

      // Get Drawing Manager Instance
      const drawingManager = gm.getDrawingManagerInstance(drawingOptions);

      console.log(drawingManager);
      // Set listeners if needed
      _.forEach(drawing, (itemToDraw) => {
        if ( itemToDraw.onDraw ) {
          if (itemToDraw.type === 'marker') {
            gm.addMarkerListener(drawingManager, (marker) => {
              const currentMapComponents = _.clone(this.state.mapComponents);

              gm.setDrawingMode(drawingManager, null);
              if (this.props.onMarkerClick) {
                gm.addListener(marker, 'click', this.onMarkerClick, [{}, marker]);
              }

              marker.type = 'marker';
              currentMapComponents.marker.push(marker);
              this.setState({mapComponents: currentMapComponents});
              itemToDraw.onDraw(marker);
            });
          } else if (itemToDraw.type === 'polygon') {
            gm.addPolygonListener(drawingManager, (polygon) => {
              const currentMapComponents = _.clone(this.state.mapComponents);

              gm.setDrawingMode(drawingManager, null);
              polygon.type = 'polygon';
              currentMapComponents.polygon.push(polygon);
              this.setState({mapComponents: currentMapComponents});

              itemToDraw.onDraw(gm.getPolygonPaths(polygon));
            });
          }
        }
      });
      gm.setToMap(drawingManager, mapInst);
    }

    // Set height of map
    /* eslint react/no-did-mount-set-state: 0 */
    const height = this.calculateMapHeight(window.innerHeight);

    this.setState({height, map: mapInst}, () => {
      const { locations } = this.props;

      if (locations) {
        this.renderNewMapComponents(this.props);
      }
    });
  }

  componentWillReceiveProps(newProps) {
    // render new markers
    const fullClean = !_.isEqual(newProps.type, this.props.type);

    if ((newProps.type === 'marker') || !_.isEqual(newProps.locations, this.props.locations) && this.state.map) {
      this.renderNewMapComponents(newProps, fullClean);
    }

    // center and zoom if new values for any
    const { map } = this.state;
    const { centerLat, centerLong, zoomValue } = newProps;

    if (centerLat !== this.props.centerLat ||
        centerLong !== this.props.centerLong ||
      zoomValue !== this.props.zoomValue) {
      gm.setMapCenter(map, centerLat, centerLong);
      gm.setMapZoom(map, zoomValue);
    }
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.props.icon && this.state.map) {
      if (this.state.selectedMarker) {
        const newIcon = _.clone(this.state.defaultIcon);

        if (this.props.markerIconColor) {
          newIcon.fillColor = this.props.markerIconColor;
        }

        gm.setIcon(this.state.selectedMarker, newIcon);
      }
      if (nextState.selectedMarker) {
        const newIcon = _.clone(this.state.defaultIcon);
        const selectedColor = {fillColor: '#2196F3'};

        if (this.props.markerIconColor) {
          selectedColor.fillColor = '#444';
        }

        gm.setIcon(nextState.selectedMarker, _.assign(newIcon, selectedColor));
      }
    }
  }

  componentDidUpdate(nextProps, nextState) {
    if (this.state.map) {
      gm.resizeMap(this.state.map);
      // gm.setMapCenter(this.state.map, this.props.centerLat, this.props.centerLong);

      setTimeout(() => {
        gm.resizeMap(this.state.map);
        // gm.setMapCenter(this.state.map, this.props.centerLat, this.props.centerLong);
      }, 100);

      if (nextState.height !== this.state.height) {
        const { zoomValue, centerLat, centerLong} = this.props;
        const mapInst = this.state.map;

        gm.setMapCenter(mapInst, centerLat, centerLong);
        gm.setMapZoom(mapInst, zoomValue);
      }
    }
  }

  componentWillUnmount() {
    AppStore.unlisten(this.onBeatsUpdate);
  }

  onBeatsUpdate(newState) {
    newState.height = this.calculateMapHeight(newState.windowHeight);
    this.setState(newState);
  }

  onMapPrintClick() {
    gm.setMapCenter(this.state.map, 0, 0);
    this.setState({print: true}, () => {
      window.print();
      this.setState({print: false});
    });
  }

  onMarkerClick(markerData, clickedMarker) {
    if (this.state.selectedMarker === clickedMarker) {
      this.setState({selectedMarker: undefined});
    } else {
      this.setState({selectedMarker: clickedMarker});
    }
    this.props.onMarkerClick.apply(this, arguments);
  }

  createInfoWindow(map, component, item) {
    const key = this.props.infoWindowKey;

    if (item[key]) {
      let windowItem = item[key];

      if (!_.isArray(windowItem)) {
        windowItem = [windowItem];
      }
      gm.createInfoWindow(map, component, _.map(windowItem, (itemName) => this.createInfoWindowContent(itemName)).join(''));
    }
  }

  createInfoWindowContent(stringData) {
    return `<p>${stringData}</p>`;
  }

  clearSelectedMarker() {
    this.setState({selectedMarker: undefined});
  }

  toggleBeats() {
    if (this.state.showBeats) {
      _.forEach(this.state.mapBeats, (item) => gm.setToMap(item, null));
    } else {
      const mapBeats = this.renderBeatsToMap(this.state.map);

      this.setState({mapBeats});
    }
    this.setState({showBeats: !this.state.showBeats});
  }

  calculateMapHeight(height) {
    const selfNode = ReactDOM.findDOMNode(this);
    let siblingSize = 0;
    let sibling = selfNode.nextSibling;

    if (!_.isEmpty(sibling)) {
      siblingSize -= sibling.getBoundingClientRect().height;
      sibling = selfNode.nextSibling;
    }
    const top = selfNode.getBoundingClientRect().top - siblingSize;

    return utils.calculateFitHeight(height, top);
  }

  fetchOrCreateMarker(lat, long, icon, loc) {
    const markers = this.state.mapComponents.marker || [];
    let idx = 0;

    for (idx; idx < markers.length; idx++) {
      try {
        const marker = markers[idx];
        const position = markers[idx].position;
        let markerLat;
        let markerLong;

        if (position.G) {
          markerLat = position.G.toFixed(12);
          markerLong = position.K.toFixed(12);
        } else {
          markerLat = position.lat().toFixed(12);
          markerLong = position.lng().toFixed(12);
        }

        // check to see if this marker matches passed lat/long
        // and return
        if (lat.toFixed(12) === markerLat && long.toFixed(12) === markerLong) {
          gm.setIcon(marker, icon);
          return marker;
        }
      } catch (exe) {
        /* eslint-disable no-console */
        console.log(exe);
        /* eslint-enable */
      }
    }

    // no existing marker found, create new marker
    const newMarker = gm.createMarker(lat, long, icon, 1, null, null);

    this.createInfoWindow(this.state.map, newMarker, loc);
    return newMarker;
  }

  createMarker(loc) {
    const { map } = this.state;
    let newIcon = _.assign(_.clone(this.state.defaultIcon), loc);

    if (loc.img && !loc.fillColor) {
      newIcon = gm.getImage(loc.img, [25, 25], [25, 25], [0, 0]);
    } else if (loc.img && loc.fillColor) {
      newIcon = gm.getImage(loc.img, [40, 40], [40, 40], [7, 7]);
    } else if (loc.path) {
      newIcon.path = loc.path;
    }

    const marker = this.fetchOrCreateMarker(loc.latitude, loc.longitude, newIcon, loc);

    // this.createInfoWindow(map, marker, loc);

    if (this.props.onMarkerClick) {
      gm.clearListener(marker, 'click');
      gm.addListener(marker, 'click', this.onMarkerClick, [loc, marker]);
    }

    if (!marker.map) {
      gm.setToMap(marker, map);
    }

    return marker;
  }

  fetchOrCreatePolygon(loc) {
    const polygons = this.state.mapComponents.polygon || [];
    let idx = 0;

    //
    for (idx; idx < polygons.length; idx++) {
      const polygon = polygons[idx];

      if (_.isEqual(loc.polygon, polygon)) {
        // gm.setIcon(marker, icon);
        return polygon;
      }
    }
    // (polygonPoints, lineColor, lineWeight, lineOpacity,
    // areaColor, areaOpacity, levelIndex, info) {
    return gm.createPolygon(
        loc.polygon,
        loc.lineColor || '#444',
        (loc.lineWeight === 0) ? 0 : loc.lineWeight || 1,
        (loc.lineOpacity === 0) ? 0 : loc.lineOpacity || 1,
        loc.areaColor || '#f00',
        (loc.areaOpacity === 0) ? 0 : loc.areaOpacity || 1,
        1,
        loc.info);
  }

  createPolygon(loc) {
    const { map } = this.state;
    const polygon = this.fetchOrCreatePolygon(loc);

    if (this.props.onMarkerClick) {
      gm.clearListener(polygon, 'click');
      gm.addListener(polygon, 'click', this.onMarkerClick, [loc, polygon]);
    }

    if (this.props.onDrag) {
      gm.setDraggable(polygon, true);
      const oldPosition = gm.getPolygonPaths(polygon);

      gm.clearListener(polygon, 'dragend');
      gm.addDragListener(polygon, (polyLoc) => {
        this.props.onDrag(gm.getPolygonPaths(polygon), oldPosition, polyLoc);
      }, [loc]);
    }

    if (!polygon.map) {
      gm.setToMap(polygon, map);
    }

    return polygon;
  }


  renderNewMapComponents(props, fullClean) {
    const map = this.state.map;
    const mapComponents = {};
    let mapBeats;
    let { type } = props;

    if (_.isString(type)) {
      type = [type];
    }
    // Clear map of existing markers
    _.forEach(this.state.mapComponents, function clearMap(components, mapType) {
      _.forEach(components, (mapComponent) => {
        if (!_.includes(['polygon', 'marker'], mapType) && !_.isUndefined(mapComponent) || fullClean) {
          gm.setToMap(mapComponent, null);
        }
      });
    });
    _.forEach(this.state.mapBeats, (item) => gm.setToMap(item, null));
    // Make and apply beats and new markers
    if (this.state.showBeats) {
      mapBeats = this.renderBeatsToMap(map);
    }

    _.forEach(type, (mapType) => {
      let { locations } = props;

      // case of multipl edata type
      if (!_.isArray(locations) && mapType !== 'beats') {
        locations = locations[mapType];
      }
      if (mapType === 'marker') {
        mapComponents[mapType] = this.renderMarkersToMap(map, locations);
      } else if (mapType === 'hotspots') {
        mapComponents[mapType] = this.renderHotspotsToMap(map, locations);
      } else if (mapType === 'heatMap') {
        mapComponents[mapType] = [this.renderHeatlayersToMap(map, locations)];
      } else if (mapType === 'beats') {
        mapComponents[mapType] = this.renderBeatsToMap(map, locations);
      } else if (mapType === 'polygon') {
        mapComponents[mapType] = this.renderPolygonsToMap(map, locations);
      }
    });

    this.setState({mapComponents, mapBeats});
  }

  renderMarkersToMap(map, locations) {
    if (map) {
      const icon = (this.props.icon) ? _.clone(this.state.defaultIcon) : undefined;

      if (this.props.markerIconColor) {
        icon.fillColor = this.props.markerIconColor;
      }
      const newMarkers = _.map(locations, this.createMarker);

      // Go through 'old' markers and remove them.
      _.forEach(this.state.mapComponents.marker, (oldMarker) => {
        if (!_.includes(newMarkers, oldMarker)) {
          gm.setToMap(oldMarker, null);
        }
      });

      return newMarkers;
    }
  }

  renderHotspotsToMap(map, locations) {
    if (map) {
      const hotspots = _.map(locations, (item) => {
        const { latitude, longitude, radius, color } = item;
        const hotspot = gm.createCircle(latitude, longitude, radius, `#${color}`, 1, 0.7, `#${color}`, 0.5, 0.7, '');

        this.createInfoWindow(map, hotspot, item);

        if (this.props.onMarkerClick) {
          gm.addListener(hotspot, 'click', this.props.onMarkerClick, [item]);
        }

        gm.setToMap(hotspot, map);
        return hotspot;
      });

      return hotspots;
    }
  }

  renderBeatsToMap(map, beats = this.state.beats) {
    if (map && beats) {
      return _.map(beats.data || beats, (item) => {
        const { coordinateList, color, displayName, opacity } = item;
        let beat;

        if (item.isSubBeat) {
          beat = gm.createPolygon(coordinateList, color, 3, opacity, color, opacity);
        } else {
          beat = gm.createPolyline(coordinateList, color, 3, opacity, 999, displayName);
        }

        gm.setToMap(beat, map);
        return beat;
      });
    }
  }

  renderHeatlayersToMap(map, list) {
    const formattedList = [];

    _.forEach(list, (item) => {
      formattedList.push(item.latitude);
      formattedList.push(item.longitude);
    });
    if (map) {
      const heatmap = gm.getHeatMapLayer(formattedList);

      gm.setToMap(heatmap, map);
      return heatmap;
    }
  }

  renderPolygonsToMap(map, locations) {
    if (map) {
      const newPolygons = _.map(locations, this.createPolygon);

      // Go through 'old' markers and remove them.
      _.forEach(this.state.mapComponents.polygon, (oldPolygon) => {
        if (!_.includes(newPolygons, oldPolygon)) {
          gm.setToMap(oldPolygon, null);
        }
      });

      return newPolygons;
    }
  }

  render() {
    const print = (this.state.print) ? 'print-section' : '';
    const shouldShow = (this.props.show === false) ? 'none' : 'block';
    const loader = (_.isUndefined(this.props.loaded) || this.props.loaded);


    return (
      <div className={print}>
        <ArmorLoader loaded={loader} />
        <div style={_.assign({height: `${this.state.height}px`, width: '100%', display: shouldShow}, this.props.style)}
             id={this.state.mapId}>
        </div>
      </div>
    );
  }
}

ArmorMap.defaultProps = defaultProps;
ArmorMap.propTypes = propTypes;
export default ArmorMap;
