import React, { PropTypes } from 'react';
import _ from 'lodash';
import Paper from 'material-ui/Paper';
import { ProgressBar } from 'react-bootstrap';
import FontAwesome from 'armorway/dist/FontAwesome';

const propTypes = {
  /*
    Rules object
   */
  rules: PropTypes.object.isRequired,
  /*
    If small is true, will render significantly smaller via smaller text.
  */
  small: PropTypes.bool,

  /*
    Styles for the component
   */
  setStyles: PropTypes.object,
};

const defaultProps = {
  setStyles: {},
};

class PasswordChange extends React.Component {
  render() {
    //  capital, lower, number, special
    const { longEnough } = this.props.rules;
    const progressLength =
      (_.filter(this.props.rules, (prop) => prop).length / _.keys(this.props.rules).length)
      * 100;
    const progressColors = {
      0: 'danger',
      20: 'danger',
      40: 'danger',
      60: 'warning',
      80: 'warning',
      100: 'success',
    };
    const ruleFontSize = (this.props.small) ? { fontSize: '10px' } : { fontSize: 'auto' };
    const headerStyles = (this.props.small) ? { fontSize: '14px' } : {};

    const progressStyles = _.assign(this.props.setStyles, {
      margin: '0px 0px 3px 0px',
      height: '12px',
    });

/**
        <div className={(capital) ? 'text-success' : 'text-danger'}>
          <FontAwesome icon={(capital) ? 'check' : 'times'} />
          <span style={ruleFontSize}> At least 1 capital letter.</span>
        </div>
        <div className={(lower) ? 'text-success' : 'text-danger'}>
          <FontAwesome icon={(lower) ? 'check' : 'times'} />
          <span style={ruleFontSize}> At least 1 lower case letter.</span>
        </div>
        <div className={(number) ? 'text-success' : 'text-danger'}>
          <FontAwesome icon={(number) ? 'check' : 'times'} />
          <span style={ruleFontSize}> At least 1 number.</span>
        </div>
        <div className={(special) ? 'text-success' : 'text-danger'}>
          <FontAwesome icon={(special) ? 'check' : 'times'} />
          <span style={ruleFontSize}> At least 1 special character.</span>
        </div>
 */
    return (
      <Paper style={{ padding: '5px' }}>
        <h4 style={headerStyles}>Password Rules</h4>
        <div className={(longEnough) ? 'text-success' : 'text-danger'}>
          <FontAwesome icon={(longEnough) ? 'check' : 'times'} />
          <span style={ruleFontSize}> At least 12 characters.</span>
        </div>

        <ProgressBar
          ref="progressBar"
          bsSize="xsmall"
          bsStyle={progressColors[progressLength]}
          now={progressLength}
          style={progressStyles}
        />
      </Paper>
    );
  }
}

PasswordChange.defaultProps = defaultProps;
PasswordChange.propTypes = propTypes;
export default PasswordChange;
