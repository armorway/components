import React, {PropTypes} from 'react';

import {Row, Col, Button, Pagination} from 'react-bootstrap';
import {utils} from 'utils';
import Griddle from 'griddle-react';
import FontAwesome from 'components/global/FontAwesome';

const propTypes = {
  'results': PropTypes.oneOfType([
    PropTypes.object.isRequired,
    PropTypes.array.isRequired,
  ]),
  'showPrint': PropTypes.bool,
  'showSave': PropTypes.bool,
  'resultsPerPage': PropTypes.number,
  'className': PropTypes.string,
  'itemsPerPage': PropTypes.number,
  'activePage': PropTypes.number,
  'showPagination': PropTypes.bool,
  'onSave': PropTypes.func,
};

const defaultProps = {
  useGriddleStyles: false,
  showSettings: false,
  showPager: false,
  tableClassName: 'table table-hover',
  sortDefaultComponent: undefined,
  sortAscendingComponent: <FontAwesome icon="long-arrow-up"/>,
  sortDescendingComponent: <FontAwesome icon="long-arrow-down"/>,
  settingsIconComponent: <FontAwesome icon="cog"/>,
  filterPlaceholderText: 'Search',
  'showPagination': true,
};

class ArmorGriddle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: utils.generateHash(),
      currentPage: props.activePage || 0,
      print: false,
      resultsPerPage: this.props.itemsPerPage || 10,
    };
    this.onPrint = this.onPrint.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onSelectPage = this.onSelectPage.bind(this);
    this.buildPagination = this.buildPagination.bind(this);
  }

  onSelectPage(event, selectedEvent) {
    event.preventDefault();
    this.setState({
      currentPage: selectedEvent.eventKey - 1,
    });
  }

  onPrint() {
    const printMode = {
      print: true,
      resultsPerPage: this.props.results.length,
      currentPage: 999,
    };
    const resetPrintMode = {
      print: false,
      resultsPerPage: this.state.resultsPerPage,
      currentPage: this.state.currentPage,
    };

    this.setState(printMode, () => {
      utils.print(this.state.id);
      this.setState(resetPrintMode);
    });
  }

  onSave(now) {
    if (now) {
      let tempResults = _.map(this.props.results, _.clone);

      if (!_.isUndefined(this.props.onSave) ) {
        tempResults = this.props.onSave(tempResults);
      }
      utils.downloadAsCSV(tempResults, 'Users', false);
    } else {
      return this.props.results;
    }
  }

  buildPrintIfNeeded() {
    if (this.props.showPrint) {
      return (
        <Button ref="clickButton"
                onClick={this.onPrint}>
          <FontAwesome icon="print" padIcon/>
          <span>Print</span>
        </Button>
      );
    }
  }

  buildSaveIfNeeded() {
    if (this.props.showSave) {
      return (
        <Button ref="saveButton"
                onClick={this.onSave}
                style={{marginLeft: '10px'}}>
          <FontAwesome icon="save" padIcon/>
          <span>Save</span>
        </Button>
      );
    }
  }

  buildPagination() {
    return (
      <Pagination
        ref="pagination"
        maxButtons={5}
        first
        last
        ellipsis
        style={{margin: '10px 10px 25px 10px', float: 'right'}}
        activePage={this.state.currentPage + 1}
        items={Math.ceil(this.props.results.length / this.state.resultsPerPage)}
        onSelect={this.onSelectPage}
        next
        prev/>
    );
  }

  render() {
    return (
      <div id={this.state.id} className={`table-striped ${this.props.className}}`}>
        <Row className="pull-right">
          <Col md={12}>
            <div style={{margin: '10px 10px 0 0'}}>
              {this.buildPrintIfNeeded()}
              {this.buildSaveIfNeeded()}
            </div>
          </Col>
        </Row>
        <Griddle
          customFormatClassName="table-striped"
          ref="griddle"
          externalCurrentPage={this.state.currentPage}
          resultsPerPage={this.state.resultsPerPage}
          {...this.props}/>
          {this.props.showPagination ? this.buildPagination() : ''}
      </div>
    );
  }
}

ArmorGriddle.defaultProps = defaultProps;
ArmorGriddle.propTypes = propTypes;
export default ArmorGriddle;
