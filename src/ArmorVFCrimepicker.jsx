import React, { PropTypes } from 'react';
import _ from 'lodash';
import ArmorVFCheckbox from './ArmorVFCheckbox';
import ArmorVFDropdown from './ArmorVFDropdown';
import FontAwesome from './FontAwesome';
import autobind from 'autobind-decorator';
// import { VelocityTransitionGroup } from 'velocity-react';

import '../styles/ArmorVFCrimepicker';

class ArmorVFCrimepicker extends React.Component {
  static propTypes = {
    /**
     * Initial selection value for the dropdown
     */
    value: PropTypes.any,
    /**
     * Function will fire when menu changes
     * signature: function(event, index, value)
     */
    onChange: PropTypes.func.isRequired,
    /**
     * List of properties supported by <MenuItem />
     * such as: primaryText, value etc...
     */
    options: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      showCheckboxes: false,
    };
  }

  @autobind
  onGroupChange(evt, index, value) {
    this.props.onDropdownChange(this.props.name, value);
  }

  @autobind
  onCheckboxSelect(name, filter, category) {
    const otherOption = this.props.groups[this.props.groups.length - 1].value;

    this.props.onDropdownChange(this.props.name, otherOption);
    this.props.onChange(name, filter, category);
  }

  @autobind
  onExpand() {
    this.setState({showCheckboxes: !this.state.showCheckboxes}, () => {
      if (this.props.scrollOnCollapse && this.state.showCheckboxes) {
        document.querySelectorAll('.filter-container').forEach(node => {
          node.scrollTop = node.scrollHeight - document.querySelector('.armor-vf-crimepicker').clientHeight;
        });
      }
    });
  }

  buildList() {
    return (
      _.map(this.props.options, (opt, cat) => {
        return (
          <span>
            <span className="category">{cat}</span>
            <ArmorVFCheckbox
              filters={opt}
              colSize={this.props.colSize}
              onChange={(name, filter) => this.onCheckboxSelect(name, filter, cat)}
              name={this.props.name}
            />
            <span className="divider"></span>
          </span>
        );
      })
    );
  }

  render() {
    return (
      <div className="armor-vf-dropdown armor-vf-crimepicker">
        <div onClick={this.onExpand}>
          <FontAwesome
            className="collapse-icon"
            icon={this.state.showCheckboxes ? 'minus-square-o' : 'plus-square-o'}
          />
        </div>
        <ArmorVFDropdown
          className="dropdown"
          style={this.props.style}
          value={this.props.selected}
          onChange={this.onGroupChange}
          options={this.props.groups}
        />
        {this.state.showCheckboxes ? (
          <span>
          {this.buildList()}
          </span>
        ) : ''}
      </div>
    );
  }
}

export default ArmorVFCrimepicker;
