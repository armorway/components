import React, {PropTypes} from 'react';

import FontAwesome from 'components/global/FontAwesome';

const propTypes = {
  data: PropTypes.any.isRequired,
};

class ArmorTrend extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      trend: props.data[0],
      variance: props.data[1],
      positiveTrend: <FontAwesome icon="arrow-up" padIcon style={{color: 'red'}}/>,
      negativeTrend: <FontAwesome icon="arrow-down" padIcon style={{color: 'green'}}/>,
      neutralTrend: <FontAwesome icon="minus" padIcon style={{color: 'black'}}/>,
    };
    this.buildIcon = this.buildIcon.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      trend: nextProps.data[0],
      variance: nextProps.data[1],
    });
  }

  buildIcon() {
    let icon = this.state.neutralTrend;

    if (this.state.variance > 0) {
      icon = this.state.positiveTrend;
    } else if (this.state.variance < 0) {
      icon = this.state.negativeTrend;
    }
    return icon;
  }

  buildLabel() {
    let label = '';

    if (this.state.variance > -999 && this.state.variance < 999 && this.state.variance !== 0) {
      label = this.state.variance + '%';
    }
    return label;
  }

  render() {
    return (
      <span>
        {this.buildIcon()} {this.buildLabel()}
      </span>
    );
  }
}

ArmorTrend.propTypes = propTypes;
export default ArmorTrend;
