import React, { PropTypes } from 'react';
import _ from 'lodash';

import { Row, Col, Button } from 'react-bootstrap';
import ArmorDivider from 'components/global/ArmorDivider';
import ArmorAgreeButton from 'components/global/ArmorAgreeButton';

import AppStore from 'stores/global/AppStore';

const propTypes = {
  isSelected: PropTypes.bool.isRequired,
  data: PropTypes.object.isRequired,
  onRespondClick: PropTypes.func,
  onUpdateClick: PropTypes.func.isRequired,
  onAgreeClick: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
};

class BugFeatureListItem extends React.Component {
  constructor() {
    super();
    this.onAgreeClick = this.onAgreeClick.bind(this);
    this.onRespondClick = this.onRespondClick.bind(this);
    this.onUpdateClick = this.onUpdateClick.bind(this);
    this.buildRespondIfNeeded = this.buildRespondIfNeeded.bind(this);
  }

  onAgreeClick(event) {
    this.props.onAgreeClick(event, this.props.data);
  }

  onRespondClick(event) {
    this.props.onRespondClick(event, this.props.data);
    event.stopPropagation();
  }

  onUpdateClick(event) {
    this.props.onUpdateClick(event, this.props.data);
    event.stopPropagation();
  }

  onRowClick(data, event) {
    this.props.onClick(event, data);
  }

  buildRespondIfNeeded() {
    this.buildRespondIfNeeded = this.buildRespondIfNeeded.bind(this);
    if (this.props.onRespondClick) {
      return (
          <span role="button"
            className="btn btn-default"
            onClick={this.onRespondClick}
            style={{ marginTop: '5px', marginLeft: '5px' }}
          >
          Respond
        </span>
      );
    }
    return '';
  }

  render() {
    const rowPad = { paddingBottom: '15px' };
    const { isSelected, data } = this.props;

    return (
      <ul className="list-group"
        style={{ marginBottom: '10px' }}
        onClick={this.onRowClick.bind(this, data)}
      >
        <div className="list-group-item">
          <Row>
            <Col xs={2}>Topic: {data.title}</Col>
            <Col xsOffset={2} xs={2}>Priority: {data.priority}</Col>
            <Col xsOffset={3} xs={3}>
              <div className="pull-right">
                <Button role="button"
                  bsStyle="default"
                  onClick={this.onUpdateClick}
                  style={{ marginTop: '5px', marginLeft: '5px' }}
                >
                  Update
                </Button>
                {this.buildRespondIfNeeded()}
              </div>
            </Col>
          </Row>
          <div style={{ display: (isSelected) ? 'block' : 'none', padding: '30px' }}>
            <Row style={rowPad}>
              <Col xs={12}>
                <div>
                  <ArmorAgreeButton
                    agreed={_.includes(data.usernamesOfOfficers, AppStore.getState().user.username)}
                    count={_.without(data.usernamesOfOfficers, null, undefined, '').length}
                    onClick={this.onAgreeClick}
                  />
                </div>
              </Col>
            </Row>
            <ArmorDivider />
              <Row style={rowPad}><Col xs={12}>Description: {data.description}</Col></Row>
              <Row style={rowPad}>
                <Col xs={6}> Requested By: {data.reportedByOfficer} </Col>
                <Col xs={6}> Date Requested: {data.dateReported} </Col>
              </Row>
              <ArmorDivider />
              <Row style={rowPad}>
                <Col xs={6}>
                  Status: {_.startCase(data.armorwayResponseStatus.toLowerCase() || 'None')}
                </Col>
                <Col xs={6}>
                  Difficulty:
                  {_.startCase(data.armorwayResponseDiffultyLevel.toLowerCase() || 'None')}
                </Col>
              </Row>
              {

              }
              <Row style={rowPad}>
                <Col xs={12}>Notes: {data.armorwayResponseNotes || 'No notes available.'}</Col>
              </Row>
              <Row style={rowPad}>
                <Col xs={12}>Date Posted: {data.armorwayResponseDate || 'No date available.'}</Col>
              </Row>
          </div>
        </div>
      </ul>
    );
  }
}

BugFeatureListItem.propTypes = propTypes;
export default BugFeatureListItem;
