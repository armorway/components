import React, { PropTypes } from 'react';
import { Row, Col } from 'react-bootstrap';
import _ from 'lodash';
import { utils } from './utils';

const defaultProps = {
  formatKeys: true,
  formatValues: true,
};

const propTypes = {
  /**
    * key-name string
  */
  keyData: PropTypes.any.isRequired,
  /**
    * value string mapped to the key-name
  */
  value: PropTypes.any.isRequired,
  /**
   * If true, makes key col xs=4
   */
  smallKey: PropTypes.bool,
  /**
    * custom className for ArmorKeyValueRow object
  */
  className: PropTypes.string,
  /**
    * If true, this will format the keyData with camelToReadable.
    * Defaults at true
  */
  formatKeys: PropTypes.bool,
  /**
    * If true, this will format the value with camelToReadable
    * Defaults at true
  */
  formatValues: PropTypes.bool,
};

/**
  * component used to render one row of a key map
*/
class ArmorKeyValueRow extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { keyData } = this.props;
    let { value } = this.props;

    if (_.isBoolean(value)) {
      value = utils.booleanToYesNo(value);
    }
    const { smallKey } = this.props;

    return (
      <Row
        style={{ marginBottom: '5px' }}
        className={`${this.props.className} armor-key-value-row`}
      >
        <Col xs={smallKey ? 4 : 6}>
          <span className="bold pull-left key-data">
            {this.props.formatKeys ?
              _.capitalize(utils.camelToReadable(keyData)) :
              keyData}<span className="key-colon">:</span>
          </span>
        </Col>
        <Col xs={smallKey ? 8 : 6}>
          <span className="pull-left value-data" >
            {this.props.formatValues ? utils.camelToReadable(value, true) : value}
          </span>
        </Col>
      </Row>
    );
  }
}

ArmorKeyValueRow.defaultProps = defaultProps;
ArmorKeyValueRow.propTypes = propTypes;
export default ArmorKeyValueRow;
