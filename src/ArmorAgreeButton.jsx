import React, { PropTypes } from 'react';

const propTypes = {
  /**
    * The current number of agrees to display.
    */
  count: PropTypes.number.isRequired,
  /**
    * Used to show if the user has agreed already.
    */
  agreed: PropTypes.bool.isRequired,
  /**
    * Called when agreeing and props.agreed is false.
    */
  onClick: PropTypes.func.isRequired,
};

/**
  * ArmorAgreeButton is used to show 'agrees' to something on a page.
  * It is similar to a +1 or like button.
  */
class ArmorAgreeButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.hasAgreed = this.hasAgreed.bind(this);
    this.onClick = this.onClick.bind(this);
  }

  onClick(event) {
    event.stopPropagation();
    if (!this.hasAgreed()) {
      this.props.onClick();
    }
  }

  hasAgreed() {
    return this.props.agreed;
  }

  render() {
    const style = (this.hasAgreed()) ?
      {} :
      { color: '#4caf50', backgroundColor: '#fff', border: '1px solid #4caf50' };

    return (
      <span role="button"
        className="btn btn-success"
        onClick={this.onClick}
        style={style}
      >
        + {this.props.count}
      </span>
    );
  }
}

ArmorAgreeButton.propTypes = propTypes;
export default ArmorAgreeButton;
