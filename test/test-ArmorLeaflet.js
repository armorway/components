// import React from 'react';
// import jsdom from 'mocha-jsdom';
// import { shallow } from 'enzyme';
// import { expect } from 'chai';
// jsdom();

// let ArmorLeaflet;
// let Marker;
// let Circle;

// before(() => {
//   ArmorLeaflet = require('../src/maps/ArmorLeaflet').default;
//   Marker = require('react-leaflet').Marker;
//   Circle = require('react-leaflet').Circle;
// });

// describe('<ArmorLeaflet />', () => {
//   it('If map renders elements correctly', () => {
//     const wrapper = shallow(
//       <ArmorLeaflet display="fullScreen" center={[25, 25]}>
//         <Marker position={{ lat: 23, lng: 25 }} />
//         <Marker position={{ lat: 26, lng: 25 }} />
//         <Circle center={{ lat: 26, lng: 25 }} radius={20} />
//       </ArmorLeaflet>
//     );

//     expect(wrapper.find('Marker').length).to.be.equal(2);
//     expect(wrapper.find('Circle').length).to.be.equal(1);
//     expect(wrapper.find('Map').length).to.be.equal(1);
//   });
// });
