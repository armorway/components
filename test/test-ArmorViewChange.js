import React from 'react';
import { shallow } from 'enzyme';
import { render } from 'enzyme';
import { expect } from 'chai';

import ArmorViewChange from '../src/ArmorViewChange.jsx';

const views = [
  {
    value: 'map',
  },
  {
    value: 'table',
  },
];

describe('<ArmorViewChange />', () => {
  it('test props.views renders pill buttons', () => {
    const wrapper =
      render(
      <ArmorViewChange
        onSelect={() => {}}
        active={views[0].value}
        title={'title'}
        views={views}
      />);

    const wrapperListItems = wrapper.find('.armor-view-item a');
    const mapItem = wrapperListItems.get(0).children[0].data;
    const tableItem = wrapperListItems.get(1).children[0].data;

    expect(mapItem).to.equal('Map');
    expect(tableItem).to.equal('Table');
  });
  it('test title', () => {
    const title = 'title';

    const wrapper =
      shallow(
      <ArmorViewChange
        onSelect={() => {}}
        active={views[0].value}
        title={title}
        views={views}
      />);

    const titleClass = wrapper.find('.active-view');

    expect(titleClass.text()).to.equal(title);
  });

  it('test active', () => {
    const wrapper =
      shallow(
      <ArmorViewChange
        onSelect={() => {}}
        active={views[0].value}
        title={'title'}
        views={views}
      />);

    const pillsClass = wrapper.find('.armor-pill');

    expect(pillsClass.props().activeKey).to.equal(views[0].value);
  });

  it('test onSelect', () => {
    let onSelectValue;
    const onSelect = (evt, selected) => {
      onSelectValue = selected;
    };

    const wrapper =
      shallow(
        <ArmorViewChange
          onSelect={onSelect}
          active={views[0].value}
          title={'title'}
          views={views}
        />);

    const pillsClass = wrapper.find('.armor-pill');

    expect(onSelectValue).to.equal(undefined);

    pillsClass.props().onSelect(views[0].value);

    expect(onSelectValue).to.equal(views[0].value);
  });
});
