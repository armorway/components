import React from 'react';
import { shallow, render } from 'enzyme';
import { expect } from 'chai';

import ArmorListItem from '../src/ArmorListItem.jsx';

describe('<ArmorDivider />', () => {


  it('Test name and datetime', () => {
    const data = {

    };
    const name = 'Crime';
    const dateTime = '1023 adsf asdf3';
    const wrapper = shallow(
      <ArmorListItem
        name={name}
        dateTime={dateTime}
        onClick={() => {}}
        data={data}
        active
      />);

    const nameText = wrapper.find('.armor-list-item-name').text();
    const dateTimeText = wrapper.find('.armor-list-item-date-time').text();

    expect(nameText).to.equal(name);
    expect(dateTimeText).to.equal(dateTime);
  });


  it('Test img string creates img tag', () => {
    const img = 'img/src/totally-a-url.png';
    const wrapper = shallow(
      <ArmorListItem
        name="asdf"
        dateTime="asdfadd"
        onClick={() => {}}
        data={{}}
        img={img}
        active
      />);

    const imgSrc = wrapper.find('.armor-list-row-img').props().src;

    expect(imgSrc).to.equal(img);
  });

  it('Test links/actions work', () => {
    let passedId;
    let passedName;
    let passedData;
    const initId = 1;
    const initName = 'asdf';
    const initData = {
      thing: 'blag',
    };
    const actions = [
      {
        name: 'Action 1',
      },
      {
        name: 'Action 2',
        action: (id, name, data) => {
          passedId = id;
          passedName = name;
          passedData = data;
        },
      },
    ];
    const wrapper = shallow(
      <ArmorListItem
        id={initId}
        name={initName}
        dateTime="asdfadd"
        onClick={() => {}}
        data={initData}
        actions={actions}
        active
      />);

    const actionButtons = wrapper.find('.armor-list-action');

    expect(actionButtons).to.have.length(2);

    expect(actionButtons.get(0).props.children)
      .to.equal(actions[0].name);
    expect(actionButtons.get(1).props.children)
      .to.equal(actions[1].name);

    // test click
    actionButtons.get(1).props.onClick();

    expect(passedId).to.equal(1);
    expect(passedName).to.equal('asdf');
    expect(passedData).to.equal(initData);
  });

  it('Test img object renders given object', () => {
    const wrapper = shallow(
      <ArmorListItem
        name="asdf"
        dateTime="asdfadd"
        onClick={() => {}}
        data={{}}
        img={<div className="totally-a-div" />}
        active
      />);

    const img = wrapper.find('.totally-a-div');

    expect(img).to.have.length(1);
  });

  it('Test onClick turns active, shows data, fires props.active', () => {
    const id = 13452;
    const data = {
      thing: 'something',
      other: 'blah',
    };
    let clickedId;
    const onClick = (passsedId) => {
      clickedId = passsedId;
    };
    const name = 'Crime';
    const dateTime = '1023 adsf asdf3';
    const wrapper = shallow(
      <ArmorListItem
        id={id}
        name={name}
        dateTime={dateTime}
        onClick={onClick}
        data={data}
      />);

    // pre click expectations
    expect(wrapper.find('.active')).to.have.length(0);
    expect(wrapper.find('.armor-list-item-data-rows')).to.have.length(0);
    wrapper.simulate('click');

    // expected changes after click
    expect(wrapper.find('.active')).to.have.length(1);
    // eslint-disable-next-line
    expect(wrapper.state().active).to.be.true;

    const rows = wrapper.find('.armor-list-item-data-rows');

    expect(rows.children()).to.have.length(2);

    expect(clickedId).to.equal(id);
  });

  it('Test row data shows when active', () => {
    const data = {
      thing: 'something',
      other: 'blah',
    };
    const name = 'Crime';
    const dateTime = '1023 adsf asdf3';
    const wrapper = render(
      <ArmorListItem
        name={name}
        dateTime={dateTime}
        onClick={() => {}}
        data={data}
        active
      />);

    // make sure its active
    expect(wrapper.find('.active')).to.have.length(1);

    const rows = wrapper.find('.armor-list-item-data-rows');

    expect(rows.children()).to.have.length(2);

    expect(rows.find('.armor-key-value-row'))
      .to.have.length(2);
  });
});
