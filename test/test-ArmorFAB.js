import React from 'react';
import { shallow, render } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import ArmorFab from '../src/ArmorFab.jsx';


const items = [
  {
    icon: 'pencil',
    color: 'pink',
    onClick: () => {},
  },
  {
    icon: 'beer',
    color: 'blue',
    onClick: () => {},
  },
];

describe('<ArmorFab />', () => {
  it('test renders icon', () => {
    const wrapper = render(
      <ArmorFab
        icon="plus"
        items={[]}
        bsStyle="default"
        onClick={() => {}}
      />);

    expect(wrapper.find('.fa-plus')).to.have.length(1);

    // sanity check
    expect(wrapper.find('.fa-other-thing')).to.have.length(0);
  });

  it('test checks color of fab', () => {
    const wrapper = render(
      <ArmorFab
        icon="plus"
        items={[]}
        bsStyle="default"
        onClick={() => {}}
      />);

    expect(wrapper.find('.btn-default')).to.have.length(1);
  });

  it('test renders fab items', () => {
    const wrapper = render(
      <ArmorFab
        icon="plus"
        items={items}
        bsStyle="default"
        onClick={() => {}}
      />);

    expect(wrapper.find('.fa-plus')).to.have.length(1);
    expect(wrapper.find('.fa-pencil')).to.have.length(1);
    expect(wrapper.find('.fa-beer')).to.have.length(1);
  });

  it('test shows fab items onClick', () => {
    const onClick = sinon.spy();
    const wrapper = shallow(
      <ArmorFab
        icon="plus"
        items={items}
        color="red"
        onClick={onClick}
      />);

    wrapper.find('.fab-button').simulate('click');
    // eslint-disable-next-line
    expect(wrapper.state().show).to.be.true;
    // eslint-disable-next-line
    expect(onClick.calledOnce).to.be.true;
  });
});
