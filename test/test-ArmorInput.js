import React from 'react';
import { shallow, render } from 'enzyme';
import { expect } from 'chai';

import ArmorInput from '../src/ArmorInput.jsx';

describe('<ArmorInput />', () => {
  it('test label and noAsterisk', () => {
    const label = 'label';
    const wrapper = render(<ArmorInput type="text" label={label} required />);
    const wrapperNoAstrsk = render(
      <ArmorInput
        type="text"
        label={label}
        noAsterisk
      />);


    const labelClass = wrapper.find('.input-label');
    const labelClassNoAstrsk = wrapperNoAstrsk.find('.input-label');

    expect(labelClass.text()).to.equal(`${label} *`);
    expect(labelClassNoAstrsk.text()).to.equal(label);
  });

  it('test errorMsg', () => {
    const errorMsg = 'this is an error';
    const wrapper = render(
      <ArmorInput
        type="text"
        errorMsg={errorMsg}
        bsStyle="error"
        required
      />);

    const inputBoxClass = wrapper.find('.input-box');

    const errorMessageAttribute = inputBoxClass.get(0).attribs['aria-describedby'];

    expect(errorMessageAttribute).to.equal(errorMsg);
  });

  it('test onChange', () => {
    let testVar = 0;
    const onChangeEvent = {
      target: { value: 'thing' },
    };
    const onChange = (event) => {
      testVar = event.target.value;
    };

    const wrapper = shallow(
      <ArmorInput
        type="text"
        onChange={onChange}
      />);

    const inputBoxClass = wrapper.find('.input-box');

    inputBoxClass.props().onChange(onChangeEvent);
    expect(wrapper.state().value).to.equal(onChangeEvent.target.value);
    expect(testVar).to.equal(onChangeEvent.target.value);
    // expect(inputBoxClass.state)).to.equal(onChangeEvent.target.value);
  });

  it('test undefined onChange', () => {
    let testVar = 0;
    const onChangeEvent = {
      target: { value: '' },
    };
    const onChange = (event) => {
      testVar = event.target.value;
    };

    const wrapper = shallow(
      <ArmorInput
        type="text"
        onChange={onChange}
        required
      />);

    const inputBoxClass = wrapper.find('.input-box');

    inputBoxClass.props().onChange(onChangeEvent);

    expect(wrapper.state().bsStyle).to.equal('error');
    expect(testVar).to.equal(onChangeEvent.target.value);
  });

  it('test children', () => {
    const wrapper = shallow(
      <ArmorInput
        type="select"
        required
      >
        <option className="selector" />
        <option className="selector" />
        <option className="selector" />
      </ArmorInput>);

    const inputBoxClass = wrapper.find('.input-box');

    expect(inputBoxClass.find('option'))
      .to.have.length(3);
  });

  it('test valid', () => {
    const wrapper = shallow(
      <ArmorInput
        type="text"
        required
        valid
      />);

    // eslint-disable-next-line
    expect(wrapper.state().valid).to.be.true;
  });

  it('test bsStyle', () => {
    const error = 'error';
    const wrapper = shallow(
      <ArmorInput
        type="text"
        required
        bsStyle="error"
      />);

    expect(wrapper.state().bsStyle).to.equal(error);
  });
});
