import React from 'react';
import { shallow, render } from 'enzyme';
import { expect } from 'chai';

import ArmorList from '../src/ArmorList.jsx';

describe('<ArmorList />', () => {
  it('Test list renders correct amount of items', () => {
    const data = [{
      dateTime: 'Blerg',
      name: 'I am A Name',
      thing: 'blah',
      otherItem: 'bleh',
      type: 'crime',
    }];
    const wrapper = render(
      <ArmorList
        onClick={() => {}}
        data={data}
        imgByType={{
          crime: 'images/crime/icon.png',
        }}
        selected="2"
      />);

    const listItems = wrapper.find('.list-group-item');

    expect(listItems).to.have.length(1);
  });

  it('Test list renders correct sort options', () => {
    const data = [{
      dateTime: 'Blerg',
      name: 'I am A Name',
      thing: 'blah',
      otherItem: 'bleh',
      type: 'crime',
    }];
    const wrapper = render(
      <ArmorList
        onClick={() => {}}
        data={data}
        imgByType={{
          crime: 'images/crime/icon.png',
        }}
        sortKeys={['thing', 'otherItem']}
        selected="2"
      />);

    const sortItems = wrapper.find('.dropdown .dropdown-menu').children();

    // 2 for sort keys, 1 for date.
    expect(sortItems).to.have.length(3);
  });

  it('Test sorting works', () => {
    const data = [{
      id: 1,
      dateTime: 'Blerg',
      name: 'I am A Name',
    }, {
      id: 2,
      dateTime: 'Blerg',
      name: 'I am A Name',
    }];
    const wrapper = shallow(
      <ArmorList
        onClick={() => {}}
        data={data}
        imgByType={{
          crime: 'images/crime/icon.png',
        }}
        sortKeys={['id']}
        selected="2"
      />);

    const idSortOption =
      wrapper.find('.armor-list-sort-options').children().get(1);

    // sets sort method to id, and ascending
    idSortOption.props.onSelect();

    expect(wrapper.state().sortMethod).to.equal('id');
    // eslint-disable-next-line
    expect(wrapper.state().ascending).to.be.true;

    // sets sort method to id, and now descending
    idSortOption.props.onSelect();

    expect(wrapper.state().sortMethod).to.equal('id');
    // eslint-disable-next-line
    expect(wrapper.state().ascending).to.be.false;
  });
});
