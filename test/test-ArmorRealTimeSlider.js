import React from 'react';
import { shallow } from 'enzyme';
// import { expect } from 'chai';

import ArmorRealTimeSlider from '../src/ArmorRealTimeSlider.jsx';

describe('<ArmorRealTimeSlider />', () => {
  it('test render', () => {
    shallow(
      <ArmorRealTimeSlider
        value="10"
        onDrag={() => {}}
        onChange={() => {}}
        startOfDay="07:00:00"
        date="2016-07-10"
      />
    );
  });
});
