import React from 'react';
import { shallow, render } from 'enzyme';
import { expect } from 'chai';

import ArmorFilterList from '../src/ArmorFilterList';

describe('<ArmorFilterList />', () => {
  it('test renders filter items', () => {
    const filters = [
      {
        type: 'beat',
        name: 'a-beat',
      },
      {
        type: 'watch',
        name: 'pm',
      },
    ];
    const wrapper = render(
      <ArmorFilterList
        filters={filters}
        onFilterRemove={() => {}}
      />);

    const filterItems = wrapper
      .find('.filter-item');

    expect(filterItems).to.have.length(2);
  });

  it('test renders filter dropdown and menu-items', () => {
    const filters = [
      {
        type: 'beat',
        name: 'a-beat',
      },
      {
        type: 'watch',
        name: 'pm',
      },
    ];
    const wrapper = render(
      <ArmorFilterList
        filters={filters}
        onFilterRemove={() => {}}
      />);

    const filterItems = wrapper
      .find('.armor-filter-menu-item');

    const countText = wrapper.find('button#armor-filter-dropdown').html();

    expect(filterItems).to.have.length(2);
    expect(countText.indexOf(`(${filters.length}) Applied Filters`))
      .to.not.equal(-1);
  });

  it('test click fires onRemove', () => {
    const filters = [
      {
        type: 'beat',
        name: 'a-beat',
      },
      {
        type: 'watch',
        name: 'pm',
      },
    ];
    let passedType;
    let passedName;
    const onRemove = (type, name) => {
      passedType = type;
      passedName = name;
    };
    const wrapper = shallow(
      <ArmorFilterList
        filters={filters}
        onFilterRemove={onRemove}
      />);


    expect(passedType).to.equal(undefined);
    expect(passedName).to.equal(undefined);

    // find first rendered filter item, and get the props from it
    // then execute the function and make sure the event bubbled up

    const firstFilterItem = wrapper
      .find('.armor-filter-list-item').get(0);

    const { onRemove: itemOnRemove, type: itemType, name: itemName }
      = firstFilterItem.props;


    itemOnRemove(itemType, itemName);
    wrapper.childAt(0).find('.remove-btn');
    expect(passedType).to.equal(filters[0].type);
    expect(passedName).to.equal(filters[0].name);
  });
});

