import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import ArmorViewFilter from '../src/ArmorViewFilter';
import { filters, overlays } from './configs/config-ArmorViewFilter';

describe('<ArmorViewFilter />', () => {
  it('Tests if the component renders view buttons', () => {
    const wrapper = shallow(
      <ArmorViewFilter style={{ marginLeft: '15px', marginTop: '15px' }}
        filters={filters}
        allKey="all"
        overlays={overlays}
        title="Aloha"
        onApply={() => {}}
      />
    );

    expect(wrapper.find('Button')).to.have.length(2);
  });

  it('Tests if the component renders filters correctly on all views', () => {
    const wrapper = shallow(
      <ArmorViewFilter
        filters={filters}
        allKey="all"
        overlays={overlays}
        title="Aloha"
        onApply={() => {}}
      />
    );

    wrapper.find('#filtersButton').simulate('click');

    expect(wrapper.find('Input')).to.have.length(2);
    expect(wrapper.find('ArmorCheckboxSelect')).to.have.length(1);

    wrapper.find('#overlaysButton').simulate('click');

    expect(wrapper.find('Input')).to.have.length(0);
    expect(wrapper.find('ArmorCheckboxSelect')).to.have.length(1);
  });

  it('Tests the filters and overlays buttons open the correct views', () => {
    const wrapper = shallow(
      <ArmorViewFilter
        filters={filters}
        allKey="all"
        overlays={overlays}
        title="Aloha"
        onApply={() => {}}
      />
    );

    wrapper.find('#filtersButton').simulate('click');

    expect(wrapper.find('.reset-button')).to.have.length(1);
    expect(wrapper.find('.apply-button')).to.have.length(1);
    expect(wrapper.state().showFilters).to.equal(true);
    expect(wrapper.state().showOverlays).to.equal(false);
    expect(wrapper.find('Input')).to.have.length(2);

    wrapper.find('#overlaysButton').simulate('click');

    expect(wrapper.state().showFilters).to.equal(false);
    expect(wrapper.state().showOverlays).to.equal(true);
    expect(wrapper.find('ArmorCheckboxSelect')).to.have.length(1);
  });

  it('Tests that the Reset button brings the previous state back ', () => {
    const wrapper = shallow(
      <ArmorViewFilter style={{ marginLeft: '15px', marginTop: '15px' }}
        filters={filters}
        allKey="all"
        overlays={overlays}
        title="Aloha"
        onApply={() => {}}
      />
    );
    const mockEvent = {
      target: {
        value: 'Hello There...',
      },
    };

    wrapper.find('#filtersButton').simulate('click');
    wrapper.find('Input').at(0).simulate('change', mockEvent);
    expect(wrapper.state().filters[0].value).to.equal('Hello There...');

    wrapper.find('.reset-button').simulate('click');

    expect(wrapper.state().filters[0].value).to.equal('');
  });

  it('Tests that the Apply button works with the input filter type', () => {
    let passedValues;
    const onApply = (values) => {
      passedValues = values;
    };
    const wrapper = shallow(
      <ArmorViewFilter style={{ marginLeft: '15px', marginTop: '15px' }}
        filters={filters}
        allKey="all"
        overlays={overlays}
        title="Aloha"
        onApply={onApply}
      />
    );
    const mockEvent = {
      target: {
        value: 'Hello There...',
      },
    };

    wrapper.find('#filtersButton').simulate('click');
    expect(passedValues).to.equal(undefined);


    wrapper.find('Input').at(0).simulate('change', mockEvent);
    wrapper.find('.apply-button').simulate('click');

    expect(passedValues['patrolLength']).to.equal('Hello There...');
  });

  it('Tests selection of a checkbox filter', () => {
    let passedValues;
    const onApply = (value) => {
      passedValues = value;
    };
    const wrapper = shallow(
      <ArmorViewFilter
        filters={filters}
        allKey="all"
        overlays={overlays}
        title="Aloha"
        onApply={onApply}
      />
    );

    wrapper.find('#filtersButton').simulate('click');

    const checkbox = wrapper.find('.checkbox-inner-filter').childAt(0);

    expect(passedValues).to.equal(undefined);

    checkbox.props().onChange('Watch', filters[1].options[1]);

    wrapper.find('.apply-button').simulate('click');

    expect(passedValues.watch.length).to.equal(1);
  });
});
