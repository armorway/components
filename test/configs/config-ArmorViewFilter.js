export const overlays = [
  {
    name: 'Views',
    type: 'checkbox',
    options: [
      {
        id: 'all',
        name: 'All',
        active: false,
      },
      {
        id: 'hotspots',
        name: 'Hotspots',
        active: true,
      },
      {
        id: 'heatMap',
        name: 'Heat Map',
        active: false,
      },
      {
        id: 'markers',
        name: 'Markers',
        active: false,
      },
    ],
  },
];

export const filters = [
  {
    name: 'Patrol Length',
    type: 'input',
    placeholder: 'Enter Length in Meters',
    value: '',
  },
  {
    name: 'Watch',
    type: 'checkbox',
    options: [
      {
        id: 'all',
        name: 'All',
        active: false,
      },
      {
        id: 'am',
        name: 'AM',
        active: false,
      },
      {
        id: 'pm',
        name: 'PM',
        active: false,
      },
    ],
  },
  {
    name: 'Keywords',
    type: 'input',
    placeholder: 'Enter Keywords',
    value: '',
  },
];