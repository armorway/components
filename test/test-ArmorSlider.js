// import React from 'react';
// import { shallow } from 'enzyme';
// import { expect } from 'chai';

// import ArmorSlider from '../src/ArmorSlider.jsx';


// describe('<Armorslider />', () => {
//   it('test value', () => {
//     const wrapper = shallow(<ArmorSlider value={5} />);

//     const valueClass = wrapper.find('.armor-slider-value');

//     expect(valueClass.text()).to.be.equal('5');
//   });

//   it('test label', () => {
//     const label = 'label';
//     const wrapper = shallow(<ArmorSlider label={label} />);

//     const labelClass = wrapper.find('.armor-slider-label');

//     expect(labelClass.text()).to.be.equal(label);
//   });

//   it('test startValue', () => {
//     const wrapper = shallow(<ArmorSlider startValue={0} />);

//     const startValueClass = wrapper.find('.armor-slider-startValue');

//     expect(startValueClass.text()).to.be.equal('0');
//   });

//   it('test endValue', () => {
//     const wrapper = shallow(<ArmorSlider endValue={100} />);

//     const endValueClass = wrapper.find('.armor-slider-endValue');

//     expect(endValueClass.text()).to.be.equal('100');
//   });
// });
