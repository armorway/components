import React from 'react';
import { shallow, render } from 'enzyme';
import { expect } from 'chai';

import ArmorDivider from '../src/ArmorDivider.jsx';

describe('<ArmorDivider />', () => {
  it('Test spaceOnly', () => {
    const spaceOnly = shallow(<ArmorDivider spaceOnly />);
    const spaceOnlyWithLabel = shallow(
      <ArmorDivider
        spaceOnly
        label="label"
      />
    );

    const spaceOnlyDivider = (spaceOnly.find('.divider'));
    const spaceOnlyWithLabelDivider = (spaceOnlyWithLabel.find('.divider'));
    const transparentStyle = 'style="background-color:transparent;"';

    expect(spaceOnlyDivider.html().indexOf(transparentStyle))
      .to.not.equal(-1);
    expect(spaceOnlyWithLabelDivider.html().indexOf(transparentStyle))
      .to.not.equal(-1);
    expect(spaceOnlyDivider.length)
      .to.equal(1);
    expect(spaceOnlyWithLabelDivider.length)
      .to.equal(1);
  });

  it('Test Label', () => {
    const label = 'Label';
    const withLabel = render(<ArmorDivider label={label} />);

    const divider = (withLabel.find('.col-xs-2'));
    const labelDividers = (withLabel.find('.col-xs-5'));

    expect(divider.text()).to.equal(label);
    expect(labelDividers.length).to.equal(2);
  });

  it('Test labelStyle', () => {
    const label = 'Label';
    const style = { color: 'blue' };
    const withStyle = render(<ArmorDivider label={label} labelStyle={style} />);

    const divider = (withStyle.find('.col-xs-2'));

    expect(divider.html().indexOf('style="color:blue;"'))
      .to.not.equal(-1);
  });
});
