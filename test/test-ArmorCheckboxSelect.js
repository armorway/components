import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import ArmorCheckboxSelect from '../src/ArmorCheckboxSelect';

describe('<ArmorCheckboxSelect />', () => {
  const data = [
    {
      id: 'all',
      name: 'All',
      active: false,
    },
    {
      id: 'as',
      name: 'assualts',
      active: true,
    },
    {
      id: 'br',
      name: 'Commercial Burglery',
      active: true,
    },
    {
      id: 'c',
      name: 'c',
      active: false,
    },
  ];

  let passedName;
  let passedFilter;
  const onChange = (name, filter) => {
    passedName = name;
    passedFilter = filter;
  };
  const wrapper = shallow(<ArmorCheckboxSelect name="Crimes" filters={data} onChange={onChange} />);

  it('Tests if the component renders all checkboxes', () => {
    expect(wrapper.find('Input')).to.have.length(4);
  });

  it('Tests if text size classes are being applied', () => {
    expect(wrapper.find('.small-filter-box')).to.have.length(1);
    expect(wrapper.find('.medium-filter-box')).to.have.length(1);
  });

  it('Tests if the onChange function works', () => {
    expect(passedName).to.equal(undefined);
    expect(passedFilter).to.equal(undefined);

    wrapper.find('Input').at(0).simulate('change');

    expect(passedName).to.equal('Crimes');
    expect(passedFilter.name).to.equal(data[0].name);
  });
});
