import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import chai from 'chai';
import chaiEnzyme from 'chai-enzyme';

chai.use(chaiEnzyme());

import ArmorColorPicker from '../src/ArmorColorPicker.jsx';

describe('<ArmorColorPicker />', () => {
  it('Test onChange passes correct values', () => {
    let payload;
    const onChangePayload = 'thing';
    const onChange = (eventPayload) => {
      payload = eventPayload;
    };
    const wrapper = shallow(
      <ArmorColorPicker onChange={onChange} color="blue" />
    );

    const colorClass = wrapper.find('.color-class');

    colorClass.props().onChangeComplete(onChangePayload);

    expect(payload).to.equal(onChangePayload);
  });

  it('Test correct color renders from props', () => {
    const color = 'blue';

    const wrapper = shallow(
      <ArmorColorPicker onChange={() => {}} color={color} />
    );

    const colorClass = wrapper.find('.color-class');

    expect(colorClass.props().color).to.equal(color);
  });

  it('Test onClick toggles color picker', () => {
    const wrapper = shallow(
      <ArmorColorPicker onChange={() => {}} color="blue" />
    );
    const colorButton = wrapper.find('.color-button').props();

    colorButton.onClick();
    // eslint-disable-next-line
    expect(wrapper.state().displayColorPicker).to.be.true;

    colorButton.onClick();
    // eslint-disable-next-line
    expect(wrapper.state().displayColorPicker).to.be.false;
  });
});
