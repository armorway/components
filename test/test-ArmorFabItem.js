import React from 'react';
import { shallow, render } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import ArmorFabItem from '../src/ArmorFabItem.jsx';


describe('<ArmorFabItem />', () => {
  it('test renders icon', () => {
    const wrapper = render(
      <ArmorFabItem
        icon="pencil"
        color="red"
        onClick={() => {}}
      />);

    expect(wrapper.find('.fa-pencil')).to.have.length(1);

    // sanity check
    expect(wrapper.find('.fa-other-thing')).to.have.length(0);
  });

  it('test renders icon component passed', () => {
    const className = 'passed-icon';
    const wrapper = shallow(
      <ArmorFabItem
        icon={<div className={className} />}
        color="red"
        onClick={() => {}}
      />);

    expect(wrapper.find(`.${className}`)).to.have.length(1);
  });

  it('test renders background color', () => {
    const wrapper = shallow(
      <ArmorFabItem
        icon="pencil"
        color="red"
        onClick={() => {}}
      />);

    expect(wrapper.html().indexOf('background-color:red'))
      .to.not.equal(-1);
  });

  it('test onClick fires', () => {
    const onClick = sinon.spy();
    const wrapper = shallow(
      <ArmorFabItem
        icon="pencil"
        color="red"
        onClick={onClick}
      />);

    wrapper.find('.fab-item').simulate('click');
    // eslint-disable-next-line no-unused-expressions
    expect(onClick.calledOnce).to.be.true;
  });

  it('test onClick passes eventKey', () => {
    let initEventKey;
    const eventKey = 'eventKey';
    const onClick = (evtKey) => {
      initEventKey = evtKey;
    };
    const wrapper = shallow(
      <ArmorFabItem
        icon="pencil"
        color="red"
        eventKey={eventKey}
        onClick={onClick}
      />);

    wrapper.find('.fab-item').simulate('click');
    expect(eventKey).to.equal(initEventKey);
  });
});
