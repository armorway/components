import React from 'react';
import { shallow, render } from 'enzyme';
import { expect } from 'chai';

import ArmorSearchAndFilter from '../src/ArmorSearchAndFilter';

describe('<ArmorSearchAndFilter />', () => {
  it('test renders filter and search', () => {
    const filters = [
      {
        type: 'beat',
        name: 'a-beat',
      },
      {
        type: 'watch',
        name: 'pm',
      },
    ];
    const wrapper = render(
      <ArmorSearchAndFilter
        filters={filters}
        onFilterRemove={() => {}}
        onSearch={() => {}}
        bsStyle="primary"
      />);

    const searchBox = wrapper
      .find('.armor-filter-search-field');

    const armorFilter = wrapper
      .find('.filter-list');

    expect(searchBox).to.have.length(1);
    expect(armorFilter).to.have.length(1);
  });
});

