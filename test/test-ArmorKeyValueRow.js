import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import chai from 'chai';
import chaiEnzyme from 'chai-enzyme';

chai.use(chaiEnzyme());

import ArmorKeyValueRow from '../src/ArmorKeyValueRow.jsx';

describe('<ArmorKeyValueRow />', () => {
  it('Test keyData', () => {
    const data = 'test';
    const value = 'value';

    const wrapper = shallow(
       <ArmorKeyValueRow
         keyData={data}
         value={value}
       />
     );


    const keyDataSpan = wrapper.find('.key-data');

    expect(keyDataSpan.text()).to.equal('Test:');
  });

  it('Test value', () => {
    const data = 'test';
    const value = 'value';

    const wrapper = shallow(
      <ArmorKeyValueRow
        keyData={data}
        value={value}
      />
    );
    const keyValueSpan = wrapper.find('.value-data');

    expect(keyValueSpan.text()).to.equal('Value');
  });

  it('Test className', () => {
    const data = 'test';
    const value = 'value';

    const wrapper = shallow(
      <ArmorKeyValueRow
        keyData={data}
        value={value}
        className="test-class"
      />
    );

    expect(wrapper).to.have.className('test-class');
  });

  it('Test formatKeys', () => {
    const data = 'testFormatKeys';
    const value = 'value';

    const wrapper = shallow(
      <ArmorKeyValueRow
        keyData={data}
        value={value}
        formatKeys
      />
    );

    const wrapperDefault = shallow(
      <ArmorKeyValueRow
        keyData={data}
        value={value}
        formatKeys
      />
    );

    const wrapperFalse = shallow(
      <ArmorKeyValueRow
        keyData={data}
        value={value}
        formatKeys={false}
      />
    );

    const keyDataSpan = wrapper.find('.key-data');
    const keyDataSpanDefault = wrapperDefault.find('.key-data');
    const keyDataSpanFalse = wrapperFalse.find('.key-data');

    expect(keyDataSpan.text()).to.equal('Test format keys:');
    expect(keyDataSpanDefault.text()).to.equal('Test format keys:');
    expect(keyDataSpanFalse.text()).to.equal('testFormatKeys:');
  });

  it('Test formatValues', () => {
    const data = 'test';
    const value = 'testFormatValues';

    const wrapper = shallow(
      <ArmorKeyValueRow
        keyData={data}
        value={value}
        formatValues
      />
    );

    const wrapperDefault = shallow(
      <ArmorKeyValueRow
        keyData={data}
        value={value}
      />
    );

    const wrapperFalse = shallow(
      <ArmorKeyValueRow
        keyData={data}
        value={value}
        formatValues={false}
      />
    );

    const valueDataSpan = wrapper.find('.value-data');
    const valueDataSpanDefault = wrapperDefault.find('.value-data');
    const valueDataSpanFalse = wrapperFalse.find('.value-data');

    expect(valueDataSpan.text()).to.equal('Test Format Values');
    expect(valueDataSpanDefault.text()).to.equal('Test Format Values');
    expect(valueDataSpanFalse.text()).to.equal('testFormatValues');
  });
});
