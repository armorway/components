import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import FontAwesome from '../src/FontAwesome.jsx';


describe('<FontAwesome />', () => {
  it('test children', () => {
    const par = (<p className="child-class" />);
    const iconWithChild = shallow(
        <FontAwesome icon="pencil">{par}</FontAwesome>
      );

    const childClass = (iconWithChild.find('.child-class'));

    expect(childClass).to.have.length(1);
  });


  it('test icon', () => {
    const pencilIcon = shallow(<FontAwesome icon="pencil" />);

    expect(pencilIcon.find('.fa-pencil')).to.have.length(1);
  });


  it('test title', () => {
    const title = 'title';
    const pencilIcon = shallow(
      <FontAwesome icon="pencil" title={title} />);

    const faTitleClass = pencilIcon.find('.fa-title');

    expect(faTitleClass.text()).to.equal(title);
  });


  it('test iconstyle and padIcon', () => {
    const iconStyleOnly =
        shallow(
         <FontAwesome
           icon="pencil"
           iconStyle={{ padding: '10px' }}
         />);
    const padIconOnly =
        shallow(
          <FontAwesome
            icon="pencil"
            padIcon
          />);
    const padIconAddsIconStyle =
        shallow(
          <FontAwesome
            icon="pencil"
            iconStyle={{ padding: '10px' }}
            padIcon
          />);
    const iconStyleOverridesPadIcon =
        shallow(
          <FontAwesome
            icon="pencil"
            iconStyle={{ marginRight: '10px', padding: '10px' }}
            padIcon
          />);
    const iconStyleMarginOnly =
        shallow(
          <FontAwesome
            icon="pencil"
            iconStyle={{ marginRight: '3px', padding: '10px' }}
          />);


    const spanIconStyleOnly = iconStyleOnly.find('.fa-pencil');
    const spanPadIconOnly = padIconOnly.find('.fa-pencil');
    const spanPadIconAddsIconStyle = padIconAddsIconStyle.find('.fa-pencil');
    const spanIconStyleOverridesPadIcon = iconStyleOverridesPadIcon.find('.fa-pencil');
    const spanIconStyleMarginOnly = iconStyleMarginOnly.find('.fa-pencil');


    expect(spanIconStyleOnly.html().indexOf('style="padding:10px;"'))
      .to.not.equal(-1);
    expect(spanPadIconOnly.html().indexOf('style="margin-right:5px;"'))
      .to.not.equal(-1);
    expect(spanPadIconAddsIconStyle.html().indexOf('style="margin-right:5px;padding:10px;"'))
      .to.not.equal(-1);
    expect(spanIconStyleOverridesPadIcon.html().indexOf('style="margin-right:10px;padding:10px;"'))
      .to.not.equal(-1);
    expect(spanIconStyleMarginOnly.html().indexOf('style="margin-right:3px;padding:10px;"'))
      .to.not.equal(-1);
  });


  it('test size', () => {
    const pencilIcon2x = shallow(
      <FontAwesome icon="pencil" size="2x" />);

    const spanClass2x = pencilIcon2x.find('.fa-pencil');

    expect(spanClass2x.html().indexOf('class="fa fa-pencil fa-2x"'))
      .to.not.equal(-1);
  });

  it('test className and style', () => {
    const className = 'test-class';
    const pencilIcon = shallow(
      <FontAwesome
        icon="pencil"
        className={className}
        style={{ margin: '5px' }}
      />
     );
    const spanClass = pencilIcon.find(`.${className}`);

    expect(spanClass).to.have.length(1);
    expect(spanClass.html().indexOf('style="margin:5px;"'))
    .to.not.equal(-1);
  });
});
