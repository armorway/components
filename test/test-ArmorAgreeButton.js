import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import ArmorAgreeButton from '../src/ArmorAgreeButton.jsx';

describe('<ArmorAgreeButton />', () => {
  it('test click', () => {
    let clicked = false;
    const onClick = () => {
      clicked = true;
    };
    const wrapper = shallow(
      <ArmorAgreeButton count={2} agreed={false} onClick={onClick} />
    );

    const btn = wrapper.find('.btn');

    btn.simulate('click', { stopPropagation: () => {} });

    // eslint-disable-next-line
    expect(clicked).to.be.true;
  });

  it('test count', () => {
    const wrapper = shallow(
      <ArmorAgreeButton count={2} agreed={false} onClick={() => {}} />
    );

    const btn = wrapper.find('.btn');

    expect(btn.text()).to.equal('+ 2');
  });

  it('test agreed button style', () => {
    const wrapperNotAgreed = shallow(
      <ArmorAgreeButton count={2} agreed={false} onClick={() => {}} />
    );
    const wrapperAgreed = shallow(
      <ArmorAgreeButton
        count={2}
        onClick={() => {}}
        agreed
      />
    );

    const btnNotAgreed = wrapperNotAgreed.find('.btn');
    const btnAgreed = wrapperAgreed.find('.btn');


    expect(btnNotAgreed.html().indexOf(
      'style="color:#4caf50;background-color:#fff;border:1px solid #4caf50;"'
    )).to.not.equal(-1);

    expect(btnAgreed.html().indexOf('style')).to.equal(-1);
  });
});
