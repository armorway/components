import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import ArmorFilterListItem from '../src/ArmorFilterListItem';

describe('<ArmorFilterListItem />', () => {
  it('test renders text', () => {
    const name = 'a-beat';
    const wrapper = shallow(
      <ArmorFilterListItem
        name={name}
        type="Beat"
        onRemove={() => {}}
      />);

    const renderedItemName = wrapper
      .find('.filter-item-name')
      .text();

    expect(renderedItemName).to.equal(name);
  });

  it('test click fires onRemove', () => {
    const renderName = 'a-beat';
    const renderType = 'beat';
    let passedType;
    let passedName;
    const onRemove = (type, name) => {
      passedType = type;
      passedName = name;
    };
    const wrapper = shallow(
      <ArmorFilterListItem
        name={renderName}
        type={renderType}
        onRemove={onRemove}
      />);


    expect(passedType).to.equal(undefined);
    expect(passedName).to.equal(undefined);

    wrapper.find('.remove-btn').simulate('click');
    expect(passedType).to.equal(renderType);
    expect(passedName).to.equal(renderName);
  });
});
