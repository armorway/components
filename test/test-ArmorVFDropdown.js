import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import ArmorVFDropdown from '../src/ArmorVFDropdown';

describe('<ArmorViewFilter />', () => {
  it('Tests if the component renders the menu items', () => {
    const wrapper = shallow(
      <ArmorVFDropdown
        options={[
          {
            primaryText: 'Item 1',
            value: 0,
          },
          {
            primaryText: 'Item 2',
            value: 1,
          },
        ]}
      />
    );

    expect(wrapper.find('DropDownMenu')).to.have.length(1);
    expect(wrapper.find('MenuItem')).to.have.length(2);
  });
});
