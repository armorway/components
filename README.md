# README #

Armorway Components
### What is this repository for? ###

* This repository exists to provide Components to all of our React-based projects.
* v0.1

### USAGE 
`import {ContentBox, FontAwesome, ArmorLoader, ArmorGriddle, ArmorDivider} from 'armorway';`

### How do I get set up? ###

* build: `npm run build`
* test: `npm test`
* dev: `npm start`
* lint/test `npm run check`

### Contribution guidelines ###

* Writing tests: Do them
* Remember to build your code before putting up a pull request!