'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _ArmorLoader = require('components/global/ArmorLoader');

var _ArmorLoader2 = _interopRequireDefault(_ArmorLoader);

var _GoogleMaps = require('services/GoogleMaps');

var _GoogleMaps2 = _interopRequireDefault(_GoogleMaps);

var _utils = require('utils');

var _AppStore = require('stores/global/AppStore');

var _AppStore2 = _interopRequireDefault(_AppStore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  zoomValue: _react.PropTypes.number.isRequired,
  centerLat: _react.PropTypes.number.isRequired,
  centerLong: _react.PropTypes.number.isRequired,
  onMarkerClick: _react.PropTypes.func,
  onDrag: _react.PropTypes.func,
  locations: _react.PropTypes.oneOfType([_react.PropTypes.array, _react.PropTypes.object]),
  type: _react.PropTypes.oneOfType([_react.PropTypes.array, _react.PropTypes.string]),
  loaded: _react.PropTypes.bool,
  markerIconColor: _react.PropTypes.string,
  markerIconScale: _react.PropTypes.number,
  icon: _react.PropTypes.object,
  beats: _react.PropTypes.any,
  mapId: _react.PropTypes.string,
  style: _react.PropTypes.object,
  noControls: _react.PropTypes.bool,
  show: _react.PropTypes.bool,
  showBeatsToggle: _react.PropTypes.bool,
  infoWindowKey: _react.PropTypes.string,
  drawing: function drawing(props, propName) {
    var drawItems = props[propName];

    if (!_lodash2.default.isArray(props[propName])) {
      return new Error('Validation failed!');
    }

    for (var idx = 0; idx < drawItems.length; idx++) {
      var type = drawItems[idx].type;
      var onDraw = drawItems[idx].onDraw;


      if (_lodash2.default.isUndefined(type) || !_lodash2.default.isString(type) || _lodash2.default.isUndefined(onDraw) || !_lodash2.default.isFunction(onDraw)) {
        return new Error('Invalid format for drawing in ArmorMaps. Requires array of {type:string, onDraw:function}');
      }
    }
  }
};

var gm = new _GoogleMaps2.default();

var defaultProps = {
  type: 'marker',
  beats: 'true',
  showBeatsToggle: true,
  infoWindowKey: 'name'
};

var iconFillColor = '#444';
var initIcon = {
  scale: 0.3,
  // strokeWeight: 0.2,
  strokeColor: 'black',
  strokeOpacity: 1,
  fillColor: iconFillColor,
  fillOpacity: 1
};

var ArmorMap = function (_React$Component) {
  _inherits(ArmorMap, _React$Component);

  function ArmorMap(props) {
    _classCallCheck(this, ArmorMap);

    var _this = _possibleConstructorReturn(this, (ArmorMap.__proto__ || Object.getPrototypeOf(ArmorMap)).call(this, props));

    var defaultIcon = _lodash2.default.clone(initIcon);

    defaultIcon.path = _this.props.icon || undefined;
    defaultIcon.fillColor = _this.props.markerIconColor || defaultIcon.fillColor;
    defaultIcon.scale = _this.props.markerIconScale || defaultIcon.scale;

    var localStates = {
      mapComponents: {
        marker: [],
        hotspots: [],
        heatMap: [],
        beats: []
      },
      mapBeats: [],
      hotspots: [],
      showBeats: _this.props.beats,
      mapId: _utils.utils.generateHash(),
      print: false,
      defaultIcon: defaultIcon,
      height: 0
    };

    _this.state = _lodash2.default.assign(localStates, _AppStore2.default.getState());

    // set default overrides from the props


    _this.renderNewMapComponents = _this.renderNewMapComponents.bind(_this);
    _this.clearSelectedMarker = _this.clearSelectedMarker.bind(_this);
    // this.onMapPrintClick = this.onMapPrintClick.bind(this);
    _this.onMarkerClick = _this.onMarkerClick.bind(_this);
    _this.renderBeatsToMap = _this.renderBeatsToMap.bind(_this);
    _this.onBeatsUpdate = _this.onBeatsUpdate.bind(_this);
    _this.toggleBeats = _this.toggleBeats.bind(_this);
    _this.renderHeatlayersToMap = _this.renderHeatlayersToMap.bind(_this);
    _this.createInfoWindow = _this.createInfoWindow.bind(_this);
    _this.fetchOrCreateMarker = _this.fetchOrCreateMarker.bind(_this);
    _this.fetchOrCreatePolygon = _this.fetchOrCreatePolygon.bind(_this);
    _this.createMarker = _this.createMarker.bind(_this);
    _this.createPolygon = _this.createPolygon.bind(_this);
    return _this;
  }

  _createClass(ArmorMap, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _this2 = this;

      _AppStore2.default.listen(this.onBeatsUpdate);
      var beatsToggleId = this.props.mapId + 'beats';
      var _props = this.props;
      var zoomValue = _props.zoomValue;
      var centerLat = _props.centerLat;
      var centerLong = _props.centerLong;


      var mapInst = gm.getMapInstance(this.state.mapId, zoomValue, centerLat, centerLong);

      if (!this.props.noControls) {
        var recenter = _utils.utils.createElement('button', {
          innerHTML: 'RE-CENTER',
          className: 'btn btn-default map-button',
          onclick: function onclick() {
            gm.setMapCenter(mapInst, centerLat, centerLong);
            gm.setMapZoom(mapInst, zoomValue);
          }
        });

        // const print = utils.createElement('button', {
        //   innerHTML: '<i class="fa fa-print"></i> PRINT',
        //   className: 'btn btn-default map-button',
        //   onclick: () => {
        //     this.onMapPrintClick();
        //   },
        // });

        // const toggleBeats = utils.createElement('button', {
        //   innerHTML: `<input id=${beatsToggleId} ${this.state.showBeats ? 'checked' : ''} type="checkbox">Beats</input>`,
        //   className: 'btn btn-default map-button',
        //   onclick: () => {
        //     document.getElementById(beatsToggleId).checked = !this.state.showBeats;
        //     this.toggleBeats();
        //   },
        // });

        gm.addElement(mapInst, recenter, google.maps.ControlPosition.TOP_RIGHT);
        // gm.addElement(mapInst, print, google.maps.ControlPosition.TOP_RIGHT);
        // if (this.props.showBeatsToggle) {
        //   gm.addElement(mapInst, toggleBeats, google.maps.ControlPosition.TOP_RIGHT);
        // }
      }

      // add drawing controls if needed
      if (this.props.drawing) {
        (function () {
          var drawingOptions = {};
          var drawing = _this2.props.drawing;

          // Build drawing manager options.

          _lodash2.default.forEach(drawing, function (itemToDraw) {
            if (itemToDraw.type === 'marker') {
              if (!_lodash2.default.isEmpty(itemToDraw.markerSymbol)) {
                drawingOptions.marker = {
                  icon: _lodash2.default.assign(initIcon, itemToDraw.markerSymbol)
                };
              } else {
                drawingOptions.marker = gm.createMarkerOptions();
              }
            } else if (itemToDraw.type === 'polygon') {
              drawingOptions.polygon = gm.createPolygonOptions('#1e8eff', 4, 1, '#1e8eff', 0.5, 1, null);
            }
          });

          // Get Drawing Manager Instance
          var drawingManager = gm.getDrawingManagerInstance(drawingOptions);

          console.log(drawingManager);
          // Set listeners if needed
          _lodash2.default.forEach(drawing, function (itemToDraw) {
            if (itemToDraw.onDraw) {
              if (itemToDraw.type === 'marker') {
                gm.addMarkerListener(drawingManager, function (marker) {
                  var currentMapComponents = _lodash2.default.clone(_this2.state.mapComponents);

                  gm.setDrawingMode(drawingManager, null);
                  if (_this2.props.onMarkerClick) {
                    gm.addListener(marker, 'click', _this2.onMarkerClick, [{}, marker]);
                  }

                  marker.type = 'marker';
                  currentMapComponents.marker.push(marker);
                  _this2.setState({ mapComponents: currentMapComponents });
                  itemToDraw.onDraw(marker);
                });
              } else if (itemToDraw.type === 'polygon') {
                gm.addPolygonListener(drawingManager, function (polygon) {
                  var currentMapComponents = _lodash2.default.clone(_this2.state.mapComponents);

                  gm.setDrawingMode(drawingManager, null);
                  polygon.type = 'polygon';
                  currentMapComponents.polygon.push(polygon);
                  _this2.setState({ mapComponents: currentMapComponents });

                  itemToDraw.onDraw(gm.getPolygonPaths(polygon));
                });
              }
            }
          });
          gm.setToMap(drawingManager, mapInst);
        })();
      }

      // Set height of map
      /* eslint react/no-did-mount-set-state: 0 */
      var height = this.calculateMapHeight(window.innerHeight);

      this.setState({ height: height, map: mapInst }, function () {
        var locations = _this2.props.locations;


        if (locations) {
          _this2.renderNewMapComponents(_this2.props);
        }
      });
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(newProps) {
      // render new markers
      var fullClean = !_lodash2.default.isEqual(newProps.type, this.props.type);

      if (newProps.type === 'marker' || !_lodash2.default.isEqual(newProps.locations, this.props.locations) && this.state.map) {
        this.renderNewMapComponents(newProps, fullClean);
      }

      // center and zoom if new values for any
      var map = this.state.map;
      var centerLat = newProps.centerLat;
      var centerLong = newProps.centerLong;
      var zoomValue = newProps.zoomValue;


      if (centerLat !== this.props.centerLat || centerLong !== this.props.centerLong || zoomValue !== this.props.zoomValue) {
        gm.setMapCenter(map, centerLat, centerLong);
        gm.setMapZoom(map, zoomValue);
      }
    }
  }, {
    key: 'componentWillUpdate',
    value: function componentWillUpdate(nextProps, nextState) {
      if (this.props.icon && this.state.map) {
        if (this.state.selectedMarker) {
          var newIcon = _lodash2.default.clone(this.state.defaultIcon);

          if (this.props.markerIconColor) {
            newIcon.fillColor = this.props.markerIconColor;
          }

          gm.setIcon(this.state.selectedMarker, newIcon);
        }
        if (nextState.selectedMarker) {
          var _newIcon = _lodash2.default.clone(this.state.defaultIcon);
          var selectedColor = { fillColor: '#2196F3' };

          if (this.props.markerIconColor) {
            selectedColor.fillColor = '#444';
          }

          gm.setIcon(nextState.selectedMarker, _lodash2.default.assign(_newIcon, selectedColor));
        }
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(nextProps, nextState) {
      var _this3 = this;

      if (this.state.map) {
        gm.resizeMap(this.state.map);
        // gm.setMapCenter(this.state.map, this.props.centerLat, this.props.centerLong);

        setTimeout(function () {
          gm.resizeMap(_this3.state.map);
          // gm.setMapCenter(this.state.map, this.props.centerLat, this.props.centerLong);
        }, 100);

        if (nextState.height !== this.state.height) {
          var _props2 = this.props;
          var zoomValue = _props2.zoomValue;
          var centerLat = _props2.centerLat;
          var centerLong = _props2.centerLong;

          var mapInst = this.state.map;

          gm.setMapCenter(mapInst, centerLat, centerLong);
          gm.setMapZoom(mapInst, zoomValue);
        }
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      _AppStore2.default.unlisten(this.onBeatsUpdate);
    }
  }, {
    key: 'onBeatsUpdate',
    value: function onBeatsUpdate(newState) {
      newState.height = this.calculateMapHeight(newState.windowHeight);
      this.setState(newState);
    }
  }, {
    key: 'onMapPrintClick',
    value: function onMapPrintClick() {
      var _this4 = this;

      gm.setMapCenter(this.state.map, 0, 0);
      this.setState({ print: true }, function () {
        window.print();
        _this4.setState({ print: false });
      });
    }
  }, {
    key: 'onMarkerClick',
    value: function onMarkerClick(markerData, clickedMarker) {
      if (this.state.selectedMarker === clickedMarker) {
        this.setState({ selectedMarker: undefined });
      } else {
        this.setState({ selectedMarker: clickedMarker });
      }
      this.props.onMarkerClick.apply(this, arguments);
    }
  }, {
    key: 'createInfoWindow',
    value: function createInfoWindow(map, component, item) {
      var _this5 = this;

      var key = this.props.infoWindowKey;

      if (item[key]) {
        var windowItem = item[key];

        if (!_lodash2.default.isArray(windowItem)) {
          windowItem = [windowItem];
        }
        gm.createInfoWindow(map, component, _lodash2.default.map(windowItem, function (itemName) {
          return _this5.createInfoWindowContent(itemName);
        }).join(''));
      }
    }
  }, {
    key: 'createInfoWindowContent',
    value: function createInfoWindowContent(stringData) {
      return '<p>' + stringData + '</p>';
    }
  }, {
    key: 'clearSelectedMarker',
    value: function clearSelectedMarker() {
      this.setState({ selectedMarker: undefined });
    }
  }, {
    key: 'toggleBeats',
    value: function toggleBeats() {
      if (this.state.showBeats) {
        _lodash2.default.forEach(this.state.mapBeats, function (item) {
          return gm.setToMap(item, null);
        });
      } else {
        var mapBeats = this.renderBeatsToMap(this.state.map);

        this.setState({ mapBeats: mapBeats });
      }
      this.setState({ showBeats: !this.state.showBeats });
    }
  }, {
    key: 'calculateMapHeight',
    value: function calculateMapHeight(height) {
      var selfNode = _reactDom2.default.findDOMNode(this);
      var siblingSize = 0;
      var sibling = selfNode.nextSibling;

      if (!_lodash2.default.isEmpty(sibling)) {
        siblingSize -= sibling.getBoundingClientRect().height;
        sibling = selfNode.nextSibling;
      }
      var top = selfNode.getBoundingClientRect().top - siblingSize;

      return _utils.utils.calculateFitHeight(height, top);
    }
  }, {
    key: 'fetchOrCreateMarker',
    value: function fetchOrCreateMarker(lat, long, icon, loc) {
      var markers = this.state.mapComponents.marker || [];
      var idx = 0;

      for (idx; idx < markers.length; idx++) {
        try {
          var marker = markers[idx];
          var position = markers[idx].position;
          var markerLat = void 0;
          var markerLong = void 0;

          if (position.G) {
            markerLat = position.G.toFixed(12);
            markerLong = position.K.toFixed(12);
          } else {
            markerLat = position.lat().toFixed(12);
            markerLong = position.lng().toFixed(12);
          }

          // check to see if this marker matches passed lat/long
          // and return
          if (lat.toFixed(12) === markerLat && long.toFixed(12) === markerLong) {
            gm.setIcon(marker, icon);
            return marker;
          }
        } catch (exe) {
          /* eslint-disable no-console */
          console.log(exe);
          /* eslint-enable */
        }
      }

      // no existing marker found, create new marker
      var newMarker = gm.createMarker(lat, long, icon, 1, null, null);

      this.createInfoWindow(this.state.map, newMarker, loc);
      return newMarker;
    }
  }, {
    key: 'createMarker',
    value: function createMarker(loc) {
      var map = this.state.map;

      var newIcon = _lodash2.default.assign(_lodash2.default.clone(this.state.defaultIcon), loc);

      if (loc.img && !loc.fillColor) {
        newIcon = gm.getImage(loc.img, [25, 25], [25, 25], [0, 0]);
      } else if (loc.img && loc.fillColor) {
        newIcon = gm.getImage(loc.img, [40, 40], [40, 40], [7, 7]);
      } else if (loc.path) {
        newIcon.path = loc.path;
      }

      var marker = this.fetchOrCreateMarker(loc.latitude, loc.longitude, newIcon, loc);

      // this.createInfoWindow(map, marker, loc);

      if (this.props.onMarkerClick) {
        gm.clearListener(marker, 'click');
        gm.addListener(marker, 'click', this.onMarkerClick, [loc, marker]);
      }

      if (!marker.map) {
        gm.setToMap(marker, map);
      }

      return marker;
    }
  }, {
    key: 'fetchOrCreatePolygon',
    value: function fetchOrCreatePolygon(loc) {
      var polygons = this.state.mapComponents.polygon || [];
      var idx = 0;

      //
      for (idx; idx < polygons.length; idx++) {
        var polygon = polygons[idx];

        if (_lodash2.default.isEqual(loc.polygon, polygon)) {
          // gm.setIcon(marker, icon);
          return polygon;
        }
      }
      // (polygonPoints, lineColor, lineWeight, lineOpacity,
      // areaColor, areaOpacity, levelIndex, info) {
      return gm.createPolygon(loc.polygon, loc.lineColor || '#444', loc.lineWeight === 0 ? 0 : loc.lineWeight || 1, loc.lineOpacity === 0 ? 0 : loc.lineOpacity || 1, loc.areaColor || '#f00', loc.areaOpacity === 0 ? 0 : loc.areaOpacity || 1, 1, loc.info);
    }
  }, {
    key: 'createPolygon',
    value: function createPolygon(loc) {
      var _this6 = this;

      var map = this.state.map;

      var polygon = this.fetchOrCreatePolygon(loc);

      if (this.props.onMarkerClick) {
        gm.clearListener(polygon, 'click');
        gm.addListener(polygon, 'click', this.onMarkerClick, [loc, polygon]);
      }

      if (this.props.onDrag) {
        (function () {
          gm.setDraggable(polygon, true);
          var oldPosition = gm.getPolygonPaths(polygon);

          gm.clearListener(polygon, 'dragend');
          gm.addDragListener(polygon, function (polyLoc) {
            _this6.props.onDrag(gm.getPolygonPaths(polygon), oldPosition, polyLoc);
          }, [loc]);
        })();
      }

      if (!polygon.map) {
        gm.setToMap(polygon, map);
      }

      return polygon;
    }
  }, {
    key: 'renderNewMapComponents',
    value: function renderNewMapComponents(props, fullClean) {
      var _this7 = this;

      var map = this.state.map;
      var mapComponents = {};
      var mapBeats = void 0;
      var type = props.type;


      if (_lodash2.default.isString(type)) {
        type = [type];
      }
      // Clear map of existing markers
      _lodash2.default.forEach(this.state.mapComponents, function clearMap(components, mapType) {
        _lodash2.default.forEach(components, function (mapComponent) {
          if (!_lodash2.default.includes(['polygon', 'marker'], mapType) && !_lodash2.default.isUndefined(mapComponent) || fullClean) {
            gm.setToMap(mapComponent, null);
          }
        });
      });
      _lodash2.default.forEach(this.state.mapBeats, function (item) {
        return gm.setToMap(item, null);
      });
      // Make and apply beats and new markers
      if (this.state.showBeats) {
        mapBeats = this.renderBeatsToMap(map);
      }

      _lodash2.default.forEach(type, function (mapType) {
        var locations = props.locations;

        // case of multipl edata type

        if (!_lodash2.default.isArray(locations) && mapType !== 'beats') {
          locations = locations[mapType];
        }
        if (mapType === 'marker') {
          mapComponents[mapType] = _this7.renderMarkersToMap(map, locations);
        } else if (mapType === 'hotspots') {
          mapComponents[mapType] = _this7.renderHotspotsToMap(map, locations);
        } else if (mapType === 'heatMap') {
          mapComponents[mapType] = [_this7.renderHeatlayersToMap(map, locations)];
        } else if (mapType === 'beats') {
          mapComponents[mapType] = _this7.renderBeatsToMap(map, locations);
        } else if (mapType === 'polygon') {
          mapComponents[mapType] = _this7.renderPolygonsToMap(map, locations);
        }
      });

      this.setState({ mapComponents: mapComponents, mapBeats: mapBeats });
    }
  }, {
    key: 'renderMarkersToMap',
    value: function renderMarkersToMap(map, locations) {
      var _this8 = this;

      if (map) {
        var _ret3 = function () {
          var icon = _this8.props.icon ? _lodash2.default.clone(_this8.state.defaultIcon) : undefined;

          if (_this8.props.markerIconColor) {
            icon.fillColor = _this8.props.markerIconColor;
          }
          var newMarkers = _lodash2.default.map(locations, _this8.createMarker);

          // Go through 'old' markers and remove them.
          _lodash2.default.forEach(_this8.state.mapComponents.marker, function (oldMarker) {
            if (!_lodash2.default.includes(newMarkers, oldMarker)) {
              gm.setToMap(oldMarker, null);
            }
          });

          return {
            v: newMarkers
          };
        }();

        if ((typeof _ret3 === 'undefined' ? 'undefined' : _typeof(_ret3)) === "object") return _ret3.v;
      }
    }
  }, {
    key: 'renderHotspotsToMap',
    value: function renderHotspotsToMap(map, locations) {
      var _this9 = this;

      if (map) {
        var hotspots = _lodash2.default.map(locations, function (item) {
          var latitude = item.latitude;
          var longitude = item.longitude;
          var radius = item.radius;
          var color = item.color;

          var hotspot = gm.createCircle(latitude, longitude, radius, '#' + color, 1, 0.7, '#' + color, 0.5, 0.7, '');

          _this9.createInfoWindow(map, hotspot, item);

          if (_this9.props.onMarkerClick) {
            gm.addListener(hotspot, 'click', _this9.props.onMarkerClick, [item]);
          }

          gm.setToMap(hotspot, map);
          return hotspot;
        });

        return hotspots;
      }
    }
  }, {
    key: 'renderBeatsToMap',
    value: function renderBeatsToMap(map) {
      var beats = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.state.beats;

      if (map && beats) {
        return _lodash2.default.map(beats.data || beats, function (item) {
          var coordinateList = item.coordinateList;
          var color = item.color;
          var displayName = item.displayName;
          var opacity = item.opacity;

          var beat = void 0;

          if (item.isSubBeat) {
            beat = gm.createPolygon(coordinateList, color, 3, opacity, color, opacity);
          } else {
            beat = gm.createPolyline(coordinateList, color, 3, opacity, 999, displayName);
          }

          gm.setToMap(beat, map);
          return beat;
        });
      }
    }
  }, {
    key: 'renderHeatlayersToMap',
    value: function renderHeatlayersToMap(map, list) {
      var formattedList = [];

      _lodash2.default.forEach(list, function (item) {
        formattedList.push(item.latitude);
        formattedList.push(item.longitude);
      });
      if (map) {
        var heatmap = gm.getHeatMapLayer(formattedList);

        gm.setToMap(heatmap, map);
        return heatmap;
      }
    }
  }, {
    key: 'renderPolygonsToMap',
    value: function renderPolygonsToMap(map, locations) {
      var _this10 = this;

      if (map) {
        var _ret4 = function () {
          var newPolygons = _lodash2.default.map(locations, _this10.createPolygon);

          // Go through 'old' markers and remove them.
          _lodash2.default.forEach(_this10.state.mapComponents.polygon, function (oldPolygon) {
            if (!_lodash2.default.includes(newPolygons, oldPolygon)) {
              gm.setToMap(oldPolygon, null);
            }
          });

          return {
            v: newPolygons
          };
        }();

        if ((typeof _ret4 === 'undefined' ? 'undefined' : _typeof(_ret4)) === "object") return _ret4.v;
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var print = this.state.print ? 'print-section' : '';
      var shouldShow = this.props.show === false ? 'none' : 'block';
      var loader = _lodash2.default.isUndefined(this.props.loaded) || this.props.loaded;

      return _react2.default.createElement(
        'div',
        { className: print },
        _react2.default.createElement(_ArmorLoader2.default, { loaded: loader }),
        _react2.default.createElement('div', { style: _lodash2.default.assign({ height: this.state.height + 'px', width: '100%', display: shouldShow }, this.props.style),
          id: this.state.mapId })
      );
    }
  }]);

  return ArmorMap;
}(_react2.default.Component);

ArmorMap.defaultProps = defaultProps;
ArmorMap.propTypes = propTypes;
exports.default = ArmorMap;