'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = require('react-bootstrap');

var _FontAwesome = require('components/global/FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

var _utils = require('utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  'list': _react.PropTypes.array.isRequired,
  'onSearch': _react.PropTypes.func.isRequired,
  'sortButtons': _react.PropTypes.element
};

var ArmorFilter = function (_React$Component) {
  _inherits(ArmorFilter, _React$Component);

  function ArmorFilter(props) {
    _classCallCheck(this, ArmorFilter);

    var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(ArmorFilter).call(this, props));

    _this.state = {
      searchString: '',
      filteredList: _this.props.list,
      isFiltered: false
    };
    _this.filterList = _this.filterList.bind(_this);
    return _this;
  }

  _createClass(ArmorFilter, [{
    key: 'filterList',
    value: function filterList(event) {
      var searchQuery = event.target.value;
      var newList = this.buildFilteredList(this.props.list, searchQuery);

      this.props.onSearch(newList);
    }
  }, {
    key: 'buildFilteredList',
    value: function buildFilteredList(list, query) {
      var result = _utils.utils.filter(list, query);

      this.setState({
        filteredList: result,
        isFiltered: true
      });
      return result;
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement("div", null, _react2.default.createElement(_reactBootstrap.Input, { onChange: this.filterList,
        type: "text",
        placeholder: "e.g John Smith...",
        addonBefore: _react2.default.createElement(_FontAwesome2.default, { icon: "search" }),
        buttonAfter: this.props.sortButtons || undefined }));
    }
  }]);

  return ArmorFilter;
}(_react2.default.Component);

ArmorFilter.propTypes = propTypes;
exports.default = ArmorFilter;