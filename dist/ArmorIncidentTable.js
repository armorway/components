'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactAddonsCssTransitionGroup = require('react-addons-css-transition-group');

var _reactAddonsCssTransitionGroup2 = _interopRequireDefault(_reactAddonsCssTransitionGroup);

var _reactBootstrap = require('react-bootstrap');

var _immutable = require('immutable');

var _immutable2 = _interopRequireDefault(_immutable);

var _Table = require('material-ui/Table');

var _ArmorIncidentRow = require('./ArmorIncidentRow');

var _ArmorIncidentRow2 = _interopRequireDefault(_ArmorIncidentRow);

var _ArmorIncidentSortArrow = require('./ArmorIncidentSortArrow');

var _ArmorIncidentSortArrow2 = _interopRequireDefault(_ArmorIncidentSortArrow);

var _ArmorIncidentLongField = require('./ArmorIncidentLongField');

var _ArmorIncidentLongField2 = _interopRequireDefault(_ArmorIncidentLongField);

var _Paper = require('material-ui/Paper');

var _Paper2 = _interopRequireDefault(_Paper);

var _ArmorKeyValueRow = require('./ArmorKeyValueRow');

var _ArmorKeyValueRow2 = _interopRequireDefault(_ArmorKeyValueRow);

var _ArmorTableFooter = require('./ArmorTableFooter');

var _ArmorTableFooter2 = _interopRequireDefault(_ArmorTableFooter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  incidents: _react.PropTypes.object.isRequired,
  printing: _react.PropTypes.bool,
  loading: _react.PropTypes.bool,
  onFindRelated: _react.PropTypes.func.isRequired
};

var defaultProps = {
  incidents: _immutable2.default.fromJS([])
};

var omittedKeys = ['mapData', 'id', 'other', 'caseNumber', 'name', 'crimeDescription', 'reportedAt', 'policeInformation', 'publicInformation', 'narrative'];

var headers = [{
  name: 'caseNumber',
  displayName: 'Case Number',
  size: '1'
}, {
  name: 'campus',
  displayName: 'Beat',
  size: '1'
},{
  name: 'building',
  displayName: 'Building',
  size: '1'
},{
  name: 'address',
  displayName: 'Address',
  size: '2'
}, {
  name: 'crimeDescription',
  displayName: 'Type',
  size: '2'
}, {
  name: 'reportedAt',
  displayName: 'Date/Time',
  size: '2'
}, {
  name: undefined,
  displayName: undefined,
  size: '1'
}];

var styles = {
  container: {
    margin: '0'
  },
  row: {
    margin: '30px auto',
    textAlign: 'center',
    minWidth: '600px'
  },
  name: {
    marginRight: '60px',
    marginBottom: '5px',
    fontWeight: '500'
  },
  caseNumber: {
    marginRight: '5px',
    fontWeight: '500'
  },
  selected: {
    background: 'rgba(224, 224, 224, .2)'
  }
};

var pageSize = 20;

var IncidentsTable = function (_React$Component) {
  _inherits(IncidentsTable, _React$Component);

  function IncidentsTable(props) {
    _classCallCheck(this, IncidentsTable);

    var _this = _possibleConstructorReturn(this, (IncidentsTable.__proto__ || Object.getPrototypeOf(IncidentsTable)).call(this, props));

    _this.state = {
      selectedCaseNumber: undefined,
      sortBy: 'caseNumber',
      sortAsc: true,
      page: 0
    };

    _this.onRowSelection = _this.onRowSelection.bind(_this);
    _this.onSearchRelated = _this.onSearchRelated.bind(_this);
    _this.onChevronClick = _this.onChevronClick.bind(_this);
    _this.onHeaderClick = _this.onHeaderClick.bind(_this);
    _this.onPageChange = _this.onPageChange.bind(_this);
    return _this;
  }

  _createClass(IncidentsTable, [{
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      return !_.isEqual(nextProps, this.props) || !_.isEqual(nextState, this.state);
    }
  }, {
    key: 'onSearchRelated',
    value: function onSearchRelated(incident) {
      this.props.onFindRelated(incident);
    }
  }, {
    key: 'onRowSelection',
    value: function onRowSelection(evt) {
      this.setState({
        selectedCaseNumber: evt.currentTarget.parentElement.dataset.cn
      });
    }
  }, {
    key: 'onChevronClick',
    value: function onChevronClick(evt, selectedCaseNumber) {
      if (selectedCaseNumber === this.state.selectedCaseNumber) {
        evt.stopPropagation();
        this.setState({ selectedCaseNumber: undefined });
      }
    }
  }, {
    key: 'onHeaderClick',
    value: function onHeaderClick(evt) {
      var newSort = evt.target.dataset.name;
      var _state = this.state;
      var currentSort = _state.sortBy;
      var sortAsc = _state.sortAsc;


      if (newSort) {
        if (currentSort === newSort) {
          this.setState({
            sortBy: newSort,
            sortAsc: !sortAsc
          });
        } else {
          this.setState({
            sortBy: newSort,
            sortAsc: true
          });
        }
      }
    }
  }, {
    key: 'onPageChange',
    value: function onPageChange(page) {
      this.setState({ page: page });
    }
  }, {
    key: 'buildHeaders',
    value: function buildHeaders() {
      var _this2 = this;

      return _.map(headers, function (header) {
        return _react2.default.createElement(
          _Table.TableHeaderColumn,
          { colSpan: header.size, 'data-name': header.name },
          header.displayName,
          _react2.default.createElement(_ArmorIncidentSortArrow2.default, {
            show: _this2.state.sortBy === header.name,
            sortAsc: _this2.state.sortAsc
          })
        );
      });
    }
  }, {
    key: 'buildIncidentRows',
    value: function buildIncidentRows() {
      var _this3 = this;

      var doesInclude = _.partial(_.includes, omittedKeys);
      var _state2 = this.state;
      var sortAsc = _state2.sortAsc;
      var sortBy = _state2.sortBy;

      // eslint-disable-next-line id-length

      var incidents = this.props.incidents.sort(function (a, b) {
        return a.get(sortBy).localeCompare(b.get(sortBy));
      });

      if (!sortAsc) {
        incidents = incidents.reverse();
      }
      return incidents.slice(this.state.page * pageSize, (this.state.page + 1) * pageSize).map(function (incident, idx) {
        var selected = _this3.state.selectedCaseNumber === incident.get('caseNumber') || _this3.props.printing;

        var row = [_react2.default.createElement(_ArmorIncidentRow2.default, {
          incident: incident,
          userSelected: selected,
          idx: idx,
          onRowSelection: _this3.onRowSelection,
          onChevronClick: _this3.onChevronClick,
          printing: _this3.props.printing
        })];

        if (selected) {
          row.push(_react2.default.createElement(
            _Table.TableRow,
            { style: styles.selected },
            _react2.default.createElement(
              _Table.TableRowColumn,
              {
                colSpan: 10,
                style: { whiteSpace: 'initial' }
              },
              _react2.default.createElement(
                _reactAddonsCssTransitionGroup2.default,
                {
                  transitionName: 'incident-detail-row-transition',
                  transitionAppear: true,
                  component: 'div',
                  style: styles.row
                },
                _react2.default.createElement(
                  _reactBootstrap.Row,
                  { style: { paddingBottom: '10px' } },
                  incident.filter(function (val, key) {
                    return !doesInclude(key);
                  }).map(function (val, key) {
                    return _react2.default.createElement(
                      _reactBootstrap.Col,
                      { xs: 12, md: 6 },
                      _react2.default.createElement(_ArmorKeyValueRow2.default, {
                        value: val,
                        formatValues: false,
                        formatKeys: false,
                        keyData: _.words(key).map(_.upperFirst).join(' '),
                        smallKey: true
                      })
                    );
                  }),
                  _react2.default.createElement(_ArmorIncidentLongField2.default, {
                    name: 'Police Information',
                    text: incident.get('policeInformation'),
                    printing: _this3.props.printing
                  }),
                  _react2.default.createElement(_ArmorIncidentLongField2.default, {
                    name: 'Public Information',
                    text: incident.get('publicInformation'),
                    printing: _this3.props.printing
                  }),
                  _react2.default.createElement(_ArmorIncidentLongField2.default, {
                    name: 'Narrative',
                    text: incident.get('narrative'),
                    printing: _this3.props.printing
                  })
                )
              )
            )
          ));
        }

        return row;
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var incidents = this.props.incidents;


      if (this.props.loading) {
        return _react2.default.createElement(
          'h3',
          null,
          'loading'
        );
      }

      return _react2.default.createElement(
        _reactBootstrap.Row,
        { style: styles.container },
        _react2.default.createElement(
          _reactBootstrap.Col,
          { xs: 12 },
          _react2.default.createElement(
            _Paper2.default,
            { zDepth: 1 },
            _react2.default.createElement(
              _Table.Table,
              { selectable: false },
              _react2.default.createElement(
                _Table.TableHeader,
                { adjustForCheckbox: false, displaySelectAll: false },
                _react2.default.createElement(
                  _Table.TableRow,
                  { onClick: this.onHeaderClick },
                  this.buildHeaders()
                )
              ),
              _react2.default.createElement(
                _Table.TableBody,
                { displayRowCheckbox: false, showRowHover: true },
                incidents.size ? this.buildIncidentRows() : _react2.default.createElement(
                  _Table.TableRow,
                  { className: 'no-results' },
                  _react2.default.createElement(
                    _Table.TableRowColumn,
                    null,
                    _react2.default.createElement(
                      'h4',
                      null,
                      'No incidents available'
                    )
                  )
                )
              ),
              _react2.default.createElement(
                _Table.TableFooter,
                null,
                _react2.default.createElement(_ArmorTableFooter2.default, {
                  pageSize: pageSize,
                  onChange: this.onPageChange,
                  numPages: Math.ceil(this.props.incidents.size / pageSize)
                })
              )
            )
          )
        )
      );
    }
  }]);

  return IncidentsTable;
}(_react2.default.Component);

IncidentsTable.defaultProps = defaultProps;
IncidentsTable.propTypes = propTypes;
exports.default = IncidentsTable;