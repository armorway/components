'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _reactSelect = require('react-select');

var _reactSelect2 = _interopRequireDefault(_reactSelect);

var _AppStore = require('stores/global/AppStore');

var _AppStore2 = _interopRequireDefault(_AppStore);

var _OSEStore = require('components/ose/OSEStore');

var _OSEStore2 = _interopRequireDefault(_OSEStore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  value: _react.PropTypes.any,
  onChange: _react.PropTypes.func
};

var ArmorWatchSelect = function (_React$Component) {
  _inherits(ArmorWatchSelect, _React$Component);

  function ArmorWatchSelect(props) {
    _classCallCheck(this, ArmorWatchSelect);

    var _this = _possibleConstructorReturn(this, (ArmorWatchSelect.__proto__ || Object.getPrototypeOf(ArmorWatchSelect)).call(this, props));

    _this.state = _lodash2.default.assign({}, _OSEStore2.default.getState(), _AppStore2.default.getState().watch.data);
    _this.onUpdate = _this.onUpdate.bind(_this);
    return _this;
  }

  _createClass(ArmorWatchSelect, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _OSEStore2.default.listen(this.onUpdate);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      _OSEStore2.default.unlisten(this.onUpdate);
    }
  }, {
    key: 'onUpdate',
    value: function onUpdate(newState) {
      this.setState({ watch: newState.watch });
    }
  }, {
    key: 'getWatchOptions',
    value: function getWatchOptions() {
      return this.formatWatch(this.state.watch || []);
    }
  }, {
    key: 'formatWatch',
    value: function formatWatch(watches) {
      return _lodash2.default.map(watches, function (watch) {
        return {
          label: watch,
          value: watch
        };
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(_reactSelect2.default, {
        className: 'watch-dropdown',
        name: 'watch-dropdown',
        placeholder: 'Select Watch',
        value: this.props.value,
        options: this.getWatchOptions(),
        onChange: this.props.onChange
      });
    }
  }]);

  return ArmorWatchSelect;
}(_react2.default.Component);

ArmorWatchSelect.propTypes = propTypes;
exports.default = ArmorWatchSelect;