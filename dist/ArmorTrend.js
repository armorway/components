'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _FontAwesome = require('components/global/FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  data: _react.PropTypes.any.isRequired
};

var ArmorTrend = function (_React$Component) {
  _inherits(ArmorTrend, _React$Component);

  function ArmorTrend(props) {
    _classCallCheck(this, ArmorTrend);

    var _this = _possibleConstructorReturn(this, (ArmorTrend.__proto__ || Object.getPrototypeOf(ArmorTrend)).call(this, props));

    _this.state = {
      trend: props.data[0],
      variance: props.data[1],
      positiveTrend: _react2.default.createElement(_FontAwesome2.default, { icon: 'arrow-up', padIcon: true, style: { color: 'red' } }),
      negativeTrend: _react2.default.createElement(_FontAwesome2.default, { icon: 'arrow-down', padIcon: true, style: { color: 'green' } }),
      neutralTrend: _react2.default.createElement(_FontAwesome2.default, { icon: 'minus', padIcon: true, style: { color: 'black' } })
    };
    _this.buildIcon = _this.buildIcon.bind(_this);
    return _this;
  }

  _createClass(ArmorTrend, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      this.setState({
        trend: nextProps.data[0],
        variance: nextProps.data[1]
      });
    }
  }, {
    key: 'buildIcon',
    value: function buildIcon() {
      var icon = this.state.neutralTrend;

      if (this.state.variance > 0) {
        icon = this.state.positiveTrend;
      } else if (this.state.variance < 0) {
        icon = this.state.negativeTrend;
      }
      return icon;
    }
  }, {
    key: 'buildLabel',
    value: function buildLabel() {
      var label = '';

      if (this.state.variance > -999 && this.state.variance < 999 && this.state.variance !== 0) {
        label = this.state.variance + '%';
      }
      return label;
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'span',
        null,
        this.buildIcon(),
        ' ',
        this.buildLabel()
      );
    }
  }]);

  return ArmorTrend;
}(_react2.default.Component);

ArmorTrend.propTypes = propTypes;
exports.default = ArmorTrend;