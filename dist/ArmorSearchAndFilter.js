'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ArmorFilterList = require('./ArmorFilterList');

var _ArmorFilterList2 = _interopRequireDefault(_ArmorFilterList);

var _FontAwesome = require('./FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

var _reactBootstrap = require('react-bootstrap');

require('../styles/ArmorSearchAndFilter');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  /**
   * Function to execute when a filter remove is clicked.
   */
  onFilterRemove: _react.PropTypes.func.isRequired,
  /**
   * Function to execute when a search field is typed in.
   */
  onSearch: _react.PropTypes.func.isRequired,
  /**
   * Array of applied filters to build into ArmorFilterListItems.
   */
  filters: _react.PropTypes.array.isRequired,
  /**
   * Bootstrap type to apply to filter labels. Default: primary
   */
  bsStyle: _react.PropTypes.string,
  /**
   * class to be applied.
   */
  className: _react.PropTypes.string,
  /**
   * Style to be applied.
   */
  style: _react.PropTypes.object
};

/**
 * ArmorFilterList is used to display applied filters.
 */

var ArmorSearchAndFilter = function (_React$Component) {
  _inherits(ArmorSearchAndFilter, _React$Component);

  function ArmorSearchAndFilter() {
    _classCallCheck(this, ArmorSearchAndFilter);

    var _this = _possibleConstructorReturn(this, (ArmorSearchAndFilter.__proto__ || Object.getPrototypeOf(ArmorSearchAndFilter)).call(this));

    _this.state = {
      show: false
    };

    _this.onFilterRemoveClick = _this.onFilterRemoveClick.bind(_this);
    _this.onSearch = _this.onSearch.bind(_this);
    return _this;
  }

  _createClass(ArmorSearchAndFilter, [{
    key: 'onFilterRemoveClick',
    value: function onFilterRemoveClick(type, name) {
      this.props.onFilterRemove(type, name);
    }
  }, {
    key: 'onSearch',
    value: function onSearch(evt) {
      this.props.onSearch(evt.target.value);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        _reactBootstrap.Row,
        { style: this.props.style, className: this.props.className + ' search-filter-list' },
        _react2.default.createElement(
          _reactBootstrap.Col,
          { md: 4, xs: 6 },
          _react2.default.createElement(_reactBootstrap.Input, {
            type: 'text',
            placeholder: 'Search',
            addonBefore: _react2.default.createElement(_FontAwesome2.default, { icon: 'search' }),
            onChange: this.onSearch,
            className: 'armor-filter-search-field'
          })
        ),
        _react2.default.createElement(
          _reactBootstrap.Col,
          { md: 8, xs: 6 },
          _react2.default.createElement(_ArmorFilterList2.default, {
            filters: this.props.filters,
            bsStyle: this.props.bsStyle,
            onFilterRemove: this.props.onFilterRemove
          })
        )
      );
    }
  }]);

  return ArmorSearchAndFilter;
}(_react2.default.Component);

ArmorSearchAndFilter.propTypes = propTypes;
exports.default = ArmorSearchAndFilter;