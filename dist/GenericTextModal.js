'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = require('react-bootstrap');

var _AppActions = require('lib/actions/AppActions');

var _AppActions2 = _interopRequireDefault(_AppActions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  /*
    Title to be displayed at top of modal
  */
  title: _react.PropTypes.string,
  /**
   * Content to be displayed within the modal body
   */
  body: _react.PropTypes.string
};

/**
  * GenericModal is a simple modal designed to display text and a body.
  * it is designed to replace constantly making simple and cookie cutter modals.
  * Use it just like you would any modal in AppActions.toggleModal
  */

var GenericTextModal = function (_React$Component) {
  _inherits(GenericTextModal, _React$Component);

  function GenericTextModal(props) {
    _classCallCheck(this, GenericTextModal);

    var _this = _possibleConstructorReturn(this, (GenericTextModal.__proto__ || Object.getPrototypeOf(GenericTextModal)).call(this, props));

    _this.onClose = _this.onClose.bind(_this);
    return _this;
  }

  _createClass(GenericTextModal, [{
    key: 'onClose',
    value: function onClose() {
      _AppActions2.default.toggleModal({ showModal: false });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          _reactBootstrap.Modal.Header,
          { closeButton: true, onHide: this.onClose },
          _react2.default.createElement(
            _reactBootstrap.Modal.Title,
            null,
            this.props.title
          )
        ),
        _react2.default.createElement(
          _reactBootstrap.Modal.Body,
          null,
          this.props.body
        )
      );
    }
  }]);

  return GenericTextModal;
}(_react2.default.Component);

GenericTextModal.propTypes = propTypes;
exports.default = GenericTextModal;