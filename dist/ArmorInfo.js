'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _IconButton = require('material-ui/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Popover = require('material-ui/Popover');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  title: _react.PropTypes.any,
  children: _react.PropTypes.any
};
var anchorOrigin = {
  horizontal: 'right',
  vertical: 'center'
};
var targetOrigin = {
  horizontal: 'left',
  vertical: 'center'
};

var styles = {
  icon: {
    fontSize: '18px'
  },
  popover: {
    padding: '5px 20px',
    width: '300px'
  }
};

var ArmorInfo = function (_React$Component) {
  _inherits(ArmorInfo, _React$Component);

  function ArmorInfo(props) {
    _classCallCheck(this, ArmorInfo);

    var _this = _possibleConstructorReturn(this, (ArmorInfo.__proto__ || Object.getPrototypeOf(ArmorInfo)).call(this, props));

    _this.state = {
      open: false,
      anchorEl: undefined
    };

    _this.showPopover = _this.showPopover.bind(_this);
    _this.closePopover = _this.closePopover.bind(_this);
    return _this;
  }

  _createClass(ArmorInfo, [{
    key: 'showPopover',
    value: function showPopover(evt) {
      this.setState({
        anchorEl: evt.currentTarget,
        open: !this.state.open
      });
    }
  }, {
    key: 'closePopover',
    value: function closePopover() {
      this.setState({ open: false });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'span',
        null,
        _react2.default.createElement(_IconButton2.default, {
          iconStyle: styles.icon,
          onClick: this.showPopover,
          iconClassName: 'fa fa-question-circle'
        }),
        _react2.default.createElement(
          _Popover.Popover,
          {
            open: this.state.open,
            anchorEl: this.state.anchorEl,
            anchorOrigin: anchorOrigin,
            targetOrigin: targetOrigin,
            onRequestClose: this.closePopover,
            animation: _Popover.PopoverAnimationVertical
          },
          _react2.default.createElement(
            'div',
            { style: styles.popover },
            _react2.default.createElement(
              'h5',
              null,
              this.props.title
            ),
            this.props.children
          )
        )
      );
    }
  }]);

  return ArmorInfo;
}(_react2.default.Component);

ArmorInfo.propTypes = propTypes;
exports.default = ArmorInfo;