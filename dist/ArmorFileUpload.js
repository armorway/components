'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = require('react-bootstrap');

var _reactDropzone = require('react-dropzone');

var _reactDropzone2 = _interopRequireDefault(_reactDropzone);

var _FontAwesome = require('./FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

var _reactLoader = require('react-loader');

var _reactLoader2 = _interopRequireDefault(_reactLoader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var defaultProps = {
  multiple: true,
  loaded: true,
  showAttachedFiles: true
};

var propTypes = {
  onFilesAttached: _react.PropTypes.func,
  onUpload: _react.PropTypes.func,
  showAttachedFiles: _react.PropTypes.bool,
  loaded: _react.PropTypes.bool,
  multiple: _react.PropTypes.bool,
  style: _react.PropTypes.object
};

var ArmorFileUpload = function (_React$Component) {
  _inherits(ArmorFileUpload, _React$Component);

  function ArmorFileUpload() {
    _classCallCheck(this, ArmorFileUpload);

    var _this = _possibleConstructorReturn(this, (ArmorFileUpload.__proto__ || Object.getPrototypeOf(ArmorFileUpload)).call(this));

    _this.state = { files: [] };
    _this.onDrop = _this.onDrop.bind(_this);
    _this.buildFileList = _this.buildFileList.bind(_this);
    _this.buildFileNames = _this.buildFileNames.bind(_this);
    _this.buildDropzone = _this.buildDropzone.bind(_this);
    _this.buildLoader = _this.buildLoader.bind(_this);
    _this.onUploadFile = _this.onUploadFile.bind(_this);
    return _this;
  }

  _createClass(ArmorFileUpload, [{
    key: 'onDrop',
    value: function onDrop(files) {
      this.setState({ files: files });
      this.props.onFilesAttached(this.props.multiple ? files : files[0]);
    }
  }, {
    key: 'onUploadFile',
    value: function onUploadFile() {
      var files = this.props.multiple ? this.state.files : this.state.files[0];

      this.props.onUpload(files);
    }
  }, {
    key: 'buildFileNames',
    value: function buildFileNames() {
      return _.map(this.state.files, function (file, key) {
        return _react2.default.createElement(
          'li',
          { key: key },
          _react2.default.createElement(
            'span',
            null,
            file.name
          ),
          _react2.default.createElement(
            'span',
            { style: { marginLeft: '5px' } },
            (file.size / 1000).toFixed(2) + 'KB'
          )
        );
      });
    }
  }, {
    key: 'buildFileList',
    value: function buildFileList() {
      return _react2.default.createElement(
        'div',
        null,
        this.state.files.length > 0 ? _react2.default.createElement(
          'h5',
          null,
          this.props.multiple ? 'Files' : 'File',
          ' Chosen'
        ) : '',
        _react2.default.createElement(
          'ul',
          null,
          this.buildFileNames()
        )
      );
    }
  }, {
    key: 'buildButton',
    value: function buildButton() {
      var _this2 = this;

      if (this.state.files.length > 0) {
        return _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement(
            _reactBootstrap.Button,
            { bsStyle: 'success',
              disabled: !this.props.loaded,
              style: { marginTop: '5px' },
              onClick: this.onUploadFile },
            _react2.default.createElement(_FontAwesome2.default, { icon: 'upload', title: 'Upload!', padIcon: true })
          ),
          _react2.default.createElement(
            _reactBootstrap.Button,
            { bsStyle: 'danger',
              disabled: !this.props.loaded,
              style: { marginTop: '5px', marginLeft: '10px' },
              onClick: function onClick() {
                _this2.setState({ files: [] });
              } },
            _react2.default.createElement(_FontAwesome2.default, { icon: 'close', title: 'Cancel', padIcon: true })
          )
        );
      }
    }
  }, {
    key: 'buildDropzone',
    value: function buildDropzone() {
      return _react2.default.createElement(
        _reactDropzone2.default,
        { style: this.props.style, onDrop: this.onDrop, className: 'dropzone', multiple: this.props.multiple },
        _react2.default.createElement(_FontAwesome2.default, { icon: 'paperclip', padIcon: true }),
        _react2.default.createElement(
          'span',
          { className: 'attach-label' },
          'Drag files or ',
          _react2.default.createElement(
            'a',
            null,
            'Browse'
          )
        )
      );
    }
  }, {
    key: 'buildLoader',
    value: function buildLoader() {
      return _react2.default.createElement(
        'div',
        { style: { position: 'relative', paddingBottom: '15px' }, className: 'dropzone' },
        _react2.default.createElement(_reactLoader2.default, { loaded: this.props.loaded })
      );
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { id: 'armorFileUpload' },
        _react2.default.createElement(
          _reactBootstrap.Row,
          null,
          _react2.default.createElement(
            _reactBootstrap.Col,
            { xs: 12 },
            this.props.loaded ? this.buildDropzone() : this.buildLoader()
          )
        ),
        _react2.default.createElement(
          _reactBootstrap.Row,
          { style: { marginTop: '5px' } },
          _react2.default.createElement(
            _reactBootstrap.Col,
            { xs: 6 },
            this.props.showAttachedFiles ? this.buildFileList() : ''
          ),
          _react2.default.createElement(
            _reactBootstrap.Col,
            { xs: 4, xsOffset: 2 },
            _react2.default.createElement(
              'div',
              { className: 'pull-right' },
              this.props.onUpload ? this.buildButton() : ''
            )
          )
        )
      );
    }
  }]);

  return ArmorFileUpload;
}(_react2.default.Component);

ArmorFileUpload.defaultProps = defaultProps;
ArmorFileUpload.propTypes = propTypes;
exports.default = ArmorFileUpload;