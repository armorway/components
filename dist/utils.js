'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.utils = exports.siteCode = exports.dateFormat = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /* eslint-disable */


var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

require('../styles/print');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// import * as encrypt from 'services/encrypt';
var dateFormat = 'YYYY-MM-DD';
var timeFormat = 'HH-MM-SS';
var siteCode = 'upc';

var Utils = function () {
  function Utils() {
    _classCallCheck(this, Utils);
  }

  _createClass(Utils, [{
    key: 'setLogoUrl',
    value: function setLogoUrl(url) {
      this.logoUrl = url;
    }
  }, {
    key: 'hashPrefix',
    value: function hashPrefix(href) {
      if (href[0] === '#') {
        return href;
      }
      return '#' + href;
    }
  }, {
    key: 'booleanToYesNo',
    value: function booleanToYesNo(bool) {
      if (bool) {
        return 'Yes';
      }
      return 'No';
    }

    // hashPassword(password) {
    //   return encrypt.calcMD5(password);
    // }

  }, {
    key: 'filter',
    value: function filter(list, query) {
      if (!query) {
        return list;
      }
      return list.filter(function (item) {
        var title = item.title.toLowerCase();
        var searchQuery = query.toLowerCase();

        return _lodash2.default.includes(title, searchQuery);
      });
    }
  }, {
    key: 'search',
    value: function search(searchString, dataset) {
      if (!searchString) {
        return dataset;
      }
      var lowerSearch = searchString.toLowerCase();

      return _lodash2.default.filter(dataset, function (data) {
        return _lodash2.default.includes(_lodash2.default.values(data).join('').toLowerCase(), lowerSearch);
      });
    }
  }, {
    key: 'searchImmutable',
    value: function searchImmutable(string, list) {
      return list.filter(function (itm) {
        return itm.valueSeq().join('').toLowerCase().includes(string.toLowerCase());
      });
    }
  }, {
    key: 'getDate',
    value: function getDate() {
      return (0, _moment2.default)().format('MMMM Do, YYYY');
    }
  }, {
    key: 'getAMorPM',
    value: function getAMorPM(currentHour) {
      var hour = (typeof currentHour === 'undefined' ? 'undefined' : _typeof(currentHour)) !== undefined ? currentHour : (0, _moment2.default)().hour();

      return hour >= 12 ? 'PM' : 'AM';
    }
  }, {
    key: 'downloadAsCSV',
    value: function downloadAsCSV(JSONData, reportTitle) {
      if ((typeof JSONData === 'undefined' ? 'undefined' : _typeof(JSONData)) === undefined) {
        return;
      }

      var link = document.createElement('a');
      var arrData = !_lodash2.default.isObject(JSONData) ? JSON.parse(JSONData) : JSONData;
      var CSV = [_lodash2.default.keys(arrData[0]).join(',')];

      CSV = CSV.concat(_lodash2.default.map(arrData, function (item) {
        return _lodash2.default.values(item).join(',');
      })).join('\r\n');

      link.href = 'data:text/xls;charset=utf-8,' + encodeURI(CSV);
      link.style.visibility = 'hidden';
      link.download = 'Report_' + reportTitle.replace(/ /g, '_') + '.csv';

      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }, {
    key: 'createElement',
    value: function createElement(type, attributes) {
      var element = document.createElement(type);

      _lodash2.default.forEach(attributes, function (value, attribute) {
        element[attribute] = value;
      });
      return element;
    }
  }, {
    key: 'camelToReadable',
    value: function camelToReadable(str, makeCamelCase) {
      var string = str;

      if (makeCamelCase) {
        string = _lodash2.default.camelCase(str);
      }
      return string.replace(/([A-Z])/g, ' $1').replace(/^./, function (str2) {
        return str2.toUpperCase();
      });
    }
  }, {
    key: 'getHotspotRadius',
    value: function getHotspotRadius(events, minCount, maxCount) {
      var count = events.length;
      var min = 60;
      var max = 120;
      var maxSum = maxCount;

      if (maxCount === minCount) {
        maxSum = minCount + 1;
      }

      return (count - minCount) / (maxSum - minCount) * (max - min) + min;
    }
  }, {
    key: 'createUniqueIds',
    value: function createUniqueIds(list) {
      _lodash2.default.forEach(list, function (item, index) {
        if (!item.id) {
          item.id = index;
        } else {
          return;
        }
      });
    }
  }, {
    key: 'containsNoFalsyValues',
    value: function containsNoFalsyValues(data) {
      return _lodash2.default.chain(data).values().every(function (val) {
        return !!_lodash2.default.trim(val);
      }).value();
    }
  }, {
    key: 'generateHash',
    value: function generateHash() {
      var min = 65; // a
      var max = 90; // z
      var numbers = _lodash2.default.map(new Array(10), function () {
        return parseInt(Math.random() * (max - min) + min, 10);
      });

      return String.fromCharCode.apply(this, numbers);
    }
  }, {
    key: 'toQueryString',
    value: function toQueryString(obj) {
      return _lodash2.default.map(obj, function (value, key) {
        return [key, value].join('=');
      }).join('&');
    }
  }, {
    key: 'getActiveView',
    value: function getActiveView() {
      return location.hash.split('/').pop().split('?')[0];
    }
  }, {
    key: 'toReadableList',
    value: function toReadableList(list) {
      var _this = this;

      return _lodash2.default.map(list, function (item) {
        return _this.toCrimeType(item);
      });
    }
  }, {
    key: 'getPieData',
    value: function getPieData(list, type) {
      var _this2 = this;

      var result = [];

      _lodash2.default.forEach(list, function (value, key) {
        if (_lodash2.default.isArray(value)) {
          if (type === 'frequency') {
            result.push(value.length || 0);
          } else if (type === 'label' && key.length) {
            result.push(_this2.toCrimeType(key));
          }
        }
      });
      return result;
    }
  }, {
    key: 'calculatePercentChange',
    value: function calculatePercentChange(curCount, prevCount) {
      var result = void 0;

      if (prevCount === 0) {
        result = 0;
      } else if (curCount === 0) {
        result = -100;
      } else {
        result = Number(Math.round((curCount * 1.0 - prevCount * 1.0) * (100 * 1.0) / (prevCount * 1.0)));
      }
      return result;
    }
  }, {
    key: 'calculateSumOfSubarrays',
    value: function calculateSumOfSubarrays(lists) {
      var sum = 0;

      _lodash2.default.forEach(lists, function (list) {
        if (list.length) {
          sum += list.length;
        }
      });
      return sum;
    }
  }, {
    key: 'getNow',
    value: function getNow() {
      return (0, _moment2.default)().format(dateFormat);
    }
  }, {
    key: 'getEndTime',
    value: function getEndTime(selection) {
      var result = '';
      var now = (0, _moment2.default)();

      switch (selection) {
        case '7Days':
          result = now.subtract(1, 'weeks');break;
        case '14Days':
          result = now.subtract(2, 'weeks');break;
        case '1Month':
          result = now.subtract(1, 'months');break;
        case '1Year':
          result = now.subtract(1, 'years');break;
        default:
          result = now.subtract(1, 'weeks');
      }

      return result.format(dateFormat);
    }
  }, {
    key: 'buildTrendIcon',
    value: function buildTrendIcon(trendValue, positiveTrend, negativeTrend, neutralTrend) {
      var icon = void 0;

      if (trendValue === 0) {
        return neutralTrend;
      } else if (trendValue > 0) {
        icon = positiveTrend;
      } else {
        icon = negativeTrend;
      }
      return icon;
    }
  }, {
    key: 'formatTime',
    value: function formatTime(time) {
      return time.split(':').splice(0, 2).join(':') || [];
    }
  }, {
    key: 'formatForDropdown',
    value: function formatForDropdown(data) {
      function ddMap(val) {
        return { label: val, value: val };
      }

      return _lodash2.default.chain(data).map(ddMap).value();
    }
  }, {
    key: 'createQuery',
    value: function createQuery(end, start, beats, watch, crimeType) {
      return this.toQueryString({
        crimeType: crimeType,
        beats: beats,
        watch: watch,
        startDate: start,
        endDate: end
      });
    }
  }, {
    key: 'createSimpleQuery',
    value: function createSimpleQuery(end, start, watch) {
      return this.toQueryString({
        watch: watch,
        startDate: start,
        endDate: end
      });
    }
  }, {
    key: 'createDateQuery',
    value: function createDateQuery(end, start) {
      return this.toQueryString({
        startDate: start,
        endDate: end
      });
    }
  }, {
    key: 'calcTrend',
    value: function calcTrend(current, prev, prev2) {
      return current - Math.min(current, prev, prev2);
    }
  }, {
    key: 'calcVariance',
    value: function calcVariance(current, prev) {
      var result = 0;

      if (current !== 0) {
        result = Math.floor((current - prev) / current * 100);
      }
      return result;
    }
  }, {
    key: 'toCrimeType',
    value: function toCrimeType(key) {
      return key;
    }
  }, {
    key: 'fromCrimeType',
    value: function fromCrimeType(str) {
      return str;
    }
  }, {
    key: 'transposeKeys',
    value: function transposeKeys(keys, targ, keyName) {
      var _this3 = this;

      var target = _lodash2.default.clone(targ);
      var sourceData = target[keyName];

      target[keyName] = _lodash2.default.chain(keys).map(function (key) {
        return [_this3.camelToReadable(key.name), sourceData[key.id]];
      }).zipObject().value();
      return target;
    }
  }, {
    key: 'getPrintHeader',
    value: function getPrintHeader() {
      var _this4 = this;

      var header = document.createElement('div');
      var logo = new Image();
      var timeStamp = document.createElement('div');
      var optionalBody = document.createElement('div');

      timeStamp.innerHTML = '<h5>' + (0, _moment2.default)().format('hh:mmA MM/DD/YYYY') + '</h5>\n     <h6>' + this.getActiveView() + '</h6>';

      timeStamp.classList.add('time-stamp-print');
      header.classList.add('header-print');

      logo.setAttribute('width', '200px');
      var promi = new Promise(function (res, rej) {
        logo.onload = function () {
          res(header);
        };
        logo.src = _this4.logoUrl;
      });

      header.appendChild(logo);
      header.appendChild(timeStamp);
      return promi;
    }

    /**
     * Expects the following options list
     * [
     *  '<h3>Filters: Beats: A, B, C</h3>',
     *  'Current view of the page',
     *  '200 Entities',
     * ]
     */

  }, {
    key: 'print',
    value: function print(selector) {
      var _this5 = this;

      var isMap = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';

      return new Promise(function (res, rej) {
        _this5.getPrintHeader().then(function (header) {
          var clonedNode = void 0;
          var optionsNode = void 0;
          var app = document.getElementById('app');
          var modal = document.getElementById('genericModal');
          var leaflet = document.querySelector(selector);
          var leafletParent = leaflet.parentNode;

          if (options) {
            optionsNode = document.createElement('span');
            optionsNode.classList.add('options-print');
            optionsNode.innerHTML = options;
          }

          app.style.display = 'none';
          if (isMap) {
            clonedNode = document.querySelector(selector);
            leaflet.classList.add('map-print');
          } else {
            clonedNode = document.querySelector(selector).cloneNode(true);
          }
          if (modal) {
            modal.style.display = 'none';
          }
          clonedNode.style.marginTop = '65px';
          document.body.appendChild(header);

          if (options) {
            document.body.appendChild(optionsNode);
          }

          document.body.appendChild(clonedNode);
          window.print();
          app.style.display = 'block';
          if (modal) {
            modal.style.display = 'block';
          }
          document.body.removeChild(header);
          if (isMap) {
            leafletParent.appendChild(clonedNode);
            leaflet.classList.remove('map-print');
          } else {
            document.body.removeChild(clonedNode);
          }
          document.body.removeChild(optionsNode);
          res();
        });
      });
    }
  }, {
    key: 'calculateFitHeight',
    value: function calculateFitHeight(windowHeight, topSpacing) {
      var bottomSpacing = 10;

      return windowHeight - topSpacing - bottomSpacing;
    }
  }, {
    key: 'dateToReadable',
    value: function dateToReadable(date) {
      var result = (0, _moment2.default)(date, dateFormat).format(dateFormat);

      if (_lodash2.default.isNumber(date)) {
        result = (0, _moment2.default)(date).format(dateFormat);
      }
      return result;
    }
  }, {
    key: 'toDateQuery',
    value: function toDateQuery(date) {
      var formattedDate = date.format('MM/DD/YYYY');

      return this.toQueryString({ date: formattedDate });
    }
  }, {
    key: 'trimToNumber',
    value: function trimToNumber(value) {
      var num = parseInt(value.replace(/\D/g, ''), 10);

      if (!_lodash2.default.isNaN(num)) {
        return num;
      } else if (_lodash2.default.includes([0, 1], value.length)) {
        return undefined;
      }
      return undefined;
    }
  }, {
    key: 'toReadableTime',
    value: function toReadableTime(time) {
      return (0, _moment2.default)(time).format(timeFormat);
    }
  }, {
    key: 'formatDuration',
    value: function formatDuration(date) {
      var duration = (0, _moment2.default)((0, _moment2.default)() - (0, _moment2.default)(date)).format('m');
      var label = void 0;

      switch (Number(duration)) {
        case 0:
          {
            label = 'Less than a minute ago';
            break;
          }
        case 1:
          {
            label = 'a minute go';
            break;
          }
        default:
          {
            label = duration + ' minutes ago';
            break;
          }
      }

      return label;
    }
  }, {
    key: 'immutableToJS',
    value: function immutableToJS(data) {
      var result = data;

      if (data instanceof Immutable.List || data instanceof Immutable.Map) {
        result = data.toJS();
      }
      return result;
    }
  }, {
    key: 'getTimezoneOffset',
    value: function getTimezoneOffset() {
      return -new Date().getTimezoneOffset();
    }
  }, {
    key: 'formatDate',
    value: function formatDate(rawDate) {
      var format = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'MMM DD, YYYY hh:mm a';
      var offset = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

      var result = (0, _moment2.default)(parseInt(rawDate));

      if (offset) {
        result.utcOffset(this.getTimezoneOffset());
      }
      return result.format(format);
    }
  }]);

  return Utils;
}();

var inst = new Utils();

exports.default = Utils;
exports.dateFormat = dateFormat;
exports.siteCode = siteCode;
exports.utils = inst;