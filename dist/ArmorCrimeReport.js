'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = require('react-bootstrap');

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  report: _react.PropTypes.object
};

var ArmorCrimeReport = function (_React$Component) {
  _inherits(ArmorCrimeReport, _React$Component);

  function ArmorCrimeReport() {
    _classCallCheck(this, ArmorCrimeReport);

    var _this = _possibleConstructorReturn(this, (ArmorCrimeReport.__proto__ || Object.getPrototypeOf(ArmorCrimeReport)).call(this));

    _this.state = {
      policeReport: false,
      publicReport: false
    };
    _this.onExpandReport = _this.onExpandReport.bind(_this);
    _this.buildLine = _this.buildLine.bind(_this);
    _this.buildReportContainer = _this.buildReportContainer.bind(_this);
    return _this;
  }

  _createClass(ArmorCrimeReport, [{
    key: 'onExpandReport',
    value: function onExpandReport(event) {
      var key = event.target.dataset.key;

      this.setState(_defineProperty({}, key, !this.state[key]));
    }
  }, {
    key: 'buildReport',
    value: function buildReport(type, reportParam) {
      var report = '';

      if (_.includes(['policeReport', 'publicReport'], type)) {
        report = reportParam;
      }
      return report;
    }
  }, {
    key: 'buildReportContainer',
    value: function buildReportContainer(reportKey, report) {
      if (report) {
        var isShortReport = report.length < 300;
        var shortReport = _.first(report.split(','));

        return _react2.default.createElement(
          'span',
          null,
          !this.state[reportKey] ? _react2.default.createElement(
            'span',
            null,
            isShortReport ? report : shortReport
          ) : '',
          _react2.default.createElement(
            'p',
            { style: { display: 'inline' } },
            this.state[reportKey] && !isShortReport ? this.buildReport(reportKey, report) : ''
          ),
          !isShortReport ? _react2.default.createElement(
            'a',
            { style: { marginLeft: '5px', cursor: 'pointer' },
              'data-key': reportKey,
              onClick: this.onExpandReport
            },
            this.state[reportKey] ? 'show less' : 'show more'
          ) : ''
        );
      }
    }
  }, {
    key: 'buildLine',
    value: function buildLine(label, value) {
      var result = value;

      if (value && value.isArray) {
        result = value.join(',');
      }
      return _react2.default.createElement(
        'p',
        null,
        _react2.default.createElement(
          'strong',
          { style: { marginRight: '5px' } },
          label
        ),
        _utils.utils.camelToReadable(result, true)
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _props$report = this.props.report;
      var address = _props$report.address;
      var beat = _props$report.beat;
      var building = _props$report.building;
      var listValue = _props$report.listValue;
      var crimeClass = _props$report.crimeClass;
      var crimeDescription = _props$report.crimeDescription;
      var dateReported = _props$report.dateReported;
      var streetName = _props$report.streetName;
      var timeReported = _props$report.timeReported;
      var policeInformation = _props$report.policeInformation;
      var publicInformation = _props$report.publicInformation;
      var caseNbr = _props$report.caseNbr;


      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          _reactBootstrap.Row,
          null,
          _react2.default.createElement(
            _reactBootstrap.Col,
            { md: 6 },
            this.buildLine('Case Number:', caseNbr),
            this.buildLine('Address:', address + ' ' + streetName),
            this.buildLine('Date Reported:', dateReported),
            this.buildLine('Location:', building),
            this.buildLine('Crime Class:', crimeClass),
            this.buildLine('Crime Description:', crimeDescription)
          ),
          _react2.default.createElement(
            _reactBootstrap.Col,
            { md: 6 },
            this.buildLine('Beat:', beat),
            this.buildLine('Building:', building),
            this.buildLine('Time Reported:', timeReported),
            this.buildLine('List Value:', listValue)
          )
        ),
        _react2.default.createElement(
          _reactBootstrap.Row,
          null,
          _react2.default.createElement(
            _reactBootstrap.Col,
            { md: 12 },
            _react2.default.createElement(
              'div',
              { style: { marginBottom: '10px' } },
              _react2.default.createElement(
                'span',
                null,
                _react2.default.createElement(
                  'strong',
                  null,
                  'Police Report:'
                ),
                ' ',
                this.buildReportContainer('policeReport', policeInformation)
              )
            ),
            _react2.default.createElement(
              'div',
              null,
              _react2.default.createElement(
                'span',
                null,
                _react2.default.createElement(
                  'strong',
                  null,
                  'Public Report:'
                ),
                ' ',
                this.buildReportContainer('publicReport', publicInformation)
              )
            )
          )
        )
      );
    }
  }]);

  return ArmorCrimeReport;
}(_react2.default.Component);

ArmorCrimeReport.propTypes = propTypes;
exports.default = ArmorCrimeReport;