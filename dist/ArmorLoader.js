'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _CircularProgress = require('material-ui/CircularProgress');

var _CircularProgress2 = _interopRequireDefault(_CircularProgress);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  loaded: _react.PropTypes.bool.isRequired,
  style: _react.PropTypes.object
};

var overlay = {
  position: 'absolute',
  width: '100%',
  height: '100%',
  zIndex: '99999',
  left: '0px'
};

var loaderStyle = {
  position: 'fixed',
  zIndex: '999',
  height: '2em',
  width: '2em',
  overflow: 'show',
  margin: 'auto',
  top: '0px',
  left: '0px',
  bottom: '0px',
  right: '0px'
};

var ArmorLoader = function (_React$Component) {
  _inherits(ArmorLoader, _React$Component);

  function ArmorLoader() {
    _classCallCheck(this, ArmorLoader);

    return _possibleConstructorReturn(this, (ArmorLoader.__proto__ || Object.getPrototypeOf(ArmorLoader)).call(this));
  }

  _createClass(ArmorLoader, [{
    key: 'render',
    value: function render() {
      var props = _lodash2.default.clone(this.props);

      if (props.width) {
        overlay.width = props.width;
        props.width = undefined;
      }
      var style = _lodash2.default.assign({ display: !props.loaded ? 'block' : 'none' }, overlay, this.props.style);

      return _react2.default.createElement(
        'div',
        _extends({ style: style }, props),
        _react2.default.createElement(
          'div',
          { style: loaderStyle },
          _react2.default.createElement(_CircularProgress2.default, { size: 1.5 })
        )
      );
    }
  }]);

  return ArmorLoader;
}(_react2.default.Component);

ArmorLoader.propTypes = propTypes;
exports.default = ArmorLoader;