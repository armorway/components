'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('./utils');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _reactBootstrap = require('react-bootstrap');

require('../styles/ArmorViewChange');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  /**
    * List of Json objects that represent the views.
    * Structure of each object is { key: 'value',... }
  */
  views: _react.PropTypes.array.isRequired,
  /**
    * Component title to be rendered as text to the left
  */
  title: _react.PropTypes.string.isRequired,
  /**
    * Value of the current active view
  */
  active: _react.PropTypes.string.isRequired,
  /**
    * Function that takes in a key event and
    * a string to be executed when a pill is selected.
  */
  onSelect: _react.PropTypes.func.isRequired
};

/**
  * ArmorViewChange creates a row of links for
  * the user to click on. This component's intended
  * usage is to change views within a page.
*/

var ArmorViewChange = function (_React$Component) {
  _inherits(ArmorViewChange, _React$Component);

  function ArmorViewChange(props) {
    _classCallCheck(this, ArmorViewChange);

    var _this = _possibleConstructorReturn(this, (ArmorViewChange.__proto__ || Object.getPrototypeOf(ArmorViewChange)).call(this, props));

    _this.state = {};
    _this.buildPills = _this.buildPills.bind(_this);
    _this.onPillSelect = _this.onPillSelect.bind(_this);
    return _this;
  }

  _createClass(ArmorViewChange, [{
    key: 'onPillSelect',
    value: function onPillSelect(toBeActive, event) {
      if (this.props.onSelect) {
        this.props.onSelect.call(this, event, toBeActive);
      }
    }
  }, {
    key: 'buildPills',
    value: function buildPills() {
      return _react2.default.createElement(
        _reactBootstrap.Nav,
        { id: 'armorViewPills',
          bsStyle: 'pills',
          activeKey: this.props.active,
          onSelect: this.onPillSelect,
          className: 'armor-pill',
          style: { display: 'inline-block', top: '4px' }
        },
        _lodash2.default.map(this.props.views, function (view, idx) {
          return _react2.default.createElement(
            _reactBootstrap.NavItem,
            { id: view.value + ' + ViewLink',
              key: idx,
              eventKey: view.value,
              className: 'armor-view-item'
            },
            _utils.utils.camelToReadable(view.value)
          );
        })
      );
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'span',
        null,
        _react2.default.createElement(
          'span',
          { className: 'active-view' },
          this.props.title
        ),
        this.buildPills()
      );
    }
  }]);

  return ArmorViewChange;
}(_react2.default.Component);

ArmorViewChange.propTypes = propTypes;
exports.default = ArmorViewChange;