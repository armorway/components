'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = require('react-bootstrap');

var _reactToggle = require('react-toggle');

var _reactToggle2 = _interopRequireDefault(_reactToggle);

require('react-toggle/style.css');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var defaultProps = {
  checked: true
};

var propTypes = {
  checked: _react.PropTypes.bool,
  labelOn: _react.PropTypes.string,
  labelOff: _react.PropTypes.string,
  onChange: _react.PropTypes.func
};

var ArmorToggle = function (_React$Component) {
  _inherits(ArmorToggle, _React$Component);

  function ArmorToggle(props) {
    _classCallCheck(this, ArmorToggle);

    var _this = _possibleConstructorReturn(this, (ArmorToggle.__proto__ || Object.getPrototypeOf(ArmorToggle)).call(this, props));

    _this.state = {
      checked: props.checked
    };
    _this.onChange = _this.onChange.bind(_this);
    return _this;
  }

  _createClass(ArmorToggle, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(newProps) {
      this.setState({ checked: newProps.checked });
    }
  }, {
    key: 'onChange',
    value: function onChange() {
      this.refs.toggle.setState({ checked: this.state.checked });
      this.props.onChange(this.state.checked, this.state.checked);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          _reactBootstrap.Row,
          { style: { width: '110px' } },
          _react2.default.createElement(
            _reactBootstrap.Col,
            { xs: 6, onClick: function onClick(evt) {
                return evt.stopPropagation();
              } },
            _react2.default.createElement(_reactToggle2.default, {
              onChange: this.onChange,
              id: 'toggle',
              ref: 'toggle',
              defaultChecked: this.state.checked })
          ),
          _react2.default.createElement(
            _reactBootstrap.Col,
            { xs: 6 },
            _react2.default.createElement(
              _reactBootstrap.Label,
              { bsStyle: this.state.checked ? 'success' : 'danger' },
              this.state.checked ? this.props.labelOn : this.props.labelOff
            )
          )
        )
      );
    }
  }]);

  return ArmorToggle;
}(_react2.default.Component);

ArmorToggle.defaultProps = defaultProps;
ArmorToggle.propTypes = propTypes;

exports.default = ArmorToggle;