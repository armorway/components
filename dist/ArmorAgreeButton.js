'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  /**
    * The current number of agrees to display.
    */
  count: _react.PropTypes.number.isRequired,
  /**
    * Used to show if the user has agreed already.
    */
  agreed: _react.PropTypes.bool.isRequired,
  /**
    * Called when agreeing and props.agreed is false.
    */
  onClick: _react.PropTypes.func.isRequired
};

/**
  * ArmorAgreeButton is used to show 'agrees' to something on a page.
  * It is similar to a +1 or like button.
  */

var ArmorAgreeButton = function (_React$Component) {
  _inherits(ArmorAgreeButton, _React$Component);

  function ArmorAgreeButton(props) {
    _classCallCheck(this, ArmorAgreeButton);

    var _this = _possibleConstructorReturn(this, (ArmorAgreeButton.__proto__ || Object.getPrototypeOf(ArmorAgreeButton)).call(this, props));

    _this.state = {};
    _this.hasAgreed = _this.hasAgreed.bind(_this);
    _this.onClick = _this.onClick.bind(_this);
    return _this;
  }

  _createClass(ArmorAgreeButton, [{
    key: 'onClick',
    value: function onClick(event) {
      event.stopPropagation();
      if (!this.hasAgreed()) {
        this.props.onClick();
      }
    }
  }, {
    key: 'hasAgreed',
    value: function hasAgreed() {
      return this.props.agreed;
    }
  }, {
    key: 'render',
    value: function render() {
      var style = this.hasAgreed() ? {} : { color: '#4caf50', backgroundColor: '#fff', border: '1px solid #4caf50' };

      return _react2.default.createElement(
        'span',
        { role: 'button',
          className: 'btn btn-success',
          onClick: this.onClick,
          style: style
        },
        '+ ',
        this.props.count
      );
    }
  }]);

  return ArmorAgreeButton;
}(_react2.default.Component);

ArmorAgreeButton.propTypes = propTypes;
exports.default = ArmorAgreeButton;