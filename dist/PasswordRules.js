'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _Paper = require('material-ui/Paper');

var _Paper2 = _interopRequireDefault(_Paper);

var _reactBootstrap = require('react-bootstrap');

var _FontAwesome = require('armorway/dist/FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  /*
    Rules object
   */
  rules: _react.PropTypes.object.isRequired,
  /*
    If small is true, will render significantly smaller via smaller text.
  */
  small: _react.PropTypes.bool,

  /*
    Styles for the component
   */
  setStyles: _react.PropTypes.object
};

var defaultProps = {
  setStyles: {}
};

var PasswordChange = function (_React$Component) {
  _inherits(PasswordChange, _React$Component);

  function PasswordChange() {
    _classCallCheck(this, PasswordChange);

    return _possibleConstructorReturn(this, (PasswordChange.__proto__ || Object.getPrototypeOf(PasswordChange)).apply(this, arguments));
  }

  _createClass(PasswordChange, [{
    key: 'render',
    value: function render() {
      //  capital, lower, number, special
      var longEnough = this.props.rules.longEnough;

      var progressLength = _lodash2.default.filter(this.props.rules, function (prop) {
        return prop;
      }).length / _lodash2.default.keys(this.props.rules).length * 100;
      var progressColors = {
        0: 'danger',
        20: 'danger',
        40: 'danger',
        60: 'warning',
        80: 'warning',
        100: 'success'
      };
      var ruleFontSize = this.props.small ? { fontSize: '10px' } : { fontSize: 'auto' };
      var headerStyles = this.props.small ? { fontSize: '14px' } : {};

      var progressStyles = _lodash2.default.assign(this.props.setStyles, {
        margin: '0px 0px 3px 0px',
        height: '12px'
      });

      /**
              <div className={(capital) ? 'text-success' : 'text-danger'}>
                <FontAwesome icon={(capital) ? 'check' : 'times'} />
                <span style={ruleFontSize}> At least 1 capital letter.</span>
              </div>
              <div className={(lower) ? 'text-success' : 'text-danger'}>
                <FontAwesome icon={(lower) ? 'check' : 'times'} />
                <span style={ruleFontSize}> At least 1 lower case letter.</span>
              </div>
              <div className={(number) ? 'text-success' : 'text-danger'}>
                <FontAwesome icon={(number) ? 'check' : 'times'} />
                <span style={ruleFontSize}> At least 1 number.</span>
              </div>
              <div className={(special) ? 'text-success' : 'text-danger'}>
                <FontAwesome icon={(special) ? 'check' : 'times'} />
                <span style={ruleFontSize}> At least 1 special character.</span>
              </div>
       */
      return _react2.default.createElement(
        _Paper2.default,
        { style: { padding: '5px' } },
        _react2.default.createElement(
          'h4',
          { style: headerStyles },
          'Password Rules'
        ),
        _react2.default.createElement(
          'div',
          { className: longEnough ? 'text-success' : 'text-danger' },
          _react2.default.createElement(_FontAwesome2.default, { icon: longEnough ? 'check' : 'times' }),
          _react2.default.createElement(
            'span',
            { style: ruleFontSize },
            ' At least 12 characters.'
          )
        ),
        _react2.default.createElement(_reactBootstrap.ProgressBar, {
          ref: 'progressBar',
          bsSize: 'xsmall',
          bsStyle: progressColors[progressLength],
          now: progressLength,
          style: progressStyles
        })
      );
    }
  }]);

  return PasswordChange;
}(_react2.default.Component);

PasswordChange.defaultProps = defaultProps;
PasswordChange.propTypes = propTypes;
exports.default = PasswordChange;