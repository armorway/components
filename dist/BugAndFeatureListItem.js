'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _reactBootstrap = require('react-bootstrap');

var _ArmorDivider = require('components/global/ArmorDivider');

var _ArmorDivider2 = _interopRequireDefault(_ArmorDivider);

var _ArmorAgreeButton = require('components/global/ArmorAgreeButton');

var _ArmorAgreeButton2 = _interopRequireDefault(_ArmorAgreeButton);

var _AppStore = require('stores/global/AppStore');

var _AppStore2 = _interopRequireDefault(_AppStore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  isSelected: _react.PropTypes.bool.isRequired,
  data: _react.PropTypes.object.isRequired,
  onRespondClick: _react.PropTypes.func,
  onUpdateClick: _react.PropTypes.func.isRequired,
  onAgreeClick: _react.PropTypes.func.isRequired,
  onClick: _react.PropTypes.func.isRequired
};

var BugFeatureListItem = function (_React$Component) {
  _inherits(BugFeatureListItem, _React$Component);

  function BugFeatureListItem() {
    _classCallCheck(this, BugFeatureListItem);

    var _this = _possibleConstructorReturn(this, (BugFeatureListItem.__proto__ || Object.getPrototypeOf(BugFeatureListItem)).call(this));

    _this.onAgreeClick = _this.onAgreeClick.bind(_this);
    _this.onRespondClick = _this.onRespondClick.bind(_this);
    _this.onUpdateClick = _this.onUpdateClick.bind(_this);
    _this.buildRespondIfNeeded = _this.buildRespondIfNeeded.bind(_this);
    return _this;
  }

  _createClass(BugFeatureListItem, [{
    key: 'onAgreeClick',
    value: function onAgreeClick(event) {
      this.props.onAgreeClick(event, this.props.data);
    }
  }, {
    key: 'onRespondClick',
    value: function onRespondClick(event) {
      this.props.onRespondClick(event, this.props.data);
      event.stopPropagation();
    }
  }, {
    key: 'onUpdateClick',
    value: function onUpdateClick(event) {
      this.props.onUpdateClick(event, this.props.data);
      event.stopPropagation();
    }
  }, {
    key: 'onRowClick',
    value: function onRowClick(data, event) {
      this.props.onClick(event, data);
    }
  }, {
    key: 'buildRespondIfNeeded',
    value: function buildRespondIfNeeded() {
      this.buildRespondIfNeeded = this.buildRespondIfNeeded.bind(this);
      if (this.props.onRespondClick) {
        return _react2.default.createElement(
          'span',
          { role: 'button',
            className: 'btn btn-default',
            onClick: this.onRespondClick,
            style: { marginTop: '5px', marginLeft: '5px' }
          },
          'Respond'
        );
      }
      return '';
    }
  }, {
    key: 'render',
    value: function render() {
      var rowPad = { paddingBottom: '15px' };
      var _props = this.props;
      var isSelected = _props.isSelected;
      var data = _props.data;


      return _react2.default.createElement(
        'ul',
        { className: 'list-group',
          style: { marginBottom: '10px' },
          onClick: this.onRowClick.bind(this, data)
        },
        _react2.default.createElement(
          'div',
          { className: 'list-group-item' },
          _react2.default.createElement(
            _reactBootstrap.Row,
            null,
            _react2.default.createElement(
              _reactBootstrap.Col,
              { xs: 2 },
              'Topic: ',
              data.title
            ),
            _react2.default.createElement(
              _reactBootstrap.Col,
              { xsOffset: 2, xs: 2 },
              'Priority: ',
              data.priority
            ),
            _react2.default.createElement(
              _reactBootstrap.Col,
              { xsOffset: 3, xs: 3 },
              _react2.default.createElement(
                'div',
                { className: 'pull-right' },
                _react2.default.createElement(
                  _reactBootstrap.Button,
                  { role: 'button',
                    bsStyle: 'default',
                    onClick: this.onUpdateClick,
                    style: { marginTop: '5px', marginLeft: '5px' }
                  },
                  'Update'
                ),
                this.buildRespondIfNeeded()
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { style: { display: isSelected ? 'block' : 'none', padding: '30px' } },
            _react2.default.createElement(
              _reactBootstrap.Row,
              { style: rowPad },
              _react2.default.createElement(
                _reactBootstrap.Col,
                { xs: 12 },
                _react2.default.createElement(
                  'div',
                  null,
                  _react2.default.createElement(_ArmorAgreeButton2.default, {
                    agreed: _lodash2.default.includes(data.usernamesOfOfficers, _AppStore2.default.getState().user.username),
                    count: _lodash2.default.without(data.usernamesOfOfficers, null, undefined, '').length,
                    onClick: this.onAgreeClick
                  })
                )
              )
            ),
            _react2.default.createElement(_ArmorDivider2.default, null),
            _react2.default.createElement(
              _reactBootstrap.Row,
              { style: rowPad },
              _react2.default.createElement(
                _reactBootstrap.Col,
                { xs: 12 },
                'Description: ',
                data.description
              )
            ),
            _react2.default.createElement(
              _reactBootstrap.Row,
              { style: rowPad },
              _react2.default.createElement(
                _reactBootstrap.Col,
                { xs: 6 },
                ' Requested By: ',
                data.reportedByOfficer,
                ' '
              ),
              _react2.default.createElement(
                _reactBootstrap.Col,
                { xs: 6 },
                ' Date Requested: ',
                data.dateReported,
                ' '
              )
            ),
            _react2.default.createElement(_ArmorDivider2.default, null),
            _react2.default.createElement(
              _reactBootstrap.Row,
              { style: rowPad },
              _react2.default.createElement(
                _reactBootstrap.Col,
                { xs: 6 },
                'Status: ',
                _lodash2.default.startCase(data.armorwayResponseStatus.toLowerCase() || 'None')
              ),
              _react2.default.createElement(
                _reactBootstrap.Col,
                { xs: 6 },
                'Difficulty:',
                _lodash2.default.startCase(data.armorwayResponseDiffultyLevel.toLowerCase() || 'None')
              )
            ),
            _react2.default.createElement(
              _reactBootstrap.Row,
              { style: rowPad },
              _react2.default.createElement(
                _reactBootstrap.Col,
                { xs: 12 },
                'Notes: ',
                data.armorwayResponseNotes || 'No notes available.'
              )
            ),
            _react2.default.createElement(
              _reactBootstrap.Row,
              { style: rowPad },
              _react2.default.createElement(
                _reactBootstrap.Col,
                { xs: 12 },
                'Date Posted: ',
                data.armorwayResponseDate || 'No date available.'
              )
            )
          )
        )
      );
    }
  }]);

  return BugFeatureListItem;
}(_react2.default.Component);

BugFeatureListItem.propTypes = propTypes;
exports.default = BugFeatureListItem;