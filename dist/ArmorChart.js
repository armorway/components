'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _bundle = require('react-highcharts/dist/bundle');

var _bundle2 = _interopRequireDefault(_bundle);

require('react-highcharts/dist/modules/exporting');

require('react-highcharts/dist/modules/offline-exporting');

var _reactBootstrap = require('react-bootstrap');

var _FontAwesome = require('components/global/FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  settings: _react.PropTypes.element,
  data: _react.PropTypes.object.isRequired
};

var ArmorChart = function (_React$Component) {
  _inherits(ArmorChart, _React$Component);

  function ArmorChart(props) {
    _classCallCheck(this, ArmorChart);

    var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(ArmorChart).call(this));

    _this.state = {
      showSettings: false,
      chartHeight: 0,
      initialHeight: props.data.chart.height,
      chartData: props.data
    };
    _this.toggleSettings = _this.toggleSettings.bind(_this);
    return _this;
  }

  _createClass(ArmorChart, [{
    key: 'toggleSettings',
    value: function toggleSettings() {
      var _this2 = this;

      var unit = -20;
      var animatingToShowSettings = true;

      if (this.state.chartHeight !== 0) {
        unit *= -1;
        animatingToShowSettings = false;
      }

      if (this.state.interval) {
        clearInterval(this.state.interval);
        return;
      }

      var interval = setInterval(function () {
        var newHeight = _this2.state.chartHeight + unit;

        _this2.setState({
          chartHeight: newHeight
        });

        /*
         rules to stop the animation
         first is to stop show settings animation
         second is to stop show chart animation
        */
        if (animatingToShowSettings && newHeight <= -_this2.state.initialHeight || !animatingToShowSettings && _this2.state.chartHeight === 0) {
          _this2.setState({ interval: undefined, animating: false });
          clearInterval(interval);
        }
      }, 1);

      this.setState({ animating: true, interval: interval });
    }
  }, {
    key: 'render',
    value: function render() {
      var settingsButton = _react2.default.createElement(_reactBootstrap.Button, { bsStyle: "default", onClick: this.toggleSettings }, _react2.default.createElement(_FontAwesome2.default, { icon: "gear" }));
      var chartStyle = {
        marginTop: this.state.chartHeight + 'px',
        height: this.state.initialHeight + 'px',
        opacity: this.state.chartOpacity,
        overflow: 'hidden'
      };

      return _react2.default.createElement(_reactBootstrap.Row, null, _react2.default.createElement(_reactBootstrap.Col, { xs: 11, style: { height: this.state.initialHeight + 'px', overflow: 'hidden' } }, _react2.default.createElement("div", { style: chartStyle }, !this.state.animating ? _react2.default.createElement(_bundle2.default, { config: this.state.chartData }) : ''), _react2.default.createElement("div", null, this.props.settings)), _react2.default.createElement(_reactBootstrap.Col, { xs: 1 }, this.props.settings ? settingsButton : ''));
    }
  }]);

  return ArmorChart;
}(_react2.default.Component);

ArmorChart.propTypes = propTypes;
exports.default = ArmorChart;