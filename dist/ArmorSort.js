'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = require('react-bootstrap');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _FontAwesome = require('components/global/FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  list: _react.PropTypes.array.isRequired,
  onSort: _react.PropTypes.func.isRequired,
  titleAlias: _react.PropTypes.string,
  descriptionAlias: _react.PropTypes.string
};

var ArmorSort = function (_React$Component) {
  _inherits(ArmorSort, _React$Component);

  function ArmorSort(props) {
    _classCallCheck(this, ArmorSort);

    var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(ArmorSort).call(this, props));

    _this.state = {
      sortedList: _this.props.list,
      title: { sorted: false, sortedAsc: false },
      description: { sorted: false, sortedAsc: false }
    };
    _this.buildSortedList = _this.buildSortedList.bind(_this);
    return _this;
  }

  _createClass(ArmorSort, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(newProps) {
      if (newProps.list) {
        this.setState({
          sortedList: newProps.list
        });
      }
    }
  }, {
    key: 'getSortedList',
    value: function getSortedList(property, asc) {
      return this.sortByDirection(this.state.sortedList, asc, property);
    }
  }, {
    key: 'sortByDirection',
    value: function sortByDirection(list, asc, property) {
      return _lodash2.default.sortByOrder(list, property, asc ? 'asc' : 'desc');
    }
  }, {
    key: 'sortBy',
    value: function sortBy(property) {
      var sortType = property.toLowerCase();
      var sorted = true;
      var sortAsc = !this.state[sortType].sorted || !this.state[sortType].sortedAsc;
      var sortedList = void 0;

      sortedList = this.getSortedList(property, sortAsc);
      this.updateState(sortType, sorted, sortAsc);
      this.props.onSort(sortedList);
    }
  }, {
    key: 'buildSortedList',
    value: function buildSortedList(event) {
      this.sortBy(event.currentTarget.getAttribute('data-category'));
    }
  }, {
    key: 'buildIcon',
    value: function buildIcon(property) {
      var state = this.state[property.toLowerCase()];

      var imgName = 'sort';

      if (state.sorted) {
        if (state.sortedAsc) {
          imgName = 'sort-asc';
        } else {
          imgName = 'sort-desc';
        }
      }
      return _react2.default.createElement(_FontAwesome2.default, { icon: imgName, padIcon: true });
    }
  }, {
    key: 'updateState',
    value: function updateState(property, sorted, sortedAsc) {
      var _this2 = this;

      this.setState(_defineProperty({}, property, { sorted: sorted, sortedAsc: sortedAsc }));
      _lodash2.default.forEach(this.state, function (value, key) {
        if (key !== property) {
          _this2.setState(_defineProperty({}, key, { sorted: false, sortedAsc: false }));
        }
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement("div", { className: "pull-right" }, _react2.default.createElement(_reactBootstrap.Button, {
        style: { marginRight: '10px' },
        "data-category": "title",
        onClick: this.buildSortedList }, this.buildIcon('title'), "Sort ", this.props.titleAlias), _react2.default.createElement(_reactBootstrap.Button, {
        "data-category": "description",
        onClick: this.buildSortedList }, this.buildIcon('description'), "Sort ", this.props.descriptionAlias));
    }
  }]);

  return ArmorSort;
}(_react2.default.Component);

ArmorSort.propTypes = propTypes;
exports.default = ArmorSort;