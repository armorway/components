'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _reactBootstrap = require('react-bootstrap');

var _reactSelect = require('react-select');

var _reactSelect2 = _interopRequireDefault(_reactSelect);

var _FontAwesome = require('components/global/FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

var _ArmorJawbone = require('components/global/ArmorJawbone');

var _ArmorJawbone2 = _interopRequireDefault(_ArmorJawbone);

var _ArmorDateRange = require('components/global/ArmorDateRange');

var _ArmorDateRange2 = _interopRequireDefault(_ArmorDateRange);

var _jawbone = require('services/jawbone');

var _ArmorIntervalDates = require('components/global/ArmorIntervalDates');

var _ArmorIntervalDates2 = _interopRequireDefault(_ArmorIntervalDates);

var _HotspotsActions = require('actions/HotspotsActions');

var _HotspotsActions2 = _interopRequireDefault(_HotspotsActions);

var _HotspotsStore = require('stores/HotspotsStore');

var _HotspotsStore2 = _interopRequireDefault(_HotspotsStore);

var _AppStore = require('stores/global/AppStore');

var _AppStore2 = _interopRequireDefault(_AppStore);

var _AppActions = require('actions/global/AppActions');

var _AppActions2 = _interopRequireDefault(_AppActions);

var _WatchModal = require('components/global/modals/WatchModal');

var _WatchModal2 = _interopRequireDefault(_WatchModal);

var _BeatsModal = require('components/global/modals/BeatsModal');

var _BeatsModal2 = _interopRequireDefault(_BeatsModal);

var _utils = require('utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// import { utils } from 'utils';


var propTypes = {
  onAccept: _react.PropTypes.func.isRequired,
  onChange: _react.PropTypes.func.isRequired,
  showBeats: _react.PropTypes.bool,
  showWatch: _react.PropTypes.bool,
  showAreas: _react.PropTypes.bool,
  showCrimeFilter: _react.PropTypes.bool,
  showApplyBtn: _react.PropTypes.bool,
  dateLabel: _react.PropTypes.string
};

var defaultProps = {
  showBeats: true,
  showWatch: true,
  showAreas: true,
  showCrimeFilter: true,
  showApplyBtn: false,
  dateLabel: 'Last:'
};

var ArmorCrimeFilter = function (_React$Component) {
  _inherits(ArmorCrimeFilter, _React$Component);

  function ArmorCrimeFilter() {
    _classCallCheck(this, ArmorCrimeFilter);

    // const { preferredBeats, preferredWatches } = AppStore.getState().user;

    var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(ArmorCrimeFilter).call(this));

    var localStates = {
      beatsValues: 'all',
      watchValues: 'all',
      crimeAreaLabel: 'all'
    };

    _this.state = _lodash2.default.assign(localStates, _HotspotsStore2.default.getState(), _AppStore2.default.getState());
    _this.onCrimeSelect = _this.onCrimeSelect.bind(_this);
    _this.onBeatsSelect = _this.onBeatsSelect.bind(_this);
    _this.onWatchSelect = _this.onWatchSelect.bind(_this);
    _this.onJawboneSelect = _this.onJawboneSelect.bind(_this);
    _this.onWatchModal = _this.onWatchModal.bind(_this);
    _this.onBeatsModal = _this.onBeatsModal.bind(_this);
    _this.onAreaSelect = _this.onAreaSelect.bind(_this);
    _this.updateDropdown = _this.updateDropdown.bind(_this);
    _this.onStateChange = _this.onStateChange.bind(_this);
    _this.onAppChange = _this.onAppChange.bind(_this);
    _this.onIntervalSelect = _this.onIntervalSelect.bind(_this);
    _this.onStartDateSelect = _this.onStartDateSelect.bind(_this);
    _this.buildWatch = _this.buildWatch.bind(_this);
    _this.buildCrimeFilter = _this.buildCrimeFilter.bind(_this);
    _this.buildBeats = _this.buildBeats.bind(_this);
    _this.buildAreas = _this.buildAreas.bind(_this);
    _this.buildApplyBtn = _this.buildApplyBtn.bind(_this);
    _this.onApplyBtn = _this.onApplyBtn.bind(_this);
    return _this;
  }

  _createClass(ArmorCrimeFilter, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _AppStore2.default.listen(this.onAppChange);
      _HotspotsStore2.default.listen(this.onAppChange);
      _HotspotsActions2.default.setTimeInterval({ timeInterval: '7Days' });
      _HotspotsActions2.default.setRules({});
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      _AppStore2.default.unlisten(this.onAppChange);
      _HotspotsStore2.default.unlisten(this.onAppChange);
    }
  }, {
    key: 'onAppChange',
    value: function onAppChange(newState) {
      this.setState(newState);
    }
  }, {
    key: 'onStateChange',
    value: function onStateChange(newState) {
      var beats = newState.beats;
      var watch = newState.watch;


      this.props.onChange({ beats: beats, watch: watch });
    }
  }, {
    key: 'onWatchModal',
    value: function onWatchModal() {
      _AppActions2.default.setWindowHeight(0);
      _AppActions2.default.toggleModal({
        showModal: true,
        modal: _react2.default.createElement(_WatchModal2.default, { data: this.state.watch.data })
      });
    }
  }, {
    key: 'onBeatsModal',
    value: function onBeatsModal() {
      _AppActions2.default.setWindowHeight(0);
      _AppActions2.default.toggleModal({
        showModal: true,
        size: 'large',
        modal: _react2.default.createElement(_BeatsModal2.default, null)
      });
    }
  }, {
    key: 'onIntervalSelect',
    value: function onIntervalSelect(selection) {
      if (selection === 'calendar') {
        this.props.onChange({ showCalendar: !this.state.showCalendar });
        this.refs.jawbone.toggleFilters('close');
        this.refs.datepicker.toggleFilters();
      } else {
        this.props.onAccept({ intervalSelected: selection.keys }, true);
        this.props.onChange({ showCalendar: false });
      }
    }
  }, {
    key: 'onAreaSelect',
    value: function onAreaSelect(value) {
      this.props.onChange({ crimeAreaLabel: value.keys });
      this.setState({ crimeAreaLabel: value });
      this.updateDropdown(value.keys, 'crimeArea');
    }
  }, {
    key: 'onCrimeSelect',
    value: function onCrimeSelect() {
      this.refs.datepicker.toggleFilters('close');
      this.refs.jawbone.toggleFilters();
    }
  }, {
    key: 'onJawboneSelect',
    value: function onJawboneSelect(selection, formattedSelection) {
      this.props.onChange({ crimeTypeFilter: formattedSelection, crimeLabel: 'other' });
      this.props.onAccept({ crimeSelected: formattedSelection }, false);
    }
  }, {
    key: 'onExtractValues',
    value: function onExtractValues(list) {
      return _lodash2.default.map(list, function (item) {
        return item.value;
      }).join(',');
    }
  }, {
    key: 'onDropdownChange',
    value: function onDropdownChange(key) {
      this.updateDropdown(this.state[key + 'Values'], key);
    }
  }, {
    key: 'onAllSelection',
    value: function onAllSelection(val, key) {
      var values = _lodash2.default.clone(val);

      if (values) {
        var isNewSelectionAll = val.pop().value === 'all';

        if (isNewSelectionAll) {
          values = _lodash2.default.remove(values, function (item) {
            return item.value === 'all';
          });
        }
        if (_lodash2.default.find(values, { value: 'all' }) && !isNewSelectionAll) {
          values = _lodash2.default.remove(values, function (item) {
            return item.value !== 'all';
          });
        }

        if (values.length < 1) {
          this.setState(_defineProperty({}, key, 'all'));
        } else {
          this.setState(_defineProperty({}, key, this.onExtractValues(values)));
        }
      } else {
        this.setState(_defineProperty({}, key, 'all'));
      }
    }
  }, {
    key: 'onBeatsSelect',
    value: function onBeatsSelect(values) {
      this.onAllSelection(values, 'beatsValues');
    }
  }, {
    key: 'onWatchSelect',
    value: function onWatchSelect(values) {
      this.onAllSelection(values, 'watchValues');
    }
  }, {
    key: 'onStartDateSelect',
    value: function onStartDateSelect(event) {
      var start = event.start;
      var end = event.end;


      this.setState({ showCalendar: false });
      this.props.onAccept({ startDate: (0, _moment2.default)(end).format(_utils.dateFormat),
        endDate: (0, _moment2.default)(start).format(_utils.dateFormat) }, true);

      _HotspotsActions2.default.setTimeInterval({ timeInterval: 'customDate' });
    }
  }, {
    key: 'onApplyBtn',
    value: function onApplyBtn() {
      if (this.refs.jawbone.state.open) {
        this.refs.jawbone.onApplyClick();
      }
      this.props.onAccept();
    }
  }, {
    key: 'getBeatOptions',
    value: function getBeatOptions() {
      var beats = [];

      beats.push({
        label: 'All Beats',
        value: 'all'
      });
      if (this.state.beats) {
        beats = _lodash2.default.union(beats, _lodash2.default.map(this.state.beats.data, function (item) {
          return {
            label: item.area,
            value: item.area
          };
        }));
      }

      return beats;
    }
  }, {
    key: 'getWatchOptions',
    value: function getWatchOptions() {
      var watch = [];
      var watchData = this.state.watch.data;

      watch.push({
        label: 'All Watch',
        value: 'all'
      });
      if (watchData) {
        watch = _lodash2.default.union(watch, _lodash2.default.map(watchData, function (item) {
          return {
            label: item.description,
            value: item.description
          };
        }));
      }

      return watch;
    }
  }, {
    key: 'buildIntervalButtons',
    value: function buildIntervalButtons(options) {
      return _react2.default.createElement(_ArmorIntervalDates2.default, { options: options,
        onSelect: this.onIntervalSelect });
    }
  }, {
    key: 'buildWatch',
    value: function buildWatch() {
      return _react2.default.createElement(_reactBootstrap.Col, { xs: 3, className: "dropdown" }, _react2.default.createElement(_reactBootstrap.Row, null, _react2.default.createElement(_reactBootstrap.Col, { xs: 10 }, _react2.default.createElement(_reactSelect2.default, {
        className: "crime-filter-watch-dropdown",
        name: "watch-dropdown",
        placeholder: "Select Watch",
        value: this.state.watchValues,
        options: this.getWatchOptions(),
        multi: true,
        onChange: this.onWatchSelect,
        onBlur: this.onDropdownChange.bind(this, 'watch') })), _react2.default.createElement(_reactBootstrap.Col, { xs: 2 }, _react2.default.createElement("a", { onClick: this.onWatchModal, style: { marginLeft: '-20px', marginTop: '5px', cursor: 'help' }, className: "pull-left" }, _react2.default.createElement(_FontAwesome2.default, { icon: "question" })))));
    }
  }, {
    key: 'buildAreas',
    value: function buildAreas() {
      return _react2.default.createElement(_reactBootstrap.Col, { xs: 2, className: "dropdown" }, _react2.default.createElement(_reactBootstrap.Row, null, _react2.default.createElement(_reactBootstrap.Col, { xs: 12 }, _react2.default.createElement(_reactSelect2.default, {
        name: "area-dropdown",
        ref: "areas",
        placeholder: "Select Areas",
        value: this.state.crimeAreaLabel,
        valueKey: "keys",
        clearable: false,
        options: _jawbone.crimeAreaOptions,
        onChange: this.onAreaSelect }))));
    }
  }, {
    key: 'buildBeats',
    value: function buildBeats() {
      return _react2.default.createElement(_reactBootstrap.Col, { xs: 4, className: "dropdown" }, _react2.default.createElement(_reactBootstrap.Row, null, _react2.default.createElement(_reactBootstrap.Col, { xs: 10 }, _react2.default.createElement(_reactSelect2.default, {
        name: "beats-dropdown",
        ref: "beats",
        placeholder: "Select Beats",
        value: this.state.beatsValues,
        options: this.getBeatOptions(),
        multi: true,
        onChange: this.onBeatsSelect,
        onBlur: this.onDropdownChange.bind(this, 'beats') })), _react2.default.createElement(_reactBootstrap.Col, { xs: 2 }, _react2.default.createElement("a", { onClick: this.onBeatsModal, style: { marginLeft: '-20px', marginTop: '5px', cursor: 'help' }, className: "pull-left" }, _react2.default.createElement(_FontAwesome2.default, { icon: "question" })))));
    }
  }, {
    key: 'buildCrimeFilter',
    value: function buildCrimeFilter() {
      return _react2.default.createElement(_reactBootstrap.Col, { xs: 1, style: { padding: '0' } }, _react2.default.createElement(_reactBootstrap.Button, { onClick: this.onCrimeSelect }, _react2.default.createElement(_FontAwesome2.default, { icon: "gear", title: "Crime Filters", padIcon: true })));
    }
  }, {
    key: 'buildApplyBtn',
    value: function buildApplyBtn() {
      return _react2.default.createElement(_reactBootstrap.Col, { xs: 1 }, _react2.default.createElement(_reactBootstrap.Button, { className: "pull-right", bsStyle: "success", onClick: this.onApplyBtn }, _react2.default.createElement(_FontAwesome2.default, { icon: "refresh", title: "Apply", padIcon: true })));
    }
  }, {
    key: 'updateDropdown',
    value: function updateDropdown(values, filterName) {
      var finalValues = void 0;

      if (!values) {
        finalValues = 'all';
      } else {
        finalValues = values;
      }
      this.updateRules(values, filterName);
      this.props.onAccept(_defineProperty({}, filterName + 'Selected', finalValues), false);
      this.props.onChange(_defineProperty({}, filterName + 'Filter', finalValues));
    }
  }, {
    key: 'updateRules',
    value: function updateRules(values, ruleKey) {
      var newState = _lodash2.default.assign(this.state.rules, {});

      newState[ruleKey] = _lodash2.default.chain(values.split(',')).map(function (value) {
        return [value, true];
      }).zipObject().value();

      _HotspotsActions2.default.setRules(newState);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement("div", { id: "armorCrimeFilter" }, _react2.default.createElement(_reactBootstrap.Row, null, _react2.default.createElement(_reactBootstrap.Col, { xs: this.props.showApplyBtn ? 3 : 4, className: "calendar-buttons" }, _react2.default.createElement(_reactBootstrap.Row, null, _react2.default.createElement(_reactBootstrap.Col, { xs: 10 }, _react2.default.createElement("span", { style: { marginRight: '5px' } }, _react2.default.createElement("strong", null, this.props.dateLabel)), this.buildIntervalButtons(_jawbone.intervals)))), this.props.showBeats ? this.buildBeats() : '', this.props.showWatch ? this.buildWatch() : '', this.props.showAreas ? this.buildAreas() : '', this.props.showCrimeFilter ? this.buildCrimeFilter() : '', this.props.showApplyBtn ? this.buildApplyBtn() : ''), _react2.default.createElement("div", { style: { marginTop: '15px' } }, _react2.default.createElement(_ArmorJawbone2.default, { ref: "jawbone", onAccept: this.onJawboneSelect })), _react2.default.createElement(_reactBootstrap.Row, { style: { marginTop: '-25px' } }, _react2.default.createElement(_reactBootstrap.Col, { xs: 8 }, _react2.default.createElement(_ArmorDateRange2.default, { ref: "datepicker",
        onAccept: this.onStartDateSelect,
        start: (0, _moment2.default)(),
        end: (0, _moment2.default)() }))));
    }
  }]);

  return ArmorCrimeFilter;
}(_react2.default.Component);

ArmorCrimeFilter.defaultProps = defaultProps;
ArmorCrimeFilter.propTypes = propTypes;
exports.default = ArmorCrimeFilter;