'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = require('react-bootstrap');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  /**
   * Determines if background is transparent or not
   * If true will automatically create a divider and ignore label
   */
  spaceOnly: _react.PropTypes.bool,
  /**
   * Label rendered as text in the center of the divider
   */
  label: _react.PropTypes.string,
  /**
   * Style to be applied to label.
   */
  labelStyle: _react.PropTypes.object
};

/**
  * ArmorDivider provides a spacing for content
  * and can display a label in the center of it.
  */

var ArmorDivider = function (_React$Component) {
  _inherits(ArmorDivider, _React$Component);

  function ArmorDivider() {
    _classCallCheck(this, ArmorDivider);

    var _this = _possibleConstructorReturn(this, (ArmorDivider.__proto__ || Object.getPrototypeOf(ArmorDivider)).call(this));

    _this.buildWithLabel = _this.buildWithLabel.bind(_this);
    return _this;
  }

  _createClass(ArmorDivider, [{
    key: 'buildDivider',
    value: function buildDivider(styleOverride) {
      return _react2.default.createElement(
        _reactBootstrap.Row,
        null,
        _react2.default.createElement('div', { className: 'divider', style: styleOverride })
      );
    }
  }, {
    key: 'buildWithLabel',
    value: function buildWithLabel(styleOverride) {
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          _reactBootstrap.Col,
          { xs: 5 },
          this.buildDivider(styleOverride)
        ),
        _react2.default.createElement(
          _reactBootstrap.Col,
          { xs: 2, style: { padding: '0' }, className: 'divider' },
          _react2.default.createElement(
            'span',
            { style: this.props.labelStyle },
            this.props.label
          )
        ),
        _react2.default.createElement(
          _reactBootstrap.Col,
          { xs: 5 },
          this.buildDivider(styleOverride)
        )
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props;
      var label = _props.label;
      var spaceOnly = _props.spaceOnly;

      var styleOverride = spaceOnly ? { backgroundColor: 'transparent' } : undefined;

      return _react2.default.createElement(
        'div',
        null,
        label && !spaceOnly ? this.buildWithLabel(styleOverride) : this.buildDivider(styleOverride)
      );
    }
  }]);

  return ArmorDivider;
}(_react2.default.Component);

ArmorDivider.propTypes = propTypes;
exports.default = ArmorDivider;