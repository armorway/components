'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _IconButton = require('material-ui/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _FontAwesome = require('./FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

require('../styles/ArmorScrollToTop.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ArmorScrollToTop = function (_React$Component) {
  _inherits(ArmorScrollToTop, _React$Component);

  function ArmorScrollToTop(props) {
    _classCallCheck(this, ArmorScrollToTop);

    var _this = _possibleConstructorReturn(this, (ArmorScrollToTop.__proto__ || Object.getPrototypeOf(ArmorScrollToTop)).call(this, props));

    _this.state = {
      showTopButton: !!window.pageYOffset
    };

    _this.onScroll = _this.onScroll.bind(_this);
    _this.onBackToTopClick = _this.onBackToTopClick.bind(_this);
    return _this;
  }

  _createClass(ArmorScrollToTop, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      window.addEventListener('scroll', this.onScroll);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      window.removeEventListener('scroll', this.onScroll);
    }
  }, {
    key: 'onScroll',
    value: function onScroll() {
      this.setState({ showTopButton: !!window.pageYOffset });
    }
  }, {
    key: 'onBackToTopClick',
    value: function onBackToTopClick() {
      window.scrollTo(0, 0);
    }
  }, {
    key: 'render',
    value: function render() {
      if (!this.state.showTopButton) {
        return _react2.default.createElement('span', null);
      }
      return _react2.default.createElement(_IconButton2.default, {
        className: 'armor-back-to-top',
        onClick: this.onBackToTopClick,
        iconClassName: 'fa-chevron-circle-up fa',
        tooltip: 'Return to Top',
        tooltipPosition: 'top-left'
      });
    }
  }]);

  return ArmorScrollToTop;
}(_react2.default.Component);

exports.default = ArmorScrollToTop;