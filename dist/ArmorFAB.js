'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _reactBootstrap = require('react-bootstrap');

var _ArmorFabItem = require('./ArmorFabItem');

var _ArmorFabItem2 = _interopRequireDefault(_ArmorFabItem);

var _FontAwesome = require('./FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

require('../styles/ArmorFAB');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  /**
   * Function to excute when FAB is clicked.
   */
  onClick: _react.PropTypes.func,
  /**
   * Array of Items to build into ArmorFABItems.
   */
  items: _react.PropTypes.array.isRequired,
  /**
   * Bootstrap type of main FAB Icon. Default: primary
   */
  bsStyle: _react.PropTypes.string,
  /**
   * Designates size of FAB Icon.
   */
  small: _react.PropTypes.bool,
  /**
   * class to be applied.
   */

  className: _react.PropTypes.string,
  /**
   * Style to be applied.
   */
  style: _react.PropTypes.object
};

var styles = {
  plusIcon: {
    color: '#fafafa',
    marginTop: '5px'
  },
  visible: {
    visibility: 'visible'
  },
  hidden: {
    visibility: 'hidden'
  }
};

/**
 * ArmorFab is used to display a list of actions when clicked on.
 */

var ArmorFab = function (_React$Component) {
  _inherits(ArmorFab, _React$Component);

  function ArmorFab() {
    _classCallCheck(this, ArmorFab);

    var _this = _possibleConstructorReturn(this, (ArmorFab.__proto__ || Object.getPrototypeOf(ArmorFab)).call(this));

    _this.state = {
      show: false
    };
    _this.onItemClick = _this.onItemClick.bind(_this);
    _this.onMainFabClick = _this.onMainFabClick.bind(_this);
    return _this;
  }

  _createClass(ArmorFab, [{
    key: 'onItemClick',
    value: function onItemClick(eventKey) {
      // expected eventKey is the Index of the clicked item
      var clickedItem = this.props.items[eventKey];

      if (clickedItem.onClick) {
        this.props.items[eventKey].onClick();
      }
      this.setState({ show: false });
    }
  }, {
    key: 'onMainFabClick',
    value: function onMainFabClick() {
      this.setState({ show: !this.state.show });
      if (this.props.onClick) {
        this.props.onClick();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var visibility = this.state.show ? styles.visible : styles.hidden;

      return _react2.default.createElement(
        'div',
        {
          className: 'fab ' + this.props.className + ' ' + (this.props.small ? 'small' : 'normal'),
          style: this.props.style
        },
        _react2.default.createElement(
          'div',
          { style: visibility },
          _lodash2.default.map(this.props.items, function (item, idx) {
            return _react2.default.createElement(_ArmorFabItem2.default, {
              key: idx,
              icon: item.icon,
              color: item.color,
              onClick: _this2.onItemClick,
              eventKey: idx
            });
          })
        ),
        _react2.default.createElement(
          _reactBootstrap.Button,
          {
            bsStyle: this.props.bsStyle || 'primary',
            className: 'fab-button',
            onClick: this.onMainFabClick
          },
          _react2.default.createElement(_FontAwesome2.default, { icon: 'plus', iconStyle: styles.plusIcon })
        )
      );
    }
  }]);

  return ArmorFab;
}(_react2.default.Component);

ArmorFab.propTypes = propTypes;
exports.default = ArmorFab;