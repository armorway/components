'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _ArmorFilterListItem = require('./ArmorFilterListItem');

var _ArmorFilterListItem2 = _interopRequireDefault(_ArmorFilterListItem);

var _FontAwesome = require('./FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

require('../styles/ArmorFilterList');

var _reactBootstrap = require('react-bootstrap');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  /**
   * Function to execute when a filter remove is clicked.
   */
  onFilterRemove: _react.PropTypes.func.isRequired,
  /**
   * Array of applied filters to build into ArmorFilterListItems.
   */
  filters: _react.PropTypes.array.isRequired,
  /**
   * Bootstrap type to apply to filter labels. Default: primary
   * Only applies to labels, not dropdown.
   */
  bsStyle: _react.PropTypes.string,
  /**
   * class to be applied.
   */
  className: _react.PropTypes.string,
  /**
   * Style to be applied.
   */
  style: _react.PropTypes.object
};

/**
 * ArmorFilterList is used to display applied filters.
 */

var ArmorFilterList = function (_React$Component) {
  _inherits(ArmorFilterList, _React$Component);

  function ArmorFilterList() {
    _classCallCheck(this, ArmorFilterList);

    var _this = _possibleConstructorReturn(this, (ArmorFilterList.__proto__ || Object.getPrototypeOf(ArmorFilterList)).call(this));

    _this.state = {};

    _this.onFilterRemoveClick = _this.onFilterRemoveClick.bind(_this);
    return _this;
  }

  _createClass(ArmorFilterList, [{
    key: 'onFilterRemoveClick',
    value: function onFilterRemoveClick(type, name) {
      this.props.onFilterRemove(type, name);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        { style: this.props.style, className: this.props.className + ' filter-list' },
        _react2.default.createElement(
          'span',
          { className: 'hidden-xs hidden-sm' },
          _lodash2.default.map(this.props.filters, function (filter, idx) {
            return _react2.default.createElement(_ArmorFilterListItem2.default, _extends({
              key: idx
            }, filter, {
              bsStyle: _this2.props.bsStyle,
              onRemove: _this2.onFilterRemoveClick,
              className: 'armor-filter-list-item'
            }));
          })
        ),
        _react2.default.createElement(
          'div',
          { className: 'armor-filter-dropdown hidden-md hidden-lg' },
          _react2.default.createElement(
            _reactBootstrap.DropdownButton,
            {
              id: 'armor-filter-dropdown',
              title: '(' + this.props.filters.length + ') Applied Filters',
              bsStyle: this.props.bsStyle ? this.props.bsStyle : 'default'
            },
            _lodash2.default.map(this.props.filters, function (filter, idx) {
              return _react2.default.createElement(
                _reactBootstrap.MenuItem,
                {
                  key: idx,
                  className: 'armor-filter-menu-item',
                  onSelect: function onSelect() {
                    return _this2.onFilterRemoveClick(filter.type, filter.name);
                  }
                },
                _react2.default.createElement(_FontAwesome2.default, { icon: 'times', className: 'remove-btn', padIcon: true }),
                filter.name
              );
            })
          )
        )
      );
    }
  }]);

  return ArmorFilterList;
}(_react2.default.Component);

ArmorFilterList.propTypes = propTypes;
exports.default = ArmorFilterList;