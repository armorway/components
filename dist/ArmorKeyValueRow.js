'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = require('react-bootstrap');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var defaultProps = {
  formatKeys: true,
  formatValues: true
};

var propTypes = {
  /**
    * key-name string
  */
  keyData: _react.PropTypes.any.isRequired,
  /**
    * value string mapped to the key-name
  */
  value: _react.PropTypes.any.isRequired,
  /**
   * If true, makes key col xs=4
   */
  smallKey: _react.PropTypes.bool,
  /**
    * custom className for ArmorKeyValueRow object
  */
  className: _react.PropTypes.string,
  /**
    * If true, this will format the keyData with camelToReadable.
    * Defaults at true
  */
  formatKeys: _react.PropTypes.bool,
  /**
    * If true, this will format the value with camelToReadable
    * Defaults at true
  */
  formatValues: _react.PropTypes.bool
};

/**
  * component used to render one row of a key map
*/

var ArmorKeyValueRow = function (_React$Component) {
  _inherits(ArmorKeyValueRow, _React$Component);

  function ArmorKeyValueRow() {
    _classCallCheck(this, ArmorKeyValueRow);

    var _this = _possibleConstructorReturn(this, (ArmorKeyValueRow.__proto__ || Object.getPrototypeOf(ArmorKeyValueRow)).call(this));

    _this.state = {};
    return _this;
  }

  _createClass(ArmorKeyValueRow, [{
    key: 'render',
    value: function render() {
      var keyData = this.props.keyData;
      var value = this.props.value;


      if (_lodash2.default.isBoolean(value)) {
        value = _utils.utils.booleanToYesNo(value);
      }
      var smallKey = this.props.smallKey;


      return _react2.default.createElement(
        _reactBootstrap.Row,
        {
          style: { marginBottom: '5px' },
          className: this.props.className + ' armor-key-value-row'
        },
        _react2.default.createElement(
          _reactBootstrap.Col,
          { xs: smallKey ? 4 : 6 },
          _react2.default.createElement(
            'span',
            { className: 'bold pull-left key-data' },
            this.props.formatKeys ? _lodash2.default.capitalize(_utils.utils.camelToReadable(keyData)) : keyData,
            _react2.default.createElement(
              'span',
              { className: 'key-colon' },
              ':'
            )
          )
        ),
        _react2.default.createElement(
          _reactBootstrap.Col,
          { xs: smallKey ? 8 : 6 },
          _react2.default.createElement(
            'span',
            { className: 'pull-left value-data' },
            this.props.formatValues ? _utils.utils.camelToReadable(value, true) : value
          )
        )
      );
    }
  }]);

  return ArmorKeyValueRow;
}(_react2.default.Component);

ArmorKeyValueRow.defaultProps = defaultProps;
ArmorKeyValueRow.propTypes = propTypes;
exports.default = ArmorKeyValueRow;