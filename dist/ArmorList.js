'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _reactBootstrap = require('react-bootstrap');

var _DropDownMenu = require('material-ui/DropDownMenu');

var _DropDownMenu2 = _interopRequireDefault(_DropDownMenu);

var _MenuItem = require('material-ui/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _ArmorListItem = require('./ArmorListItem');

var _ArmorListItem2 = _interopRequireDefault(_ArmorListItem);

var _FontAwesome = require('./FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _utils = require('./utils');

require('../styles/ArmorList.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var defaultProps = {
  resetPageOnNewData: true
};

var propTypes = {
  /**
   * Each item in this array will be a row in the list.
   * The named keys below are all 'special' in that they
   * each have a unique useage in ArmorListItem.
   * Every other key is displayed as a ArmorKeyValueRow.
   * ```
   *  {
   *  id: String | Number,
   *  img: String | Element,
   *  dateTime: String
   *  name: String,
   *  ... rest of data
   *  }
   * ```
   */
  data: _react.PropTypes.array.isRequired,
  /**
   * Name for list when printing/saving.
   */
  name: _react.PropTypes.string,
  /**
   * Value for ArmorTabbedList to key in on
   */
  value: _react.PropTypes.string,
  /**
   * Function to execute when a list item is clicked.
   */
  onClick: _react.PropTypes.func.isRequired,
  /**
   * Function to execute when the close button is clicked
   */
  onClear: _react.PropTypes.func,
  /**
   * Id of currently active/selected list item.
   */
  selected: _react.PropTypes.any,
  /**
   * Makes row expanded and permanenmt active.
   */
  allActive: _react.PropTypes.bool,
  /**
   * Key to sort on by default.
   * If not present, sort is on dateTime
   */
  defaultSortKey: _react.PropTypes.string,
  /**
   * Direction to sort on by default.
   * If not present, sort is ascending
   */
  defaultSortAscending: _react.PropTypes.bool,
  /**
   * Keys matching data to sort on
   */
  sortKeys: _react.PropTypes.array,
  /**
   * Keys in data object to not visually show. Still sortable.
   */
  /**
   * Display names of the sortKeys list, must be same length as sortKeys
   */
  sortKeyDisplayNames: _react.PropTypes.array,
  hideKeys: _react.PropTypes.array,
  /**
   * Array of two actions in an array to be displayed
   * at the bottom of the component.
   * ```
   * {
   *  name: String
   *  action: Function
   * }
   * ```
   */
  actions: _react.PropTypes.array,
  /**
   * Array in the format used for a ArmorFab Component
   */
  fabItems: _react.PropTypes.array,
  /**
   * Function to be executed before printing
   */
  onPrint: _react.PropTypes.func,
  /**
   * Function to be executed before saving
   */
  onSave: _react.PropTypes.func,
  /**
   * class to be applied.
   */
  className: _react.PropTypes.string,
  /**
   * Default sort key to replace dateTime
   */
  defaultKey: _react.PropTypes.string,
  /**
   * Sets size for ArmorKeyValueRow
   * use in ArmorListItem
   */
  smallKey: _react.PropTypes.string,
  /**
   * Style to be applied.
   */
  style: _react.PropTypes.object,
  /**
   * Boolean which will format keys in material design if true
   */
  formatKeys: _react.PropTypes.bool,
  /**
   * Boolean which will format values in material design if true
   */
  formatValues: _react.PropTypes.bool,
  /**
   * Number which specifies how many items per page.
   * If not specified, all will render.
   */
  pageSize: _react.PropTypes.number,
  /**
   * Removes Datetime from the sort list.
   */
  noDateTimeSort: _react.PropTypes.bool,
  /**
   * Stops ArmorList from going to page 1 when new data comes in.
   */
  resetPageOnNewData: _react.PropTypes.bool
};

/**
 * Creates a list of for displaying data. Also supports actions.
 */

var ArmorList = function (_React$Component) {
  _inherits(ArmorList, _React$Component);

  function ArmorList(props) {
    _classCallCheck(this, ArmorList);

    var _this = _possibleConstructorReturn(this, (ArmorList.__proto__ || Object.getPrototypeOf(ArmorList)).call(this, props));

    var ascending = !_lodash2.default.isUndefined(props.defaultSortAscending) ? props.defaultSortAscending : true;
    var sortKey = !_lodash2.default.isUndefined(props.defaultSortKey) ? props.defaultSortKey : _this.getDefaultKey();

    if (_this.props.noDateTimeSort) {
      sortKey = _this.props.sortKeys[0];
    }

    var sortedData = _this.sort(props.data, sortKey, ascending);
    var currentPage = 1;

    if (props.pageSize && props.selected) {
      var index = _lodash2.default.findIndex(sortedData, function (dataItem) {
        return dataItem.id === props.selected;
      }) + 1;

      currentPage = Math.ceil(index / props.pageSize);
    }

    _this.state = {
      print: false,
      sortMethod: sortKey,
      currentPage: currentPage,
      sortedData: sortedData,
      ascending: ascending
    };

    _this.onClick = _this.onClick.bind(_this);
    _this.buildSortOptions = _this.buildSortOptions.bind(_this);
    _this.onSortOptionSelect = _this.onSortOptionSelect.bind(_this);
    _this.getDefaultKey = _this.getDefaultKey.bind(_this);
    _this.onPaginationSelect = _this.onPaginationSelect.bind(_this);
    _this.onPrint = _this.onPrint.bind(_this);
    _this.onSave = _this.onSave.bind(_this);
    return _this;
  }

  _createClass(ArmorList, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      var newState = {};

      if (!_lodash2.default.isEqual(this.props.data, nextProps.data)) {
        var _state = this.state;
        var sortMethod = _state.sortMethod;
        var ascending = _state.ascending;

        var sortedData = this.sort(nextProps.data, sortMethod, ascending);

        newState.sortedData = sortedData;
        // reset page because data is new

        var currentData = _lodash2.default.map(this.props.data, function (data) {
          return _lodash2.default.omit(data, 'img');
        });
        var nextData = _lodash2.default.map(nextProps.data, function (data) {
          return _lodash2.default.omit(data, 'img');
        });

        if (!_lodash2.default.isEqual(currentData, nextData)) {
          newState.currentPage = 1;
        }
      }
      // change page to newly selected list item
      if ((nextProps.selected !== this.props.selected || !this.props.resetPageOnNewData) && nextProps.selected && nextProps.pageSize) {
        var data = newState.sortedData || this.state.sortedData;
        var index = _lodash2.default.findIndex(data, function (dataItem) {
          return dataItem.id === nextProps.selected;
        }) + 1;

        newState.currentPage = Math.ceil(index / nextProps.pageSize);
      }

      if (!newState.currentPage || newState.currentPage <= 0) {
        newState.currentPage = 1;
      }

      if (!_lodash2.default.isEmpty(newState)) {
        this.setState(newState);
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      if (this.props.value !== prevProps.value) {
        var sortMethod = !_lodash2.default.isUndefined(this.props.defaultSortKey) ? this.props.defaultSortKey : this.getDefaultKey();
        var ascending = !_lodash2.default.isUndefined(prevProps.defaultSortAscending) ? prevProps.defaultSortAscending : true;
        var sortedData = this.sort(prevProps.data, sortMethod, ascending);

        // eslint-disable-next-line
        this.setState({
          ascending: ascending,
          sortMethod: sortMethod,
          sortedData: sortedData
        });
      }
    }
  }, {
    key: 'onClick',
    value: function onClick() {
      var _props;

      (_props = this.props).onClick.apply(_props, arguments);
    }
  }, {
    key: 'onPrint',
    value: function onPrint() {
      var _this2 = this;

      // set view into print mode to expand all elements
      this.setState({ print: true }, function () {
        _utils.utils.print('.armor-list-print', false, '<p>' + (_this2.props.name || '') + '</p>').then(function () {
          // revert print view
          _this2.setState({ print: false });
        });
      });
    }
  }, {
    key: 'onSave',
    value: function onSave() {
      var data = this.props.onPrint(this.props.data) || this.props.data;
      var formattedData = _lodash2.default.map(data, function (dat) {
        return _lodash2.default.omit(dat, 'img');
      });

      _utils.utils.downloadAsCSV(formattedData, this.props.name);
    }
  }, {
    key: 'onPaginationSelect',
    value: function onPaginationSelect(event, eventData) {
      this.setState({ currentPage: eventData.eventKey });

      try {
        _reactDom2.default.findDOMNode(this.refs.listGroup).scrollTop = 0;
      } catch (exe) {
        // eslint-disable-next-line
        console.log(exe);
      }
    }
  }, {
    key: 'onSortOptionSelect',
    value: function onSortOptionSelect(idx, evt, sortMethod) {
      var ascending = sortMethod === this.state.sortMethod ? !this.state.ascending : true;
      var sortedData = this.sort(this.props.data, sortMethod, ascending);

      this.setState({
        ascending: ascending,
        sortMethod: sortMethod,
        sortedData: sortedData
      });
    }
  }, {
    key: 'getListData',
    value: function getListData() {
      var currentPage = this.state.currentPage;
      var pageSize = this.props.pageSize;

      var listData = this.state.sortedData;

      if (pageSize) {
        var start = (currentPage - 1) * pageSize;
        var end = currentPage * pageSize;

        return listData.slice(start, end);
      }
      return listData;
    }
  }, {
    key: 'getDefaultKey',
    value: function getDefaultKey(type) {
      if (type === 'key') {
        return this.props.defaultKey || 'dateTime';
      }
      return this.props.defaultKey || 'Date';
    }
  }, {
    key: 'getHiddenKeys',
    value: function getHiddenKeys(dateTimeKey) {
      var defaultHiddenKeys = ['img', 'actions', 'name', dateTimeKey, 'id', 'className', 'style', 'customRow'];

      return defaultHiddenKeys.concat(this.props.hideKeys || []);
    }
  }, {
    key: 'buildSortOptions',
    value: function buildSortOptions() {
      var _this3 = this;

      var dateTimeKey = this.getDefaultKey();
      var options = _lodash2.default.cloneDeep(this.props.sortKeys || []);

      if (!this.props.noDateTimeSort) {
        options = _lodash2.default.concat([dateTimeKey], options);
      }

      var sortKeyDisplayNames = this.props.sortKeyDisplayNames || options;

      return _lodash2.default.map(options, function (option, idx) {
        return _react2.default.createElement(_MenuItem2.default, {
          value: option,
          key: idx,
          className: 'sort-menu-item',
          primaryText: sortKeyDisplayNames[idx],
          label: _this3.buildMenuItemLabel(sortKeyDisplayNames[idx])
        });
      });
    }
  }, {
    key: 'buildMenuItemLabel',
    value: function buildMenuItemLabel(displayName) {
      var title = _react2.default.createElement(
        'span',
        null,
        _react2.default.createElement(
          'span',
          null,
          'Sort: ' + displayName
        ),
        _react2.default.createElement(_FontAwesome2.default, {
          icon: this.state.ascending ? 'arrow-up' : 'arrow-down',
          style: { padding: '0 3px 0px 6px' }
        })
      );

      return title;
    }
  }, {
    key: 'sort',
    value: function sort(data, method, ascending) {
      var sortedData = void 0;
      var dateTimeKey = this.getDefaultKey();

      switch (method) {
        case dateTimeKey:
          sortedData = _lodash2.default.sortBy(data, dateTimeKey);
          break;
        default:
          sortedData = _lodash2.default.sortBy(data, method);
          break;
      }
      if (!ascending) {
        sortedData = sortedData.reverse();
      }
      return sortedData;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this4 = this;

      var sortMethod = this.state.sortMethod;

      var listData = this.getListData();

      var dateTimeKey = this.getDefaultKey('key');
      var hiddenKeys = this.getHiddenKeys(dateTimeKey);
      var printClass = this.state.print ? 'print' : '';

      return _react2.default.createElement(
        'div',
        {
          className: 'armor-list armor-list-print ' + printClass
        },
        _react2.default.createElement(
          'div',
          { className: 'armor-list-header' },
          _react2.default.createElement(
            'div',
            { className: 'armor-list-sort' },
            _react2.default.createElement(
              _DropDownMenu2.default,
              {
                id: 'armor-list-sort-dropdown',
                className: 'armor-list-sort-options',
                value: sortMethod,
                onChange: this.onSortOptionSelect
              },
              this.buildSortOptions()
            )
          ),
          _react2.default.createElement(
            'div',
            { className: 'armor-list-print-save' },
            _react2.default.createElement(
              'div',
              null,
              this.props.onPrint ? _react2.default.createElement(
                _reactBootstrap.Button,
                { bsStyle: 'link', onClick: this.onPrint },
                _react2.default.createElement(_FontAwesome2.default, { icon: 'print' })
              ) : '',
              this.props.onSave ? _react2.default.createElement(
                _reactBootstrap.Button,
                { bsStyle: 'link', onClick: this.onSave },
                _react2.default.createElement(_FontAwesome2.default, { icon: 'download' })
              ) : ''
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'armor-list-and-pagination' },
          _react2.default.createElement(
            _reactBootstrap.ListGroup,
            {
              ref: 'listGroup',
              onClick: this.onItemClicked,
              className: this.props.className + ' armor-list'
            },
            _lodash2.default.map(listData, function (data) {
              var trimmedData = _lodash2.default.omit(data, hiddenKeys);

              return _react2.default.createElement(_ArmorListItem2.default, {
                ref: 'listItem',
                key: data.id,
                img: data.img,
                id: data.id,
                name: data.name,
                customRow: data.customRow,
                actions: _this4.props.actions,
                dateTime: data[dateTimeKey],
                data: trimmedData,
                onClick: _this4.onClick,
                fabItems: _this4.props.fabItems,
                active: _this4.props.selected === data.id,
                allActive: _this4.props.allActive || _this4.state.print,
                formatKeys: _this4.props.formatKeys,
                formatValues: _this4.props.formatValues,
                smallKey: _this4.props.smallKey,
                className: data.className,
                style: data.style,
                printing: _this4.state.print,
                clearable: _this4.props.onClear !== undefined,
                onClear: _this4.props.onClear
              });
            })
          ),
          this.props.pageSize && this.state.sortedData.length ? _react2.default.createElement(
            'div',
            { className: 'pagination-wrapper' },
            _react2.default.createElement(_reactBootstrap.Pagination, {
              prev: true,
              next: true,
              maxButtons: 4,
              items: Math.ceil(this.props.data.length / this.props.pageSize),
              activePage: this.state.currentPage,
              onSelect: this.onPaginationSelect
            })
          ) : ''
        )
      );
    }
  }]);

  return ArmorList;
}(_react2.default.Component);

ArmorList.defaultProps = defaultProps;
ArmorList.propTypes = propTypes;
exports.default = ArmorList;