'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Table = require('material-ui/Table');

var _IconButton = require('material-ui/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _arrowBack = require('material-ui/svg-icons/navigation/arrow-back');

var _arrowBack2 = _interopRequireDefault(_arrowBack);

var _arrowForward = require('material-ui/svg-icons/navigation/arrow-forward');

var _arrowForward2 = _interopRequireDefault(_arrowForward);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  onChange: _react.PropTypes.func.isRequired,
  pageSize: _react.PropTypes.number.isRequired,
  numPages: _react.PropTypes.number.isRequired
};

var styles = {
  pageText: {
    position: 'relative',
    top: '-8px',
    margin: '10px'
  }
};

var ArmorTableFooter = function (_React$Component) {
  _inherits(ArmorTableFooter, _React$Component);

  function ArmorTableFooter() {
    _classCallCheck(this, ArmorTableFooter);

    var _this = _possibleConstructorReturn(this, (ArmorTableFooter.__proto__ || Object.getPrototypeOf(ArmorTableFooter)).call(this));

    _this.state = {
      page: 0
    };

    _this.onPageClick = _this.onPageClick.bind(_this);
    _this.onNavigate = _this.onNavigate.bind(_this);
    return _this;
  }

  _createClass(ArmorTableFooter, [{
    key: 'onPageClick',
    value: function onPageClick() {
      var _this2 = this;

      this.setState({ page: (1 + this.state.page) % this.props.numPages }, function () {
        _this2.props.onChange(_this2.state.page);
      });
    }
  }, {
    key: 'onNavigate',
    value: function onNavigate(evt) {
      var _this3 = this;

      var amount = parseInt(evt.currentTarget.dataset.navigate, 10);
      var page = (amount + this.props.numPages + this.state.page) % this.props.numPages;

      this.setState({ page: page }, function () {
        _this3.props.onChange(_this3.state.page);
      });
    }
  }, {
    key: 'render',
    value: function render() {
      if (this.props.numPages <= 1) {
        return null;
      }

      return _react2.default.createElement(
        _Table.TableRow,
        { style: { float: 'right' } },
        _react2.default.createElement(
          _Table.TableRowColumn,
          null,
          _react2.default.createElement(
            _IconButton2.default,
            {
              'data-navigate': '-1',
              onClick: this.onNavigate
            },
            _react2.default.createElement(_arrowBack2.default, null)
          ),
          _react2.default.createElement(
            'span',
            { style: styles.pageText },
            this.state.page + 1,
            ' of ',
            this.props.numPages
          ),
          _react2.default.createElement(
            _IconButton2.default,
            {
              'data-navigate': '1',
              onClick: this.onNavigate
            },
            _react2.default.createElement(_arrowForward2.default, null)
          )
        )
      );
    }
  }]);

  return ArmorTableFooter;
}(_react2.default.Component);

ArmorTableFooter.propTypes = propTypes;
exports.default = ArmorTableFooter;