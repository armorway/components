'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _FontAwesome = require('./FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

var _reactBootstrap = require('react-bootstrap');

require('../styles/ArmorFilterListItem.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  /**
   * Function to excute when item is removed.
   * onRemove(type, name)
   */
  onRemove: _react.PropTypes.func.isRequired,
  /**
   *  Name/text of the component. It is the displayed text.
   */
  name: _react.PropTypes.string.isRequired,
  /**
   *  Type of the filter. Used to identify the filter.
   */
  type: _react.PropTypes.string.isRequired,
  /**
    * Bootstrap type to apply to filter labels. Default: primary
    */
  bsStyle: _react.PropTypes.string,
  /**
   * class to be applied.
   */
  className: _react.PropTypes.string
};

/**
 * ArmorFilterListItem is designed to be used as an internal component
 * to the ArmorFilterListItem component. The applied filters will be
 * displayed with a remove button.
 */

var ArmorFilterListItem = function (_React$Component) {
  _inherits(ArmorFilterListItem, _React$Component);

  function ArmorFilterListItem(props) {
    _classCallCheck(this, ArmorFilterListItem);

    var _this = _possibleConstructorReturn(this, (ArmorFilterListItem.__proto__ || Object.getPrototypeOf(ArmorFilterListItem)).call(this, props));

    _this.onRemoveClick = _this.onRemoveClick.bind(_this);
    return _this;
  }

  _createClass(ArmorFilterListItem, [{
    key: 'onRemoveClick',
    value: function onRemoveClick() {
      this.props.onRemove(this.props.type, this.props.name);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        _reactBootstrap.Label,
        { bsStyle: this.props.bsStyle || 'primary', className: (this.props.className || '') + ' filter-item' },
        _react2.default.createElement(
          'button',
          { onClick: this.onRemoveClick, className: 'remove-btn' },
          _react2.default.createElement(_FontAwesome2.default, { icon: 'close', className: 'remove-divider' })
        ),
        _react2.default.createElement(
          'span',
          { className: 'filter-item-name', onClick: this.onRemoveClick },
          this.props.name
        )
      );
    }
  }]);

  return ArmorFilterListItem;
}(_react2.default.Component);

ArmorFilterListItem.propTypes = propTypes;
exports.default = ArmorFilterListItem;