'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _FontAwesome = require('./FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

require('../styles/ArmorFabItem.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  /**
   * Function to excute when item is clicked.
   */
  onClick: _react.PropTypes.func.isRequired,
  /**
   *  When a string is passed, a FontAwesome icon will be rendered.
   *  When an object is passed, that will be rendered as the icon.
   */
  icon: _react.PropTypes.oneOfType([_react.PropTypes.string, _react.PropTypes.object]).isRequired,
  /**
   * Background color for the Item
   */
  color: _react.PropTypes.string.isRequired,
  /**
   * Event key passed on click to identify the clicked item
   */
  eventKey: _react.PropTypes.oneOfType([_react.PropTypes.string, _react.PropTypes.number])
};

var styles = {
  icon: {
    color: '#fff'
  },
  innerIcon: {
    position: 'relative'
  }
};

/**
 * ArmorFabItem is designed to be used as an internal component
 * to the ArmorFab component. They will be displayed above the
 * FAB and have represent actions one can take.
 */

var ArmorFabItem = function (_React$Component) {
  _inherits(ArmorFabItem, _React$Component);

  function ArmorFabItem(props) {
    _classCallCheck(this, ArmorFabItem);

    var _this = _possibleConstructorReturn(this, (ArmorFabItem.__proto__ || Object.getPrototypeOf(ArmorFabItem)).call(this, props));

    _this.onClick = _this.onClick.bind(_this);
    return _this;
  }

  _createClass(ArmorFabItem, [{
    key: 'onClick',
    value: function onClick() {
      this.props.onClick(this.props.eventKey);
    }
  }, {
    key: 'render',
    value: function render() {
      var icon = _lodash2.default.isString(this.props.icon) ? _react2.default.createElement(_FontAwesome2.default, { icon: this.props.icon, size: '2x', iconStyle: styles.icon }) : this.props.icon;

      var backgroundColor = this.props.color;


      return _react2.default.createElement(
        'button',
        { className: 'fab-item', style: { backgroundColor: backgroundColor }, onClick: this.onClick },
        _react2.default.createElement(
          'div',
          { className: 'icon-wrapper', style: styles.innerIcon },
          icon
        )
      );
    }
  }]);

  return ArmorFabItem;
}(_react2.default.Component);

ArmorFabItem.propTypes = propTypes;
exports.default = ArmorFabItem;