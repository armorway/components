'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _IconButton = require('material-ui/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _FlatButton = require('material-ui/FlatButton');

var _FlatButton2 = _interopRequireDefault(_FlatButton);

var _reactBootstrap = require('react-bootstrap');

var _ArmorKeyValueRow = require('./ArmorKeyValueRow');

var _ArmorKeyValueRow2 = _interopRequireDefault(_ArmorKeyValueRow);

var _ArmorFAB = require('./ArmorFAB');

var _ArmorFAB2 = _interopRequireDefault(_ArmorFAB);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

require('../styles/ArmorListItem');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  /**
   * Id for the List item, it used to know which row is clicked
   */
  id: _react.PropTypes.any.isRequired,
  /**
   * Name to be displayed
   */
  name: _react.PropTypes.string.isRequired,
  /**
   * DateTime of event
   */
  dateTime: _react.PropTypes.string.isRequired,
  /**
   * Logo for type
   */
  img: _react.PropTypes.oneOfType([_react.PropTypes.element, _react.PropTypes.string]),
  /**
   * Triggered when row is clicked.
   */
  onClick: _react.PropTypes.func.isRequired,
  /**
   * Function to be called when clear button is clicked
   */
  onClear: _react.PropTypes.func,
  /**
   * Meta data to be displayed as rows when item is active.
   */
  data: _react.PropTypes.object,
  /**
   * When active, the row expands and shows more data
   * and the FAB Icon.
   */
  active: _react.PropTypes.bool,
  /**
   * Makes row expanded and permanent active.
   */
  allActive: _react.PropTypes.bool,
  /**
   * Shows close button on selected items
   */
  clearable: _react.PropTypes.bool,
  /**
   * Actions to be displayed at buttom of component.
   * If provided, it expects 2.
   */
  actions: _react.PropTypes.array,
  /**
    * Array in the format used for a ArmorFab Component.
    */
  fabItems: _react.PropTypes.array,
  /**
   * React Component to be rendered above actions
   * and below data rows
   */
  customRow: _react.PropTypes.object,
  /**
   * class to be applied.
   */
  className: _react.PropTypes.string,
  /**
   * Style to be applied.
   */
  style: _react.PropTypes.object,

  /**
   * Boolean which will format keys in material design if true
   */
  formatKeys: _react.PropTypes.bool,
  /**
   * Boolean which will format values in material design if true
   */
  formatValues: _react.PropTypes.bool,
  /**
   * Sets size for ArmorKeyValueRow
   */
  smallKey: _react.PropTypes.string,
  /**
   * Used to inform the component printing is happening
   */
  printing: _react.PropTypes.bool
};

/**
 * ArmorListItem is designed to be used as an internal component
 * to the ArmorList component. It is a list of information with
 * actions, FAB and an image.
 */

var ArmorListItem = function (_React$Component) {
  _inherits(ArmorListItem, _React$Component);

  function ArmorListItem(props) {
    _classCallCheck(this, ArmorListItem);

    var _this = _possibleConstructorReturn(this, (ArmorListItem.__proto__ || Object.getPrototypeOf(ArmorListItem)).call(this, props));

    _this.onItemClick = _this.onItemClick.bind(_this);
    _this.buildDataRows = _this.buildDataRows.bind(_this);
    _this.onActionClick = _this.onActionClick.bind(_this);
    _this.onClearClick = _this.onClearClick.bind(_this);
    return _this;
  }

  _createClass(ArmorListItem, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (this.props.active) {
        this.scrollIntoView();
      }
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.active !== this.props.active || nextProps.printing !== this.props.printing || nextProps.customRow !== this.props.customRow || !_lodash2.default.isEqual(nextProps.img, this.props.img);
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      if (this.props.active && !prevProps.active) {
        this.scrollIntoView();
      }
    }
  }, {
    key: 'onClearClick',
    value: function onClearClick(evt) {
      evt.stopPropagation();
      this.props.onClear();
    }
  }, {
    key: 'onItemClick',
    value: function onItemClick() {
      this.props.onClick(this.props.id);
    }
  }, {
    key: 'onActionClick',
    value: function onActionClick(evt) {
      var idx = evt.currentTarget.value;
      var action = this.props.actions[idx].action;


      if (action) {
        action(evt, this.props.id, this.props.name, this.props.data);
      }
    }
  }, {
    key: 'scrollIntoView',
    value: function scrollIntoView() {
      this.refs.listItem.scrollIntoView();
    }
  }, {
    key: 'buildDataRows',
    value: function buildDataRows(data) {
      var _this2 = this;

      var _props = this.props;
      var actions = _props.actions;
      var fabItems = _props.fabItems;
      var customRow = _props.customRow;

      // either you have both actions or you have none.

      return _react2.default.createElement(
        'span',
        null,
        this.props.fabItems && !this.props.printing ? _react2.default.createElement(_ArmorFAB2.default, {
          items: fabItems,
          className: 'armor-list-item-fab',
          small: true
        }) : '',
        _react2.default.createElement(
          _reactBootstrap.Col,
          { xs: 12, className: 'armor-list-data' },
          _react2.default.createElement(
            'div',
            { className: 'armor-list-item-data-rows' },
            _lodash2.default.map(data, function (value, rowKey) {
              return _react2.default.createElement(_ArmorKeyValueRow2.default, {
                key: rowKey,
                keyData: rowKey,
                value: value,
                formatKeys: _this2.props.formatKeys,
                formatValues: _this2.props.formatValues,
                smallKey: _this2.props.smallKey
              });
            })
          ),
          customRow ? _react2.default.createElement(
            'div',
            null,
            customRow
          ) : '',
          actions && actions.length >= 2 ? _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              _reactBootstrap.Col,
              { xs: 6, className: 'btn-col' },
              _react2.default.createElement(_FlatButton2.default, {
                value: 0,
                className: 'armor-list-action',
                onClick: this.onActionClick,
                label: actions[0].name,
                primary: true
              })
            ),
            _react2.default.createElement(
              _reactBootstrap.Col,
              { xs: 6, className: 'btn-col' },
              _react2.default.createElement(_FlatButton2.default, {
                value: 1,
                className: 'armor-list-action',
                onClick: this.onActionClick,
                label: actions[1].name,
                primary: true
              })
            )
          ) : ''
        )
      );
    }
  }, {
    key: 'buildImg',
    value: function buildImg() {
      var img = this.props.img;


      if (!img) {
        return '';
      }
      return _lodash2.default.isString(img) ? _react2.default.createElement('img', { src: this.props.img, className: 'armor-list-row-img' }) : _react2.default.createElement(
        'span',
        { className: 'armor-list-row-img armor-list-row-element' },
        img
      );
    }
  }, {
    key: 'isActive',
    value: function isActive() {
      return this.props.active;
    }
  }, {
    key: 'render',
    value: function render() {
      var className = this.props.className ? this.props.className : '';
      var active = this.isActive() ? 'active' : '';
      var allActive = this.props.allActive ? 'all-active' : '';

      return _react2.default.createElement(
        'div',
        {
          onClick: this.onItemClick,
          className: className + ' ' + active + ' list-group-item ' + allActive,
          ref: 'listItem',
          style: this.props.style
        },
        _react2.default.createElement(
          _reactBootstrap.Grid,
          { fluid: true },
          this.props.clearable && active ? _react2.default.createElement(_IconButton2.default, {
            className: 'close-button-container',
            iconClassName: 'fa fa-close close-clear',
            onClick: this.onClearClick,
            tooltipPosition: 'bottom-left',
            tooltip: 'Close Selection'
          }) : '',
          _react2.default.createElement(
            _reactBootstrap.Row,
            null,
            _react2.default.createElement(
              _reactBootstrap.Col,
              { xs: 2, className: 'armor-list-item-img-col' },
              this.buildImg()
            ),
            _react2.default.createElement(
              _reactBootstrap.Col,
              { xs: 10 },
              _react2.default.createElement(
                'div',
                { className: 'armor-list-item-name' },
                this.props.name
              ),
              _react2.default.createElement(
                'div',
                { className: 'armor-list-item-date-time' },
                this.props.dateTime
              )
            )
          ),
          _react2.default.createElement(
            _reactBootstrap.Row,
            null,
            _react2.default.createElement(
              _reactBootstrap.Col,
              { xs: 12 },
              _react2.default.createElement(
                'div',
                null,
                this.isActive() || this.props.allActive ? this.buildDataRows(this.props.data) : ''
              )
            )
          )
        )
      );
    }
  }]);

  return ArmorListItem;
}(_react2.default.Component);

ArmorListItem.propTypes = propTypes;
exports.default = ArmorListItem;