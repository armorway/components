'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _reactBootstrap = require('react-bootstrap');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  /**
    * Label for the input box
    * placed before the box on the left
  */
  label: _react.PropTypes.string,
  /**
    * Message to be displayed when error occurs
  */
  errorMsg: _react.PropTypes.string,
  /**
    * Function which takes in an event to be called
    * when a change occurs
  */
  onChange: _react.PropTypes.func,
  /**
    * inner children of the component
  */
  children: _react.PropTypes.object,
  /**
    * Is true when input box is valid to update,
    * Defaults at true and will not update if false
  */
  valid: _react.PropTypes.bool,
  /**
    * Style for the input box
    * Is only used for 'error', otherwise remains undefined
  */
  bsStyle: _react.PropTypes.string,
  /**
    * If true, input box will not display an asterisk
    * which indicates a required label.
  */
  noAsterisk: _react.PropTypes.bool
};

/**
  * This component represents an inputBox with
  * an optional label and error configuration
*/

var ArmorInput = function (_React$Component) {
  _inherits(ArmorInput, _React$Component);

  function ArmorInput(props) {
    _classCallCheck(this, ArmorInput);

    var _this = _possibleConstructorReturn(this, (ArmorInput.__proto__ || Object.getPrototypeOf(ArmorInput)).call(this, props));

    _this.state = _lodash2.default.assign({ valid: true, bsStyle: undefined }, _this.props);

    _this.state.label = _this.formatLabel(_this.state);

    _this.checkValidity = _this.checkValidity.bind(_this);
    _this.buildToolTipIfNeeded = _this.buildToolTipIfNeeded.bind(_this);
    return _this;
  }

  /*
    When the Virtual DOM rerenders, it doesnt change
    the component and since we are pushing props through,
    we must update.
  */


  _createClass(ArmorInput, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(newProps) {
      var otherProps = _lodash2.default.clone(newProps);

      otherProps.label = this.formatLabel(newProps);
      this.setState(otherProps);
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      return nextState.valid !== this.state.valid || nextState.value !== this.state.value || nextProps.bsStyle !== this.state.bsStyle || nextProps !== this.props;
    }
  }, {
    key: 'formatLabel',
    value: function formatLabel(state) {
      var label = '';

      try {
        if (state.required && !state.noAsterisk) {
          label = _react2.default.createElement(
            'span',
            { className: 'input-label' },
            state.label,
            ' ',
            _react2.default.createElement(
              'span',
              { className: 'text-danger' },
              '*'
            )
          );
        } else {
          label = _react2.default.createElement(
            'span',
            { className: 'input-label' },
            state.label
          );
        }
      } catch (exe) {
        /* eslint-disable no-console */
        console.log(exe);
        /* eslint-enable no-console */
      }

      return label;
    }
  }, {
    key: 'checkValidity',
    value: function checkValidity(event) {
      if (event) {
        var bsStyle = this.state.required && !_lodash2.default.trim(event.target.value) ? 'error' : undefined;

        this.setState({
          bsStyle: bsStyle,
          value: event.target.value
        });
      }
      // This has to be after above code.
      // or else you will over ride the props.onChange changes
      if (this.state.onChange) {
        this.state.onChange(event);
      }
    }
  }, {
    key: 'buildToolTipIfNeeded',
    value: function buildToolTipIfNeeded() {
      if (this.state.bsStyle === 'error' && this.state.errorMsg) {
        return _react2.default.createElement(
          _reactBootstrap.Tooltip,
          { placement: 'top', className: 'in', id: this.state.errorMsg },
          this.state.errorMsg
        );
      }
      return _react2.default.createElement('span', null);
    }
  }, {
    key: 'render',
    value: function render() {
      var inputBox = _react2.default.createElement(
        _reactBootstrap.Input,
        _extends({}, this.state, { onChange: this.checkValidity, className: 'input-box' }),
        this.props.children
      );

      return _react2.default.createElement(
        _reactBootstrap.OverlayTrigger,
        { placement: 'top', overlay: this.buildToolTipIfNeeded() },
        inputBox
      );
    }
  }]);

  return ArmorInput;
}(_react2.default.Component);

ArmorInput.propTypes = propTypes;

exports.default = ArmorInput;