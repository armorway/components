'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _desc, _value, _class, _class2, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _FlatButton = require('material-ui/FlatButton');

var _FlatButton2 = _interopRequireDefault(_FlatButton);

var _MenuItem = require('material-ui/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _IconMenu = require('material-ui/IconMenu');

var _IconMenu2 = _interopRequireDefault(_IconMenu);

var _IconButton = require('material-ui/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _DatePicker = require('material-ui/DatePicker');

var _DatePicker2 = _interopRequireDefault(_DatePicker);

var _TimePicker = require('material-ui/TimePicker');

var _TimePicker2 = _interopRequireDefault(_TimePicker);

var _RaisedButton = require('material-ui/RaisedButton');

var _RaisedButton2 = _interopRequireDefault(_RaisedButton);

var _Checkbox = require('material-ui/Checkbox');

var _Checkbox2 = _interopRequireDefault(_Checkbox);

var _moreVert = require('material-ui/svg-icons/navigation/more-vert');

var _moreVert2 = _interopRequireDefault(_moreVert);

var _reactBootstrap = require('react-bootstrap');

var _velocityReact = require('velocity-react');

var _autobindDecorator = require('autobind-decorator');

var _autobindDecorator2 = _interopRequireDefault(_autobindDecorator);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

require('../styles/ArmorViewFilter');

var _ArmorVFCheckbox = require('./ArmorVFCheckbox');

var _ArmorVFCheckbox2 = _interopRequireDefault(_ArmorVFCheckbox);

var _ArmorVFDropdown = require('./ArmorVFDropdown');

var _ArmorVFDropdown2 = _interopRequireDefault(_ArmorVFDropdown);

var _ArmorVFCrimepicker = require('./ArmorVFCrimepicker');

var _ArmorVFCrimepicker2 = _interopRequireDefault(_ArmorVFCrimepicker);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
  var desc = {};
  Object['ke' + 'ys'](descriptor).forEach(function (key) {
    desc[key] = descriptor[key];
  });
  desc.enumerable = !!desc.enumerable;
  desc.configurable = !!desc.configurable;

  if ('value' in desc || desc.initializer) {
    desc.writable = true;
  }

  desc = decorators.slice().reverse().reduce(function (desc, decorator) {
    return decorator(target, property, desc) || desc;
  }, desc);

  if (context && desc.initializer !== void 0) {
    desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
    desc.initializer = undefined;
  }

  if (desc.initializer === void 0) {
    Object['define' + 'Property'](target, property, desc);
    desc = null;
  }

  return desc;
}

var ArmorViewFilter = (_class = (_temp = _class2 = function (_React$Component) {
  _inherits(ArmorViewFilter, _React$Component);

  function ArmorViewFilter(props) {
    _classCallCheck(this, ArmorViewFilter);

    var _this = _possibleConstructorReturn(this, (ArmorViewFilter.__proto__ || Object.getPrototypeOf(ArmorViewFilter)).call(this, props));

    var filters = props.filters;
    var overlays = props.overlays;
    var staticOverlays = props.staticOverlays;


    _this.state = {
      showFilters: false,
      showOverlays: false,
      showStaticOverlays: true,
      filters: filters,
      overlays: overlays,
      staticOverlays: staticOverlays
    };
    _this.closeState = {
      showFilters: false,
      showOverlays: false,
      showStaticOverlays: true
    };
    _this.hideTimout = undefined;
    _this.applyFilters = _this.exportActiveFilters;
    _this.onCloseFilters = _this.onCloseFilters.bind(_this);

    _this.closeFilters = _this.onCloseFilters;
    return _this;
  }

  _createClass(ArmorViewFilter, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      var filters = nextProps.filters;
      var overlays = nextProps.overlays;
      var staticOverlays = nextProps.staticOverlays;


      if (!_lodash2.default.isEqual(this.props.filters, nextProps.filters) || !_lodash2.default.isEqual(this.props.overlays, nextProps.overlays) || !_lodash2.default.isEqual(this.props.staticOverlays, nextProps.overlays)) {
        this.setState({
          filters: filters,
          overlays: overlays,
          staticOverlays: staticOverlays
        });
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      window.clearTimeout(this.hideTimeout);
    }
  }, {
    key: 'onFilterChange',
    value: function onFilterChange(name, filter) {
      var _this2 = this;

      // Grab metadata about the filter
      var id = filter.id;
      var optionName = filter.name;
      var children = filter.children;

      var view = this.getView();
      // Clones the state of the opened window (filters or overlas)
      var newState = this.state[view];
      // Gets the correct filter, searches by name
      var idxType = _lodash2.default.findIndex(this.state[view], function (itm) {
        return itm.name === name;
      });
      // Gets the correct filter selected from the new set of filters
      var newFilter = newState[idxType];

      // Grabs 3 indices, the old filter, new filter and the All selection
      // Then uses them by getting the actual options based of the index
      var idxNewState = this.getIndex(newFilter, id);
      var idxState = this.getIndex(this.state[view][idxType], id);
      var idxAll = this.getAllIndex(newFilter);
      var shouldSelectOption = !this.state[view][idxType].options[idxState].active;
      var allFilter = newFilter.options[idxAll];

      newFilter.options[idxNewState].active = shouldSelectOption;

      // Looks for anything else related (children, all options) then selects them
      this.setState(_defineProperty({}, view, newState), function () {
        _this2.selectAllWithId(filter, newFilter.name, shouldSelectOption);
        if (_lodash2.default.includes(_this2.props.dynamicFilters, name)) {
          _this2.props.onChange(name, filter);
        }
      });
    }
  }, {
    key: 'onSomeChange',
    value: function onSomeChange(filterName, value) {
      var _this3 = this;

      var view = this.getView();
      var newState = this.state[view];
      var idx = _lodash2.default.findIndex(newState, function (filter) {
        return filter.name === filterName;
      });

      newState[idx].value = value;

      this.setState(_defineProperty({}, view, newState), function () {
        if (_lodash2.default.includes(_this3.props.dynamicFilters, filterName)) {
          _this3.props.onChange(filterName, value);
        }
      });
    }
  }, {
    key: 'onInputChange',
    value: function onInputChange(filterName, event) {
      this.onSomeChange(filterName, event.target.value);
    }
  }, {
    key: 'onDropdownChange',
    value: function onDropdownChange(filterName, event, index, value) {
      this.onSomeChange(filterName, value);
    }
  }, {
    key: 'onTimeChange',
    value: function onTimeChange(filterName, event, value) {
      this.onSomeChange(filterName, value);
    }
  }, {
    key: 'onCrimepickerDropdown',
    value: function onCrimepickerDropdown(filterName, value) {
      var view = this.getView();
      var newState = this.state[view];
      var idx = _lodash2.default.findIndex(newState, function (filter) {
        return filter.name === filterName;
      });

      newState[idx].selected = value;

      var crimeGroup = newState[idx].groups[newState[idx].selected].metadata;

      if (crimeGroup !== 'other') {
        _lodash2.default.forEach(newState[idx].options, function (option) {
          console.log('op', option);
          _lodash2.default.forEach(option, function (opVal) {
            opVal.active = _lodash2.default.includes(crimeGroup, opVal.id);
          });
        });
      }

      this.setState(_defineProperty({}, view, newState));
    }
  }, {
    key: 'onCrimepickerChange',
    value: function onCrimepickerChange(filterName, newOption, category) {
      var view = this.getView();
      var newState = this.state[view];
      var idx = _lodash2.default.findIndex(newState, function (filter) {
        return filter.name === filterName;
      });

      _lodash2.default.forEach(newState[idx].options[category], function (option) {
        if (newOption.id === option.id) {
          option.active = !option.active;
        }
      });
      this.setState(_defineProperty({}, view, newState));
    }
  }, {
    key: 'onReset',
    value: function onReset() {
      this.props.onReset();
      this.onCloseFilters();
    }
  }, {
    key: 'onResetStaticOverlays',
    value: function onResetStaticOverlays() {
      var resetState = this.props.staticOverlays;

      this.setState({ staticOverlays: resetState });
    }
  }, {
    key: 'onApply',
    value: function onApply(externalView) {
      this.exportActiveFilters(this.getView());
    }
  }, {
    key: 'onMouseLeave',
    value: function onMouseLeave() {
      var _this4 = this;

      this.hideTimeout = window.setTimeout(function () {
        _this4.onCloseFilters();
      }, this.props.timeoutInterval);
    }
  }, {
    key: 'onMouseEnter',
    value: function onMouseEnter() {
      window.clearTimeout(this.hideTimeout);
    }
  }, {
    key: 'onClick',
    value: function onClick(view) {
      var shouldOpenFilters = view === 'filters' && !this.state.showFilters;
      var shouldOpenOverlays = view === 'overlays' && !this.state.showOverlays;

      this.setState({
        showFilters: shouldOpenFilters,
        showOverlays: shouldOpenOverlays,
        showStaticOverlays: !(shouldOpenFilters || shouldOpenOverlays)
      });
    }
  }, {
    key: 'exportActiveFilters',
    value: function exportActiveFilters(view) {
      if (this.props.formatData) {
        this.props.onApply(this.props.formatData(this.state[view]));
      } else {
        this.props.onApply(this.formatData(this.state[view]));
      }
    }
  }, {
    key: 'getIndex',
    value: function getIndex(list, id) {
      return _lodash2.default.findIndex(list.options, function (itm) {
        return itm.id === id;
      });
    }
  }, {
    key: 'getAllIndex',
    value: function getAllIndex(list) {
      var _this5 = this;

      return _lodash2.default.findIndex(list.options, function (itm) {
        return itm.name === _this5.props.allKey;
      });
    }
  }, {
    key: 'getActiveFilters',
    value: function getActiveFilters(idxType) {
      var view = this.getView();
      var result = _lodash2.default.filter(this.state[view][idxType].options, function (filter, key) {
        if (filter.active) {
          return filter;
        }
      });
      return result;
    }
  }, {
    key: 'getView',
    value: function getView() {
      var result = 'filters';

      if (this.state.showOverlays) {
        result = 'overlays';
      } else if (this.state.showStaticOverlays) {
        result = 'staticOverlays';
      }
      return result;
    }
  }, {
    key: 'getFormattedValue',
    value: function getFormattedValue(filter) {
      var value = void 0;

      switch (filter.type) {
        case 'checkbox':
          value = _lodash2.default.filter(filter.options, function (itm) {
            return itm.active === true;
          });
          break;
        case 'input':
          value = filter.value;
          break;
        case 'dropdown':
          value = filter.value;
          break;
        case 'datepicker':
          value = filter.value;
          break;
        case 'timepicker':
          value = filter.value;
          break;
        case 'crimepicker':
          value = _lodash2.default.chain(filter.options).values().flatten().filter(function (itm) {
            return itm.active;
          }).value();
          break;
        default:
          value = _lodash2.default.filter(filter.options, function (itm) {
            return itm.active === true;
          });
          break;
      }
      return value;
    }
  }, {
    key: 'chooseFilterToBuild',
    value: function chooseFilterToBuild(name, type, filter, showLabel) {
      var result = void 0;

      switch (type) {
        case 'checkbox':
          result = _react2.default.createElement(_ArmorVFCheckbox2.default, _extends({
            name: name,
            filters: filter.options,
            onChange: this.onFilterChange,
            colSize: showLabel ? 4 : 3
          }, filter));
          break;
        case 'input':
          result = _react2.default.createElement(_reactBootstrap.Input, {
            placeholder: filter.placeholder,
            type: 'text',
            value: filter.value,
            onChange: this.onInputChange.bind(this, name)
          });
          break;
        case 'dropdown':
          result = _react2.default.createElement(_ArmorVFDropdown2.default, {
            value: filter.value,
            style: { marginLeft: '-37px', marginBottom: '5px' },
            onChange: this.onDropdownChange.bind(this, name),
            options: filter.options
          });
          break;
        case 'datepicker':
          result = _react2.default.createElement(_DatePicker2.default, _extends({
            value: filter.value,
            style: { marginLeft: '-15px' },
            onChange: this.onTimeChange.bind(this, name)
          }, filter));
          break;
        case 'timepicker':
          result = _react2.default.createElement(_TimePicker2.default, _extends({
            value: filter.value,
            style: { marginLeft: '-15px' },
            onChange: this.onTimeChange.bind(this, name)
          }, filter));
          break;
        case 'crimepicker':
          result = _react2.default.createElement(_ArmorVFCrimepicker2.default, _extends({
            value: filter.value,
            style: { marginLeft: '-37px' },
            onChange: this.onCrimepickerChange,
            onDropdownChange: this.onCrimepickerDropdown
          }, filter));
          break;
        default:
          result = _react2.default.createElement(_ArmorVFCheckbox2.default, {
            name: name,
            filters: filter.options,
            onChange: this.onFilterChange,
            colSize: showLabel ? 4 : 3
          });
          break;
      }
      return _react2.default.createElement(
        'div',
        { className: type + '-inner-filter inner-filter' },
        result
      );
    }
  }, {
    key: 'buildFilters',
    value: function buildFilters(view) {
      var _this6 = this;

      var showLabel = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

      var result = _lodash2.default.map(this.state[view], function (filter, idx) {
        var name = filter.name;
        var type = filter.type;


        return _react2.default.createElement(
          _reactBootstrap.Row,
          { key: idx, className: 'filter-row' },
          _react2.default.createElement(
            _reactBootstrap.Col,
            { md: 3, style: !showLabel ? { display: 'none' } : {}, className: 'filter-label' },
            _react2.default.createElement(
              'h6',
              null,
              name
            )
          ),
          _react2.default.createElement(
            _reactBootstrap.Col,
            { md: showLabel ? 9 : 12, className: 'filters' },
            _this6.chooseFilterToBuild(name, type, filter, showLabel)
          )
        );
      });
      return _react2.default.createElement(
        'div',
        { className: 'filter-container' },
        view === 'filters' ? this.buildChildFilters() : '',
        result,
        view === 'overlays' && this.props.overlayMessage ? _react2.default.createElement(
          'h4',
          { className: 'overlay-message' },
          this.props.overlayMessage
        ) : ''
      );
    }
  }, {
    key: 'buildChildFilters',
    value: function buildChildFilters() {
      return _react2.default.Children.map(this.props.children, function (child, idx) {
        if (child) {
          return _react2.default.createElement(
            _reactBootstrap.Row,
            { key: idx, className: 'filter-row' },
            _react2.default.createElement(
              _reactBootstrap.Col,
              { md: 3, style: !child.props.showLabel ? { display: 'none' } : {}, className: 'filter-label' },
              _react2.default.createElement(
                'h6',
                null,
                child.props.name
              )
            ),
            _react2.default.createElement(
              _reactBootstrap.Col,
              { md: child.props.showLabel ? 9 : 12, className: 'filters' },
              _react2.default.cloneElement(child, {})
            )
          );
        }
      });
    }
  }, {
    key: 'buildIconMenu',
    value: function buildIconMenu() {
      return _react2.default.createElement(
        'div',
        { Col: true, className: 'armor-view-filter-icon-menu' },
        _react2.default.createElement(
          _IconMenu2.default,
          {
            iconButtonElement: _react2.default.createElement(
              _IconButton2.default,
              null,
              _react2.default.createElement(_moreVert2.default, null)
            ),
            anchorOrigin: { horizontal: 'left', vertical: 'top' },
            targetOrigin: { horizontal: 'left', vertical: 'top' },
            onChange: this.props.onIconMenuChange
          },
          _lodash2.default.map(this.props.iconMenu, function (item) {
            return _react2.default.createElement(_MenuItem2.default, { value: item.value, primaryText: item.label });
          })
        )
      );
    }
  }, {
    key: 'formatData',
    value: function formatData(filters) {
      var _this7 = this;

      var results = {};

      _lodash2.default.forEach(filters, function (filter) {
        results[_lodash2.default.camelCase(filter.name)] = _this7.getFormattedValue(filter);
      });

      return results;
    }
  }, {
    key: 'shouldSelectAll',
    value: function shouldSelectAll(idxType) {
      var result = void 0;
      var view = this.getView();
      var filter = this.state[view][idxType];
      var idx = this.getAllIndex(filter);

      if (filter.options[idx].active) {
        result = filter.options.length;
      } else {
        result = filter.options.length - 1;
      }
      return this.getActiveFilters(idxType).length === result;
    }
  }, {
    key: 'selectAllWithId',
    value: function selectAllWithId(filter, filterName, selected) {
      var _this8 = this;

      var id = filter.id;
      var name = filter.name;
      var children = filter.children;

      var newState = this.state;

      _lodash2.default.forEach(newState, function (view) {
        _lodash2.default.forEach(view, function (filter, index) {
          if (_lodash2.default.has(filter, 'options')) {
            _lodash2.default.forEach(filter.options, function (option) {
              var allSelected = filterName === filter.name && name === _this8.props.allKey;
              var isChild = _lodash2.default.includes(children, option.id);

              if (id === option.id || allSelected || isChild) {
                option.active = selected;
              }
            });
            if (name !== _this8.props.allKey) {
              var activeOptions = _lodash2.default.filter(filter.options, function (itm) {
                return itm.active === true;
              }).length;
              var allOptions = filter.options.length;
              var idxAll = _this8.getAllIndex(filter);

              try {
                if (allOptions - 1 === activeOptions) {
                  filter.options[idxAll].active = true;
                }
                if (!selected && allOptions > activeOptions) {
                  filter.options[idxAll].active = false;
                }
              } catch (exe) {}
            }
          }
        });
      });
      this.setState(newState, this.applyStaticFilters);
    }
  }, {
    key: 'applyStaticFilters',
    value: function applyStaticFilters() {
      if (this.state.showStaticOverlays) {
        this.exportActiveFilters(this.getView());
      }
    }
  }, {
    key: 'onCloseFilters',
    value: function onCloseFilters() {
      this.setState(this.closeState);
    }
  }, {
    key: 'render',
    value: function render() {
      var filterClasses = {
        'pull-right': true,
        'active': this.state.showFilters
      };

      var overlayClasses = {
        'pull-right': true,
        'active': this.state.showOverlays
      };

      return _react2.default.createElement(
        'div',
        { className: this.props.className,
          onMouseLeave: this.onMouseLeave,
          onMouseEnter: this.onMouseEnter,
          style: this.props.style },
        _react2.default.createElement(
          _reactBootstrap.Row,
          { className: 'header' },
          _react2.default.createElement(
            _reactBootstrap.Col,
            { sm: 7 },
            this.props.iconMenu ? this.buildIconMenu() : null,
            _react2.default.createElement(
              'div',
              { className: 'header-title' },
              this.props.title ? this.props.title : _react2.default.createElement(
                'h5',
                null,
                'Filters'
              )
            )
          ),
          _react2.default.createElement(
            _reactBootstrap.Col,
            { sm: 5, className: 'buttons-col' },
            _react2.default.createElement(
              _reactBootstrap.Button,
              { id: 'overlaysButton',
                className: overlayClasses,
                bsStyle: 'link',
                onClick: this.onClick.bind(this, 'overlays') },
              _react2.default.createElement(
                'span',
                null,
                this.props.overlaysButtonLabel
              )
            ),
            _react2.default.createElement(
              _reactBootstrap.Button,
              { id: 'filtersButton',
                className: filterClasses,
                bsStyle: 'link',
                onClick: this.onClick.bind(this, 'filters') },
              _react2.default.createElement(
                'span',
                null,
                this.props.filtersButtonLabel
              )
            )
          )
        ),
        _react2.default.createElement(
          _velocityReact.VelocityTransitionGroup,
          { enter: { animation: "slideDown" }, leave: { animation: "slideUp" } },
          this.state.showFilters ? _react2.default.createElement(
            'div',
            null,
            this.buildFilters('filters')
          ) : ''
        ),
        _react2.default.createElement(
          _velocityReact.VelocityTransitionGroup,
          { enter: { animation: "slideDown" }, leave: { animation: "slideUp" } },
          this.state.showOverlays ? this.buildFilters('overlays', false) : ''
        ),
        _react2.default.createElement(
          'div',
          { className: 'static-overlays' },
          _react2.default.createElement(
            _velocityReact.VelocityTransitionGroup,
            { enter: { animation: "slideDown" }, leave: { animation: "slideUp" } },
            this.state.showStaticOverlays ? this.buildFilters('staticOverlays', false) : ''
          )
        ),
        this.state.showFilters ? _react2.default.createElement(
          'div',
          { className: 'action-buttons' },
          _react2.default.createElement(
            'div',
            { className: 'pull-right' },
            _react2.default.createElement(_FlatButton2.default, {
              onClick: this.onReset,
              className: 'reset-button pull-left',
              label: 'Reset'
            }),
            _react2.default.createElement(_RaisedButton2.default, {
              onClick: this.onApply,
              className: 'apply-button pull-right',
              label: 'Apply',
              primary: true
            })
          ),
          _react2.default.createElement('div', { style: { clear: 'both' } })
        ) : ''
      );
    }
  }]);

  return ArmorViewFilter;
}(_react2.default.Component), _class2.propTypes = {
  /**
   * Array of multiple types of filters
   * ```
   * [
   *   {
   *     name: 'Beats',
   *     type: 'checkbox',
   *     options: [
   *       {
   *         id: 'all',
   *         name: 'All',
   *         active: false,
   *       },
   *       {
   *         id: 'a',
   *         name: 'A',
   *         children: ['b', 'c'],
   *         active: false,
   *       },
   *       {
   *         id: 'b',
   *         name: 'B',
   *         active: false,
   *       },
   *     ],
   *   },
   *   {
   *     name: 'Patrol Length',
   *     type: 'input',
   *     placeholder: 'Enter Length in Meters',
   *     value: '',
   *   },
   * ]
   * ```
   */
  filters: _react.PropTypes.array.isRequired,
  /**
   * Array of overlay filters
   *```
   * [
   *  {
   *    name: 'Views',
   *    type: 'checkbox',
   *    options: [
   *      {
   *        id: 'all',
   *        name: 'All',
   *        active: false,
   *      },
   *      {
   *        id: 'hotspots',
   *        name: 'Hotspots',
   *        active: true,
   *      },
   *      {
   *        id: 'heatMap',
   *        name: 'Heat Map',
   *        active: false,
   *      },
   *    ],
   *  },
   * ]
   * ```
   */
  overlays: _react.PropTypes.array.isRequired,
  /**
   * Array of staticOverlay filters displayed by default when Filters or Overlays are not open.
   *```
   * [
   *  {
   *    name: 'Views',
   *    type: 'checkbox',
   *    options: [
   *      {
   *        id: 'all',
   *        name: 'All',
   *        active: false,
   *      },
   *      {
   *        id: 'hotspots',
   *        name: 'Hotspots',
   *        active: true,
   *      },
   *      {
   *        id: 'heatMap',
   *        name: 'Heat Map',
   *        active: false,
   *      },
   *    ],
   *  },
   * ]
   * ```
   */
  staticOverlays: _react.PropTypes.array,
  /**
   * Function called when pressing Apply.
   * First parameter contains Object representing applied filters.
   */
  onApply: _react.PropTypes.func.isRequired,
  /**
   * Function called when reset is hit, Function()
   */
  onReset: _react.PropTypes.func,
  /**
   * Object or string that will be the heading
   */
  title: _react.PropTypes.any,
  /**
   * Function called before apply is pressed.
   * Function will format the data before it has been passed to onApply()
   */
  formatData: _react.PropTypes.func,
  /**
   * A menu which only appears on mobile view requiring a
   * ```{label: 'String', value: 'string'}```
   */
  iconMenu: _react.PropTypes.array,
  /**
   * Function fired upon the selection from the IconMenu, signature: function(event, value);
   */
  onIconMenuChange: _react.PropTypes.func,
  /**
   * String representing the key that will select/deselect all checkboxes
   */
  allKey: _react.PropTypes.string,
  /**
   * Class name for the entire component.
   */
  className: _react.PropTypes.string,
  /**
   * A list of filters which will automatically trigger an export when changes
   */
  dynamicFilters: _react.PropTypes.array,
  /**
   * Label for filters button, defaults to "Filters"
   */
  filtersButtonLabel: _react.PropTypes.string,
  /**
   * Label for overlays button, defaults to "Overlays"
   */
  overlaysButtonLabel: _react.PropTypes.string
}, _class2.defaultProps = {
  allKey: 'All',
  className: 'armor-view-filter',
  timeoutInterval: Infinity,
  filtersButtonLabel: 'Filters',
  overlaysButtonLabel: 'Overlays'
}, _temp), (_applyDecoratedDescriptor(_class.prototype, 'onFilterChange', [_autobindDecorator2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'onFilterChange'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'onSomeChange', [_autobindDecorator2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'onSomeChange'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'onCrimepickerDropdown', [_autobindDecorator2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'onCrimepickerDropdown'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'onCrimepickerChange', [_autobindDecorator2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'onCrimepickerChange'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'onReset', [_autobindDecorator2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'onReset'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'onApply', [_autobindDecorator2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'onApply'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'onMouseLeave', [_autobindDecorator2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'onMouseLeave'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'onMouseEnter', [_autobindDecorator2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'onMouseEnter'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'chooseFilterToBuild', [_autobindDecorator2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'chooseFilterToBuild'), _class.prototype)), _class);
exports.default = ArmorViewFilter;