'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _FontAwesome = require('./FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var propTypes = {
  show: _react.PropTypes.bool.isRequired,
  sortAsc: _react.PropTypes.bool
};

var styles = {
  arrow: { marginLeft: '10px' }
};

var SortArrow = function SortArrow(props) {
  if (!props.show) {
    return _react2.default.createElement('span', null);
  }
  return _react2.default.createElement(_FontAwesome2.default, {
    icon: props.sortAsc ? 'arrow-down' : 'arrow-up',
    iconStyle: styles.arrow
  });
};

SortArrow.propTypes = propTypes;
exports.default = SortArrow;