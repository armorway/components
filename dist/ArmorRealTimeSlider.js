'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _reactBootstrap = require('react-bootstrap');

var _FontAwesome = require('./FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _reactSlide = require('react-slide');

var _reactSlide2 = _interopRequireDefault(_reactSlide);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var sliderStyles = {
  height: '20px',
  width: '94%',
  display: 'inline-block',
  paddingTop: '10px',
  marginLeft: '5px'
};
var propTypes = {
  value: _react.PropTypes.number,
  onDrag: _react.PropTypes.func.isRequired,
  onChange: _react.PropTypes.func.isRequired,
  startOfDay: _react.PropTypes.string.isRequired,
  date: _react.PropTypes.string.isRequired
};

var startValue = 1;
var endValue = 1439;

var ArmorRealTimeSlider = function (_React$Component) {
  _inherits(ArmorRealTimeSlider, _React$Component);

  function ArmorRealTimeSlider(props) {
    _classCallCheck(this, ArmorRealTimeSlider);

    var _this = _possibleConstructorReturn(this, (ArmorRealTimeSlider.__proto__ || Object.getPrototypeOf(ArmorRealTimeSlider)).call(this, props));

    var _this$getTime = _this.getTime();

    var time = _this$getTime.time;


    _this.state = {
      value: time,
      sliderId: _utils.utils.generateHash(),
      titleLeftPosition: 0
    };

    _this.getSliderTitle = _this.getSliderTitle.bind(_this);
    _this.getTime = _this.getTime.bind(_this);
    _this.moveNextMinute = _this.moveNextMinute.bind(_this);
    _this.startRealTimeSliding = _this.startRealTimeSliding.bind(_this);
    _this.setValue = _this.setValue.bind(_this);
    _this.onDrag = _this.onDrag.bind(_this);
    _this.onChange = _this.onChange.bind(_this);
    _this.onResetSlidingClick = _this.onResetSlidingClick.bind(_this);
    return _this;
  }

  _createClass(ArmorRealTimeSlider, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var titleLeftPosition = this.getTitlePosition();
      var slider = _reactDom2.default.findDOMNode(this);
      var maxRightPosition = slider.clientWidth - _reactDom2.default.findDOMNode(this).clientWidth * 0.02;

      this.startRealTimeSliding();
      this.startRealTimeSliding = this.startRealTimeSliding.bind(this);
      /* eslint react/no-did-mount-set-state: 0 */
      this.setState({ titleLeftPosition: titleLeftPosition, maxRightPosition: maxRightPosition });

      this._sliderInterval = undefined;
      this._sliderDebounce = undefined;
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(newProps, newState) {
      return newState.value !== this.state.value || newState.titleLeftPosition !== this.state.titleLeftPosition || newProps.date !== this.props.date;
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      window.clearTimeout(this.sliderIntialTimeout);
      window.clearInterval(this._sliderInterval);
    }
  }, {
    key: 'onDrag',
    value: function onDrag(value) {
      var _this2 = this,
          _arguments = arguments;

      var titleLeftPosition = this.getTitlePosition();

      this.setState({ value: value, titleLeftPosition: titleLeftPosition });

      window.clearTimeout(this._sliderDebounce);

      // eslint-disable-next-line
      this._sliderDebounce = setTimeout(function () {
        var _props;

        return (_props = _this2.props).onDrag.apply(_props, _arguments);
      }, 100);
    }
  }, {
    key: 'onChange',
    value: function onChange(value, keepSliding) {
      var _this3 = this,
          _props2;

      // keepSliding is set true for automatic increments
      // undefined for user changes
      if (!keepSliding) {
        window.clearInterval(this._sliderInterval);
      }
      this.setState({ value: value }, function () {
        _this3.setState({ titleLeftPosition: _this3.getTitlePosition() });
      });

      // eslint-disable-next-line
      (_props2 = this.props).onChange.apply(_props2, arguments);
    }
  }, {
    key: 'onResetSlidingClick',
    value: function onResetSlidingClick() {
      this.startRealTimeSliding();
    }
  }, {
    key: 'getTitlePosition',
    value: function getTitlePosition() {
      var handle = _reactDom2.default.findDOMNode(this).querySelector('#' + this.state.sliderId + ' .z-handle');

      // console.log(handle.offsetLeft, handle.getBoundingClientRect().left);
      return handle.offsetLeft;
    }
  }, {
    key: 'getTime',
    value: function getTime() {
      var currentTime = (0, _moment2.default)().format('HH:mm:ss');
      var _props3 = this.props;
      var shiftDate = _props3.date;
      var startOfDay = _props3.startOfDay;


      var ret = {
        time: (0, _moment2.default)(shiftDate + ' ' + currentTime).diff((0, _moment2.default)(shiftDate + ' ' + startOfDay), 'minutes')
      };

      if (currentTime < startOfDay) {
        ret.time = 1440 + ret.time;
      }

      return ret;
    }
  }, {
    key: 'setValue',
    value: function setValue(value) {
      this.setState({ value: value });
    }
  }, {
    key: 'getSliderTitle',
    value: function getSliderTitle() {
      var _this4 = this;

      var _props4 = this.props;
      var date = _props4.date;
      var startOfDay = _props4.startOfDay;

      var dateTime = '';
      var value = this.state.value;

      var calculatePosition = function calculatePosition(left) {
        if (left <= 0) {
          return 0;
        } else if (left >= _this4.state.maxRightPosition) {
          return _this4.state.maxRightPosition;
        }
        return left;
      };

      var titleLeftPosition = this.state.titleLeftPosition - 24;
      var titleStyles = {
        position: 'relative',
        left: calculatePosition(titleLeftPosition),
        display: 'inline',
        top: '10px'
      };

      dateTime = (0, _moment2.default)(date + ' ' + startOfDay).add(value, 'minutes');

      return _react2.default.createElement(
        'span',
        { style: titleStyles },
        _react2.default.createElement(
          'div',
          null,
          dateTime.format('MMM DD, h:mm A')
        )
      );
    }
  }, {
    key: 'startRealTimeSliding',
    value: function startRealTimeSliding() {
      var _this5 = this;

      window.clearTimeout(this.sliderIntialTimeout);

      var _getTime = this.getTime();

      var time = _getTime.time;


      this.onChange(time, true);

      this.sliderIntialTimeout = setTimeout(function () {
        _this5.moveNextMinute();
        window.clearInterval(_this5._sliderInterval);
        _this5._sliderInterval = setInterval(_this5.moveNextMinute, 60000);
      }, (60 - parseInt((0, _moment2.default)().format('ss'), 10)) * 1000);
    }
  }, {
    key: 'moveNextMinute',
    value: function moveNextMinute() {
      var value = this.state.value + 1;

      if (value < 1440) {
        this.onChange(value, true);
      }
    }
  }, {
    key: 'resetTime',
    value: function resetTime() {
      this.setState({ value: this.getTime() });
      this.startRealTimeSliding();
    }
  }, {
    key: 'render',
    value: function render() {
      var trimmedProps = _lodash2.default.omit(this.props, 'onChange', 'onDrag', 'value');

      return _react2.default.createElement(
        'div',
        { className: this.props.className, style: { padding: '5px 0', marginTop: '-15px' } },
        this.getSliderTitle(),
        _react2.default.createElement(_reactSlide2.default, _extends({ id: this.state.sliderId
        }, trimmedProps, {
          tickStep: 1,
          trackRadius: 100,
          style: sliderStyles,
          value: this.state.value,
          onChange: this.onChange,
          onDrag: this.onDrag,
          startValue: startValue,
          endValue: endValue })),
        _react2.default.createElement(
          'span',
          { style: { marginLeft: '10px' } },
          _react2.default.createElement(
            _reactBootstrap.Button,
            { onClick: this.onResetSlidingClick },
            _react2.default.createElement(_FontAwesome2.default, { icon: 'refresh' })
          )
        )
      );
    }
  }]);

  return ArmorRealTimeSlider;
}(_react2.default.Component);

ArmorRealTimeSlider.propTypes = propTypes;
exports.default = ArmorRealTimeSlider;