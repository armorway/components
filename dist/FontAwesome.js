'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  /*
    Nested components of rendered FontAwesome component.
  */
  children: _react.PropTypes.any,
  /*
   Name of icon from FontAwesome library.
  */
  icon: _react.PropTypes.oneOfType([_react.PropTypes.object, _react.PropTypes.string]).isRequired,
  /*
    Text to be rendered after icon.
  */
  title: _react.PropTypes.string,
  /*
     Adds 5px right margin to icon.
  */
  padIcon: _react.PropTypes.bool,
  /*
    Any valid font size from FontAwesome library.
    Sizes include: 2x, 3x, 4x, 5x, or lg (33% increase)
  */
  size: _react.PropTypes.string,
  /*
    Class name to be applied to wrapper span on icon.
  */
  className: _react.PropTypes.string,
  /*
    Style to be applied to wrappper span on icon.
  */
  style: _react.PropTypes.object,
  /*
    Style to be applied to the FontAwesome icon
  */
  iconStyle: _react.PropTypes.object
};

/*
  Wrapper React component to be used for rendering FontAwesome Icons
*/

var FontAwesome = function (_React$Component) {
  _inherits(FontAwesome, _React$Component);

  function FontAwesome(props) {
    _classCallCheck(this, FontAwesome);

    return _possibleConstructorReturn(this, (FontAwesome.__proto__ || Object.getPrototypeOf(FontAwesome)).call(this, props));
    // this.state = {};
  }

  _createClass(FontAwesome, [{
    key: 'render',
    value: function render() {
      var iconStyles = _lodash2.default.assign(this.props.padIcon ? { marginRight: '5px' } : {}, this.props.iconStyle || {});

      return _react2.default.createElement(
        'span',
        { className: this.props.className, style: this.props.style },
        _react2.default.createElement('span', {
          className: 'fa fa-' + this.props.icon + ' ' + (this.props.size ? 'fa-' + this.props.size : ''),
          style: iconStyles
        }),
        _react2.default.createElement(
          'span',
          { className: 'fa-title' },
          this.props.title
        ),
        this.props.children
      );
    }
  }]);

  return FontAwesome;
}(_react2.default.Component);

FontAwesome.propTypes = propTypes;
exports.default = FontAwesome;