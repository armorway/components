'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = require('react-bootstrap');

var _FontAwesome = require('components/global/FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var editTooltip = _react2.default.createElement(
  _reactBootstrap.Tooltip,
  { id: '1' },
  'Edit'
);

var deleteTooltip = _react2.default.createElement(
  _reactBootstrap.Tooltip,
  { id: '2' },
  'Delete'
);

var propTypes = {
  onEdit: _react.PropTypes.func,
  onDelete: _react.PropTypes.func
};

var ArmorEdit = function (_React$Component) {
  _inherits(ArmorEdit, _React$Component);

  function ArmorEdit() {
    _classCallCheck(this, ArmorEdit);

    var _this = _possibleConstructorReturn(this, (ArmorEdit.__proto__ || Object.getPrototypeOf(ArmorEdit)).call(this));

    _this.onEdit = _this.onEdit.bind(_this);
    _this.onDelete = _this.onDelete.bind(_this);
    return _this;
  }

  _createClass(ArmorEdit, [{
    key: 'onEdit',
    value: function onEdit(event) {
      event.stopPropagation();
      this.props.onEdit(event);
    }
  }, {
    key: 'onDelete',
    value: function onDelete(event) {
      event.stopPropagation();
      this.props.onDelete(event);
    }
  }, {
    key: 'buildEditIfNeeded',
    value: function buildEditIfNeeded() {
      return this.buildModifyBtn('onEdit', editTooltip, this.onEdit, 'pencil');
    }
  }, {
    key: 'buildDeleteIfNeeded',
    value: function buildDeleteIfNeeded() {
      return this.buildModifyBtn('onDelete', deleteTooltip, this.onDelete, 'trash-o');
    }
  }, {
    key: 'buildModifyBtn',
    value: function buildModifyBtn(propName, tooltip, action, icon) {
      if (this.props[propName]) {
        return _react2.default.createElement(
          _reactBootstrap.OverlayTrigger,
          { placement: 'top', overlay: tooltip },
          _react2.default.createElement(
            'span',
            { role: 'button', onClick: action, style: { fontSize: '24px', padding: '10px 20px 0 0' } },
            _react2.default.createElement(_FontAwesome2.default, { icon: icon })
          )
        );
      }
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'pull-right' },
        this.buildEditIfNeeded(),
        this.buildDeleteIfNeeded()
      );
    }
  }]);

  return ArmorEdit;
}(_react2.default.Component);

ArmorEdit.propTypes = propTypes;
exports.default = ArmorEdit;