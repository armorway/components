'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = require('react-bootstrap');

var _utils = require('utils');

var _griddleReact = require('griddle-react');

var _griddleReact2 = _interopRequireDefault(_griddleReact);

var _FontAwesome = require('components/global/FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  'results': _react.PropTypes.oneOfType([_react.PropTypes.object.isRequired, _react.PropTypes.array.isRequired]),
  'showPrint': _react.PropTypes.bool,
  'showSave': _react.PropTypes.bool,
  'resultsPerPage': _react.PropTypes.number,
  'className': _react.PropTypes.string,
  'itemsPerPage': _react.PropTypes.number,
  'activePage': _react.PropTypes.number,
  'showPagination': _react.PropTypes.bool,
  'onSave': _react.PropTypes.func
};

var defaultProps = {
  useGriddleStyles: false,
  showSettings: false,
  showPager: false,
  tableClassName: 'table table-hover',
  sortDefaultComponent: undefined,
  sortAscendingComponent: _react2.default.createElement(_FontAwesome2.default, { icon: 'long-arrow-up' }),
  sortDescendingComponent: _react2.default.createElement(_FontAwesome2.default, { icon: 'long-arrow-down' }),
  settingsIconComponent: _react2.default.createElement(_FontAwesome2.default, { icon: 'cog' }),
  filterPlaceholderText: 'Search',
  'showPagination': true
};

var ArmorGriddle = function (_React$Component) {
  _inherits(ArmorGriddle, _React$Component);

  function ArmorGriddle(props) {
    _classCallCheck(this, ArmorGriddle);

    var _this = _possibleConstructorReturn(this, (ArmorGriddle.__proto__ || Object.getPrototypeOf(ArmorGriddle)).call(this, props));

    _this.state = {
      id: _utils.utils.generateHash(),
      currentPage: props.activePage || 0,
      print: false,
      resultsPerPage: _this.props.itemsPerPage || 10
    };
    _this.onPrint = _this.onPrint.bind(_this);
    _this.onSave = _this.onSave.bind(_this);
    _this.onSelectPage = _this.onSelectPage.bind(_this);
    _this.buildPagination = _this.buildPagination.bind(_this);
    return _this;
  }

  _createClass(ArmorGriddle, [{
    key: 'onSelectPage',
    value: function onSelectPage(event, selectedEvent) {
      event.preventDefault();
      this.setState({
        currentPage: selectedEvent.eventKey - 1
      });
    }
  }, {
    key: 'onPrint',
    value: function onPrint() {
      var _this2 = this;

      var printMode = {
        print: true,
        resultsPerPage: this.props.results.length,
        currentPage: 999
      };
      var resetPrintMode = {
        print: false,
        resultsPerPage: this.state.resultsPerPage,
        currentPage: this.state.currentPage
      };

      this.setState(printMode, function () {
        _utils.utils.print(_this2.state.id);
        _this2.setState(resetPrintMode);
      });
    }
  }, {
    key: 'onSave',
    value: function onSave(now) {
      if (now) {
        var tempResults = _.map(this.props.results, _.clone);

        if (!_.isUndefined(this.props.onSave)) {
          tempResults = this.props.onSave(tempResults);
        }
        _utils.utils.downloadAsCSV(tempResults, 'Users', false);
      } else {
        return this.props.results;
      }
    }
  }, {
    key: 'buildPrintIfNeeded',
    value: function buildPrintIfNeeded() {
      if (this.props.showPrint) {
        return _react2.default.createElement(
          _reactBootstrap.Button,
          { ref: 'clickButton',
            onClick: this.onPrint },
          _react2.default.createElement(_FontAwesome2.default, { icon: 'print', padIcon: true }),
          _react2.default.createElement(
            'span',
            null,
            'Print'
          )
        );
      }
    }
  }, {
    key: 'buildSaveIfNeeded',
    value: function buildSaveIfNeeded() {
      if (this.props.showSave) {
        return _react2.default.createElement(
          _reactBootstrap.Button,
          { ref: 'saveButton',
            onClick: this.onSave,
            style: { marginLeft: '10px' } },
          _react2.default.createElement(_FontAwesome2.default, { icon: 'save', padIcon: true }),
          _react2.default.createElement(
            'span',
            null,
            'Save'
          )
        );
      }
    }
  }, {
    key: 'buildPagination',
    value: function buildPagination() {
      return _react2.default.createElement(_reactBootstrap.Pagination, {
        ref: 'pagination',
        maxButtons: 5,
        first: true,
        last: true,
        ellipsis: true,
        style: { margin: '10px 10px 25px 10px', float: 'right' },
        activePage: this.state.currentPage + 1,
        items: Math.ceil(this.props.results.length / this.state.resultsPerPage),
        onSelect: this.onSelectPage,
        next: true,
        prev: true });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { id: this.state.id, className: 'table-striped ' + this.props.className + '}' },
        _react2.default.createElement(
          _reactBootstrap.Row,
          { className: 'pull-right' },
          _react2.default.createElement(
            _reactBootstrap.Col,
            { md: 12 },
            _react2.default.createElement(
              'div',
              { style: { margin: '10px 10px 0 0' } },
              this.buildPrintIfNeeded(),
              this.buildSaveIfNeeded()
            )
          )
        ),
        _react2.default.createElement(_griddleReact2.default, _extends({
          customFormatClassName: 'table-striped',
          ref: 'griddle',
          externalCurrentPage: this.state.currentPage,
          resultsPerPage: this.state.resultsPerPage
        }, this.props)),
        this.props.showPagination ? this.buildPagination() : ''
      );
    }
  }]);

  return ArmorGriddle;
}(_react2.default.Component);

ArmorGriddle.defaultProps = defaultProps;
ArmorGriddle.propTypes = propTypes;
exports.default = ArmorGriddle;