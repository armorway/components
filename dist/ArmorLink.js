'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _CrimeModal = require('components/global/modals/CrimeModal');

var _CrimeModal2 = _interopRequireDefault(_CrimeModal);

var _AppActions = require('actions/global/AppActions');

var _AppActions2 = _interopRequireDefault(_AppActions);

var _CrimeStore = require('stores/CrimeStore');

var _CrimeStore2 = _interopRequireDefault(_CrimeStore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  'data': _react.PropTypes.any.isRequired
};

var ArmorLink = function (_React$Component) {
  _inherits(ArmorLink, _React$Component);

  function ArmorLink(props) {
    _classCallCheck(this, ArmorLink);

    var _this = _possibleConstructorReturn(this, (ArmorLink.__proto__ || Object.getPrototypeOf(ArmorLink)).call(this, props));

    _this.state = _CrimeStore2.default.getState();
    _this.updateStates = _this.updateStates.bind(_this);
    _this.onCrimeDetails = _this.onCrimeDetails.bind(_this);
    return _this;
  }

  _createClass(ArmorLink, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      _CrimeStore2.default.listen(this.updateStates);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      _CrimeStore2.default.unlisten(this.updateStates);
    }
  }, {
    key: 'onCrimeDetails',
    value: function onCrimeDetails() {
      _AppActions2.default.toggleModal({
        showModal: true,
        size: 'large',
        modal: _react2.default.createElement(_CrimeModal2.default, { caseNumber: this.props.data })
      });
    }
  }, {
    key: 'updateStates',
    value: function updateStates(newState) {
      this.setState(newState);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'a',
        { style: { cursor: 'pointer' }, onClick: this.onCrimeDetails },
        this.props.data
      );
    }
  }]);

  return ArmorLink;
}(_react2.default.Component);

ArmorLink.propTypes = propTypes;
exports.default = ArmorLink;