'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _FlatButton = require('material-ui/FlatButton');

var _FlatButton2 = _interopRequireDefault(_FlatButton);

require('../styles/ArmorTabbedList');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var proptypes = {
  /**
   * Child ArmorList components, must have a value prop.
   */
  children: _react.PropTypes.array.isRequired,
  /**
   * Value that matches a child component prop to set as active.
   */
  value: _react.PropTypes.string
};
var contextTypes = {
  muiTheme: _react.PropTypes.array
};

var styles = {
  inkBar: { display: 'none' },
  tabContainer: {
    width: '100%',
    overflowX: 'auto',
    backgroundColor: 'transparent',
    overflowY: 'hidden',
    whiteSpace: 'nowrap'
  },
  tab: {
    padding: '0 10px',
    width: 'auto',
    display: 'inline',
    borderRadius: '0px',
    color: '#fff'
  },
  activeTab: {
    padding: '0 10px',
    width: 'auto',
    display: 'inline',
    borderRadius: '0px',
    color: '#fff',
    backgroundColor: '#fff'
  }
};

var ArmorTabbedList = function (_React$Component) {
  _inherits(ArmorTabbedList, _React$Component);

  function ArmorTabbedList(props) {
    _classCallCheck(this, ArmorTabbedList);

    var _this = _possibleConstructorReturn(this, (ArmorTabbedList.__proto__ || Object.getPrototypeOf(ArmorTabbedList)).call(this, props));

    var active = undefined;

    if (props.value) {
      active = props.value;
    } else if (props.children) {
      active = props.children[0].props.value;
    }

    _this.state = {
      children: _this.getOnlyDefined(props.children),
      active: active
    };

    _this.onChange = _this.onChange.bind(_this);
    return _this;
  }

  _createClass(ArmorTabbedList, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      styles.tabContainer.backgroundColor = this.context.muiTheme.palette.primary1Color;
      styles.activeTab.backgroundColor = this.context.muiTheme.palette.primary2Color;
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      var passedActive = nextProps.value !== this.props.value || nextProps.value !== this.state.value ? nextProps.value : undefined;

      var children = this.getOnlyDefined(nextProps.children);

      if (nextProps.children && nextProps.children.length === 0) {
        this.setState({
          active: undefined,
          children: children
        });
      } else {
        var childValues = _react2.default.Children.map(children, function (child) {
          return child.props.value;
        });

        if (passedActive && _.includes(childValues, passedActive)) {
          this.setState({
            active: passedActive,
            children: children
          });
        } else if (!_.includes(childValues, this.state.active)) {
          this.setState({
            active: children[0].props.value,
            children: children
          });
        } else {
          this.setState({ children: children });
        }
      }
    }
  }, {
    key: 'onChange',
    value: function onChange(evt) {
      this.setState({
        active: evt.currentTarget.getAttribute('value')
      });
    }
  }, {
    key: 'getOnlyDefined',
    value: function getOnlyDefined(children) {
      return _react2.default.Children.toArray(children).filter(function (child) {
        return child;
      });
    }
  }, {
    key: 'renderTabs',
    value: function renderTabs() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        {
          style: styles.tabContainer,
          initialSelectedIndex: 0,
          onChange: this.onChange,
          value: this.state.active
        },
        _react2.default.Children.map(this.state.children, function (child) {
          if (child && child.props) {
            return _react2.default.createElement(_FlatButton2.default, {
              value: child.props.value,
              label: child.props.value,
              style: child.props.value === _this2.state.active ? styles.activeTab : styles.tab,
              onClick: _this2.onChange
            });
          }
          return undefined;
        })
      );
    }
  }, {
    key: 'renderComponent',
    value: function renderComponent() {
      var _this3 = this;

      return _.find(this.state.children, function (child) {
        return child.props.value === _this3.state.active;
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'armor-tabbed-list' },
        this.renderTabs(),
        this.renderComponent()
      );
    }
  }]);

  return ArmorTabbedList;
}(_react2.default.Component);

ArmorTabbedList.propTypes = proptypes;
ArmorTabbedList.contextTypes = contextTypes;
exports.default = ArmorTabbedList;