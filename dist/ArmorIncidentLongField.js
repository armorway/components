'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _FlatButton = require('material-ui/FlatButton');

var _FlatButton2 = _interopRequireDefault(_FlatButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  text: _react.PropTypes.string,
  name: _react.PropTypes.string,
  printing: _react.PropTypes.bool
};

var defaultProps = {
  text: ''
};
var styles = {
  container: {
    margin: '10px 0 5px 15px'
  },
  name: {
    textAlign: 'initial',
    fontWeight: '600'
  },
  text: {
    textAlign: 'initial'
  }
};

var ArmorIncidentLongField = function (_React$Component) {
  _inherits(ArmorIncidentLongField, _React$Component);

  function ArmorIncidentLongField(props) {
    _classCallCheck(this, ArmorIncidentLongField);

    var _this = _possibleConstructorReturn(this, (ArmorIncidentLongField.__proto__ || Object.getPrototypeOf(ArmorIncidentLongField)).call(this, props));

    _this.state = {
      open: false
    };

    _this.maxSize = 250;

    _this.onShowMoreClick = _this.onShowMoreClick.bind(_this);
    _this.onShowLessClick = _this.onShowLessClick.bind(_this);
    return _this;
  }

  _createClass(ArmorIncidentLongField, [{
    key: 'onShowMoreClick',
    value: function onShowMoreClick() {
      this.setState({
        open: true
      });
    }
  }, {
    key: 'onShowLessClick',
    value: function onShowLessClick() {
      this.setState({
        open: false
      });
    }
  }, {
    key: 'getText',
    value: function getText() {
      var onClick = this.onShowLessClick;
      var text = this.props.text;
      var buttonText = 'Show Less';
      var isLongEnough = this.props.text.length > this.maxSize;

      if (isLongEnough && !this.state.open && !this.props.printing) {
        onClick = this.onShowMoreClick;
        text = this.props.text.slice(0, this.maxSize);
        buttonText = 'Show More';
      }

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'span',
          null,
          text
        ),
        _react2.default.createElement(
          'span',
          null,
          isLongEnough && !this.props.printing ? _react2.default.createElement(_FlatButton2.default, { onClick: onClick, label: buttonText, primary: true }) : ''
        )
      );
    }
  }, {
    key: 'render',
    value: function render() {
      if (!this.props.text) {
        return _react2.default.createElement('span', null);
      }

      return _react2.default.createElement(
        'div',
        { style: styles.container },
        _react2.default.createElement(
          'div',
          { style: styles.name },
          this.props.name
        ),
        _react2.default.createElement(
          'div',
          { style: styles.text },
          this.getText()
        )
      );
    }
  }]);

  return ArmorIncidentLongField;
}(_react2.default.Component);

ArmorIncidentLongField.propTypes = propTypes;
ArmorIncidentLongField.defaultProps = defaultProps;
exports.default = ArmorIncidentLongField;