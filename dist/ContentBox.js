'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _Paper = require('material-ui/Paper');

var _Paper2 = _interopRequireDefault(_Paper);

var _reactBootstrap = require('react-bootstrap');

var _utils = require('./utils');

require('../styles/ContentBox.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  /**
   * Contents for inside the ContentBox.
   */
  children: _react.PropTypes.any,
  /**
   * When true, props.title takes the whole title row.
   */
  header: _react.PropTypes.bool,
  /**
   * Title element to be displayed on the top left.
   */
  title: _react.PropTypes.any,
  /**
   * Side element to be displayed on the top right.
   */
  sideElement: _react.PropTypes.element,
  /**
   * Number of cols to allocate for props.sideElement
   * props.title automatically gets the inverse from 12
   * titleSize = 12 - props.sizeSize
   * range: 0-12
   */
  sideSize: _react.PropTypes.number,
  /**
   * Style object to be applied.
   */
  style: _react.PropTypes.object,

  /**
   * Set a height for the container.
   */
  height: _react.PropTypes.number,

  /**
   * When true, ignores all props related
   * to the header except sideElement.
   * Renders no header, w/ sideElement
   * positioned in the top right
   */
  noHeader: _react.PropTypes.bool
};

var ContentBox = function (_React$Component) {
  _inherits(ContentBox, _React$Component);

  function ContentBox(props) {
    _classCallCheck(this, ContentBox);

    var _this = _possibleConstructorReturn(this, (ContentBox.__proto__ || Object.getPrototypeOf(ContentBox)).call(this, props));

    _this.state = {
      sideOpen: false,
      height: 'auto'
    };

    if (props.height) {
      _this.state.height = props.height;
    }
    _this.getColSizes = _this.getColSizes.bind(_this);
    return _this;
  }

  _createClass(ContentBox, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (this.props.height) {
        var top = _reactDom2.default.findDOMNode(this).getBoundingClientRect().top;
        var height = _utils.utils.calculateFitHeight(this.props.height, top);

        /* eslint react/no-did-mount-set-state: 0 */
        this.setState({ height: height });
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(newProps, oldProps) {
      if (newProps.height !== oldProps.height) {
        var top = _reactDom2.default.findDOMNode(this).getBoundingClientRect().top;
        var height = _utils.utils.calculateFitHeight(newProps.windowHeight, top);

        this.setState({ height: height });
      }
    }
  }, {
    key: 'getHeader',
    value: function getHeader() {
      var sizes = this.getColSizes();

      if (this.props.noHeader) {
        return _react2.default.createElement(
          'div',
          { className: 'side-no-header' },
          this.props.sideElement
        );
      }
      return _react2.default.createElement(
        _reactBootstrap.Row,
        { className: 'content-box-title-row', style: { margin: 0 } },
        _react2.default.createElement(
          _reactBootstrap.Col,
          { xs: sizes.left, className: 'title' },
          _react2.default.createElement(
            'span',
            { className: 'title' },
            this.props.title
          )
        ),
        _react2.default.createElement(
          _reactBootstrap.Col,
          { xs: sizes.right, className: 'title-side' },
          _react2.default.createElement(
            'span',
            { className: 'pull-right' },
            this.props.sideElement
          )
        )
      );
    }
  }, {
    key: 'getColSizes',
    value: function getColSizes() {
      var sizes = {
        left: 6,
        right: 6
      };

      if (this.props.header) {
        sizes.left = 12;
        sizes.right = 0;
      } else if (this.props.sideSize !== undefined) {
        sizes.left = 12 - this.props.sideSize;
        sizes.right = this.props.sideSize;
      }

      return sizes;
    }
  }, {
    key: 'render',
    value: function render() {
      var header = this.getHeader();

      var style = _.assign({
        height: (this.state.height === 'auto' ? 'auto' : this.state.height) + 'px'
      }, this.props.style);

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          _Paper2.default,
          { className: 'content-box ' + (this.props.className || ''), style: style, zDepth: 1 },
          _react2.default.createElement(
            'div',
            null,
            header,
            _react2.default.createElement(
              'div',
              null,
              this.props.children
            )
          )
        )
      );
    }
  }]);

  return ContentBox;
}(_react2.default.Component);

ContentBox.propTypes = propTypes;

exports.default = ContentBox;