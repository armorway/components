'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _desc, _value, _class, _class2, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _RaisedButton = require('material-ui/RaisedButton');

var _RaisedButton2 = _interopRequireDefault(_RaisedButton);

var _utils = require('./utils');

var _autobindDecorator = require('autobind-decorator');

var _autobindDecorator2 = _interopRequireDefault(_autobindDecorator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
  var desc = {};
  Object['ke' + 'ys'](descriptor).forEach(function (key) {
    desc[key] = descriptor[key];
  });
  desc.enumerable = !!desc.enumerable;
  desc.configurable = !!desc.configurable;

  if ('value' in desc || desc.initializer) {
    desc.writable = true;
  }

  desc = decorators.slice().reverse().reduce(function (desc, decorator) {
    return decorator(target, property, desc) || desc;
  }, desc);

  if (context && desc.initializer !== void 0) {
    desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
    desc.initializer = undefined;
  }

  if (desc.initializer === void 0) {
    Object['define' + 'Property'](target, property, desc);
    desc = null;
  }

  return desc;
}

var ArmorPrintMap = (_class = (_temp = _class2 = function (_Component) {
  _inherits(ArmorPrintMap, _Component);

  function ArmorPrintMap() {
    _classCallCheck(this, ArmorPrintMap);

    return _possibleConstructorReturn(this, (ArmorPrintMap.__proto__ || Object.getPrototypeOf(ArmorPrintMap)).apply(this, arguments));
  }

  _createClass(ArmorPrintMap, [{
    key: 'onPrint',
    value: function onPrint() {
      _utils.utils.print(this.props.mapId, true, this.props.options);
    }
    /**
     * List of strings that may have HTML markup contained, which will
     * render into the printed page.
     */

  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { id: 'armorPrintMap' },
        _react2.default.createElement(_RaisedButton2.default, { onMouseDown: this.onPrint, label: 'Print', primary: true })
      );
    }
  }]);

  return ArmorPrintMap;
}(_react.Component), _class2.propTypes = {
  options: _react.PropTypes.arrayOf(_react.PropTypes.string)
}, _temp), (_applyDecoratedDescriptor(_class.prototype, 'onPrint', [_autobindDecorator2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'onPrint'), _class.prototype)), _class);
exports.default = ArmorPrintMap;