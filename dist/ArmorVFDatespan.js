'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _desc, _value, _class, _class2, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _ArmorVFDropdown = require('./ArmorVFDropdown');

var _ArmorVFDropdown2 = _interopRequireDefault(_ArmorVFDropdown);

var _autobindDecorator = require('autobind-decorator');

var _autobindDecorator2 = _interopRequireDefault(_autobindDecorator);

var _DatePicker = require('material-ui/DatePicker');

var _DatePicker2 = _interopRequireDefault(_DatePicker);

var _reactBootstrap = require('react-bootstrap');

require('../styles/ArmorVFDatespan');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
  var desc = {};
  Object['ke' + 'ys'](descriptor).forEach(function (key) {
    desc[key] = descriptor[key];
  });
  desc.enumerable = !!desc.enumerable;
  desc.configurable = !!desc.configurable;

  if ('value' in desc || desc.initializer) {
    desc.writable = true;
  }

  desc = decorators.slice().reverse().reduce(function (desc, decorator) {
    return decorator(target, property, desc) || desc;
  }, desc);

  if (context && desc.initializer !== void 0) {
    desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
    desc.initializer = undefined;
  }

  if (desc.initializer === void 0) {
    Object['define' + 'Property'](target, property, desc);
    desc = null;
  }

  return desc;
}

var ArmorVFDatespan = (_class = (_temp = _class2 = function (_React$Component) {
  _inherits(ArmorVFDatespan, _React$Component);

  function ArmorVFDatespan(props) {
    _classCallCheck(this, ArmorVFDatespan);

    var _this = _possibleConstructorReturn(this, (ArmorVFDatespan.__proto__ || Object.getPrototypeOf(ArmorVFDatespan)).call(this, props));

    _this.allDateSpans = [{
      primaryText: 'Last 7 Days',
      value: '7 days'
    }, {
      primaryText: 'Last 14 Days',
      value: '14 days'
    }, {
      primaryText: 'Last Month',
      value: '1 months'
    }, {
      primaryText: 'Last Year',
      value: '1 years'
    }, {
      primaryText: 'Custom',
      value: 'custom'
    }];
    _this.state = {
      datespanValue: _this.props.datespanValue,
      startDate: _this.props.value.startDate,
      endDate: _this.props.value.endDate
    };
    return _this;
  }

  _createClass(ArmorVFDatespan, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      this.setState({
        datespanValue: nextProps.datespanValue,
        startDate: nextProps.value.startDate,
        endDate: nextProps.value.endDate
      });
    }
  }, {
    key: 'onSpanChange',
    value: function onSpanChange(evt, index, value) {
      var _this2 = this;

      if (value !== 'custom') {
        var _value$split = value.split(' ');

        var _value$split2 = _slicedToArray(_value$split, 2);

        var amount = _value$split2[0];
        var unit = _value$split2[1];


        this.setState({
          datespanValue: value,
          startDate: (0, _moment2.default)().subtract(amount, unit),
          endDate: (0, _moment2.default)()
        }, function () {
          _this2.exportValue();
        });
      }
    }
  }, {
    key: 'onDateChange',
    value: function onDateChange(filterName, index, datePicked) {
      var _this3 = this;

      console.log(filterName, index, datePicked);

      this.setState(_defineProperty({ datespanValue: 'custom' }, filterName, (0, _moment2.default)(datePicked)), function () {
        _this3.exportValue();
      });
    }
  }, {
    key: 'exportValue',
    value: function exportValue() {
      var _state = this.state;
      var startDate = _state.startDate;
      var endDate = _state.endDate;
      var datespanValue = _state.datespanValue;


      this.props.onChange(this.props.name, { startDate: startDate, endDate: endDate });
      this.props.onDatespanChange(datespanValue);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'armor-vf-dropdown' },
        _react2.default.createElement(
          _reactBootstrap.Row,
          null,
          _react2.default.createElement(
            _reactBootstrap.Col,
            { md: 12 },
            _react2.default.createElement(_ArmorVFDropdown2.default, {
              value: this.state.datespanValue,
              onChange: this.onSpanChange,
              options: this.allDateSpans
            })
          ),
          _react2.default.createElement(
            _reactBootstrap.Col,
            { md: 6 },
            _react2.default.createElement(_DatePicker2.default, {
              value: this.state.startDate.toDate(),
              onChange: this.onDateChange.bind(this, 'startDate')
            })
          ),
          _react2.default.createElement(
            _reactBootstrap.Col,
            { md: 6, className: 'end-date' },
            _react2.default.createElement(_DatePicker2.default, {
              value: this.state.endDate.toDate(),
              onChange: this.onDateChange.bind(this, 'endDate')
            })
          )
        )
      );
    }
  }]);

  return ArmorVFDatespan;
}(_react2.default.Component), _class2.propTypes = {
  /**
   * Initial selection value for the dropdown
   */
  value: _react.PropTypes.any,
  /**
   * Function will fire when menu changes
   * signature: function(event, index, value)
   */
  onChange: _react.PropTypes.func.isRequired,
  /**
   * List of properties supported by <MenuItem />
   * such as: primaryText, value etc...
   */
  options: _react.PropTypes.array.isRequired
}, _temp), (_applyDecoratedDescriptor(_class.prototype, 'onSpanChange', [_autobindDecorator2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'onSpanChange'), _class.prototype)), _class);
exports.default = ArmorVFDatespan;