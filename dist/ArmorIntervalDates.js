'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _reactBootstrap = require('react-bootstrap');

var _FontAwesome = require('components/global/FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

var _utils = require('utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  options: _react.PropTypes.array.isRequired,
  onSelect: _react.PropTypes.func.isRequired
};

var ArmorIntervalDates = function (_React$Component) {
  _inherits(ArmorIntervalDates, _React$Component);

  function ArmorIntervalDates() {
    _classCallCheck(this, ArmorIntervalDates);

    var _this = _possibleConstructorReturn(this, (ArmorIntervalDates.__proto__ || Object.getPrototypeOf(ArmorIntervalDates)).call(this));

    _this.state = {};
    _this.onSelect = _this.onSelect.bind(_this);
    return _this;
  }

  _createClass(ArmorIntervalDates, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.onSelect(this.props.options[0]);
    }
  }, {
    key: 'onSelect',
    value: function onSelect(item) {
      var active = _lodash2.default.isString(item) ? item : item.keys;
      var previousActive = this.state.active;

      this.setState({ active: active });
      this.props.onSelect(item, previousActive);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var result = _lodash2.default.map(this.props.options, function (item, idx) {
        return _react2.default.createElement(
          _reactBootstrap.OverlayTrigger,
          { key: idx, placement: 'top', overlay: _react2.default.createElement(
              _reactBootstrap.Tooltip,
              { id: 'dateId' },
              _utils.utils.camelToReadable(item.keys)
            ) },
          _react2.default.createElement(
            _reactBootstrap.Button,
            { onClick: _this2.onSelect.bind(_this2, item),
              active: _this2.state.active === item.keys },
            item.label
          )
        );
      });

      result.push(_react2.default.createElement(
        _reactBootstrap.OverlayTrigger,
        { key: this.props.options.length + 1,
          placement: 'top',
          overlay: _react2.default.createElement(
            _reactBootstrap.Tooltip,
            { id: 'dateTip' },
            'Custom Date'
          ) },
        _react2.default.createElement(
          _reactBootstrap.Button,
          { onClick: this.onSelect.bind(this, 'calendar'),
            active: this.state.active === 'calendar' },
          _react2.default.createElement(_FontAwesome2.default, { icon: 'calendar' })
        )
      ));

      return _react2.default.createElement(
        _reactBootstrap.ButtonGroup,
        null,
        result
      );
    }
  }]);

  return ArmorIntervalDates;
}(_react2.default.Component);

ArmorIntervalDates.propTypes = propTypes;
exports.default = ArmorIntervalDates;