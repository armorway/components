'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _velocityAnimate = require('velocity-animate');

var _velocityAnimate2 = _interopRequireDefault(_velocityAnimate);

require('react-datepicker/dist/react-datepicker.css');

var _reactDatepicker = require('react-datepicker');

var _reactDatepicker2 = _interopRequireDefault(_reactDatepicker);

require('react-daterange-picker/dist/css/react-calendar.css');

var _reactBootstrap = require('react-bootstrap');

var _ArmorDivider = require('components/global/ArmorDivider');

var _ArmorDivider2 = _interopRequireDefault(_ArmorDivider);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var defaultProps = {
  start: '',
  end: ''
};

var propTypes = {
  onAccept: _react.PropTypes.func.isRequired,
  style: _react.PropTypes.object,
  className: _react.PropTypes.string,
  start: _react.PropTypes.object,
  end: _react.PropTypes.object
};

var ArmorDateRange = function (_React$Component) {
  _inherits(ArmorDateRange, _React$Component);

  function ArmorDateRange(props) {
    _classCallCheck(this, ArmorDateRange);

    var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(ArmorDateRange).call(this, props));

    _this.state = { maxHeight: 140, start: props.start, end: props.end };
    _this.onSelect = _this.onSelect.bind(_this);
    _this.onApply = _this.onApply.bind(_this);
    _this.toggleFilters = _this.toggleFilters.bind(_this);
    _this.isInvalid = _this.isInvalid.bind(_this);
    return _this;
  }

  _createClass(ArmorDateRange, [{
    key: 'onSelect',
    value: function onSelect(point, event) {
      this.setState(_defineProperty({}, point, event));
    }
  }, {
    key: 'onApply',
    value: function onApply() {
      this.toggleFilters('close');
      this.props.onAccept({ start: this.state.start, end: this.state.end });
    }
  }, {
    key: 'toggleFilters',
    value: function toggleFilters(action) {
      var element = _reactDom2.default.findDOMNode(this.refs.slideDown);
      var shouldOpen = false;

      if (action === 'close') {
        shouldOpen = false;
      } else if (element.clientHeight === 0 || action === 'open') {
        shouldOpen = true;
      }

      (0, _velocityAnimate2.default)(element, { height: shouldOpen ? this.state.maxHeight : 0 }, 100);
    }
  }, {
    key: 'isInvalid',
    value: function isInvalid() {
      var result = false;

      if (!this.state.start || !this.state.end) {
        result = true;
      }
      return result;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var margin = { marginRight: '5px' };
      var styles = _.assign({
        height: '0',
        overflow: 'hidden',
        border: 'none',
        padding: 0,
        minHeight: 0
      }, this.props.style || {});

      return _react2.default.createElement("div", { className: 'armor-date-range ' + this.props.className,
        ref: "slideDown",
        style: styles }, _react2.default.createElement(_reactBootstrap.Row, { className: "armor-date-row" }, _react2.default.createElement(_reactBootstrap.Row, null, _react2.default.createElement(_reactBootstrap.Col, { xs: 6 }, _react2.default.createElement("label", null, "Start Date:"), _react2.default.createElement(_reactDatepicker2.default, { selected: this.state.start, onChange: this.onSelect.bind(this, 'start') })), _react2.default.createElement(_reactBootstrap.Col, { xs: 6, className: "pull-right" }, _react2.default.createElement("label", null, "End Date:"), _react2.default.createElement(_reactDatepicker2.default, { selected: this.state.end, onChange: this.onSelect.bind(this, 'end') }))), _react2.default.createElement(_reactBootstrap.Row, null, _react2.default.createElement(_reactBootstrap.Col, { xs: 12 }, _react2.default.createElement(_ArmorDivider2.default, null), _react2.default.createElement(_reactBootstrap.Button, { disabled: this.isInvalid(), bsStyle: "primary", className: "pull-right", onClick: this.onApply }, _react2.default.createElement("span", null, "Apply")), _react2.default.createElement(_reactBootstrap.Button, { bsStyle: "link", className: "pull-right", style: margin, onClick: function onClick() {
          _this2.toggleFilters('close');
        } }, _react2.default.createElement("span", null, "Cancel")), _react2.default.createElement(_reactBootstrap.Button, { bsStyle: "warning", onClick: function onClick() {
          _this2.setState({ start: '', end: '' });
        } }, _react2.default.createElement("span", null, "Clear"))))));
    }
  }]);

  return ArmorDateRange;
}(_react2.default.Component);

ArmorDateRange.defaultProps = defaultProps;
ArmorDateRange.propTypes = propTypes;
exports.default = ArmorDateRange;