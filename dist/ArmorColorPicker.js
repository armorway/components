'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactColor = require('react-color');

var _reactColor2 = _interopRequireDefault(_reactColor);

var _reactBootstrap = require('react-bootstrap');

var _FontAwesome = require('./FontAwesome.jsx');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

require('../styles/ArmorColorPicker.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  /**
    * onChange is called when a new color is picked
  */
  onChange: _react.PropTypes.func.isRequired,
  /**
    * Color to set as currently selected color.
  */
  color: _react.PropTypes.string.isRequired
};
var wrapperClassName = 'wrapper-color-picker';

/**
  * ArmorColorPicker renders a button
  * that toggles a color picker.
*/

var ArmorColorPicker = function (_React$Component) {
  _inherits(ArmorColorPicker, _React$Component);

  function ArmorColorPicker(props) {
    _classCallCheck(this, ArmorColorPicker);

    var _this = _possibleConstructorReturn(this, (ArmorColorPicker.__proto__ || Object.getPrototypeOf(ArmorColorPicker)).call(this, props));

    _this.state = {
      displayColorPicker: false
    };
    _this.onColorChange = _this.onColorChange.bind(_this);
    _this.onClick = _this.onClick.bind(_this);
    _this.onWindowClick = _this.onWindowClick.bind(_this);
    return _this;
  }

  _createClass(ArmorColorPicker, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      window.addEventListener('mousedown', this.onWindowClick);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      window.removeEventListener('mousedown', this.onWindowClick);
    }
  }, {
    key: 'onColorChange',
    value: function onColorChange() {
      var _props;

      // eslint-disable-next-line
      (_props = this.props).onChange.apply(_props, arguments);
    }
  }, {
    key: 'onClick',
    value: function onClick(newState) {
      this.setState({ displayColorPicker: newState });
    }
  }, {
    key: 'onWindowClick',
    value: function onWindowClick(event) {
      var target = event.target;


      while (target.parentElement) {
        if (target.parentElement.className === wrapperClassName) {
          return;
        }
        target = target.parentElement;
      }
      this.setState({ displayColorPicker: false });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'span',
        { style: { marginLeft: '15px' }, className: wrapperClassName },
        _react2.default.createElement(
          _reactBootstrap.Button,
          {
            id: 'color',
            onClick: function onClick() {
              return _this2.onClick(!_this2.state.displayColorPicker);
            },
            className: 'color-button'
          },
          _react2.default.createElement('span', {
            style: { color: this.props.color },
            className: 'color'
          }),
          _react2.default.createElement(_FontAwesome2.default, {
            icon: 'caret-down',
            iconStyle: { position: 'absolute', marginTop: '6px' }
          })
        ),
        _react2.default.createElement(
          'div',
          {
            style: {
              display: this.state.displayColorPicker ? 'block' : 'none', backgroundColor: 'white' }
          },
          _react2.default.createElement(_reactColor2.default, {
            type: 'chrome',
            onChangeComplete: this.onColorChange,
            onClose: this.onClick.bind(this, false),
            color: this.props.color,
            positionCSS: { width: '100%', position: 'absolute', zIndex: 10 },
            className: 'color-class'
          })
        )
      );
    }
  }]);

  return ArmorColorPicker;
}(_react2.default.Component);

ArmorColorPicker.propTypes = propTypes;
exports.default = ArmorColorPicker;