'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactSlide = require('react-slide');

var _reactSlide2 = _interopRequireDefault(_reactSlide);

require('../styles/ArmorSlider.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  /**
     * current value of slider
  */
  value: _react.PropTypes.number,
  /**
     * label for slider
     * displayed as label: value
  */
  label: _react.PropTypes.string,
  /**
     * Min value for slider
     * defaults at 0
  */
  startValue: _react.PropTypes.number,
  /**
     * Max value for slider
     * defaults at 100
  */
  endValue: _react.PropTypes.number,

  /**
   * name of component. Pass in change/drag
  */
  name: _react.PropTypes.any,

  onDrag: _react.PropTypes.func,
  onChange: _react.PropTypes.func
};

/**
  * Slider object with a value that can slide from
  * left to right and has an optional label.
  * Start and end values (min and max) can be customized
  * but default at 0 and 100
*/

var ArmorSlider = function (_React$Component) {
  _inherits(ArmorSlider, _React$Component);

  function ArmorSlider() {
    _classCallCheck(this, ArmorSlider);

    var _this = _possibleConstructorReturn(this, (ArmorSlider.__proto__ || Object.getPrototypeOf(ArmorSlider)).call(this));

    _this.state = {};

    _this.onChange = _this.onChange.bind(_this);
    _this.onDrag = _this.onDrag.bind(_this);
    return _this;
  }

  _createClass(ArmorSlider, [{
    key: 'onChange',
    value: function onChange() {
      var _props;

      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      (_props = this.props).onChange.apply(_props, [this.props.name].concat(args));
    }
  }, {
    key: 'onDrag',
    value: function onDrag() {
      var _props2;

      for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      (_props2 = this.props).onDrag.apply(_props2, [this.props.name].concat(args));
    }
  }, {
    key: 'render',
    value: function render() {
      var props = _.omit(this.props, ['onChange', 'onDrag']);

      return _react2.default.createElement(
        'div',
        { className: 'armor-slider' },
        _react2.default.createElement(
          'span',
          { className: 'armor-slider-label-start' },
          _react2.default.createElement(
            'span',
            { style: { fontWeight: 800 }, className: 'center-slider armor-slider-label' },
            this.props.label
          ),
          _react2.default.createElement(
            'span',
            { className: 'armor-slider-startValue pull-right' },
            this.props.startValue
          )
        ),
        _react2.default.createElement(
          'span',
          { className: 'armor-slider-wrapper' },
          _react2.default.createElement(
            'div',
            { className: 'tear-wrapper' },
            _react2.default.createElement(
              'div',
              { style: { marginLeft: this.props.value * 100 / this.props.endValue + '%' } },
              _react2.default.createElement(
                'div',
                { className: 'tear-sizer' },
                _react2.default.createElement('div', { className: '' }),
                _react2.default.createElement(
                  'div',
                  { className: 'value' },
                  this.props.value
                )
              )
            )
          ),
          _react2.default.createElement(_reactSlide2.default, _extends({
            onDrag: this.onDrag,
            onChange: this.onChange
          }, props))
        ),
        _react2.default.createElement(
          'span',
          { xs: 1, className: 'armor-slider-endValue' },
          _react2.default.createElement(
            'span',
            null,
            this.props.endValue
          )
        )
      );
    }
  }]);

  return ArmorSlider;
}(_react2.default.Component);

ArmorSlider.propTypes = propTypes;
exports.default = ArmorSlider;