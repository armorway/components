'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _DropDownMenu = require('material-ui/DropDownMenu');

var _DropDownMenu2 = _interopRequireDefault(_DropDownMenu);

var _MenuItem = require('material-ui/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

require('../styles/ArmorVFCheckbox');

var _FontAwesome = require('./FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ArmorVFDropdown = (_temp = _class = function (_React$Component) {
  _inherits(ArmorVFDropdown, _React$Component);

  function ArmorVFDropdown(props) {
    _classCallCheck(this, ArmorVFDropdown);

    return _possibleConstructorReturn(this, (ArmorVFDropdown.__proto__ || Object.getPrototypeOf(ArmorVFDropdown)).call(this, props));
  }

  _createClass(ArmorVFDropdown, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        { className: 'armor-vf-dropdown' },
        _react2.default.createElement(
          _DropDownMenu2.default,
          {
            style: this.props.style,
            value: this.props.value,
            onChange: this.props.onChange
          },
          _lodash2.default.map(this.props.options, function (option) {
            return _react2.default.createElement(_MenuItem2.default, option);
          })
        ),
        this.props.onHelpClick ? _react2.default.createElement(
          'div',
          { onClick: function onClick() {
              return _this2.props.onHelpClick(_this2.props.name);
            } },
          _react2.default.createElement(_FontAwesome2.default, { className: 'help-icon', icon: 'question-circle' })
        ) : ''
      );
    }
  }]);

  return ArmorVFDropdown;
}(_react2.default.Component), _class.propTypes = {
  /**
   * Initial selection value for the dropdown
   */
  value: _react.PropTypes.any,
  /**
   * Function will fire when menu changes
   * signature: function(event, index, value)
   */
  onChange: _react.PropTypes.func.isRequired,
  /**
   * List of properties supported by <MenuItem />
   * such as: primaryText, value etc...
   */
  options: _react.PropTypes.array.isRequired
}, _temp);
exports.default = ArmorVFDropdown;