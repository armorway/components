'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = require('react-bootstrap');

var _reactRouter = require('react-router');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Error404 = function Error404() {
  return _react2.default.createElement(
    _reactBootstrap.Grid,
    null,
    _react2.default.createElement(
      _reactBootstrap.Row,
      null,
      _react2.default.createElement(
        _reactBootstrap.Col,
        { xs: 6, xsOffset: 3 },
        _react2.default.createElement(
          _reactBootstrap.Well,
          { bsSize: 'large' },
          _react2.default.createElement(
            'h4',
            null,
            'Sorry but that Page does not exist!'
          ),
          _react2.default.createElement(
            _reactRouter.Link,
            { to: '/' },
            'Return Home'
          )
        )
      )
    )
  );
};

exports.default = Error404;