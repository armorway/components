'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _reactBootstrap = require('react-bootstrap');

var _autobindDecorator = require('autobind-decorator');

var _autobindDecorator2 = _interopRequireDefault(_autobindDecorator);

var _FontAwesome = require('./FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

require('../styles/ArmorCheckboxSelect');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  /** List of objects with id (String), name (String), active (boolean)
      to generate checkboxes from.
   * ```
   * [
   *   {
   *     id: 'all',
   *     name: 'All',
   *     active: false,
   *   },
   *   {
   *     id: 'a',
   *     name: 'A',
   *     active: true,
   *   },
   *   {
   *     id: 'b',
   *     name: 'B',
   *     active: false,
   *   },
   * ]
   *```
   */
  filters: _react.PropTypes.array.isRequired,
  /** Callback function returning the results (name, id) of filter clicked.
   */
  onChange: _react.PropTypes.func.isRequired,
  /** Name of the filter that will be returned when onChange is fired.
   */
  name: _react.PropTypes.string.isRequired,
  /**
   * Column size for each checkbox
  */
  colSize: _react.PropTypes.number,
  /** Object representing custom styles.
   */
  styles: _react.PropTypes.object,
  /**
   * Function which will return the name of this filter, when a help tip is clicked on
   */
  onHelpClick: _react.PropTypes.func
};

var defaultProps = {
  colSize: 4
};

var ArmorCheckboxSelect = function (_React$Component) {
  _inherits(ArmorCheckboxSelect, _React$Component);

  function ArmorCheckboxSelect(props) {
    _classCallCheck(this, ArmorCheckboxSelect);

    return _possibleConstructorReturn(this, Object.getPrototypeOf(ArmorCheckboxSelect).call(this, props));
  }

  _createClass(ArmorCheckboxSelect, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        { id: 'armorCheckboxSelect', style: this.props.style },
        _react2.default.createElement(
          _reactBootstrap.Row,
          null,
          _lodash2.default.map(this.props.filters, function (filter, idx) {
            var filterLength = filter.name.length;
            var classes = (0, _classnames2.default)({
              'small-filter-box': filterLength >= 10,
              'medium-filter-box': filterLength > 7 && filterLength < 10
            });

            return _react2.default.createElement(
              _reactBootstrap.Col,
              { xs: _this2.props.colSize, key: idx, className: classes },
              _react2.default.createElement(_reactBootstrap.Input, { type: 'checkbox',
                label: filter.name,
                checked: filter.active,
                onChange: function onChange() {
                  return _this2.props.onChange(_this2.props.name, filter);
                }
              })
            );
          })
        ),
        this.props.onHelpClick ? _react2.default.createElement(
          'div',
          { onClick: function onClick() {
              return _this2.props.onHelpClick(_this2.props.name);
            } },
          _react2.default.createElement(_FontAwesome2.default, { className: 'help-icon', icon: 'question-circle' })
        ) : ''
      );
    }
  }]);

  return ArmorCheckboxSelect;
}(_react2.default.Component);

ArmorCheckboxSelect.defaultProps = defaultProps;
ArmorCheckboxSelect.propTypes = propTypes;
exports.default = ArmorCheckboxSelect;