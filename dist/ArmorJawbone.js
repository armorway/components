'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _velocityAnimate = require('velocity-animate');

var _velocityAnimate2 = _interopRequireDefault(_velocityAnimate);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _reactBootstrap = require('react-bootstrap');

var _ArmorDivider = require('components/global/ArmorDivider');

var _ArmorDivider2 = _interopRequireDefault(_ArmorDivider);

var _FontAwesome = require('components/global/FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

var _config = require('components/crimes/config');

var _config2 = _interopRequireDefault(_config);

var _AppActions = require('actions/global/AppActions');

var _AppActions2 = _interopRequireDefault(_AppActions);

var _jawbone = require('services/jawbone');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  className: _react.PropTypes.string,
  date: _react.PropTypes.string,
  onAccept: _react.PropTypes.func.isRequired
};

var rightMargin = { marginRight: '10px' };

var ArmorJawbone = function (_React$Component) {
  _inherits(ArmorJawbone, _React$Component);

  function ArmorJawbone() {
    _classCallCheck(this, ArmorJawbone);

    var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(ArmorJawbone).call(this));

    _this.state = {
      maxHeight: 390,
      radioSelected: {},
      rules: {
        crime: {}
      }
    };
    _this.toggleFilters = _this.toggleFilters.bind(_this);
    _this.formReset = _this.formReset.bind(_this);
    _this.onCheckBoxChange = _this.onCheckBoxChange.bind(_this);
    _this.onCancelClick = _this.onCancelClick.bind(_this);
    _this.onApplyClick = _this.onApplyClick.bind(_this);
    _this.buildCheckBoxes = _this.buildCheckBoxes.bind(_this);
    return _this;
  }

  _createClass(ArmorJawbone, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.getCrimes(_jawbone.eventRadios[0]);
      // this.refs.radio1.refs.input.checked = true;
    }
  }, {
    key: 'onCancelClick',
    value: function onCancelClick() {
      this.toggleFilters('close');
    }
  }, {
    key: 'onApplyClick',
    value: function onApplyClick() {
      var crimes = _lodash2.default.omit(this.state.rules.crime, 'undefined');
      var formattedSelection = _lodash2.default.chain(crimes).pick(_lodash2.default.identity).keys().value().toString();

      if (!formattedSelection) {
        _AppActions2.default.setWindowHeight(0);
        _AppActions2.default.notify({
          level: 'warning',
          title: 'Warning',
          message: 'Please select crimes to filter by.'
        });
      } else {
        this.toggleFilters('close');
        this.props.onAccept(this.state.rules, formattedSelection);
      }
    }
  }, {
    key: 'onCheckBoxChange',
    value: function onCheckBoxChange(event) {
      var _event$target$dataset = event.target.dataset;
      var key = _event$target$dataset.key;
      var type = _event$target$dataset.type;

      var newKeys = key.split(',');
      var updatedRules = _lodash2.default.chain(newKeys).map(function (rule) {
        return [rule, event.target.checked];
      }).zipObject().value();
      var newState = _lodash2.default.assign({}, this.state);

      newState.rules[type] = _lodash2.default.assign(newState.rules[type], updatedRules);
      this.setState(newState);
      if (this.state.radioSelected.Other !== true) {
        this.setState({ radioSelected: _defineProperty({}, 'Other', true) });
      }
    }
  }, {
    key: 'getCrimes',
    value: function getCrimes(eventRadio) {
      var isOtherSelected = false;

      if (eventRadio.label === 'Other') {
        isOtherSelected = true;
      }
      this.setState({ radioSelected: { Other: isOtherSelected } });

      var keys = eventRadio.keys;

      var newState = _lodash2.default.assign({}, this.state);

      newState.rules.crime = _lodash2.default.chain(keys).map(function (rule) {
        return [rule, true];
      }).zipObject().value();
      this.setState({ newState: newState });
    }
  }, {
    key: 'generateCheckBox',
    value: function generateCheckBox(checkbox, idx, type, styles) {
      var _this2 = this;

      var label = _lodash2.default.camelCase(checkbox.label);
      var isChecked = this.state.rules[type][checkbox.key];
      var iconSrc = _lodash2.default.has(checkbox, 'icon') ? checkbox.icon : label;

      if (checkbox.label === 'allCrimes') {
        isChecked = _lodash2.default.reduce(checkbox.key, function (curr, next) {
          return curr && _this2.state.rules[type][next];
        }, true);
      }
      if (_config2.default.icons[iconSrc]) {
        label = _config2.default.icons[iconSrc].name;
      }

      return _react2.default.createElement("span", { style: styles ? styles : {}, key: idx }, label !== 'allCrimes' ? _react2.default.createElement("img", { src: label, style: { float: 'left', height: '25px', marginRight: '5px' } }) : _react2.default.createElement("span", null), _react2.default.createElement(_reactBootstrap.Input, { type: "checkbox",
        label: _react2.default.createElement(_FontAwesome2.default, { icon: label, title: checkbox.label, padIcon: true }),
        onChange: this.onCheckBoxChange,
        "data-key": checkbox.key,
        checked: isChecked,
        "data-type": type }));
    }
  }, {
    key: 'buildCheckBoxes',
    value: function buildCheckBoxes(colData, checkboxChunk, styles) {
      var _this3 = this;

      var keys = [];
      var checkBoxes = _lodash2.default.map(checkboxChunk, function (checkbox, idx) {
        keys.push(checkbox.key);
        return _this3.generateCheckBox(checkbox, idx + 1, colData.type, styles);
      });

      return _react2.default.createElement("div", null, checkBoxes);
    }
  }, {
    key: 'formReset',
    value: function formReset() {
      this.setState({ rules: { crime: {} }, radioSelected: { Other: false } });
    }
  }, {
    key: 'toggleFilters',
    value: function toggleFilters(action) {
      var element = _reactDom2.default.findDOMNode(this.refs.slideDown);
      var shouldOpen = false;

      if (action === 'close') {
        shouldOpen = false;
      } else if (element.clientHeight === 0 || action === 'open') {
        shouldOpen = true;
      }

      this.setState({ open: shouldOpen });

      (0, _velocityAnimate2.default)(element, { height: shouldOpen ? this.state.maxHeight : 0 }, 200);
    }
  }, {
    key: 'componentsWillRecieveProps',
    value: function componentsWillRecieveProps(newProps) {
      this.setState(newProps);
    }
  }, {
    key: 'buildCrimeShortcuts',
    value: function buildCrimeShortcuts() {
      var _this4 = this;

      return _lodash2.default.map(_jawbone.eventRadios, function (item, index) {
        return _react2.default.createElement("span", { className: "radio-span", key: index }, _react2.default.createElement(_reactBootstrap.Input, { type: "radio",
          ref: 'radio' + index,
          name: "crimeRadioSelects",
          label: item.label,
          checked: _this4.state.radioSelected[item.label],
          onClick: _this4.getCrimes.bind(_this4, item) }));
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var crimeChunks = _lodash2.default.chain(_jawbone.allCrimes.checkboxes).groupBy(function (itm, idx) {
        return Math.floor(idx % 4);
      }).toArray().value();

      return _react2.default.createElement("div", { className: this.props.className }, _react2.default.createElement(_reactBootstrap.Panel, { ref: "slideDown", style: { height: '0px', overflow: 'hidden', border: 'none' } }, _react2.default.createElement("form", { ref: "filterForm" }, _react2.default.createElement(_reactBootstrap.Row, null, _react2.default.createElement(_reactBootstrap.Col, { xs: 12 }, _react2.default.createElement(_reactBootstrap.Row, null, _react2.default.createElement(_reactBootstrap.Col, { xs: 12 }, this.generateCheckBox({ label: 'All Crimes', key: _lodash2.default.map(_jawbone.allCrimes.checkboxes, function (itm) {
          return itm.key;
        }) }, 0, _jawbone.allCrimes.type))), _react2.default.createElement(_reactBootstrap.Row, null, _react2.default.createElement(_reactBootstrap.Col, { xs: 3 }, this.buildCheckBoxes(_jawbone.allCrimes, crimeChunks[0])), _react2.default.createElement(_reactBootstrap.Col, { xs: 3 }, this.buildCheckBoxes(_jawbone.allCrimes, crimeChunks[1])), _react2.default.createElement(_reactBootstrap.Col, { xs: 3 }, this.buildCheckBoxes(_jawbone.allCrimes, crimeChunks[2])), _react2.default.createElement(_reactBootstrap.Col, { xs: 3 }, this.buildCheckBoxes(_jawbone.allCrimes, crimeChunks[3]))), _react2.default.createElement(_ArmorDivider2.default, null), _react2.default.createElement(_reactBootstrap.Row, null, _react2.default.createElement(_reactBootstrap.Col, { xs: 1 }, _react2.default.createElement(_reactBootstrap.Button, { bsStyle: "warning", onClick: this.formReset }, "Clear")), _react2.default.createElement(_reactBootstrap.Col, { xsOffset: 9, xs: 2 }, _react2.default.createElement(_reactBootstrap.Button, { className: "pull-right", bsStyle: "primary", onClick: this.onApplyClick }, "Done"), _react2.default.createElement(_reactBootstrap.Button, { className: "pull-right", bsStyle: "link", style: rightMargin, onClick: this.onCancelClick }, "Cancel"))))))));
    }
  }]);

  return ArmorJawbone;
}(_react2.default.Component);

ArmorJawbone.propTypes = propTypes;
exports.default = ArmorJawbone;