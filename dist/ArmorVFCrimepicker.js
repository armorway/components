'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _desc, _value, _class, _class2, _temp;
// import { VelocityTransitionGroup } from 'velocity-react';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _ArmorVFCheckbox = require('./ArmorVFCheckbox');

var _ArmorVFCheckbox2 = _interopRequireDefault(_ArmorVFCheckbox);

var _ArmorVFDropdown = require('./ArmorVFDropdown');

var _ArmorVFDropdown2 = _interopRequireDefault(_ArmorVFDropdown);

var _FontAwesome = require('./FontAwesome');

var _FontAwesome2 = _interopRequireDefault(_FontAwesome);

var _autobindDecorator = require('autobind-decorator');

var _autobindDecorator2 = _interopRequireDefault(_autobindDecorator);

require('../styles/ArmorVFCrimepicker');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
  var desc = {};
  Object['ke' + 'ys'](descriptor).forEach(function (key) {
    desc[key] = descriptor[key];
  });
  desc.enumerable = !!desc.enumerable;
  desc.configurable = !!desc.configurable;

  if ('value' in desc || desc.initializer) {
    desc.writable = true;
  }

  desc = decorators.slice().reverse().reduce(function (desc, decorator) {
    return decorator(target, property, desc) || desc;
  }, desc);

  if (context && desc.initializer !== void 0) {
    desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
    desc.initializer = undefined;
  }

  if (desc.initializer === void 0) {
    Object['define' + 'Property'](target, property, desc);
    desc = null;
  }

  return desc;
}

var ArmorVFCrimepicker = (_class = (_temp = _class2 = function (_React$Component) {
  _inherits(ArmorVFCrimepicker, _React$Component);

  function ArmorVFCrimepicker(props) {
    _classCallCheck(this, ArmorVFCrimepicker);

    var _this = _possibleConstructorReturn(this, (ArmorVFCrimepicker.__proto__ || Object.getPrototypeOf(ArmorVFCrimepicker)).call(this, props));

    _this.state = {
      showCheckboxes: false
    };
    return _this;
  }

  _createClass(ArmorVFCrimepicker, [{
    key: 'onGroupChange',
    value: function onGroupChange(evt, index, value) {
      this.props.onDropdownChange(this.props.name, value);
    }
  }, {
    key: 'onCheckboxSelect',
    value: function onCheckboxSelect(name, filter, category) {
      var otherOption = this.props.groups[this.props.groups.length - 1].value;

      this.props.onDropdownChange(this.props.name, otherOption);
      this.props.onChange(name, filter, category);
    }
  }, {
    key: 'onExpand',
    value: function onExpand() {
      var _this2 = this;

      this.setState({ showCheckboxes: !this.state.showCheckboxes }, function () {
        if (_this2.props.scrollOnCollapse && _this2.state.showCheckboxes) {
          document.querySelectorAll('.filter-container').forEach(function (node) {
            node.scrollTop = node.scrollHeight - document.querySelector('.armor-vf-crimepicker').clientHeight;
          });
        }
      });
    }
  }, {
    key: 'buildList',
    value: function buildList() {
      var _this3 = this;

      return _lodash2.default.map(this.props.options, function (opt, cat) {
        return _react2.default.createElement(
          'span',
          null,
          _react2.default.createElement(
            'span',
            { className: 'category' },
            cat
          ),
          _react2.default.createElement(_ArmorVFCheckbox2.default, {
            filters: opt,
            colSize: _this3.props.colSize,
            onChange: function onChange(name, filter) {
              return _this3.onCheckboxSelect(name, filter, cat);
            },
            name: _this3.props.name
          }),
          _react2.default.createElement('span', { className: 'divider' })
        );
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'armor-vf-dropdown armor-vf-crimepicker' },
        _react2.default.createElement(
          'div',
          { onClick: this.onExpand },
          _react2.default.createElement(_FontAwesome2.default, {
            className: 'collapse-icon',
            icon: this.state.showCheckboxes ? 'minus-square-o' : 'plus-square-o'
          })
        ),
        _react2.default.createElement(_ArmorVFDropdown2.default, {
          className: 'dropdown',
          style: this.props.style,
          value: this.props.selected,
          onChange: this.onGroupChange,
          options: this.props.groups
        }),
        this.state.showCheckboxes ? _react2.default.createElement(
          'span',
          null,
          this.buildList()
        ) : ''
      );
    }
  }]);

  return ArmorVFCrimepicker;
}(_react2.default.Component), _class2.propTypes = {
  /**
   * Initial selection value for the dropdown
   */
  value: _react.PropTypes.any,
  /**
   * Function will fire when menu changes
   * signature: function(event, index, value)
   */
  onChange: _react.PropTypes.func.isRequired,
  /**
   * List of properties supported by <MenuItem />
   * such as: primaryText, value etc...
   */
  options: _react.PropTypes.array.isRequired
}, _temp), (_applyDecoratedDescriptor(_class.prototype, 'onGroupChange', [_autobindDecorator2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'onGroupChange'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'onCheckboxSelect', [_autobindDecorator2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'onCheckboxSelect'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'onExpand', [_autobindDecorator2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'onExpand'), _class.prototype)), _class);
exports.default = ArmorVFCrimepicker;