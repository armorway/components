'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Table = require('material-ui/Table');

var _IconButton = require('material-ui/IconButton');

var _IconButton2 = _interopRequireDefault(_IconButton);

var _keyboardArrowDown = require('material-ui/svg-icons/hardware/keyboard-arrow-down');

var _keyboardArrowDown2 = _interopRequireDefault(_keyboardArrowDown);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  incident: _react.PropTypes.object,
  userSelected: _react.PropTypes.bool,
  idx: _react.PropTypes.number,
  onRowSelection: _react.PropTypes.func,
  onChevronClick: _react.PropTypes.func,
  printing: _react.PropTypes.bool
};

var styles = {
  rotate: {
    transform: 'rotate(180deg)'
  },
  selected: {
    background: 'rgba(224, 224, 224, .2)',
    borderBottom: '0px solid transparent'
  },
  caseStyle: {
    textOverflow: 'initial'
  }
};

var IncidentRow = function (_React$Component) {
  _inherits(IncidentRow, _React$Component);

  function IncidentRow(props) {
    _classCallCheck(this, IncidentRow);

    var _this = _possibleConstructorReturn(this, (IncidentRow.__proto__ || Object.getPrototypeOf(IncidentRow)).call(this, props));

    _this.onChevronClick = _this.onChevronClick.bind(_this);
    return _this;
  }

  _createClass(IncidentRow, [{
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.incident !== this.props.incident || nextProps.userSelected !== this.props.userSelected;
    }
  }, {
    key: 'onChevronClick',
    value: function onChevronClick(evt) {
      this.props.onChevronClick(evt, this.props.incident.get('caseNumber'));
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        _Table.TableRow,
        _extends({}, this.props, {
          'data-cn': this.props.incident.get('caseNumber'),
          onRowClick: this.props.onRowSelection,
          style: this.props.userSelected ? styles.selected : {}
        }),
        _react2.default.createElement(
          _Table.TableRowColumn,
          { style: styles.caseStyle },
          this.props.incident.get('caseNumber')
        ),
        _react2.default.createElement(
          _Table.TableRowColumn,
          { colSpan: 4 },
          this.props.incident.get('address')
        ),
        _react2.default.createElement(
          _Table.TableRowColumn,
          { colSpan: 2 },
          this.props.incident.get('crimeDescription')
        ),
        _react2.default.createElement(
          _Table.TableRowColumn,
          { colSpan: 2 },
          this.props.incident.get('occurredFrom')
        ),
        _react2.default.createElement(
          _Table.TableRowColumn,
          null,
          !this.props.printing ? _react2.default.createElement(
            _IconButton2.default,
            {
              style: this.props.userSelected ? styles.rotate : {},
              onClick: this.onChevronClick
            },
            _react2.default.createElement(_keyboardArrowDown2.default, null)
          ) : ''
        )
      );
    }
  }]);

  return IncidentRow;
}(_react2.default.Component);

IncidentRow.propTypes = propTypes;
exports.default = IncidentRow;